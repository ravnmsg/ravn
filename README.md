# Ravn

Ravn is a message orchestrator. Clients can request messages to be delivered by specifying what kind of message it is, how to deliver it, and to whom.

A *message* resource is an instance of such a request, while a *delivery* resource is a specific attempt to deliver that message.

## Services

The Ravn application is separated into the following services.

The project's [services wiki](https://gitlab.com/ravnmsg/ravn/-/wikis/services) contains more information about its services, and how they work together.

### Receiver

The Receiver service is the API entrypoint of the application. Clients can request message deliveries through it and configure resources.

### Deliverer

The Deliverer service is in charge of handling a delivery. It will generate and translate content to be forwarded to the various Requester services.

### Requesters

A Requester service handles the actual delivery by connecting to a specific third-party.

### Retryer

The Retryer service runs periodically in the background in find failed messages that need to be retried.

### Scheduler

The Scheduler service runs periodically in the background to find messages that are due to be delivered.

## Installation

It is recommended to install the Ravn application on [Kubernetes](https://kubernetes.io/), although on-premise installation should be easy and straightforward.

The project's [setup wiki](https://gitlab.com/ravnmsg/ravn/-/wikis/setup) contains a walkthrough with instructions about how to install the application, and how to configure it after a fresh installation.

See the `CONTRIBUTING.md` file for ways to quickly install the application locally.

## Usage

Given a working and configured Ravn installation, messages can be requested and retrieved with the following examples.

A valid JWT must be provided in the `Authorization` header. Its payload indicates which permissions the current user has. For example:

```none
Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.[...]
```


The project's [usage wiki](https://gitlab.com/ravnmsg/ravn/-/wikis/usage) contains more information and examples.

### Message delivery request

A message delivery can be requested by performing an HTTP request to the application:

```none
POST https://api.ravnmsg.io/v1/messages
```

The request body must be an `application/json` content similar to the following:

```json
{
    "messageKind": "password_reset",
    "deliveryKind": "sendgrid",
    "languageTag": "en",
    "input": {
        "from": "from@example.com",
        "to": "to@example.com",
        "userName": "John"
    }
}
```

This will ask Ravn to deliver a `password_reset` message, in english, using the delivery kind with code [`sendgrid`](https://sendgrid.com). The response will look like the following:

```json
{
  "data": {
    "id": "1435e9dd-795a-4032-a774-79869a52ef81",
    "messageKind": "password_reset",
    "input": {
        "from": "from@example.com",
        "to": "to@example.com",
        "userName": "John"
    },
    "status": "active",
    "createTime": "2022-02-18T13:40:07.684577Z",
    "deliveries": [
      {
        "deliveryKind": "sendgrid",
        "languageTag": "en",
        "status": "pending",
        "deliverTime": null
      }
    ]
  },
  "meta": {
    "requestId": "15ce0652-fa11-4ca5-b9c8-7bb0602856d5"
  }
}
```

This indicates that the delivery is currently in the queue (`pending`), waiting to be picked up by the Deliverer service.

### Message retrieval

A specific message can be retrieved with its id:

```none
GET https://api.ravnmsg.io/v1/messages/1435e9dd-795a-4032-a774-79869a52ef81
```

The response will be similar to the one from a message request:

```json
{
  "data": {
    "id": "1435e9dd-795a-4032-a774-79869a52ef81",
    "messageKind": "password_reset",
    "input": {
        "from": "from@example.com",
        "to": "to@example.com",
        "userName": "John"
    },
    "status": "succeeded",
    "createTime": "2022-02-18T13:40:07.684577Z",
    "deliveries": [
      {
        "deliveryKind": "sendgrid",
        "languageTag": "en",
        "status": "delivered",
        "deliverTime": "2022-02-18T13:40:07.736547Z"
      }
    ]
  },
  "meta": {
    "requestId": "83ae0b08-090b-400c-a45b-55afc737a504"
  }
}
```

Multiple messages can also be retrieved:

```none
GET https://api.ravnmsg.io/v1/messages
```

In this case, the response's `data` will contain an array of messages. By default, the first 20 messages are returned, ordered by their `createTime` attributes. To specify different values, or to filter results, the `$filter`, `$orderby`, `$top`, and `$skip` queries can be provided. They are based on [OData system query options](https://www.odata.org/getting-started/basic-tutorial/#queryData) syntax.

For example, this request would return the third page of 50 active messages, ordered by their `createTime` in descending order:

```none
GET https://api.ravnmsg.io/v1/messages?$filter=status%20eq%20active&orderby=createTime%20desc&$top=50&$skip=100
```
