#!/bin/sh
set -e

function cleanup() {
    printf "Stopping services..."

    if [ -n "${MQ_CTN_NAME}" ]; then
        docker rm -f ${MQ_CTN_NAME} > /dev/null && printf "."
    fi

    if [ -n "${CACHE_CTN_NAME}" ]; then
        docker rm -f ${CACHE_CTN_NAME} > /dev/null && printf "."
    fi

    if [ -n "${DB_CTN_NAME}" ]; then
        docker rm -f ${DB_CTN_NAME} > /dev/null && printf "."
    fi

    if [ -n "${RAVN_CTN_NAME}" ]; then
        docker rm -f ${RAVN_CTN_NAME} > /dev/null && printf "."
        docker rm -f ${RAVN_CTN_NAME}_unsecured > /dev/null && printf "."
        docker image rm ravn:${RAVN_CTN_NAME} > /dev/null && printf "."
    fi

    if [ -n "${RAVN_CTN_NETWORK}" ]; then
        docker network rm ${RAVN_CTN_NETWORK} > /dev/null && printf "."
    fi

    echo
}

# When the script fails, stops the container.
trap cleanup EXIT

RAVN_CTN_NETWORK=ravn-$(uuidgen)
docker network create ${RAVN_CTN_NETWORK} > /dev/null

# Starts a temporary message queue if no NATS host received.
if [ -z "${RAVN_NATS_HOST}" ]; then
    echo "RAVN_NATS_HOST not provided."
    MQ_CTN_NAME=mq-$(uuidgen)
    CTN_NAME=${MQ_CTN_NAME} CTN_NETWORK=${RAVN_CTN_NETWORK} sh ./.meta/scripts/test-mq.sh
    RAVN_NATS_HOST="$(docker inspect $MQ_CTN_NAME | jq -r ".[0].NetworkSettings.Networks.\"${RAVN_CTN_NETWORK}\".Aliases[0]"):4222"
fi

# Starts a temporary cache if no Redis host received.
if [ -z "${RAVN_REDIS_HOST}" ]; then
    echo "RAVN_REDIS_HOST not provided."
    CACHE_CTN_NAME=cache-$(uuidgen)
    CTN_NAME=${CACHE_CTN_NAME} CTN_NETWORK=${RAVN_CTN_NETWORK} sh ./.meta/scripts/test-cache.sh
    RAVN_REDIS_HOST="$(docker inspect $CACHE_CTN_NAME | jq -r ".[0].NetworkSettings.Networks.\"${RAVN_CTN_NETWORK}\".Aliases[0]"):6379"
fi

# Starts a temporary database if no PostgreSQL host received.
if [ -z "${RAVN_POSTGRESQL_HOST}" ]; then
    echo "RAVN_POSTGRESQL_HOST not provided."
    DB_CTN_NAME=db-$(uuidgen)
    CTN_NAME=${DB_CTN_NAME} CTN_NETWORK=${RAVN_CTN_NETWORK} sh ./.meta/scripts/test-db.sh
    RAVN_POSTGRESQL_HOST="$(docker inspect $DB_CTN_NAME | jq -r ".[0].NetworkSettings.Networks.\"${RAVN_CTN_NETWORK}\".Aliases[0]"):5432"
fi

echo "Building Docker image..."
RAVN_CTN_NAME=e2e-$(uuidgen)
docker build --tag ravn:${RAVN_CTN_NAME} --quiet . > /dev/null

echo "Starting Ravn..."
docker run \
    --name ${RAVN_CTN_NAME} \
    --network ${RAVN_CTN_NETWORK} \
    --publish 50000 \
    --detach \
    --rm \
    --env RAVN_RECEIVER_SERVER_PORT=50000 \
    --env RAVN_NATS_URI=nats://${RAVN_NATS_HOST} \
    --env RAVN_NATS_SUBJECT=delivery_requests \
    --env RAVN_REDIS_URI=${RAVN_REDIS_HOST} \
    --env RAVN_POSTGRESQL_URI=postgres://ravn:ravn@${RAVN_POSTGRESQL_HOST}/ravntest \
    ravn:${RAVN_CTN_NAME} --receiver --deliverer > /dev/null

sleep 2

RAVN_RECEIVER_HOST="localhost:$(docker port ${RAVN_CTN_NAME} 50000/tcp | cut -d ":" -f2)"
echo "Ravn ready, Receiver service listening on ${RAVN_RECEIVER_HOST}."

echo "Starting Ravn (unsecured)..."
docker run \
    --name ${RAVN_CTN_NAME}_unsecured \
    --network ${RAVN_CTN_NETWORK} \
    --publish 50000 \
    --detach \
    --rm \
    --env RAVN_RECEIVER_SERVER_PORT=50000 \
    --env RAVN_UNSECURED=true \
    --env RAVN_NATS_URI=nats://${RAVN_NATS_HOST} \
    --env RAVN_NATS_SUBJECT=delivery_requests \
    --env RAVN_REDIS_URI=${RAVN_REDIS_HOST} \
    --env RAVN_POSTGRESQL_URI=postgres://ravn:ravn@${RAVN_POSTGRESQL_HOST}/ravntest \
    ravn:${RAVN_CTN_NAME} --receiver --deliverer > /dev/null

sleep 2

RAVN_RECEIVER_HOST_UNSECURED="localhost:$(docker port ${RAVN_CTN_NAME}_unsecured 50000/tcp | cut -d ":" -f2)"
echo "Ravn (unsecured) ready, Receiver service listening on ${RAVN_RECEIVER_HOST_UNSECURED}."

RAVN_RECEIVER_HOST="${RAVN_RECEIVER_HOST}" \
    RAVN_RECEIVER_HOST_UNSECURED="${RAVN_RECEIVER_HOST_UNSECURED}" \
    RAVN_NATS_HOST="${RAVN_NATS_HOST}" \
    RAVN_POSTGRESQL_HOST="${RAVN_POSTGRESQL_HOST}" \
    go test -tags=endtoend ${TEST_ARGS}
