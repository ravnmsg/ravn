#!/bin/sh
# set -e

trap 'rm -f resp.head resp.body resp.err' EXIT

RAVN_SERVER_HOST=localhost:50000

echo "GET /v1/messages"
curl --request GET http://${RAVN_SERVER_HOST}/v1/messages --silent --show-error --dump-header resp.head >resp.body 2>resp.err
if [ "$?" != 0 ]; then
    echo "  Expected request to succeed"
    echo "  Error: $(cat resp.err)"
    exit 1
elif [ -z "$(grep '200 OK' resp.head)" ]; then
    echo "  Expected request to succeed, but was $(head -n1 resp.head)"
    cat resp.body
    exit 1
elif [ "$(jq '.data[] | length' -r resp.body)" == "0" ]; then
    echo "  Expected at least one message, but there were none"
    exit 1
fi;

MESSAGE_ID=$(jq .data[0].id -r resp.body)
MESSAGE_KIND_CODE=$(jq .data[0].messageKind -r resp.body)
DELIVERY_KIND_CODE=$(jq .data[0].deliveries[0].deliveryKind -r resp.body)

echo "GET /v1/message-kinds?\$filter=code%20eq%20${MESSAGE_KIND_CODE}"
curl --request GET http://${RAVN_SERVER_HOST}/v1/message-kinds?\$filter=code%20eq%20${MESSAGE_KIND_CODE} --silent --show-error --dump-header resp.head >resp.body 2>resp.err
if [ "$?" != 0 ]; then
    echo "  Expected request to succeed"
    echo "  Error: $(cat resp.err)"
    exit 1
elif [ -z "$(grep '200 OK' resp.head)" ]; then
    echo "  Expected request to succeed, but was $(head -n1 resp.head)"
    cat resp.body
    exit 1
fi;

MESSAGE_KIND_ID=$(jq .data[0].id -r resp.body)

echo "GET /v1/message-kinds/${MESSAGE_KIND_ID}"
curl --request GET http://${RAVN_SERVER_HOST}/v1/message-kinds/${MESSAGE_KIND_ID} --silent --show-error --dump-header resp.head >resp.body 2>resp.err
if [ "$?" != 0 ]; then
    echo "  Expected request to succeed"
    echo "  Error: $(cat resp.err)"
    exit 1
elif [ -z "$(grep '200 OK' resp.head)" ]; then
    echo "  Expected request to succeed, but was $(head -n1 resp.head)"
    cat resp.body
    exit 1
fi;

NAMESPACE_ID=$(jq .data.namespaceId -r resp.body)

echo "GET /v1/delivery-kinds?\$filter=code%20eq%20${DELIVERY_KIND_CODE}"
curl --request GET http://${RAVN_SERVER_HOST}/v1/delivery-kinds?\$filter=code%20eq%20${DELIVERY_KIND_CODE} --silent --show-error --dump-header resp.head >resp.body 2>resp.err
if [ "$?" != 0 ]; then
    echo "  Expected request to succeed"
    echo "  Error: $(cat resp.err)"
    exit 1
elif [ -z "$(grep '200 OK' resp.head)" ]; then
    echo "  Expected request to succeed, but was $(head -n1 resp.head)"
    cat resp.body
    exit 1
fi;

DELIVERY_KIND_ID=$(jq .data[0].id -r resp.body)

echo "GET /v1/delivery-kinds/${DELIVERY_KIND_ID}"
curl --request GET http://${RAVN_SERVER_HOST}/v1/delivery-kinds/${DELIVERY_KIND_ID} --silent --show-error --dump-header resp.head >resp.body 2>resp.err
if [ "$?" != 0 ]; then
    echo "  Expected request to succeed"
    echo "  Error: $(cat resp.err)"
    exit 1
elif [ -z "$(grep '200 OK' resp.head)" ]; then
    echo "  Expected request to succeed, but was $(head -n1 resp.head)"
    cat resp.body
    exit 1
fi;

echo "GET /v1/namespaces/${NAMESPACE_ID}"
curl --request GET http://${RAVN_SERVER_HOST}/v1/namespaces/${NAMESPACE_ID} --silent --show-error --dump-header resp.head >resp.body 2>resp.err
if [ "$?" != 0 ]; then
    echo "  Expected request to succeed"
    echo "  Error: $(cat resp.err)"
    exit 1
elif [ -z "$(grep '200 OK' resp.head)" ]; then
    echo "  Expected request to succeed, but was $(head -n1 resp.head)"
    cat resp.body
    exit 1
fi;

echo "GET /v1/${NAMESPACE_ID}/messages/${MESSAGE_ID}"
curl --request GET http://${RAVN_SERVER_HOST}/v1/${NAMESPACE_ID}/messages/${MESSAGE_ID} --silent --show-error --dump-header resp.head >resp.body 2>resp.err
if [ "$?" != 0 ]; then
    echo "  Expected request to succeed"
    echo "  Error: $(cat resp.err)"
    exit 1
elif [ -z "$(grep '200 OK' resp.head)" ]; then
    echo "  Expected request to succeed, but was $(head -n1 resp.head)"
    cat resp.body
    exit 1
fi;

echo "All tests succeeded."
