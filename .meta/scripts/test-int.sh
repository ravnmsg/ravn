#!/bin/sh
set -e

function stop_services() {
    printf "Stopping services..."

    if [ -n "${MQ_CTN_NAME}" ]; then
        docker rm -f ${MQ_CTN_NAME} > /dev/null && printf "."
    fi

    if [ -n "${CACHE_CTN_NAME}" ]; then
        docker rm -f ${CACHE_CTN_NAME} > /dev/null && printf "."
    fi

    if [ -n "${DB_CTN_NAME}" ]; then
        docker rm -f ${DB_CTN_NAME} > /dev/null && printf "."
    fi

    echo
}

# When the script fails, stops the container.
trap stop_services EXIT

# Starts a temporary message queue if no NATS host received.
if [ -z "${RAVN_NATS_HOST}" ]; then
    echo "RAVN_NATS_HOST not provided."
    MQ_CTN_NAME=mq-$(uuidgen)
    CTN_NAME=${MQ_CTN_NAME} sh ./.meta/scripts/test-mq.sh
    RAVN_NATS_HOST=localhost:$(docker port ${MQ_CTN_NAME} 4222/tcp | cut -d ":" -f2)
fi

# Starts a temporary cache if no Redis host received.
if [ -z "${RAVN_REDIS_HOST}" ]; then
    echo "RAVN_REDIS_HOST not provided."
    CACHE_CTN_NAME=cache-$(uuidgen)
    CTN_NAME=${CACHE_CTN_NAME} sh ./.meta/scripts/test-cache.sh
    RAVN_REDIS_HOST=localhost:$(docker port ${CACHE_CTN_NAME} 6379/tcp | cut -d ":" -f2)
fi

# Starts a temporary database if no PostgreSQL host received.
if [ -z "${RAVN_POSTGRESQL_HOST}" ]; then
    echo "RAVN_POSTGRESQL_HOST not provided."
    DB_CTN_NAME=db-$(uuidgen)
    CTN_NAME=${DB_CTN_NAME} sh ./.meta/scripts/test-db.sh
    RAVN_POSTGRESQL_HOST=localhost:$(docker port ${DB_CTN_NAME} 5432/tcp | cut -d ":" -f2)
fi

RAVN_NATS_HOST="${RAVN_NATS_HOST}" \
    RAVN_REDIS_HOST="${RAVN_REDIS_HOST}" \
    RAVN_POSTGRESQL_HOST="${RAVN_POSTGRESQL_HOST}" \
    go test -tags=integration ${TEST_ARGS}
