#!/bin/sh
set -e

if [ -z "${CTN_NAME}" ]; then
    CTN_NAME=db-$(uuidgen)
fi

echo "Starting NATS service..."

if [ -n "${CTN_NETWORK}" ]; then
    CTN_NETWORK="--network ${CTN_NETWORK}"
fi

docker run --name ${CTN_NAME} --publish 4222 ${CTN_NETWORK} --detach --rm nats > /dev/null

NATS_HOST=localhost:$(docker port ${CTN_NAME} 4222/tcp | cut -d ":" -f2)

# Wait for service readiness?

echo "NATS daemon ready and listening on ${NATS_HOST}."
