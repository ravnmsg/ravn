DELETE FROM deliveries *;
DELETE FROM messages *;
DELETE FROM templates *;
DELETE FROM template_kinds *;
DELETE FROM message_kinds *;
DELETE FROM delivery_kinds *;
DELETE FROM namespaces *;

INSERT INTO namespaces
    (code,      parent_id,                                              deleted) VALUES
    ('ns1',     null,                                                   false),
    ('ns2',     (SELECT id FROM namespaces WHERE code = 'ns1' LIMIT 1), false),
    ('ns3',     (SELECT id FROM namespaces WHERE code = 'ns2' LIMIT 1), false),
    ('ns4',     null,                                                   false);

INSERT INTO delivery_kinds
    (code,   namespace_id,                                           host, disabled, deleted) VALUES
    ('noop', (SELECT id FROM namespaces WHERE code = 'ns2' LIMIT 1), '',   false,    false),
    ('log',  (SELECT id FROM namespaces WHERE code = 'ns2' LIMIT 1), '',   false,    false);

INSERT INTO message_kinds
    (code,   namespace_id,                                           disabled, deleted) VALUES
    ('test', (SELECT id FROM namespaces WHERE code = 'ns2' LIMIT 1), false,    false);

INSERT INTO template_kinds
    (delivery_kind_id,                                           target_attr, deleted) VALUES
    ((SELECT id FROM delivery_kinds WHERE code = 'log' LIMIT 1), 'content',   false);

INSERT INTO templates
    (template_kind_id,                                                      message_kind_id,                                            language_tag, body,                     deleted) VALUES
    ((SELECT id FROM template_kinds WHERE target_attr = 'content' LIMIT 1), (SELECT id FROM message_kinds WHERE code = 'test' LIMIT 1), 'en',         'Hello {{.username}}!',   false),
    ((SELECT id FROM template_kinds WHERE target_attr = 'content' LIMIT 1), (SELECT id FROM message_kinds WHERE code = 'test' LIMIT 1), 'fr',         'Bonjour {{.username}}!', false);

INSERT INTO messages
    (message_kind_id,                                            input,                                 status,      schedule_time) VALUES
    ((SELECT id FROM message_kinds WHERE code = 'test' LIMIT 1), json_build_object('username', 'John'), 'succeeded', NULL);

INSERT INTO deliveries
    (message_id,                        delivery_kind_id,                                           language_tag, status,        status_message, deliver_time, input,                                                           output,                                 external_id) VALUES
    ((SELECT id FROM messages LIMIT 1), (SELECT id FROM delivery_kinds WHERE code = 'log' LIMIT 1), 'en',         'undelivered', 'failed',       NOW(),        json_build_object(),                                             json_build_object(),                    ''),
    ((SELECT id FROM messages LIMIT 1), (SELECT id FROM delivery_kinds WHERE code = 'log' LIMIT 1), 'en',         'delivered',   '',             NOW(),        json_build_object('content', 'Hello John!', 'username', 'John'), json_build_object('externalId', 'log'), 'log');
