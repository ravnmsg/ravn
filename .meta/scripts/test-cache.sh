#!/bin/sh
set -e

if [ -z "${CTN_NAME}" ]; then
    CTN_NAME=cache-$(uuidgen)
fi

echo "Starting Redis service..."

if [ -n "${CTN_NETWORK}" ]; then
    CTN_NETWORK="--network ${CTN_NETWORK}"
fi

docker run --name ${CTN_NAME} --publish 6379 ${CTN_NETWORK} --detach --rm redis > /dev/null

REDIS_HOST=localhost:$(docker port ${CTN_NAME} 6379/tcp | cut -d ":" -f2)

# Wait for service readiness?

echo "Redis daemon ready and listening on ${REDIS_HOST}."
