#!/bin/sh
set -e

if [ -z "${CTN_NAME}" ]; then
    CTN_NAME=db-$(uuidgen)
fi

echo "Starting PostgreSQL service..."

if [ -n "${CTN_NETWORK}" ]; then
    CTN_NETWORK="--network ${CTN_NETWORK}"
fi

docker run --name ${CTN_NAME} --publish 5432 ${CTN_NETWORK} --detach --rm --env POSTGRES_DB=ravntest --env POSTGRES_USER=ravn --env POSTGRES_PASSWORD=ravn postgres:14 > /dev/null

POSTGRESQL_HOST=localhost:$(docker port ${CTN_NAME} 5432/tcp | cut -d ":" -f2)

printf 'Waiting for daemon readiness...'
c=0
until docker exec --user postgres ${CTN_NAME} pg_ctl status >/dev/null; [ "$?" -eq "0" ]; do
    printf '.'
    if [ "$c" -ge "4" ]; then
        echo
        echo "PostgreSQL daemon not ready after ${c} seconds"
        false
    fi
    c=$((c+1))
    sleep 1
done
echo

printf 'Waiting for database readiness...'
c=0
until docker exec --user postgres ${CTN_NAME} psql --dbname=ravntest --username=ravn --command 'SELECT 1;' >/dev/null 2>&1; [ "$?" -eq "0" ]; do
    printf '.'
    if [ "$c" -ge "12" ]; then
        echo
        echo "PostgreSQL database not ready after ${c} seconds"
        false
    fi
    c=$((c+1))
    sleep 1
done
echo

echo "Migrating schema..."
cat $(ls ./db/migrations/*.up.sql | sort) | docker exec -i ${CTN_NAME} psql --dbname=ravntest --username=ravn --quiet --echo-errors

echo "Inserting test data..."
cat ./.meta/scripts/test-seed.sql | docker exec -i ${CTN_NAME} psql --dbname=ravntest --username=ravn --quiet --echo-errors

echo "PostgreSQL daemon ready and listening on ${POSTGRESQL_HOST}."
