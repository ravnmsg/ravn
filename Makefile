# Build the Docker images defined in the docker compose file. If docker compose
# is already up, this will restart the services.
.PHONY: build
build:
	@if [[ -n "$(shell docker compose top)" ]]; then \
		docker compose up --detach --build; \
	else \
		docker compose build; \
	fi

# Migrate the database. Migrates "up" by default, but this can be changed with
# the CMD argument, eg. `make migrate CMD=down`. This assumes PostgreSQL from
# the docker compose file, listening on postgres:5432.
.PHONY: migrate
CMD = "up"
migrate:
	@docker run \
	  --volume ${PWD}/db/migrations:/db/migrations \
	  --network $(shell docker inspect $(shell docker compose ps -q postgres) -f '{{.HostConfig.NetworkMode}}') \
	  --rm \
	  migrate/migrate:latest \
	    -database postgres://ravn:ravn@postgres:5432/ravn?sslmode=disable \
	    -path=/db/migrations/ \
	    $(CMD)

# Deletes all resources from the database, and creates a basic set of new ones.
# This assumes PostgreSQL from the docker compose file.
.PHONY: seed
seed:
	@docker compose exec --no-TTY postgres psql --dbname=ravn --username=ravn --echo-errors < ./.meta/scripts/seed.sql

# Publish a message to the NATS server. This assumes NATS from the docker
# compose file, listening on nats:4222.
.PHONY: publish
publish:
	@if [ -z $(MSG) ]; then \
		echo "MSG argument not provided: eg. make publish MSG='{\"deliveryId\":\"00000000-0000-0000-0000-000000000000\"}'" && false; \
	else \
		docker run \
		  --entrypoint=nats \
		  --network $(shell docker inspect $(shell docker compose ps -q postgres) -f '{{.HostConfig.NetworkMode}}') \
		  --rm \
		  synadia/nats-box:latest \
		    pub \
			-s 'nats://ravn:ravn@nats:4222' \
		    delivery_requests \
		    '$(MSG)'; \
	fi

# Run code style tests using golangci-lint. If the binary is not available
# locally, the style tests will run in a Docker container.
#
# ref. https://golangci-lint.run/
.PHONY: test-style
test-style:
	@if which golangci-lint > /dev/null; [ $$? -eq 0 ]; then \
		golangci-lint run; \
	else \
		docker run --rm --workdir /app --volume ${PWD}/:/app golangci/golangci-lint golangci-lint run; \
	fi

# Run unit tests only. No dependencies are required.
.PHONY: test-unit
TEST_ARGS = "./..."
test-unit:
	@go test -tags=unit ${TEST_ARGS}

# Run integration tests only. A test database and message queue are required.
.PHONY: test-int
RAVN_POSTGRESQL_HOST = ""
RAVN_NATS_HOST = ""
TEST_ARGS = "./..."
test-int:
	@RAVN_NATS_HOST="$(RAVN_NATS_HOST)" RAVN_POSTGRESQL_HOST="$(RAVN_POSTGRESQL_HOST)" TEST_ARGS="$(TEST_ARGS)" .meta/scripts/test-int.sh

# Run end-to-end tests only. A test database and message queue are required, as
# well as some running instances of Ravn.
.PHONY: test-e2e
RAVN_POSTGRESQL_HOST = ""
RAVN_NATS_HOST = ""
TEST_ARGS = "./..."
test-e2e:
	@RAVN_NATS_HOST="$(RAVN_NATS_HOST)" RAVN_POSTGRESQL_HOST="$(RAVN_POSTGRESQL_HOST)" TEST_ARGS="$(TEST_ARGS)" .meta/scripts/test-e2e.sh

# Run quick smoke tests that doesn't need a separate test database.
.PHONY: test-smoke
test-smoke:
	@sh .meta/scripts/test-smoke.sh

.DEFAULT_GOAL := build
