START TRANSACTION;

ALTER TABLE messages
    ADD COLUMN status VARCHAR(100);

-- Would be better to look at a message's deliveries to find the correct status, but oh well...
UPDATE messages SET
    status = 'succeeded';

ALTER TABLE messages
    ALTER COLUMN status SET NOT NULL;

CREATE INDEX messages_status_idx ON messages (status);

ALTER TABLE deliveries
    DROP COLUMN schedule_time;

COMMIT;
