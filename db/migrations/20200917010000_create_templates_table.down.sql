START TRANSACTION;

DROP INDEX templates_message_kind_id_idx;
DROP INDEX templates_delivery_kind_id_idx;
DROP INDEX templates_message_kind_id_delivery_kind_id_idx;

DROP TABLE templates;

COMMIT;
