START TRANSACTION;

DROP INDEX user_tokens_code_idx;
DROP INDEX user_tokens_user_id_idx;

DROP TABLE user_tokens;

COMMIT;
