START TRANSACTION;

ALTER TABLE messages
    ADD COLUMN language_tag VARCHAR(100);

UPDATE messages m SET
    language_tag = (SELECT language_tag FROM deliveries d WHERE d.message_id = m.id LIMIT 1);

ALTER TABLE messages
    ALTER COLUMN language_tag SET NOT NULL;

CREATE INDEX messages_language_tag_idx ON messages (language_tag);

DROP INDEX deliveries_language_tag_idx;

ALTER TABLE deliveries
    DROP COLUMN language_tag;

COMMIT;
