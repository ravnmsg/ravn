START TRANSACTION;

DROP INDEX deliveries_message_id_idx;
DROP INDEX deliveries_delivery_kind_id_idx;
DROP INDEX deliveries_status_idx;
DROP INDEX deliveries_external_id_idx;

DROP TABLE deliveries;

COMMIT;
