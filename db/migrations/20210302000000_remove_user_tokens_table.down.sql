START TRANSACTION;

CREATE TABLE user_tokens (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    code VARCHAR(100) NOT NULL,
    user_id UUID NOT NULL REFERENCES users,
    bcrypt_hash VARCHAR(100) NOT NULL,
    revoke_time TIMESTAMPTZ,
    create_time TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    update_time TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

-- Adds a `ravn` password to the admin, to help setting everything up again.
INSERT INTO user_tokens (code, user_id, bcrypt_hash, revoke_time) VALUES
    ('ravn', (SELECT id FROM users WHERE name = 'admin' LIMIT 1), '$2a$04$JpsVf2xuNV09hh.UIidiQeuX.hEqjwOVYA9vtE7MBVQ6bYyLhyHlW', NULL);

CREATE INDEX user_tokens_code_idx ON user_tokens (code);
CREATE INDEX user_tokens_user_id_idx ON user_tokens (user_id);

COMMIT;
