START TRANSACTION;

ALTER TABLE messages
    DROP COLUMN requested_delivery_kind_id;

COMMIT;
