START TRANSACTION;

ALTER TABLE messages
    ADD COLUMN requested_delivery_kind_id UUID REFERENCES delivery_kinds;

-- Really not ideal, but assumes that all messages are already done.
UPDATE messages SET
    requested_delivery_kind_id = (SELECT id FROM delivery_kinds LIMIT 1);

ALTER TABLE messages
    ALTER COLUMN requested_delivery_kind_id SET NOT NULL;

COMMIT;
