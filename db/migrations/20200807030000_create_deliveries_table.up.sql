START TRANSACTION;

CREATE TABLE deliveries (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    message_id UUID NOT NULL REFERENCES messages,
    delivery_kind_id UUID NOT NULL REFERENCES delivery_kinds,
    status VARCHAR(100) NOT NULL,
    status_message VARCHAR(4096),
    schedule_time TIMESTAMPTZ,
    deliver_time TIMESTAMPTZ,
    external_id VARCHAR(1024),
    create_time TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    update_time TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE INDEX deliveries_message_id_idx ON deliveries (message_id);
CREATE INDEX deliveries_delivery_kind_id_idx ON deliveries (delivery_kind_id);
CREATE INDEX deliveries_status_idx ON deliveries (status);
CREATE INDEX deliveries_external_id_idx ON deliveries (external_id);

COMMIT;
