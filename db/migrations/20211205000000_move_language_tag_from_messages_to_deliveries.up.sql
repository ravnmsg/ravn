START TRANSACTION;

ALTER TABLE deliveries
    ADD COLUMN language_tag VARCHAR(100);

UPDATE deliveries d SET
    language_tag = (SELECT language_tag FROM messages m WHERE m.id = d.message_id);

ALTER TABLE deliveries
    ALTER COLUMN language_tag SET NOT NULL;

CREATE INDEX deliveries_language_tag_idx ON deliveries (language_tag);

DROP INDEX messages_language_tag_idx;

ALTER TABLE messages
    DROP COLUMN language_tag;

COMMIT;
