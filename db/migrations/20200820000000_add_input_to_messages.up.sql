START TRANSACTION;

ALTER TABLE messages
    ADD COLUMN input JSONB NOT NULL DEFAULT '{}'::JSONB;

COMMIT;
