START TRANSACTION;

ALTER TABLE namespaces
    ADD COLUMN parent_id UUID REFERENCES namespaces;

CREATE INDEX namespaces_parent_id_idx ON namespaces (parent_id);

COMMIT;
