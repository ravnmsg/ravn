START TRANSACTION;

ALTER TABLE messages
    ADD COLUMN language_tag VARCHAR(100);

UPDATE messages SET
    language_tag = 'en';

ALTER TABLE messages
    ALTER COLUMN language_tag SET NOT NULL;

CREATE INDEX messages_language_tag_idx ON messages (language_tag);

COMMIT;
