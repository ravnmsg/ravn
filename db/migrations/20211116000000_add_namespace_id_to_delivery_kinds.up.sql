START TRANSACTION;

ALTER TABLE delivery_kinds
    ADD COLUMN namespace_id UUID REFERENCES namespaces;

CREATE INDEX delivery_kinds_namespace_id_idx ON delivery_kinds (namespace_id);

COMMIT;
