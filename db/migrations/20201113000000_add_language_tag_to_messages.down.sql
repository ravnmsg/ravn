START TRANSACTION;

DROP INDEX messages_language_tag_idx;

ALTER TABLE messages
    DROP COLUMN language_tag;

COMMIT;
