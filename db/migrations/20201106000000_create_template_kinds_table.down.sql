START TRANSACTION;

DROP INDEX templates_template_kind_id_idx;
DROP INDEX template_kinds_delivery_kind_id_idx;

ALTER TABLE templates
    ADD COLUMN delivery_kind_id UUID REFERENCES delivery_kinds,
    ADD COLUMN target_attr VARCHAR(100);

CREATE INDEX templates_delivery_kind_id_idx ON templates (delivery_kind_id);
CREATE INDEX templates_message_kind_id_delivery_kind_id_idx ON templates (message_kind_id, delivery_kind_id);

-- Revert each template kinds.
DO $$
DECLARE
    t record;
BEGIN
    FOR t IN (SELECT * FROM templates) LOOP
        UPDATE templates SET
            delivery_kind_id = (SELECT delivery_kind_id FROM template_kinds WHERE id = t.template_kind_id),
            target_attr = (SELECT target_attr FROM template_kinds WHERE id = t.template_kind_id)
            WHERE id = t.id;
    END LOOP;
END;
$$;

ALTER TABLE templates
    ALTER COLUMN delivery_kind_id SET NOT NULL,
    ALTER COLUMN target_attr SET NOT NULL,
    DROP COLUMN template_kind_id;

DROP TABLE template_kinds;

COMMIT;
