START TRANSACTION;

DROP INDEX namespaces_code_idx;
DROP INDEX message_kinds_namespace_id_idx;

ALTER TABLE message_kinds
    DROP COLUMN namespace_id;

DROP TABLE namespaces;

COMMIT;
