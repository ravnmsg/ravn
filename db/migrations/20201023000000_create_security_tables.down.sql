START TRANSACTION;

DROP INDEX users_name_idx;
DROP INDEX user_tokens_code_idx;
DROP INDEX user_tokens_user_id_idx;
DROP INDEX roles_code_idx;
DROP INDEX user_roles_user_id_idx;

DROP TABLE user_roles;

DROP TABLE roles;

DROP TABLE user_tokens;

DROP TABLE users;

COMMIT;
