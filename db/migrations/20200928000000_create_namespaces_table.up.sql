-- This will fail if there are any message kinds in the database (NOT
-- NULL namespace_id column on the message_kinds table). Since this is
-- only a temporary situation (no real message kinds should have been
-- created by now), the database should first be cleared before running
-- this migration.

START TRANSACTION;

CREATE TABLE namespaces (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    code VARCHAR(100) NOT NULL,
    create_time TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    update_time TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

ALTER TABLE message_kinds
    ADD COLUMN namespace_id UUID NOT NULL REFERENCES namespaces;

CREATE INDEX namespaces_code_idx ON namespaces (code);
CREATE INDEX message_kinds_namespace_id_idx ON message_kinds (namespace_id);

COMMIT;
