START TRANSACTION;

ALTER TABLE messages
    DROP COLUMN requested_delivery_kind_id,
    DROP COLUMN schedule_time;

COMMIT;
