START TRANSACTION;

ALTER TABLE deliveries
    ADD COLUMN schedule_time TIMESTAMPTZ;

DROP INDEX messages_status_idx;

ALTER TABLE messages
    DROP COLUMN status;

COMMIT;
