START TRANSACTION;

ALTER TABLE delivery_kinds
    DROP COLUMN namespace_id;

DROP INDEX delivery_kinds_namespace_id_idx;

COMMIT;
