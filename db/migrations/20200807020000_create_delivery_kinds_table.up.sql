START TRANSACTION;

CREATE TABLE delivery_kinds (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    code VARCHAR(100) NOT NULL,
    host VARCHAR(250) NOT NULL,
    disabled BOOLEAN NOT NULL DEFAULT FALSE,
    create_time TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    update_time TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE INDEX delivery_kinds_code_idx ON delivery_kinds (code);

COMMIT;
