START TRANSACTION;

DROP INDEX message_kinds_code_idx;
DROP INDEX messages_message_kind_id_idx;

ALTER TABLE messages
    DROP COLUMN message_kind_id;

DROP TABLE message_kinds;

COMMIT;
