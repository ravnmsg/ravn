START TRANSACTION;

CREATE TABLE template_kinds (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    delivery_kind_id UUID NOT NULL REFERENCES delivery_kinds,
    target_attr VARCHAR(100) NOT NULL,
    create_time TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    update_time TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

ALTER TABLE templates
    ADD COLUMN template_kind_id UUID REFERENCES template_kinds;

-- Creates new template kinds using the data in each templates.
DO $$
DECLARE
    t record;
    tk record;
BEGIN
    FOR t IN (SELECT * FROM templates) LOOP
        SELECT id
            FROM template_kinds
            INTO tk
            WHERE delivery_kind_id = t.delivery_kind_id
            AND target_attr = t.target_attr
            LIMIT 1;

        -- Creates template kind if it doesn't yet exists.
        IF tk IS NULL THEN
            INSERT
                INTO template_kinds (delivery_kind_id, target_attr)
                VALUES (t.delivery_kind_id, t.target_attr)
                RETURNING id INTO tk;
        END IF;

        UPDATE templates
            SET template_kind_id = tk.id
            WHERE id = t.id;
    END LOOP;
END;
$$;

DROP INDEX templates_delivery_kind_id_idx;
DROP INDEX templates_message_kind_id_delivery_kind_id_idx;

ALTER TABLE templates
    ALTER COLUMN template_kind_id SET NOT NULL,
    DROP COLUMN delivery_kind_id,
    DROP COLUMN target_attr;

CREATE INDEX templates_template_kind_id_idx ON templates (template_kind_id);
CREATE INDEX template_kinds_delivery_kind_id_idx ON template_kinds (delivery_kind_id);

COMMIT;
