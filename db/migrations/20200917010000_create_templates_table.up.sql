START TRANSACTION;

CREATE TABLE templates (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    message_kind_id UUID NOT NULL REFERENCES message_kinds,
    delivery_kind_id UUID NOT NULL REFERENCES delivery_kinds,
    language_tag VARCHAR(100) NOT NULL,
    body TEXT NOT NULL,
    target_attr VARCHAR(100) NOT NULL,
    create_time TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    update_time TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

CREATE INDEX templates_message_kind_id_idx ON templates (message_kind_id);
CREATE INDEX templates_delivery_kind_id_idx ON templates (delivery_kind_id);
CREATE INDEX templates_message_kind_id_delivery_kind_id_idx ON templates (message_kind_id, delivery_kind_id);

COMMIT;
