START TRANSACTION;

ALTER TABLE templates
    DROP COLUMN deleted;

ALTER TABLE template_kinds
    DROP COLUMN deleted;

ALTER TABLE message_kinds
    DROP COLUMN deleted;

ALTER TABLE delivery_kinds
    DROP COLUMN deleted;

ALTER TABLE namespaces
    DROP COLUMN deleted;

COMMIT;
