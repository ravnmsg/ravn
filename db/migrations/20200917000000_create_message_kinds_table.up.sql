-- This will fail if there are any messages in the database (NOT NULL
-- message_kind_id column on the messages table). Since this is only a
-- temporary situation (no real messages should have been created by
-- now), the database should first be cleared before running this
-- migration.

START TRANSACTION;

CREATE TABLE message_kinds (
    id UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    code VARCHAR(100) NOT NULL,
    disabled BOOLEAN NOT NULL DEFAULT FALSE,
    create_time TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    update_time TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

ALTER TABLE messages
    ADD COLUMN message_kind_id UUID NOT NULL REFERENCES message_kinds;

CREATE INDEX message_kinds_code_idx ON message_kinds (code);
CREATE INDEX messages_message_kind_id_idx ON messages (message_kind_id);

COMMIT;
