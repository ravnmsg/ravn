START TRANSACTION;

ALTER TABLE namespaces
    DROP COLUMN parent_id;

DROP INDEX namespaces_parent_id_idx;

COMMIT;
