package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/cache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/httpserver"
	"gitlab.com/ravnmsg/ravn/internal/msgqueue"
	"gitlab.com/ravnmsg/ravn/internal/receiver/handler"
)

type receiverService struct {
	srv       *httpserver.Server
	unsecured bool
	mq        msgqueue.Publisher
	cache     cache.Cacher
	db        database.ReadWriter
	clk       clock.Clocker
}

func startReceiverService(ctx context.Context) error {
	unsecured := os.Getenv("RAVN_UNSECURED") != ""

	srv, err := httpserver.New(ctx, os.Getenv("RAVN_RECEIVER_SERVER_PORT"), unsecured)
	if err != nil {
		return fmt.Errorf("error creating HTTP server: %s", err)
	}

	log.Printf("HTTP Server listening on `%s`...", srv.Host)
	if unsecured {
		log.Printf("UNSECURED mode - everyone has all permissions")
	}

	mq, err := newNatsMsgQueue(ctx, os.Getenv("RAVN_NATS_URI"), os.Getenv("RAVN_NATS_SUBJECT"))
	if err != nil {
		return fmt.Errorf("error connecting to message queue: %s", err)
	}

	c, err := newRedisCache(ctx, os.Getenv("RAVN_REDIS_URI"))
	if err != nil {
		return fmt.Errorf("error connecting to cache: %s", err)
	}

	db, err := newPostgreSQLDatabase(ctx, os.Getenv("RAVN_POSTGRESQL_URI"))
	if err != nil {
		return fmt.Errorf("error connecting to database: %s", err)
	}

	svc := &receiverService{
		srv:       srv,
		unsecured: unsecured,
		mq:        mq,
		cache:     c,
		db:        db,
		clk:       newClock(),
	}

	svc.registerHTTPEndpoints()

	// Starts listening for HTTP requests. This is expected to block until the application
	// context ends.
	if err := svc.srv.Serve(); err != nil {
		return fmt.Errorf("error serving requests: %s", err)
	}

	log.Printf("Receiver service stopped.")

	return nil
}

func (svc *receiverService) registerHTTPEndpoints() {
	na := action.NewNamespaceAction(svc.cache, svc.db, svc.clk)
	dka := action.NewDeliveryKindAction(svc.cache, svc.db, svc.clk)
	mka := action.NewMessageKindAction(svc.cache, svc.db, svc.clk)
	tka := action.NewTemplateKindAction(svc.cache, svc.db, svc.clk)
	ta := action.NewTemplateAction(svc.cache, svc.db, svc.clk)
	ma := action.NewMessageAction(svc.cache, svc.db, svc.clk)
	da := action.NewDeliveryAction(svc.mq, svc.cache, svc.db, svc.clk)

	nh := handler.NewNamespaceHandler(na, svc.authz(na))
	svc.registerHTTPNamespaceEndpoints(nh, na, "/{api_version}/namespaces")
	svc.registerHTTPNamespaceEndpoints(nh, na, "/{api_version}/{namespace_id}/namespaces")

	dkh := handler.NewDeliveryKindHandler(dka, na, svc.authz(na))
	svc.registerHTTPDeliveryKindEndpoints(dkh, na, "/{api_version}/delivery-kinds")
	svc.registerHTTPDeliveryKindEndpoints(dkh, na, "/{api_version}/{namespace_id}/delivery-kinds")

	mkh := handler.NewMessageKindHandler(mka, na, svc.authz(na))
	svc.registerHTTPMessageKindEndpoints(mkh, na, "/{api_version}/message-kinds")
	svc.registerHTTPMessageKindEndpoints(mkh, na, "/{api_version}/{namespace_id}/message-kinds")

	tkh := handler.NewTemplateKindHandler(tka, dka, na, svc.authz(na))
	svc.registerHTTPTemplateKindEndpoints(tkh, na, "/{api_version}/template-kinds")
	svc.registerHTTPTemplateKindEndpoints(tkh, na, "/{api_version}/{namespace_id}/template-kinds")

	th := handler.NewTemplateHandler(ta, tka, mka, dka, na, svc.authz(na))
	svc.srv.RegisterHandler(http.MethodPost, "/{api_version}/templates/preview", handler.HandleRequest(th.Preview, na))
	svc.registerHTTPTemplateEndpoints(th, na, "/{api_version}/templates")
	svc.registerHTTPTemplateEndpoints(th, na, "/{api_version}/{namespace_id}/templates")

	mh := handler.NewMessageHandler(ma, da, mka, dka, na, svc.authz(na), svc.clk)
	svc.registerHTTPMessageEndpoints(mh, na, "/{api_version}/messages")
	svc.registerHTTPMessageEndpoints(mh, na, "/{api_version}/{namespace_id}/messages")
}

func (svc *receiverService) registerHTTPNamespaceEndpoints(
	nh *handler.NamespaceHandler,
	na action.NamespaceActioner,
	path string,
) {
	svc.srv.RegisterHandler(http.MethodGet, path, handler.HandleRequest(nh.GetMany, na))
	svc.srv.RegisterHandler(http.MethodPost, path, handler.HandleRequest(nh.Post, na))

	pathWithID := fmt.Sprintf("%s/{id}", path)
	svc.srv.RegisterHandler(http.MethodGet, pathWithID, handler.HandleRequest(nh.GetByID, na))
	svc.srv.RegisterHandler(http.MethodPut, pathWithID, handler.HandleRequest(nh.Put, na))
	svc.srv.RegisterHandler(http.MethodPatch, pathWithID, handler.HandleRequest(nh.Patch, na))
	svc.srv.RegisterHandler(http.MethodDelete, pathWithID, handler.HandleRequest(nh.Delete, na))
}

func (svc *receiverService) registerHTTPDeliveryKindEndpoints(
	dkh *handler.DeliveryKindHandler,
	na action.NamespaceActioner,
	path string,
) {
	svc.srv.RegisterHandler(http.MethodGet, path, handler.HandleRequest(dkh.GetMany, na))
	svc.srv.RegisterHandler(http.MethodPost, path, handler.HandleRequest(dkh.Post, na))

	pathWithID := fmt.Sprintf("%s/{id}", path)
	svc.srv.RegisterHandler(http.MethodGet, pathWithID, handler.HandleRequest(dkh.GetByID, na))
	svc.srv.RegisterHandler(http.MethodPut, pathWithID, handler.HandleRequest(dkh.Put, na))
	svc.srv.RegisterHandler(http.MethodPatch, pathWithID, handler.HandleRequest(dkh.Patch, na))
	svc.srv.RegisterHandler(http.MethodDelete, pathWithID, handler.HandleRequest(dkh.Delete, na))
}

func (svc *receiverService) registerHTTPMessageKindEndpoints(
	mkh *handler.MessageKindHandler,
	na action.NamespaceActioner,
	path string,
) {
	svc.srv.RegisterHandler(http.MethodGet, path, handler.HandleRequest(mkh.GetMany, na))
	svc.srv.RegisterHandler(http.MethodPost, path, handler.HandleRequest(mkh.Post, na))

	pathWithID := fmt.Sprintf("%s/{id}", path)
	svc.srv.RegisterHandler(http.MethodGet, pathWithID, handler.HandleRequest(mkh.GetByID, na))
	svc.srv.RegisterHandler(http.MethodPut, pathWithID, handler.HandleRequest(mkh.Put, na))
	svc.srv.RegisterHandler(http.MethodPatch, pathWithID, handler.HandleRequest(mkh.Patch, na))
	svc.srv.RegisterHandler(http.MethodDelete, pathWithID, handler.HandleRequest(mkh.Delete, na))
}

func (svc *receiverService) registerHTTPTemplateKindEndpoints(
	tkh *handler.TemplateKindHandler,
	na action.NamespaceActioner,
	path string,
) {
	svc.srv.RegisterHandler(http.MethodGet, path, handler.HandleRequest(tkh.GetMany, na))
	svc.srv.RegisterHandler(http.MethodPost, path, handler.HandleRequest(tkh.Post, na))

	pathWithID := fmt.Sprintf("%s/{id}", path)
	svc.srv.RegisterHandler(http.MethodGet, pathWithID, handler.HandleRequest(tkh.GetByID, na))
	svc.srv.RegisterHandler(http.MethodPut, pathWithID, handler.HandleRequest(tkh.Put, na))
	svc.srv.RegisterHandler(http.MethodPatch, pathWithID, handler.HandleRequest(tkh.Patch, na))
	svc.srv.RegisterHandler(http.MethodDelete, pathWithID, handler.HandleRequest(tkh.Delete, na))
}

func (svc *receiverService) registerHTTPTemplateEndpoints(
	th *handler.TemplateHandler,
	na action.NamespaceActioner,
	path string,
) {
	svc.srv.RegisterHandler(http.MethodGet, path, handler.HandleRequest(th.GetMany, na))
	svc.srv.RegisterHandler(http.MethodPost, path, handler.HandleRequest(th.Post, na))

	pathWithID := fmt.Sprintf("%s/{id}", path)
	svc.srv.RegisterHandler(http.MethodGet, pathWithID, handler.HandleRequest(th.GetByID, na))
	svc.srv.RegisterHandler(http.MethodPut, pathWithID, handler.HandleRequest(th.Put, na))
	svc.srv.RegisterHandler(http.MethodPatch, pathWithID, handler.HandleRequest(th.Patch, na))
	svc.srv.RegisterHandler(http.MethodDelete, pathWithID, handler.HandleRequest(th.Delete, na))
}

func (svc *receiverService) registerHTTPMessageEndpoints(
	mh *handler.MessageHandler,
	na action.NamespaceActioner,
	path string,
) {
	svc.srv.RegisterHandler(http.MethodGet, path, handler.HandleRequest(mh.GetMany, na))
	svc.srv.RegisterHandler(http.MethodPost, path, handler.HandleRequest(mh.Post, na))

	pathWithID := fmt.Sprintf("%s/{id}", path)
	svc.srv.RegisterHandler(http.MethodGet, pathWithID, handler.HandleRequest(mh.GetByID, na))
}

func (svc *receiverService) authz(na action.NamespaceActioner) authorizer.Authorizer {
	if svc.unsecured {
		return authorizer.NewUnsecuredAuthorizer(na)
	}

	return authorizer.NewAuthorizer(na)
}
