package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/cache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/msgqueue"
	"gitlab.com/ravnmsg/ravn/internal/scheduler"
)

type schedulerService struct {
	mq    msgqueue.Publisher
	cache cache.Cacher
	db    database.ReadWriter
	clk   clock.Clocker
}

func startSchedulerService(ctx context.Context) error {
	timerDuration, err := parseTimerDuration(os.Getenv("RAVN_SCHEDULER_TIMER_SECONDS"), 1*time.Minute)
	if err != nil {
		return err
	}

	log.Printf("Scheduler running with period of %s.", timerDuration)

	mq, err := newNatsMsgQueue(ctx, os.Getenv("RAVN_NATS_URI"), os.Getenv("RAVN_NATS_SUBJECT"))
	if err != nil {
		return fmt.Errorf("error connecting to message queue: %s", err)
	}

	c, err := newRedisCache(ctx, os.Getenv("RAVN_REDIS_URI"))
	if err != nil {
		return fmt.Errorf("error connecting to cache: %s", err)
	}

	db, err := newPostgreSQLDatabase(ctx, os.Getenv("RAVN_POSTGRESQL_URI"))
	if err != nil {
		return fmt.Errorf("error connecting to database: %s", err)
	}

	svc := &schedulerService{
		mq:    mq,
		cache: c,
		db:    db,
		clk:   newClock(),
	}

	sched := svc.newScheduler()

	ticker := time.NewTicker(timerDuration)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			log.Printf("Scheduler service stopped.")
			return nil
		case <-ticker.C:
			if err := svc.searchLoop(sched); err != nil {
				log.Print(err.Error())
			}
		}
	}
}

func (svc *schedulerService) newScheduler() *scheduler.Scheduler {
	da := action.NewDeliveryAction(svc.mq, svc.cache, svc.db, svc.clk)
	ma := action.NewMessageAction(svc.cache, svc.db, svc.clk)

	return scheduler.New(da, ma, svc.clk)
}

func (svc *schedulerService) searchLoop(sched *scheduler.Scheduler) error {
	log.Printf("Searching for scheduled messages...")
	messages, err := sched.RetrieveScheduledMessages()
	if err != nil {
		return err
	}

	logMessageIDs(messages)

	for _, m := range messages {
		if err := sched.DeliverMessage(m); err != nil {
			return err
		}
	}

	return nil
}
