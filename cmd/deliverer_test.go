//go:build !unit && !integration
// +build !unit,!integration

package main

import (
	"bytes"
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/receiver/handler"
)

func TestReceiverMessageDelivery(t *testing.T) {
	var status int

	host := os.Getenv("RAVN_RECEIVER_HOST_UNSECURED")

	// POST /v1/messages
	var message *handler.MessageResponse
	reqBody := []byte(`{"messageKind":"message_kind_000000000001","deliveryKind":"noop","languageTag":"en","input":{"username":"John"}}`)
	status = reqData(http.MethodPost, fmt.Sprintf("http://%s/v1/messages", host), nil, bytes.NewReader(reqBody), &message, t)
	assert.Equal(t, http.StatusCreated, status)
	assert.Equal(t, "active", message.Status)
	assert.Equal(t, 1, len(message.Deliveries))
	assert.Equal(t, "pending", message.Deliveries[0].Status)

	// Wait for message to be delivered.
	time.Sleep(500 * time.Millisecond)

	// GET /v1/messages/:message_id
	messageID := message.ID
	message = nil
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/messages/%s", host, messageID), nil, nil, &message, t)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, "succeeded", message.Status)
	assert.Equal(t, 1, len(message.Deliveries))
	assert.Equal(t, "delivered", message.Deliveries[0].Status)
}
