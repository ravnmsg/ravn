//go:build !unit && !integration
// +build !unit,!integration

package main

import (
	"bytes"
	"fmt"
	"net/http"
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/receiver/handler"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestReceiverGetMessages(t *testing.T) {
	var status int

	host := os.Getenv("RAVN_RECEIVER_HOST_UNSECURED")

	// GET /v1/messages
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/messages", host), nil, nil, nil, t)
	assert.Equal(t, http.StatusOK, status)

	// GET /v1/messages?$filter=status%20eq%20active
	var messages []*handler.MessageResponse
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/messages?$filter=status%%20eq%%20active", host), nil, nil, &messages, t)
	assert.Equal(t, http.StatusOK, status)

	foundMessage := false
	for _, m := range messages {
		if m.ID == uuid.MustParse("00000000-0000-0000-0006-000000000001") {
			foundMessage = true
		}
	}
	assert.True(t, foundMessage)

	// GET /v1/messages/00000000-0000-0000-0006-000000000000
	var message *handler.MessageResponse
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/messages/00000000-0000-0000-0006-000000000000", host), nil, nil, &message, t)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, uuid.MustParse("00000000-0000-0000-0006-000000000000"), message.ID)
	assert.Equal(t, "message_kind_000000000000", message.MessageKind)
	assert.Equal(t, map[string]interface{}{"key": "value_000000000000"}, message.Input)
	assert.Equal(t, "succeeded", message.Status)
	assert.Equal(t, (*time.Time)(nil), message.ScheduleTime)
	assert.Equal(t, test.MakeTime("2000-01-01T06:00:58Z", t), message.CreateTime)
	assert.Equal(t, 1, len(message.Deliveries))
	assert.Equal(t, "delivery_kind_000000000000", message.Deliveries[0].DeliveryKind)
	assert.Equal(t, "language_tag_000000000000", message.Deliveries[0].LanguageTag)
	assert.Equal(t, "delivered", message.Deliveries[0].Status)
	assert.Equal(t, "", message.Deliveries[0].StatusMessage)
	assert.Equal(t, test.MakeTimeRef("2000-01-01T07:00:00Z", t), message.Deliveries[0].DeliverTime)

	// GET /v1/:namespace_id/messages
	messages = nil
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/00000000-0000-0000-0001-000000000001/messages", host), nil, nil, &messages, t)
	assert.Equal(t, http.StatusOK, status)

	foundMessage = false
	for _, m := range messages {
		if m.ID == uuid.MustParse("00000000-0000-0000-0006-000000000001") {
			foundMessage = true
		}
	}
	assert.True(t, foundMessage)
}

func TestReceiverDeliveryKinds(t *testing.T) {
	var status int

	host := os.Getenv("RAVN_RECEIVER_HOST_UNSECURED")

	// GET /v1/delivery-kinds
	var deliveryKinds []*resource.DeliveryKind
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/delivery-kinds", host), nil, nil, &deliveryKinds, t)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, 3, len(deliveryKinds))

	// GET /v1/delivery-kinds?$filter=code%20eq%20noop
	deliveryKinds = nil
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/delivery-kinds?$filter=code%%20eq%%20noop", host), nil, nil, &deliveryKinds, t)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, 1, len(deliveryKinds))
	assert.Equal(t, uuid.MustParse("00000000-0000-0000-0002-000000000001"), deliveryKinds[0].ID)

	// GET /v1/delivery-kinds/00000000-0000-0000-0002-000000000001
	var deliveryKind *resource.DeliveryKind
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/delivery-kinds/00000000-0000-0000-0002-000000000001", host), nil, nil, &deliveryKind, t)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, uuid.MustParse("00000000-0000-0000-0002-000000000001"), deliveryKind.ID)
	assert.Equal(t, "noop", deliveryKind.Code)
	assert.Equal(t, uuid.MustParse("00000000-0000-0000-0001-000000000001"), deliveryKind.NamespaceID)
	assert.Equal(t, "000000000001.example.com", deliveryKind.Host)
	assert.Equal(t, false, deliveryKind.Disabled)
	assert.Equal(t, test.MakeTime("2000-01-01T02:01:58Z", t), deliveryKind.CreateTime)
	assert.Equal(t, test.MakeTime("2000-01-01T02:01:59Z", t), deliveryKind.UpdateTime)

	// POST /v1/delivery-kinds
	deliveryKind = nil
	reqBody := []byte(`{"code":"test","namespaceId":"00000000-0000-0000-0001-000000000000","host":"new_host","disabled":true}`)
	status = reqData(http.MethodPost, fmt.Sprintf("http://%s/v1/delivery-kinds", host), nil, bytes.NewReader(reqBody), &deliveryKind, t)
	assert.Equal(t, http.StatusCreated, status)
	assert.NotEqual(t, uuid.Nil, deliveryKind.ID)
	assert.Equal(t, "test", deliveryKind.Code)
	assert.Equal(t, uuid.MustParse("00000000-0000-0000-0001-000000000000"), deliveryKind.NamespaceID)
	assert.Equal(t, "new_host", deliveryKind.Host)
	assert.Equal(t, true, deliveryKind.Disabled)
	assert.True(t, time.Since(deliveryKind.CreateTime) <= 100*time.Millisecond)
	assert.True(t, time.Since(deliveryKind.UpdateTime) <= 100*time.Millisecond)

	// GET /v1/delivery-kinds/:delivery_kind_id
	deliveryKindID := deliveryKind.ID
	deliveryKind = nil
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKindID), nil, nil, &deliveryKind, t)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, deliveryKindID, deliveryKind.ID)
	assert.Equal(t, "test", deliveryKind.Code)

	// PUT /v1/delivery-kinds/:delivery_kind_id
	deliveryKind = nil
	reqBody = []byte(`{"code":"updated_test","namespaceId":"00000000-0000-0000-0001-000000000000","host":"updated_host","disabled":false}`)
	status = reqData(http.MethodPut, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKindID), nil, bytes.NewReader(reqBody), &deliveryKind, t)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, deliveryKindID, deliveryKind.ID)
	assert.Equal(t, "updated_test", deliveryKind.Code)
	assert.Equal(t, uuid.MustParse("00000000-0000-0000-0001-000000000000"), deliveryKind.NamespaceID)
	assert.Equal(t, "updated_host", deliveryKind.Host)
	assert.Equal(t, false, deliveryKind.Disabled)
	assert.True(t, time.Since(deliveryKind.CreateTime) <= 200*time.Millisecond)
	assert.True(t, time.Since(deliveryKind.UpdateTime) <= 100*time.Millisecond)

	// GET /v1/delivery-kinds/:delivery_kind_id
	deliveryKind = nil
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKindID), nil, nil, &deliveryKind, t)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, "updated_test", deliveryKind.Code)

	// PATCH /v1/delivery-kinds/:delivery_kind_id
	deliveryKind = nil
	reqBody = []byte(`{"code":"patched_test"}`)
	status = reqData(http.MethodPatch, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKindID), nil, bytes.NewReader(reqBody), &deliveryKind, t)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, deliveryKindID, deliveryKind.ID)
	assert.Equal(t, "patched_test", deliveryKind.Code)
	assert.Equal(t, uuid.MustParse("00000000-0000-0000-0001-000000000000"), deliveryKind.NamespaceID)
	assert.Equal(t, "updated_host", deliveryKind.Host)
	assert.Equal(t, false, deliveryKind.Disabled)
	assert.True(t, time.Since(deliveryKind.CreateTime) <= 300*time.Millisecond)
	assert.True(t, time.Since(deliveryKind.UpdateTime) <= 100*time.Millisecond)

	// GET /v1/delivery-kinds/:delivery_kind_id
	deliveryKind = nil
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKindID), nil, nil, &deliveryKind, t)
	assert.Equal(t, http.StatusOK, status)
	assert.Equal(t, "patched_test", deliveryKind.Code)
	assert.NotEmpty(t, deliveryKind.Host)

	// DELETE /v1/delivery-kinds/:delivery_kind_id
	status = reqData(http.MethodDelete, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKindID), nil, nil, nil, t)
	assert.Equal(t, http.StatusOK, status)

	// GET /v1/delivery-kinds/:delivery_kind_id
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKindID), nil, nil, nil, t)
	assert.Equal(t, http.StatusNotFound, status)
}

func TestReceiverDeletedDeliveryKind(t *testing.T) {
	var status int

	host := os.Getenv("RAVN_RECEIVER_HOST_UNSECURED")

	// POST /v1/delivery-kinds
	var deliveryKind *resource.DeliveryKind
	reqBody := []byte(`{"code":"test","namespaceId":"00000000-0000-0000-0001-000000000000"}`)
	status = reqData(http.MethodPost, fmt.Sprintf("http://%s/v1/delivery-kinds", host), nil, bytes.NewReader(reqBody), &deliveryKind, t)
	assert.Equal(t, http.StatusCreated, status)

	// GET /v1/delivery-kinds/:delivery_kind_id
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKind.ID), nil, nil, nil, t)
	assert.Equal(t, http.StatusOK, status)

	// DELETE /v1/delivery-kinds/:delivery_kind_id
	status = reqData(http.MethodDelete, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKind.ID), nil, nil, nil, t)
	assert.Equal(t, http.StatusOK, status)

	// GET /v1/delivery-kinds/:delivery_kind_id
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKind.ID), nil, nil, nil, t)
	assert.Equal(t, http.StatusNotFound, status)
}

func TestReceiverDeliveryKindInDeletedNamespaceBranch(t *testing.T) {
	var status int

	host := os.Getenv("RAVN_RECEIVER_HOST_UNSECURED")

	// POST /v1/namespaces
	var parentNamespace *resource.Namespace
	reqBody := []byte(`{"code":"test","parentId":"00000000-0000-0000-0001-000000000001"}`)
	status = reqData(http.MethodPost, fmt.Sprintf("http://%s/v1/namespaces", host), nil, bytes.NewReader(reqBody), &parentNamespace, t)
	assert.Equal(t, http.StatusCreated, status)

	// POST /v1/namespaces
	var childNamespace *resource.Namespace
	reqBody = []byte(fmt.Sprintf(`{"code":"test","parentId":%q}`, parentNamespace.ID))
	status = reqData(http.MethodPost, fmt.Sprintf("http://%s/v1/namespaces", host), nil, bytes.NewReader(reqBody), &childNamespace, t)
	assert.Equal(t, http.StatusCreated, status)

	// POST /v1/delivery-kinds
	var deliveryKind *resource.DeliveryKind
	reqBody = []byte(fmt.Sprintf(`{"code":"test","namespaceId":%q}`, childNamespace.ID))
	status = reqData(http.MethodPost, fmt.Sprintf("http://%s/v1/delivery-kinds", host), nil, bytes.NewReader(reqBody), &deliveryKind, t)
	assert.Equal(t, http.StatusCreated, status)

	// GET /v1/delivery-kinds/:delivery_kind_id
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKind.ID), nil, nil, nil, t)
	assert.Equal(t, http.StatusOK, status)

	// DELETE /v1/namespaces/:namespace_id
	status = reqData(http.MethodDelete, fmt.Sprintf("http://%s/v1/namespaces/%s", host, parentNamespace.ID), nil, nil, nil, t)
	assert.Equal(t, http.StatusOK, status)

	// GET /v1/delivery-kinds/:delivery_kind_id
	status = reqData(http.MethodGet, fmt.Sprintf("http://%s/v1/delivery-kinds/%s", host, deliveryKind.ID), nil, nil, nil, t)
	assert.Equal(t, http.StatusNotFound, status)
}

func TestReceiverJWTSecurity(t *testing.T) {
	var status int
	var headers map[string]string
	var respBody []byte

	host := os.Getenv("RAVN_RECEIVER_HOST")
	url := fmt.Sprintf("http://%s/v1/delivery-kinds/00000000-0000-0000-0002-000000000000", host)

	// Without JWT
	status, respBody = req(http.MethodGet, url, nil, nil, t)
	assert.Equal(t, http.StatusForbidden, status)
	assert.Equal(t, respErrorsExpectation("invalid auth token"), string(respBody))

	// With invalid JWT
	headers = map[string]string{"Authorization": "Bearer invalid"}
	status, respBody = req(http.MethodGet, url, headers, nil, t)
	assert.Equal(t, http.StatusForbidden, status)
	assert.Equal(t, respErrorsExpectation("invalid JWT"), string(respBody))

	// With invalid JWT payload
	headers = map[string]string{"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.E5urBL-NPzmEQTG5hJQSyAaIxNdhbGiClIoMCE7B0n4"}
	status, respBody = req(http.MethodGet, url, headers, nil, t)
	assert.Equal(t, http.StatusForbidden, status)
	assert.Equal(t, respErrorsExpectation("invalid JWT payload"), string(respBody))

	// With valid JWT
	headers = map[string]string{"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlcyI6W3siY29kZSI6Im5zX2VkaXRvciIsIm5hbWVzcGFjZUlkIjoiMDAwMDAwMDAtMDAwMC0wMDAwLTAwMDEtMDAwMDAwMDAwMDAwIn1dfQ.uK6i6qh6QnCgQz4n6jdM7k5bzDvbvLCdWh2eBilyZPc"}
	var deliveryKind *resource.DeliveryKind
	status = reqData(http.MethodGet, url, headers, nil, &deliveryKind, t)
	assert.Equal(t, http.StatusOK, status)
}
