package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/cache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/msgqueue"
	"gitlab.com/ravnmsg/ravn/internal/retryer"
)

type retryerService struct {
	mq    msgqueue.Publisher
	cache cache.Cacher
	db    database.ReadWriter
	clk   clock.Clocker
}

func startRetryerService(ctx context.Context) error {
	timerDuration, err := parseTimerDuration(os.Getenv("RAVN_RETRYER_TIMER_SECONDS"), 10*time.Second)
	if err != nil {
		return err
	}

	log.Printf("Retryer running with period of %s.", timerDuration)

	mq, err := newNatsMsgQueue(ctx, os.Getenv("RAVN_NATS_URI"), os.Getenv("RAVN_NATS_SUBJECT"))
	if err != nil {
		return fmt.Errorf("error connecting to message queue: %s", err)
	}

	c, err := newRedisCache(ctx, os.Getenv("RAVN_REDIS_URI"))
	if err != nil {
		return fmt.Errorf("error connecting to cache: %s", err)
	}

	db, err := newPostgreSQLDatabase(ctx, os.Getenv("RAVN_POSTGRESQL_URI"))
	if err != nil {
		return fmt.Errorf("error connecting to database: %s", err)
	}

	svc := &retryerService{
		mq:    mq,
		cache: c,
		db:    db,
		clk:   newClock(),
	}

	ret := svc.newRetryer()

	ticker := time.NewTicker(timerDuration)
	defer ticker.Stop()

	for {
		select {
		case <-ctx.Done():
			log.Printf("Retryer service stopped.")
			return nil
		case <-ticker.C:
			if err := svc.searchLoop(ret); err != nil {
				log.Print(err.Error())
			}
		}
	}
}

func (svc *retryerService) newRetryer() *retryer.Retryer {
	da := action.NewDeliveryAction(svc.mq, svc.cache, svc.db, svc.clk)
	ma := action.NewMessageAction(svc.cache, svc.db, svc.clk)

	return retryer.New(da, ma, svc.clk)
}

func (svc *retryerService) searchLoop(ret *retryer.Retryer) error {
	log.Printf("Searching for failed messages...")
	messages, err := ret.RetrieveFailedMessages()
	if err != nil {
		return err
	}

	logMessageIDs(messages)

	for _, m := range messages {
		if err := ret.DeliverMessage(m); err != nil {
			return err
		}
	}

	return nil
}
