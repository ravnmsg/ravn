package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/cache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/deliverer"
	"gitlab.com/ravnmsg/ravn/internal/msgqueue"
)

type delivererService struct {
	mq    msgqueue.Subscriber
	cache cache.Cacher
	db    database.ReadWriter
	clk   clock.Clocker
}

func startDelivererService(ctx context.Context) error {
	mq, err := newNatsMsgQueue(ctx, os.Getenv("RAVN_NATS_URI"), os.Getenv("RAVN_NATS_SUBJECT"))
	if err != nil {
		return fmt.Errorf("error connecting to message queue: %s", err)
	}

	c, err := newRedisCache(ctx, os.Getenv("RAVN_REDIS_URI"))
	if err != nil {
		return fmt.Errorf("error connecting to cache: %s", err)
	}

	db, err := newPostgreSQLDatabase(ctx, os.Getenv("RAVN_POSTGRESQL_URI"))
	if err != nil {
		return fmt.Errorf("error connecting to database: %s", err)
	}

	svc := &delivererService{
		mq:    mq,
		cache: c,
		db:    db,
		clk:   newClock(),
	}

	// Starts listening for delivery requests. This is expected to block until the application
	// context ends.
	del := svc.buildDeliverer()
	if err := svc.mq.Subscribe(deliverer.DeliveryHandler(del)); err != nil {
		return fmt.Errorf("error listening for requests: %s", err)
	}

	log.Printf("Deliverer service stopped.")

	return nil
}

func (svc *delivererService) buildDeliverer() deliverer.Deliverer {
	da := action.NewDeliveryAction(nil, svc.cache, svc.db, svc.clk)
	ma := action.NewMessageAction(svc.cache, svc.db, svc.clk)
	ta := action.NewTemplateAction(svc.cache, svc.db, svc.clk)
	tka := action.NewTemplateKindAction(svc.cache, svc.db, svc.clk)
	mka := action.NewMessageKindAction(svc.cache, svc.db, svc.clk)
	dka := action.NewDeliveryKindAction(svc.cache, svc.db, svc.clk)

	return deliverer.New(da, ma, ta, tka, mka, dka, svc.clk)
}
