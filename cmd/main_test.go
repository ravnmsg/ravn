//go:build !unit && !integration
// +build !unit,!integration

package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

func reqData(method, url string, headers map[string]string, body io.Reader, target interface{}, t *testing.T) int {
	status, respBytes := req(method, url, headers, body, t)

	respBody := make(map[string]interface{})
	if err := json.Unmarshal(respBytes, &respBody); !assert.NoError(t, err) {
		assert.FailNow(t, fmt.Sprintf("unexpected error parsing response: %s", err))
	}

	respData, err := json.Marshal(respBody["data"])
	if !assert.NoError(t, err) {
		assert.FailNow(t, fmt.Sprintf("unexpected error reading response: %s", err))
	}

	if target != nil {
		if err := json.Unmarshal(respData, target); !assert.NoError(t, err) {
			assert.FailNow(t, fmt.Sprintf("unexpected error parsing response: %s", err))
		}
	}

	return status
}

func req(method, url string, headers map[string]string, body io.Reader, t *testing.T) (status int, respBytes []byte) {
	req, err := http.NewRequest(method, url, body)
	if !assert.NoError(t, err) {
		assert.FailNow(t, fmt.Sprintf("unexpected error creating request: %s", err))
	}

	for k, v := range headers {
		req.Header.Add(k, v)
	}

	resp, err := http.DefaultClient.Do(req)
	if !assert.NoError(t, err) {
		assert.FailNow(t, fmt.Sprintf("unexpected error performing request: %s", err))
	}

	respBytes, err = io.ReadAll(resp.Body)
	defer resp.Body.Close()
	if !assert.NoError(t, err) {
		assert.FailNow(t, fmt.Sprintf("unexpected error reading response: %s", err))
	}

	return resp.StatusCode, respBytes
}

func respErrorsExpectation(errMsg string) string {
	return "{" +
		`"errors":[` +
		fmt.Sprintf("%q", errMsg) +
		`],` +
		`"meta":{}` +
		"}"
}
