package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"sync"
	"time"

	"gitlab.com/ravnmsg/ravn/internal/cache/rediscache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database/postgresqldatabase"
	"gitlab.com/ravnmsg/ravn/internal/msgqueue/natsmsgqueue"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// nolint:funlen // This method is straightforward and should be easy to read.
func main() {
	ctx, cancel := context.WithCancel(context.Background())

	// Handles graceful termination of the application.
	go func() {
		defer cancel()

		ch := make(chan os.Signal, 1)
		signal.Notify(ch, os.Interrupt)

		// Blocks until an interrupt signal is caught.
		<-ch
	}()

	startReceiver := flag.Bool("receiver", false, "starts the receiver service")
	startDeliverer := flag.Bool("deliverer", false, "starts the deliverer service")
	startRetryer := flag.Bool("retryer", false, "starts the retryer service")
	startScheduler := flag.Bool("scheduler", false, "starts the scheduler service")
	flag.Parse()

	var wg sync.WaitGroup

	if *startReceiver {
		wg.Add(1)
		go func() {
			defer wg.Done()
			defer cancel()

			// Starts listening for HTTP requests. This is expected to block until the application
			// context ends.
			if err := startReceiverService(ctx); err != nil {
				log.Printf("Error with Receiver service: %s", err)
				os.Exit(1)
			}
		}()
	}

	if *startDeliverer {
		wg.Add(1)
		go func() {
			defer wg.Done()
			defer cancel()

			// Starts listening for delivery requests. This is expected to block until the
			// application context ends.
			if err := startDelivererService(ctx); err != nil {
				log.Printf("Error with Deliverer service: %s", err)
				os.Exit(1)
			}
		}()
	}

	if *startRetryer {
		wg.Add(1)
		go func() {
			defer wg.Done()
			defer cancel()

			// Starts periodically looking for failed messages that should be retried.
			if err := startRetryerService(ctx); err != nil {
				log.Printf("Error with Retryer service: %s", err)
				os.Exit(1)
			}
		}()
	}

	if *startScheduler {
		wg.Add(1)
		go func() {
			defer wg.Done()
			defer cancel()

			// Starts periodically looking for scheduled messages that are due to be delivered.
			if err := startSchedulerService(ctx); err != nil {
				log.Printf("Error with Scheduler service: %s", err)
				os.Exit(1)
			}
		}()
	}

	wg.Wait()

	log.Printf("Everything turned off.")
}

func newNatsMsgQueue(ctx context.Context, uri, subject string) (*natsmsgqueue.MSGQueue, error) {
	mq, err := natsmsgqueue.New(ctx, uri, subject)
	if err != nil {
		return nil, fmt.Errorf("error creating connection to NATS message queue: %s", err)
	}

	return mq, nil
}

func newPostgreSQLDatabase(ctx context.Context, uri string) (*postgresqldatabase.Database, error) {
	db, err := postgresqldatabase.New(ctx, uri)
	if err != nil {
		return nil, fmt.Errorf("error creating connection to PostgreSQL database: %s", err)
	}

	return db, nil
}

func newRedisCache(ctx context.Context, uri string) (*rediscache.Cache, error) {
	c, err := rediscache.New(ctx, uri)
	if err != nil {
		return nil, fmt.Errorf("error creating connection to Redis cache: %s", err)
	}

	return c, nil
}

func newClock() *clock.Clock {
	return clock.New()
}

func parseTimerDuration(durationSeconds string, defaultDuration time.Duration) (time.Duration, error) {
	if durationSeconds == "" {
		return defaultDuration, nil
	}

	timer, err := strconv.ParseFloat(durationSeconds, 64)
	if err != nil {
		return 0 * time.Second, fmt.Errorf("error parsing duration to a float: %s", err)
	}

	return time.Duration(timer) * time.Second, nil
}

func logMessageIDs(messages []*resource.Message) {
	if len(messages) == 0 {
		log.Printf("Found none.")
		return
	}

	msg := "Found: "
	for i, m := range messages {
		if i != 0 {
			msg += ", "
		}
		msg += m.ID.String()
	}
	log.Printf("%s.", msg)
}
