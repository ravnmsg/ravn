# golang:1.17.7-alpine
FROM golang@sha256:1dc6a836407ef26c761af27bd39eb86ec385bab0f89a6c969bb1a04b342f7074 as builder
WORKDIR /app

COPY go.mod go.sum /app/
RUN go mod download

ENV CGO_ENABLED=0
COPY . /app/
RUN go build -o /go/bin/ravn ./cmd

# If image size is an issue, "FROM scratch" can be used instead, at the expense
# of being less convenient (eg. lack of shell access).
FROM debian:11-slim
LABEL maintainer="sbernier@ravnmsg.io"
WORKDIR /app

COPY --from=builder /go/bin/ravn /app/

USER 10001:10001

ENTRYPOINT ["./ravn"]
