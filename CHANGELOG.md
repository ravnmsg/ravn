# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

- soft-delete to namespace, delivery kind, message kind, template kind, and template resources
- endpoint /v1/templates/preview to preview templates
- Redis cache at the action level

## v0.0.3 - 2022-03-07

### Added

- [c5e9f87] allow unsecured mode
- [5e78176] end-to-end tests
- [dcee3e2] input and output attributes to delivery resource

### Changed

- [9db0d14] merge all services into one single binary
- [5e78176] make smoke tests idempotent
- [dcee3e2] deliverer, processor and requester structure
- [9a6c9b6] receiver paths from camel-case to kebab-case

## v0.0.2 - 2022-02-14

### Added

- [cc074cc] receiver unit and integration tests
- [8c26a4b] basic continuous integration flow (GitLab)
- [01088b9] basic deliverer code
- [4ffa142] use Ping Requester service
- [d737d26] message kind resources
- [72e1a56] template resources
- [c3054ab] namespace resources
- [71625bc] basic authentication and authorization
- [1838307] template kind resources
- [fe17529] language tag support
- [d3d013c] versioning in path
- [c8dafe2] OPTIONS method and CORS headers
- [7563606] path namespace validation
- [082adfb] smoke tests
- [fb29197] message statuses
- [6fb0e5e] scheduler service
- [6fb0e5e] clock interface
- [c62e41f] action layer
- [c62e41f] retryer service

### Changed

- [2ef1928] http handlers structure
- [0b1dee1] build-in requesters
- [e487f73] database interface signature
- [20ae56a] error handling definition
- [fb29197] delivery statuses
- [6fb0e5e] data required for a delivery
- [9d68d90] relationship between deliverer, processor, and requester
- [c62e41f] total refactor of the application internals
- [c62e41f] authorization by JWT

### Removed

- [1107e62] references to Ping Requester service
- [97b9311] user authentication
- [fb29197] delivery schedule time
- [47822f7] Google PubSub message queue
- [c62e41f] authentication
- [c62e41f] user, role, and user role resources

## v0.0.1 - 2020-08-10

### Added

- [2018ab3] basic receiver code
