package action

import (
	"fmt"
	"time"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/cache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type NamespaceActioner interface {
	RetrieveMany(*query.Query) ([]*resource.Namespace, error)
	RetrieveByID(uuid.UUID) (*resource.Namespace, error)
	RetrieveByIDOrCode(string) (*resource.Namespace, error)
	RetrieveParents(uuid.UUID) ([]*resource.Namespace, error)
	RetrieveChildren(uuid.UUID) ([]*resource.Namespace, error)
	Create(*resource.Namespace) (*resource.Namespace, error)
	Update(uuid.UUID, *resource.Namespace) (*resource.Namespace, error)
	Delete(uuid.UUID) (*resource.Namespace, error)
}

type NamespaceAction struct {
	cache cache.Cacher
	db    database.ReadWriter
	clk   clock.Clocker
}

// NewNamespaceAction creates a new message action.
func NewNamespaceAction(c cache.Cacher, db database.ReadWriter, clk clock.Clocker) *NamespaceAction {
	return &NamespaceAction{
		cache: c,
		db:    db,
		clk:   clk,
	}
}

func (a *NamespaceAction) RetrieveMany(q *query.Query) ([]*resource.Namespace, error) {
	namespaces := make([]*resource.Namespace, 0)
	if err := a.db.RetrieveMany(resource.TypeNamespace, q, &namespaces); err != nil {
		return nil, fmt.Errorf("error retrieving namespaces: %s", err)
	}

	return namespaces, nil
}

func (a *NamespaceAction) RetrieveByID(id uuid.UUID) (*resource.Namespace, error) {
	namespace := new(resource.Namespace)

	if ok := a.cache.Get(cachedResourceKey(resource.TypeNamespace, id), namespace); ok {
		return namespace, nil
	}

	err := a.db.RetrieveByID(resource.TypeNamespace, id, namespace)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving namespace: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeNamespace, namespace.ID), namespace)

	return namespace, nil
}

func (a *NamespaceAction) RetrieveByIDOrCode(idOrCode string) (*resource.Namespace, error) {
	namespace := new(resource.Namespace)
	err := a.db.RetrieveByIDOrCode(resource.TypeNamespace, idOrCode, namespace)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceIDOrCode
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving namespace: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeNamespace, namespace.ID), namespace)

	return namespace, nil
}

// RetrieveParents returns the namespace tree ordered from the specified namespace down to the
// namespace root. The specified namespace is included in the list, and is always the first item.
//
// Because any encountered deleted namespace means that all of its children, including the specified
// namespace, are also not available, an ErrNamespaceDeleted error will be returned.
func (a *NamespaceAction) RetrieveParents(namespaceID uuid.UUID) ([]*resource.Namespace, error) {
	namespaces := make([]*resource.Namespace, 0)
	var currentNamespaceID = namespaceID

	for {
		// Detects infinite loops by comparing the current namespace ID with all namespaces that
		// are currently accepted.
		for _, n := range namespaces {
			if currentNamespaceID == n.ID {
				return nil, ErrInfiniteLoopDetected
			}
		}

		namespace, err := a.RetrieveByID(currentNamespaceID)
		if err != nil {
			return nil, err
		} else if namespace.Deleted {
			return nil, ErrNamespaceDeleted
		}

		namespaces = append(namespaces, namespace)

		if namespace.ParentID == nil {
			// Current namespace is a root namespace.
			break
		}

		currentNamespaceID = *namespace.ParentID
	}

	return namespaces, nil
}

// RetrieveChildren returns the namespace tree from the specified namespace up to all of its leaves.
// No particular order should be assumed within the returned array, but it will nonetheless
// generally be ordered generation by generation. The specified namespace is included in the list,
// and is always the first item.
//
// Any encountered deleted namespace also invalidate its children, but other namespace branches are
// still valid.
func (a *NamespaceAction) RetrieveChildren(namespaceID uuid.UUID) ([]*resource.Namespace, error) {
	namespace, err := a.RetrieveByID(namespaceID)
	if err != nil {
		return nil, err
	}

	if namespace.Deleted {
		return nil, ErrNamespaceDeleted
	}

	namespaces := []*resource.Namespace{namespace}
	currentIndex := 0

	for {
		if currentIndex >= len(namespaces) {
			break
		}

		q := query.NewQuery(
			query.WithFilterArgs("ParentID", query.OpEqual, namespaces[currentIndex].ID),
		)
		childNamespaces, err := a.RetrieveMany(q)
		if err != nil {
			return nil, err
		}

		for _, n := range childNamespaces {
			// A deleted namespace invalidates all of its children, so the current namespace branch
			// is ignored.
			if n.Deleted {
				continue
			}

			// Detects infinite loops by comparing the child namespace n with all namespaces that
			// are currently accepted.
			for _, n2 := range namespaces[0:currentIndex] {
				if n.ID == n2.ID {
					return nil, ErrInfiniteLoopDetected
				}
			}

			namespaces = append(namespaces, n)
		}

		currentIndex++
	}

	return namespaces, nil
}

func (a *NamespaceAction) Create(namespace *resource.Namespace) (*resource.Namespace, error) {
	namespace.ID = uuid.Nil
	namespace.CreateTime = a.clk.Now()
	namespace.UpdateTime = a.clk.Now()

	namespaceID, err := a.db.Create(resource.TypeNamespace, namespace)
	if err != nil {
		return nil, fmt.Errorf("error creating namespace: %s", err)
	}

	namespace = new(resource.Namespace)
	if err := a.db.RetrieveByID(resource.TypeNamespace, namespaceID, namespace); err != nil {
		return nil, fmt.Errorf("error retrieving namespace: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeNamespace, namespace.ID), namespace)

	return namespace, nil
}

func (a *NamespaceAction) Update(id uuid.UUID, namespace *resource.Namespace) (*resource.Namespace, error) {
	namespace.ID = id
	namespace.CreateTime = time.Time{}
	namespace.UpdateTime = a.clk.Now()

	err := a.db.Update(resource.TypeNamespace, id, namespace)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error updating namespace: %s", err)
	}

	namespace = new(resource.Namespace)
	if err := a.db.RetrieveByID(resource.TypeNamespace, id, namespace); err != nil {
		return nil, fmt.Errorf("error retrieving namespace: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeNamespace, namespace.ID), namespace)

	return namespace, nil
}

func (a *NamespaceAction) Delete(id uuid.UUID) (*resource.Namespace, error) {
	namespace := new(resource.Namespace)
	err := a.db.RetrieveByID(resource.TypeNamespace, id, namespace)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving namespace: %s", err)
	}

	namespace.Deleted = true

	if err := a.db.Update(resource.TypeNamespace, id, namespace); err != nil {
		return nil, fmt.Errorf("error updating namespace: %s", err)
	}

	a.cache.Del(cachedResourceKey(resource.TypeNamespace, namespace.ID))

	return namespace, nil
}

var _ = NamespaceActioner(new(NamespaceAction))
