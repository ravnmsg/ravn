//go:build !integration && !endtoend
// +build !integration,!endtoend

package action_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestMessageKindActionRetrieveMany(t *testing.T) {
	tests := map[string]struct {
		query                           *query.Query
		databaseRetrieveManyExpectation *test.DatabaseRetrieveManyExpectation
		expectedMessageKinds            []*resource.MessageKind
		expectedError                   error
	}{
		"when successful": {
			query: query.NewQuery(
				query.WithFilterArgs("Code", query.OpEqual, "message_kind_000000000000"),
				query.WithOrderArgs("Code", query.DirDescending),
				query.WithPageArgs(8, 5),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Code", query.OpEqual, "message_kind_000000000000"),
					query.WithOrderArgs("Code", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ExpectedResources: &[]*resource.MessageKind{},
				ReturnedResources: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			expectedMessageKinds: []*resource.MessageKind{
				test.ArbitraryMessageKind(t),
			},
			expectedError: nil,
		},
		"when query not specified": {
			query: nil,
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedQuery:        nil,
				ExpectedResources:    &[]*resource.MessageKind{},
				ReturnedResources: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			expectedMessageKinds: []*resource.MessageKind{
				test.ArbitraryMessageKind(t),
			},
			expectedError: nil,
		},
		"when message kinds failed to be retrieved": {
			query: query.NewQuery(
				query.WithFilterArgs("Code", query.OpEqual, "message_kind_000000000000"),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Code", query.OpEqual, "message_kind_000000000000"),
				),
				ExpectedResources: &[]*resource.MessageKind{},
				ReturnedError:     test.ErrArbitrary,
			},
			expectedMessageKinds: nil,
			expectedError:        fmt.Errorf("error retrieving message kinds: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			db := test.NewDatabase(t)
			db.ExpectRetrieveMany(tt.databaseRetrieveManyExpectation)

			a := action.NewMessageKindAction(nil, db, test.NewClock("2000-01-01T00:00:00Z"))
			messageKinds, err := a.RetrieveMany(tt.query)

			assert.Equal(t, tt.expectedMessageKinds, messageKinds)
			assert.Equal(t, tt.expectedError, err)

			db.AssertExpectations(t)
		})
	}
}

func TestMessageKindActionRetrieveByID(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		cacheGetExpectation             *test.CacheGetExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedMessageKind             *resource.MessageKind
		expectedError                   error
	}{
		"when successful from cache": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "messageKind_00000000-0000-0000-0003-000000000000",
				ExpectedResource: &resource.MessageKind{},
				ReturnedResource: test.ArbitraryMessageKind(t),
				ReturnedResult:   true,
			},
			expectedMessageKind: test.ArbitraryMessageKind(t),
			expectedError:       nil,
		},
		"when successful from database": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "messageKind_00000000-0000-0000-0003-000000000000",
				ExpectedResource: &resource.MessageKind{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedResource:     test.ArbitraryMessageKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "messageKind_00000000-0000-0000-0003-000000000000",
				ExpectedResource: test.ArbitraryMessageKind(t),
				ReturnedResult:   true,
			},
			expectedMessageKind: test.ArbitraryMessageKind(t),
			expectedError:       nil,
		},
		"when message kind failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "messageKind_00000000-0000-0000-0003-000000000000",
				ExpectedResource: &resource.MessageKind{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedMessageKind: nil,
			expectedError:       action.ErrInvalidResourceID,
		},
		"when message kind failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "messageKind_00000000-0000-0000-0003-000000000000",
				ExpectedResource: &resource.MessageKind{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedMessageKind: nil,
			expectedError:       fmt.Errorf("error retrieving message kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectGet(tt.cacheGetExpectation)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewMessageKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			messageKind, err := a.RetrieveByID(tt.id)

			assert.Equal(t, tt.expectedMessageKind, messageKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestMessageKindActionRetrieveByIDOrCode(t *testing.T) {
	tests := map[string]struct {
		idOrCode                              string
		databaseRetrieveByIDOrCodeExpectation *test.DatabaseRetrieveByIDOrCodeExpectation
		cacheSetExpectation                   *test.CacheSetExpectation
		expectedMessageKind                   *resource.MessageKind
		expectedError                         error
	}{
		"when id specified as uuid": {
			idOrCode: "00000000-0000-0000-0003-000000000000",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedIDOrCode:     "00000000-0000-0000-0003-000000000000",
				ExpectedResource:     &resource.MessageKind{},
				ReturnedResource:     test.ArbitraryMessageKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "messageKind_00000000-0000-0000-0003-000000000000",
				ExpectedResource: test.ArbitraryMessageKind(t),
				ReturnedResult:   true,
			},
			expectedMessageKind: test.ArbitraryMessageKind(t),
			expectedError:       nil,
		},
		"when id specified as code": {
			idOrCode: "code",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedIDOrCode:     "code",
				ExpectedResource:     &resource.MessageKind{},
				ReturnedResource:     test.ArbitraryMessageKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "messageKind_00000000-0000-0000-0003-000000000000",
				ExpectedResource: test.ArbitraryMessageKind(t),
				ReturnedResult:   true,
			},
			expectedMessageKind: test.ArbitraryMessageKind(t),
			expectedError:       nil,
		},
		"when message kind failed to be found": {
			idOrCode: "00000000-0000-0000-0003-000000000000",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedIDOrCode:     "00000000-0000-0000-0003-000000000000",
				ExpectedResource:     &resource.MessageKind{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedMessageKind: nil,
			expectedError:       action.ErrInvalidResourceIDOrCode,
		},
		"when message kind failed to be retrieved": {
			idOrCode: "00000000-0000-0000-0003-000000000000",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedIDOrCode:     "00000000-0000-0000-0003-000000000000",
				ExpectedResource:     &resource.MessageKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedMessageKind: nil,
			expectedError:       fmt.Errorf("error retrieving message kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByIDOrCode(tt.databaseRetrieveByIDOrCodeExpectation)

			a := action.NewMessageKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			messageKind, err := a.RetrieveByIDOrCode(tt.idOrCode)

			assert.Equal(t, tt.expectedMessageKind, messageKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestMessageKindActionCreate(t *testing.T) {
	tests := map[string]struct {
		messageKind                     *resource.MessageKind
		databaseCreateExpectation       *test.DatabaseCreateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedMessageKind             *resource.MessageKind
		expectedError                   error
	}{
		"when successful": {
			messageKind: &resource.MessageKind{
				Code:        "new_message_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Disabled:    false,
				Deleted:     false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedResource: &resource.MessageKind{
					ID:          uuid.Nil,
					Code:        "new_message_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedResource:     test.ArbitraryMessageKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "messageKind_00000000-0000-0000-0003-000000000000",
				ExpectedResource: test.ArbitraryMessageKind(t),
				ReturnedResult:   true,
			},
			expectedMessageKind: test.ArbitraryMessageKind(t),
			expectedError:       nil,
		},
		"when id and times are modified": {
			messageKind: &resource.MessageKind{
				ID:          uuid.MustParse("00000000-0000-0000-0003-ffffffffffff"),
				Code:        "new_message_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Disabled:    false,
				Deleted:     false,
				CreateTime:  test.MakeTime("2000-01-01T03:59:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T03:59:59Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedResource: &resource.MessageKind{
					ID:          uuid.Nil,
					Code:        "new_message_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedResource:     test.ArbitraryMessageKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "messageKind_00000000-0000-0000-0003-000000000000",
				ExpectedResource: test.ArbitraryMessageKind(t),
				ReturnedResult:   true,
			},
			expectedMessageKind: test.ArbitraryMessageKind(t),
			expectedError:       nil,
		},
		"when message kind failed to be created": {
			messageKind: &resource.MessageKind{
				Code:        "new_message_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Disabled:    false,
				Deleted:     false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedResource: &resource.MessageKind{
					ID:          uuid.Nil,
					Code:        "new_message_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedMessageKind: nil,
			expectedError:       fmt.Errorf("error creating message kind: arbitrary"),
		},
		"when message kind failed to be retrieved": {
			messageKind: &resource.MessageKind{
				Code:        "new_message_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Disabled:    false,
				Deleted:     false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedResource: &resource.MessageKind{
					ID:          uuid.Nil,
					Code:        "new_message_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedMessageKind: nil,
			expectedError:       fmt.Errorf("error retrieving message kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectCreate(tt.databaseCreateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewMessageKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			messageKind, err := a.Create(tt.messageKind)

			assert.Equal(t, tt.expectedMessageKind, messageKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestMessageKindActionUpdate(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		messageKind                     *resource.MessageKind
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedMessageKind             *resource.MessageKind
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			messageKind: &resource.MessageKind{
				ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Code:        "updated_message_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Disabled:    true,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Disabled:    true,
					Deleted:     true,
					CreateTime:  time.Time{},
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedResource:     test.ArbitraryMessageKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "messageKind_00000000-0000-0000-0003-000000000000",
				ExpectedResource: test.ArbitraryMessageKind(t),
				ReturnedResult:   true,
			},
			expectedMessageKind: test.ArbitraryMessageKind(t),
			expectedError:       nil,
		},
		"when id and times are modified": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			messageKind: &resource.MessageKind{
				ID:          uuid.MustParse("00000000-0000-0000-0003-ffffffffffff"),
				Code:        "updated_message_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Disabled:    true,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T03:59:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T03:59:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Disabled:    true,
					Deleted:     true,
					CreateTime:  time.Time{},
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedResource:     test.ArbitraryMessageKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "messageKind_00000000-0000-0000-0003-000000000000",
				ExpectedResource: test.ArbitraryMessageKind(t),
				ReturnedResult:   true,
			},
			expectedMessageKind: test.ArbitraryMessageKind(t),
			expectedError:       nil,
		},
		"when message kind failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			messageKind: &resource.MessageKind{
				ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Code:        "updated_message_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Disabled:    true,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Disabled:    true,
					Deleted:     true,
					CreateTime:  time.Time{},
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: database.ErrInvalidPrimaryKey,
			},
			expectedMessageKind: nil,
			expectedError:       action.ErrInvalidResourceID,
		},
		"when message kind failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			messageKind: &resource.MessageKind{
				ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Code:        "updated_message_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Disabled:    true,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Disabled:    true,
					Deleted:     true,
					CreateTime:  time.Time{},
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedMessageKind: nil,
			expectedError:       fmt.Errorf("error updating message kind: arbitrary"),
		},
		"when message kind failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			messageKind: &resource.MessageKind{
				ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Code:        "updated_message_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Disabled:    true,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Disabled:    true,
					Deleted:     true,
					CreateTime:  time.Time{},
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedMessageKind: nil,
			expectedError:       fmt.Errorf("error retrieving message kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectUpdate(tt.databaseUpdateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewMessageKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			messageKind, err := a.Update(tt.id, tt.messageKind)

			assert.Equal(t, tt.expectedMessageKind, messageKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestMessageKindActionDelete(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		cacheDelExpectation             *test.CacheDelExpectation
		expectedMessageKind             *resource.MessageKind
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedResource:     test.ArbitraryMessageKind(t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "message_kind_000000000000",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Disabled:    false,
					Deleted:     true,
					CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
				},
			},
			cacheDelExpectation: &test.CacheDelExpectation{
				ExpectedKey:    "messageKind_00000000-0000-0000-0003-000000000000",
				ReturnedResult: true,
			},
			expectedMessageKind: &resource.MessageKind{
				ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Code:        "message_kind_000000000000",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Disabled:    false,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
			},
			expectedError: nil,
		},
		"when message kind failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedMessageKind: nil,
			expectedError:       action.ErrInvalidResourceID,
		},
		"when message kind failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedMessageKind: nil,
			expectedError:       fmt.Errorf("error retrieving message kind: arbitrary"),
		},
		"when message kind failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource:     &resource.MessageKind{},
				ReturnedResource:     test.ArbitraryMessageKind(t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessageKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedResource: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "message_kind_000000000000",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Disabled:    false,
					Deleted:     true,
					CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedMessageKind: nil,
			expectedError:       fmt.Errorf("error updating message kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectDel(tt.cacheDelExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)
			db.ExpectUpdate(tt.databaseUpdateExpectation)

			a := action.NewMessageKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			messageKind, err := a.Delete(tt.id)

			assert.Equal(t, tt.expectedMessageKind, messageKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}
