package action

import (
	"fmt"
	"time"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/cache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type MessageKindActioner interface {
	RetrieveMany(*query.Query) ([]*resource.MessageKind, error)
	RetrieveByID(uuid.UUID) (*resource.MessageKind, error)
	RetrieveByIDOrCode(string) (*resource.MessageKind, error)
	Create(*resource.MessageKind) (*resource.MessageKind, error)
	Update(uuid.UUID, *resource.MessageKind) (*resource.MessageKind, error)
	Delete(uuid.UUID) (*resource.MessageKind, error)
}

type MessageKindAction struct {
	cache cache.Cacher
	db    database.ReadWriter
	clk   clock.Clocker
}

// NewMessageKindAction creates a new message kind action.
func NewMessageKindAction(c cache.Cacher, db database.ReadWriter, clk clock.Clocker) *MessageKindAction {
	return &MessageKindAction{
		cache: c,
		db:    db,
		clk:   clk,
	}
}

func (a *MessageKindAction) RetrieveMany(q *query.Query) ([]*resource.MessageKind, error) {
	messageKinds := make([]*resource.MessageKind, 0)
	if err := a.db.RetrieveMany(resource.TypeMessageKind, q, &messageKinds); err != nil {
		return nil, fmt.Errorf("error retrieving message kinds: %s", err)
	}

	return messageKinds, nil
}

func (a *MessageKindAction) RetrieveByID(id uuid.UUID) (*resource.MessageKind, error) {
	messageKind := new(resource.MessageKind)

	if ok := a.cache.Get(cachedResourceKey(resource.TypeMessageKind, id), messageKind); ok {
		return messageKind, nil
	}

	err := a.db.RetrieveByID(resource.TypeMessageKind, id, messageKind)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving message kind: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeMessageKind, messageKind.ID), messageKind)

	return messageKind, nil
}

func (a *MessageKindAction) RetrieveByIDOrCode(idOrCode string) (*resource.MessageKind, error) {
	messageKind := new(resource.MessageKind)
	err := a.db.RetrieveByIDOrCode(resource.TypeMessageKind, idOrCode, messageKind)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceIDOrCode
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving message kind: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeMessageKind, messageKind.ID), messageKind)

	return messageKind, nil
}

func (a *MessageKindAction) Create(messageKind *resource.MessageKind) (*resource.MessageKind, error) {
	messageKind.ID = uuid.Nil
	messageKind.CreateTime = a.clk.Now()
	messageKind.UpdateTime = a.clk.Now()

	messageKindID, err := a.db.Create(resource.TypeMessageKind, messageKind)
	if err != nil {
		return nil, fmt.Errorf("error creating message kind: %s", err)
	}

	messageKind = new(resource.MessageKind)
	if err := a.db.RetrieveByID(resource.TypeMessageKind, messageKindID, messageKind); err != nil {
		return nil, fmt.Errorf("error retrieving message kind: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeMessageKind, messageKind.ID), messageKind)

	return messageKind, nil
}

func (a *MessageKindAction) Update(id uuid.UUID, messageKind *resource.MessageKind) (*resource.MessageKind, error) {
	messageKind.ID = id
	messageKind.CreateTime = time.Time{}
	messageKind.UpdateTime = a.clk.Now()

	err := a.db.Update(resource.TypeMessageKind, id, messageKind)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error updating message kind: %s", err)
	}

	messageKind = new(resource.MessageKind)
	if err := a.db.RetrieveByID(resource.TypeMessageKind, id, messageKind); err != nil {
		return nil, fmt.Errorf("error retrieving message kind: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeMessageKind, messageKind.ID), messageKind)

	return messageKind, nil
}

func (a *MessageKindAction) Delete(id uuid.UUID) (*resource.MessageKind, error) {
	messageKind := new(resource.MessageKind)
	err := a.db.RetrieveByID(resource.TypeMessageKind, id, messageKind)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving message kind: %s", err)
	}

	messageKind.Deleted = true

	if err := a.db.Update(resource.TypeMessageKind, id, messageKind); err != nil {
		return nil, fmt.Errorf("error updating message kind: %s", err)
	}

	a.cache.Del(cachedResourceKey(resource.TypeMessageKind, messageKind.ID))

	return messageKind, nil
}

var _ = MessageKindActioner(new(MessageKindAction))
