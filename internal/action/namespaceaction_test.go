//go:build !integration && !endtoend
// +build !integration,!endtoend

package action_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestNamespaceActionRetrieveMany(t *testing.T) {
	tests := map[string]struct {
		query                           *query.Query
		databaseRetrieveManyExpectation *test.DatabaseRetrieveManyExpectation
		expectedNamespaces              []*resource.Namespace
		expectedError                   error
	}{
		"when successful": {
			query: query.NewQuery(
				query.WithFilterArgs("Code", query.OpEqual, "namespace_000000000001"),
				query.WithOrderArgs("Code", query.DirDescending),
				query.WithPageArgs(8, 5),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Code", query.OpEqual, "namespace_000000000001"),
					query.WithOrderArgs("Code", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ExpectedResources: &[]*resource.Namespace{},
				ReturnedResources: []*resource.Namespace{
					test.ArbitraryNamespace(t),
				},
			},
			expectedNamespaces: []*resource.Namespace{
				test.ArbitraryNamespace(t),
			},
			expectedError: nil,
		},
		"when query not specified": {
			query: nil,
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedQuery:        nil,
				ExpectedResources:    &[]*resource.Namespace{},
				ReturnedResources: []*resource.Namespace{
					test.ArbitraryNamespace(t),
				},
			},
			expectedNamespaces: []*resource.Namespace{
				test.ArbitraryNamespace(t),
			},
			expectedError: nil,
		},
		"when namespaces failed to be retrieved": {
			query: query.NewQuery(
				query.WithFilterArgs("Code", query.OpEqual, "namespace_000000000001"),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Code", query.OpEqual, "namespace_000000000001"),
				),
				ExpectedResources: &[]*resource.Namespace{},
				ReturnedError:     test.ErrArbitrary,
			},
			expectedNamespaces: nil,
			expectedError:      fmt.Errorf("error retrieving namespaces: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			db := test.NewDatabase(t)
			db.ExpectRetrieveMany(tt.databaseRetrieveManyExpectation)

			a := action.NewNamespaceAction(nil, db, test.NewClock("2000-01-01T00:00:00Z"))
			namespaces, err := a.RetrieveMany(tt.query)

			assert.Equal(t, tt.expectedNamespaces, namespaces)
			assert.Equal(t, tt.expectedError, err)

			db.AssertExpectations(t)
		})
	}
}

func TestNamespaceActionRetrieveByID(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		cacheGetExpectation             *test.CacheGetExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedNamespace               *resource.Namespace
		expectedError                   error
	}{
		"when successful from cache": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: &resource.Namespace{},
				ReturnedResource: test.ArbitraryNamespace(t),
				ReturnedResult:   true,
			},
			expectedNamespace: test.ArbitraryNamespace(t),
			expectedError:     nil,
		},
		"when successful from database": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: &resource.Namespace{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedResource:     test.ArbitraryNamespace(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: test.ArbitraryNamespace(t),
				ReturnedResult:   true,
			},
			expectedNamespace: test.ArbitraryNamespace(t),
			expectedError:     nil,
		},
		"when namespace failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: &resource.Namespace{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedNamespace: nil,
			expectedError:     action.ErrInvalidResourceID,
		},
		"when namespace failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: &resource.Namespace{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedNamespace: nil,
			expectedError:     fmt.Errorf("error retrieving namespace: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectGet(tt.cacheGetExpectation)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewNamespaceAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			namespace, err := a.RetrieveByID(tt.id)

			assert.Equal(t, tt.expectedNamespace, namespace)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestNamespaceActionRetrieveByIDOrCode(t *testing.T) {
	tests := map[string]struct {
		idOrCode                              string
		databaseRetrieveByIDOrCodeExpectation *test.DatabaseRetrieveByIDOrCodeExpectation
		cacheSetExpectation                   *test.CacheSetExpectation
		expectedNamespace                     *resource.Namespace
		expectedError                         error
	}{
		"when id specified as uuid": {
			idOrCode: "00000000-0000-0000-0001-000000000001",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedIDOrCode:     "00000000-0000-0000-0001-000000000001",
				ExpectedResource:     &resource.Namespace{},
				ReturnedResource:     test.ArbitraryNamespace(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: test.ArbitraryNamespace(t),
				ReturnedResult:   true,
			},
			expectedNamespace: test.ArbitraryNamespace(t),
			expectedError:     nil,
		},
		"when id specified as code": {
			idOrCode: "code",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedIDOrCode:     "code",
				ExpectedResource:     &resource.Namespace{},
				ReturnedResource:     test.ArbitraryNamespace(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: test.ArbitraryNamespace(t),
				ReturnedResult:   true,
			},
			expectedNamespace: test.ArbitraryNamespace(t),
			expectedError:     nil,
		},
		"when namespace failed to be found": {
			idOrCode: "00000000-0000-0000-0001-000000000001",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedIDOrCode:     "00000000-0000-0000-0001-000000000001",
				ExpectedResource:     &resource.Namespace{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedNamespace: nil,
			expectedError:     action.ErrInvalidResourceIDOrCode,
		},
		"when namespace failed to be retrieved": {
			idOrCode: "00000000-0000-0000-0001-000000000001",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedIDOrCode:     "00000000-0000-0000-0001-000000000001",
				ExpectedResource:     &resource.Namespace{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedNamespace: nil,
			expectedError:     fmt.Errorf("error retrieving namespace: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByIDOrCode(tt.databaseRetrieveByIDOrCodeExpectation)

			a := action.NewNamespaceAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			namespace, err := a.RetrieveByIDOrCode(tt.idOrCode)

			assert.Equal(t, tt.expectedNamespace, namespace)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestNamespaceActionRetrieveParents(t *testing.T) {
	tests := map[string]struct {
		id                               uuid.UUID
		cacheGetExpectations             []*test.CacheGetExpectation
		databaseRetrieveByIDExpectations []*test.DatabaseRetrieveByIDExpectation
		expectedNamespaces               []*resource.Namespace
		expectedError                    error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectations: []*test.CacheGetExpectation{
				{
					ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
					ExpectedResource: &resource.Namespace{},
					ReturnedResource: &resource.Namespace{
						ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
						ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
						Deleted:  false,
					},
					ReturnedResult: true,
				},
				{
					ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000000",
					ExpectedResource: &resource.Namespace{},
					ReturnedResource: &resource.Namespace{
						ID:       uuid.MustParse("00000000-0000-0000-0001-000000000000"),
						ParentID: nil,
						Deleted:  false,
					},
					ReturnedResult: true,
				},
			},
			expectedNamespaces: []*resource.Namespace{
				{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:  false,
				},
				{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000000"),
					ParentID: nil,
					Deleted:  false,
				},
			},
			expectedError: nil,
		},
		"when namespace failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectations: []*test.CacheGetExpectation{
				{
					ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
					ExpectedResource: &resource.Namespace{},
					ReturnedResult:   false,
				},
			},
			databaseRetrieveByIDExpectations: []*test.DatabaseRetrieveByIDExpectation{
				{
					ExpectedResourceType: resource.TypeNamespace,
					ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ExpectedResource:     &resource.Namespace{},
					ReturnedError:        test.ErrArbitrary,
				},
			},
			expectedNamespaces: nil,
			expectedError:      fmt.Errorf("error retrieving namespace: arbitrary"),
		},
		"when namespace is deleted": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectations: []*test.CacheGetExpectation{
				{
					ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
					ExpectedResource: &resource.Namespace{},
					ReturnedResource: &resource.Namespace{
						ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
						ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
						Deleted:  true,
					},
					ReturnedResult: true,
				},
			},
			expectedNamespaces: nil,
			expectedError:      action.ErrNamespaceDeleted,
		},
		"when a parent namespace is deleted": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectations: []*test.CacheGetExpectation{
				{
					ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
					ExpectedResource: &resource.Namespace{},
					ReturnedResource: &resource.Namespace{
						ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
						ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
						Deleted:  false,
					},
					ReturnedResult: true,
				},
				{
					ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000000",
					ExpectedResource: &resource.Namespace{},
					ReturnedResource: &resource.Namespace{
						ID:       uuid.MustParse("00000000-0000-0000-0001-000000000000"),
						ParentID: nil,
						Deleted:  true,
					},
					ReturnedResult: true,
				},
			},
			expectedNamespaces: nil,
			expectedError:      action.ErrNamespaceDeleted,
		},
		"when an infinite loop is detected": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectations: []*test.CacheGetExpectation{
				{
					ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
					ExpectedResource: &resource.Namespace{},
					ReturnedResource: &resource.Namespace{
						ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
						ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
						Deleted:  false,
					},
					ReturnedResult: true,
				},
				{
					ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000000",
					ExpectedResource: &resource.Namespace{},
					ReturnedResource: &resource.Namespace{
						ID:       uuid.MustParse("00000000-0000-0000-0001-000000000000"),
						ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
						Deleted:  false,
					},
					ReturnedResult: true,
				},
			},
			expectedNamespaces: nil,
			expectedError:      action.ErrInfiniteLoopDetected,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			for _, e := range tt.cacheGetExpectations {
				c.ExpectGet(e)
			}

			db := test.NewDatabase(t)
			for _, e := range tt.databaseRetrieveByIDExpectations {
				db.ExpectRetrieveByID(e)
			}

			a := action.NewNamespaceAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			namespaces, err := a.RetrieveParents(tt.id)

			assert.Equal(t, tt.expectedNamespaces, namespaces)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestNamespaceActionRetrieveChildren(t *testing.T) {
	tests := map[string]struct {
		id                               uuid.UUID
		cacheGetExpectation              *test.CacheGetExpectation
		databaseRetrieveByIDExpectation  *test.DatabaseRetrieveByIDExpectation
		databaseRetrieveManyExpectations []*test.DatabaseRetrieveManyExpectation
		expectedNamespaces               []*resource.Namespace
		expectedError                    error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: &resource.Namespace{},
				ReturnedResource: &resource.Namespace{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:  false,
				},
				ReturnedResult: true,
			},
			databaseRetrieveManyExpectations: []*test.DatabaseRetrieveManyExpectation{
				{
					ExpectedResourceType: resource.TypeNamespace,
					ExpectedQuery: query.NewQuery(
						query.WithFilterArgs("ParentID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0001-000000000001")),
					),
					ExpectedResources: &[]*resource.Namespace{},
					ReturnedResources: []*resource.Namespace{
						{
							ID:       uuid.MustParse("00000000-0000-0000-0001-000000000002"),
							ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
							Deleted:  false,
						},
						{
							ID:       uuid.MustParse("00000000-0000-0000-0001-000000000004"),
							ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
							Deleted:  false,
						},
					},
				},
				{
					ExpectedResourceType: resource.TypeNamespace,
					ExpectedQuery: query.NewQuery(
						query.WithFilterArgs("ParentID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0001-000000000002")),
					),
					ExpectedResources: &[]*resource.Namespace{},
					ReturnedResources: []*resource.Namespace{
						{
							ID:       uuid.MustParse("00000000-0000-0000-0001-000000000003"),
							ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000002", t),
							Deleted:  false,
						},
					},
				},
				{
					ExpectedResourceType: resource.TypeNamespace,
					ExpectedQuery: query.NewQuery(
						query.WithFilterArgs("ParentID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0001-000000000004")),
					),
					ExpectedResources: &[]*resource.Namespace{},
					ReturnedResources: []*resource.Namespace{},
				},
				{
					ExpectedResourceType: resource.TypeNamespace,
					ExpectedQuery: query.NewQuery(
						query.WithFilterArgs("ParentID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0001-000000000003")),
					),
					ExpectedResources: &[]*resource.Namespace{},
					ReturnedResources: []*resource.Namespace{},
				},
			},
			expectedNamespaces: []*resource.Namespace{
				{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:  false,
				},
				{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
					Deleted:  false,
				},
				{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000004"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
					Deleted:  false,
				},
				{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000003"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000002", t),
					Deleted:  false,
				},
			},
			expectedError: nil,
		},
		"when namespace failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: &resource.Namespace{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedNamespaces: nil,
			expectedError:      fmt.Errorf("error retrieving namespace: arbitrary"),
		},
		"when namespace is deleted": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: &resource.Namespace{},
				ReturnedResource: &resource.Namespace{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:  true,
				},
				ReturnedResult: true,
			},
			expectedNamespaces: nil,
			expectedError:      action.ErrNamespaceDeleted,
		},
		"when children namespaces failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: &resource.Namespace{},
				ReturnedResource: &resource.Namespace{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:  false,
				},
				ReturnedResult: true,
			},
			databaseRetrieveManyExpectations: []*test.DatabaseRetrieveManyExpectation{
				{
					ExpectedResourceType: resource.TypeNamespace,
					ExpectedQuery: query.NewQuery(
						query.WithFilterArgs("ParentID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0001-000000000001")),
					),
					ExpectedResources: &[]*resource.Namespace{},
					ReturnedError:     test.ErrArbitrary,
				},
			},
			expectedNamespaces: nil,
			expectedError:      fmt.Errorf("error retrieving namespaces: arbitrary"),
		},
		"when a child namespace is deleted": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: &resource.Namespace{},
				ReturnedResource: &resource.Namespace{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:  false,
				},
				ReturnedResult: true,
			},
			databaseRetrieveManyExpectations: []*test.DatabaseRetrieveManyExpectation{
				{
					ExpectedResourceType: resource.TypeNamespace,
					ExpectedQuery: query.NewQuery(
						query.WithFilterArgs("ParentID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0001-000000000001")),
					),
					ExpectedResources: &[]*resource.Namespace{},
					ReturnedResources: []*resource.Namespace{
						{
							ID:       uuid.MustParse("00000000-0000-0000-0001-000000000002"),
							ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
							Deleted:  true,
						},
						{
							ID:       uuid.MustParse("00000000-0000-0000-0001-000000000004"),
							ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
							Deleted:  false,
						},
					},
				},
				{
					ExpectedResourceType: resource.TypeNamespace,
					ExpectedQuery: query.NewQuery(
						query.WithFilterArgs("ParentID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0001-000000000004")),
					),
					ExpectedResources: &[]*resource.Namespace{},
					ReturnedResources: []*resource.Namespace{},
				},
			},
			expectedNamespaces: []*resource.Namespace{
				{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:  false,
				},
				{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000004"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
					Deleted:  false,
				},
			},
			expectedError: nil,
		},
		"when an infinite loop is detected": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: &resource.Namespace{},
				ReturnedResource: &resource.Namespace{
					ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:  false,
				},
				ReturnedResult: true,
			},
			databaseRetrieveManyExpectations: []*test.DatabaseRetrieveManyExpectation{
				{
					ExpectedResourceType: resource.TypeNamespace,
					ExpectedQuery: query.NewQuery(
						query.WithFilterArgs("ParentID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0001-000000000001")),
					),
					ExpectedResources: &[]*resource.Namespace{},
					ReturnedResources: []*resource.Namespace{
						{
							ID:       uuid.MustParse("00000000-0000-0000-0001-000000000002"),
							ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
							Deleted:  false,
						},
					},
				},
				{
					ExpectedResourceType: resource.TypeNamespace,
					ExpectedQuery: query.NewQuery(
						query.WithFilterArgs("ParentID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0001-000000000002")),
					),
					ExpectedResources: &[]*resource.Namespace{},
					ReturnedResources: []*resource.Namespace{
						{
							ID:       uuid.MustParse("00000000-0000-0000-0001-000000000001"),
							ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000002", t),
							Deleted:  false,
						},
					},
				},
			},
			expectedNamespaces: nil,
			expectedError:      action.ErrInfiniteLoopDetected,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectGet(tt.cacheGetExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)
			for _, e := range tt.databaseRetrieveManyExpectations {
				db.ExpectRetrieveMany(e)
			}

			a := action.NewNamespaceAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			namespaces, err := a.RetrieveChildren(tt.id)

			assert.Equal(t, tt.expectedNamespaces, namespaces)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestNamespaceActionCreate(t *testing.T) {
	tests := map[string]struct {
		namespace                       *resource.Namespace
		databaseCreateExpectation       *test.DatabaseCreateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedNamespace               *resource.Namespace
		expectedError                   error
	}{
		"when successful": {
			namespace: &resource.Namespace{
				Code:     "new_namespace",
				ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:  false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedResource: &resource.Namespace{
					ID:         uuid.Nil,
					Code:       "new_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    false,
					CreateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedResource:     test.ArbitraryNamespace(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: test.ArbitraryNamespace(t),
				ReturnedResult:   true,
			},
			expectedNamespace: test.ArbitraryNamespace(t),
			expectedError:     nil,
		},
		"when id and times are modified": {
			namespace: &resource.Namespace{
				ID:         uuid.MustParse("00000000-0000-0000-0001-ffffffffffff"),
				Code:       "new_namespace",
				ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:    false,
				CreateTime: test.MakeTime("2000-01-01T01:59:58Z", t),
				UpdateTime: test.MakeTime("2000-01-01T01:59:59Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedResource: &resource.Namespace{
					ID:         uuid.Nil,
					Code:       "new_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    false,
					CreateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedResource:     test.ArbitraryNamespace(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: test.ArbitraryNamespace(t),
				ReturnedResult:   true,
			},
			expectedNamespace: test.ArbitraryNamespace(t),
			expectedError:     nil,
		},
		"when namespace failed to be created": {
			namespace: &resource.Namespace{
				Code:     "new_namespace",
				ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:  false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedResource: &resource.Namespace{
					ID:         uuid.Nil,
					Code:       "new_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    false,
					CreateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedNamespace: nil,
			expectedError:     fmt.Errorf("error creating namespace: arbitrary"),
		},
		"when namespace failed to be retrieved": {
			namespace: &resource.Namespace{
				Code:     "new_namespace",
				ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:  false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedResource: &resource.Namespace{
					ID:         uuid.Nil,
					Code:       "new_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    false,
					CreateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedNamespace: nil,
			expectedError:     fmt.Errorf("error retrieving namespace: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectCreate(tt.databaseCreateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewNamespaceAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			namespace, err := a.Create(tt.namespace)

			assert.Equal(t, tt.expectedNamespace, namespace)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestNamespaceActionUpdate(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		namespace                       *resource.Namespace
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedNamespace               *resource.Namespace
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			namespace: &resource.Namespace{
				ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Code:       "updated_namespace",
				ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:    true,
				CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
				UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    true,
					CreateTime: time.Time{},
					UpdateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedResource:     test.ArbitraryNamespace(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: test.ArbitraryNamespace(t),
				ReturnedResult:   true,
			},
			expectedNamespace: test.ArbitraryNamespace(t),
			expectedError:     nil,
		},
		"when id and times are modified": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			namespace: &resource.Namespace{
				ID:         uuid.MustParse("00000000-0000-0000-0001-ffffffffffff"),
				Code:       "updated_namespace",
				ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:    true,
				CreateTime: test.MakeTime("2000-01-01T01:59:58Z", t),
				UpdateTime: test.MakeTime("2000-01-01T01:59:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    true,
					CreateTime: time.Time{},
					UpdateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedResource:     test.ArbitraryNamespace(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "namespace_00000000-0000-0000-0001-000000000001",
				ExpectedResource: test.ArbitraryNamespace(t),
				ReturnedResult:   true,
			},
			expectedNamespace: test.ArbitraryNamespace(t),
			expectedError:     nil,
		},
		"when namespace failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			namespace: &resource.Namespace{
				ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Code:       "updated_namespace",
				ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:    true,
				CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
				UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    true,
					CreateTime: time.Time{},
					UpdateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: database.ErrInvalidPrimaryKey,
			},
			expectedNamespace: nil,
			expectedError:     action.ErrInvalidResourceID,
		},
		"when namespace failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			namespace: &resource.Namespace{
				ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Code:       "updated_namespace",
				ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:    true,
				CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
				UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    true,
					CreateTime: time.Time{},
					UpdateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedNamespace: nil,
			expectedError:     fmt.Errorf("error updating namespace: arbitrary"),
		},
		"when namespace failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			namespace: &resource.Namespace{
				ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Code:       "updated_namespace",
				ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:    true,
				CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
				UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    true,
					CreateTime: time.Time{},
					UpdateTime: test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedNamespace: nil,
			expectedError:     fmt.Errorf("error retrieving namespace: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectUpdate(tt.databaseUpdateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewNamespaceAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			namespace, err := a.Update(tt.id, tt.namespace)

			assert.Equal(t, tt.expectedNamespace, namespace)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestNamespaceActionDelete(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		cacheDelExpectation             *test.CacheDelExpectation
		expectedNamespace               *resource.Namespace
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedResource:     test.ArbitraryNamespace(t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "namespace_000000000001",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    true,
					CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
				},
			},
			cacheDelExpectation: &test.CacheDelExpectation{
				ExpectedKey:    "namespace_00000000-0000-0000-0001-000000000001",
				ReturnedResult: true,
			},
			expectedNamespace: &resource.Namespace{
				ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Code:       "namespace_000000000001",
				ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:    true,
				CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
				UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
			},
			expectedError: nil,
		},
		"when namespace failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedNamespace: nil,
			expectedError:     action.ErrInvalidResourceID,
		},
		"when namespace failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedNamespace: nil,
			expectedError:     fmt.Errorf("error retrieving namespace: arbitrary"),
		},
		"when namespace failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource:     &resource.Namespace{},
				ReturnedResource:     test.ArbitraryNamespace(t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeNamespace,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedResource: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "namespace_000000000001",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    true,
					CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedNamespace: nil,
			expectedError:     fmt.Errorf("error updating namespace: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectDel(tt.cacheDelExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)
			db.ExpectUpdate(tt.databaseUpdateExpectation)

			a := action.NewNamespaceAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			namespace, err := a.Delete(tt.id)

			assert.Equal(t, tt.expectedNamespace, namespace)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}
