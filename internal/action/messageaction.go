package action

import (
	"fmt"
	"time"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/cache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type MessageActioner interface {
	RetrieveMany(*query.Query) ([]*resource.Message, error)
	RetrieveByID(uuid.UUID) (*resource.Message, error)
	RetrievePotentiallyFailed(time.Duration) ([]*resource.Message, error)
	Create(*resource.Message) (*resource.Message, error)
	Update(uuid.UUID, *resource.Message) (*resource.Message, error)
}

type MessageAction struct {
	cache cache.Cacher
	db    database.ReadWriter
	clk   clock.Clocker
}

// NewMessageAciton creates a new message action.
func NewMessageAction(c cache.Cacher, db database.ReadWriter, clk clock.Clocker) *MessageAction {
	return &MessageAction{
		cache: c,
		db:    db,
		clk:   clk,
	}
}

func (a *MessageAction) RetrieveMany(q *query.Query) ([]*resource.Message, error) {
	messages := make([]*resource.Message, 0)
	if err := a.db.RetrieveMany(resource.TypeMessage, q, &messages); err != nil {
		return nil, fmt.Errorf("error retrieving messages: %s", err)
	}

	return messages, nil
}

func (a *MessageAction) RetrieveByID(id uuid.UUID) (*resource.Message, error) {
	message := new(resource.Message)

	if ok := a.cache.Get(cachedResourceKey(resource.TypeMessage, id), message); ok {
		return message, nil
	}

	err := a.db.RetrieveByID(resource.TypeMessage, id, message)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving message: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeMessage, message.ID), message)

	return message, nil
}

// Failed messages are retried on an exponential backoff schedule, waiting 4^i seconds with each
// retry. Messages that have been tried six times will be considered as permanently failed.
//
// The following table lists the wait time of each retry:
//
//      deliveries |    wait time    | cumul. wait time
//     -------------------------------------------------
//               0 |    1s,       1s |               1s
//               1 |    4s,       4s |               5s
//               2 |   16s,      16s |              21s
//               3 |   64s,    1m04s |             1m25
//               4 |  256s,    4m16s |             5m41
//               5 | 1024s,   17m04s |            22m45
//               6 | 4096s, 1h08m16s |          1h31m01
//
// Notes:
// * It should not happen for a failed message to have no deliveries. In this case, the message will
//   still be added to the list of messages to deliver.
// * The cumulative wait time column is an approximation. The Scheduler service is run periodically,
//   and the exact wait time cannot be calculated beforehand.
//nolint:gocritic // Keeping this one for future reference.
func (a *MessageAction) RetrievePotentiallyFailed(since time.Duration) ([]*resource.Message, error) {
	// Retrieve all recent "active" messages that only have undelivered deliveries.

	// All recent active messages whose latest delivery is undelivered.

	// selector := r.sess.SQL().
	// 	Select("m.id", db.Raw("COUNT(d.message_id) AS dcount"), db.Raw("MAX(d.deliver_time) AS dtime")).
	// 	From("messages m").
	// 	LeftJoin("deliveries d ON m.id = d.message_id").
	// 	Where("m.status = 'active' AND m.create_time >= NOW() - INTERVAL '2h'").
	// 	GroupBy("m.id")

	// messages := make([]*MessageMetadata, 0)
	// if err := selector.All(&messages); err != nil {
	// 	return nil, err
	// }

	// MessageMetadata is a helper struct used to retrieve failed messages.
	// type MessageMetadata struct {
	// 	ID     uuid.UUID  `db:"id"`
	// 	DCount int        `db:"dcount"`
	// 	DTime  *time.Time `db:"dtime"`
	// }

	return []*resource.Message{}, nil
}

func (a *MessageAction) Create(message *resource.Message) (*resource.Message, error) {
	message.ID = uuid.Nil
	message.CreateTime = a.clk.Now()
	message.UpdateTime = a.clk.Now()

	messageID, err := a.db.Create(resource.TypeMessage, message)
	if err != nil {
		return nil, fmt.Errorf("error creating message: %s", err)
	}

	message = new(resource.Message)
	if err := a.db.RetrieveByID(resource.TypeMessage, messageID, message); err != nil {
		return nil, fmt.Errorf("error retrieving message: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeMessage, message.ID), message)

	return message, nil
}

func (a *MessageAction) Update(id uuid.UUID, message *resource.Message) (*resource.Message, error) {
	message.ID = id
	message.CreateTime = time.Time{}
	message.UpdateTime = a.clk.Now()

	err := a.db.Update(resource.TypeMessage, id, message)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error updating message: %s", err)
	}

	message = new(resource.Message)
	if err := a.db.RetrieveByID(resource.TypeMessage, id, message); err != nil {
		return nil, fmt.Errorf("error retrieving message: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeMessage, message.ID), message)

	return message, nil
}

var _ = MessageActioner(new(MessageAction))
