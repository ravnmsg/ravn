//go:build !integration && !endtoend
// +build !integration,!endtoend

package action_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestTemplateKindActionRetrieveMany(t *testing.T) {
	tests := map[string]struct {
		query                           *query.Query
		databaseRetrieveManyExpectation *test.DatabaseRetrieveManyExpectation
		expectedTemplateKinds           []*resource.TemplateKind
		expectedError                   error
	}{
		"when successful": {
			query: query.NewQuery(
				query.WithFilterArgs("DeliveryKindID", query.OpEqual, "00000000-0000-0000-0002-000000000000"),
				query.WithOrderArgs("TargetAttr", query.DirDescending),
				query.WithPageArgs(8, 5),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpEqual, "00000000-0000-0000-0002-000000000000"),
					query.WithOrderArgs("TargetAttr", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ExpectedResources: &[]*resource.TemplateKind{},
				ReturnedResources: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			expectedTemplateKinds: []*resource.TemplateKind{
				test.ArbitraryTemplateKind(t),
			},
			expectedError: nil,
		},
		"when query not specified": {
			query: nil,
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedQuery:        nil,
				ExpectedResources:    &[]*resource.TemplateKind{},
				ReturnedResources: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			expectedTemplateKinds: []*resource.TemplateKind{
				test.ArbitraryTemplateKind(t),
			},
			expectedError: nil,
		},
		"when template kinds failed to be retrieved": {
			query: query.NewQuery(
				query.WithFilterArgs("DeliveryKindID", query.OpEqual, "00000000-0000-0000-0002-000000000000"),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpEqual, "00000000-0000-0000-0002-000000000000"),
				),
				ExpectedResources: &[]*resource.TemplateKind{},
				ReturnedError:     test.ErrArbitrary,
			},
			expectedTemplateKinds: nil,
			expectedError:         fmt.Errorf("error retrieving template kinds: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			db := test.NewDatabase(t)
			db.ExpectRetrieveMany(tt.databaseRetrieveManyExpectation)

			a := action.NewTemplateKindAction(nil, db, test.NewClock("2000-01-01T00:00:00Z"))
			templateKinds, err := a.RetrieveMany(tt.query)

			assert.Equal(t, tt.expectedTemplateKinds, templateKinds)
			assert.Equal(t, tt.expectedError, err)

			db.AssertExpectations(t)
		})
	}
}

func TestTemplateKindActionRetrieveByID(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		cacheGetExpectation             *test.CacheGetExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedTemplateKind            *resource.TemplateKind
		expectedError                   error
	}{
		"when successful from cache": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "templateKind_00000000-0000-0000-0004-000000000000",
				ExpectedResource: &resource.TemplateKind{},
				ReturnedResource: test.ArbitraryTemplateKind(t),
				ReturnedResult:   true,
			},
			expectedTemplateKind: test.ArbitraryTemplateKind(t),
			expectedError:        nil,
		},
		"when successful from database": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "templateKind_00000000-0000-0000-0004-000000000000",
				ExpectedResource: &resource.TemplateKind{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedResource:     test.ArbitraryTemplateKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "templateKind_00000000-0000-0000-0004-000000000000",
				ExpectedResource: test.ArbitraryTemplateKind(t),
				ReturnedResult:   true,
			},
			expectedTemplateKind: test.ArbitraryTemplateKind(t),
			expectedError:        nil,
		},
		"when template kind failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "templateKind_00000000-0000-0000-0004-000000000000",
				ExpectedResource: &resource.TemplateKind{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedTemplateKind: nil,
			expectedError:        action.ErrInvalidResourceID,
		},
		"when template kind failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "templateKind_00000000-0000-0000-0004-000000000000",
				ExpectedResource: &resource.TemplateKind{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedTemplateKind: nil,
			expectedError:        fmt.Errorf("error retrieving template kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectGet(tt.cacheGetExpectation)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewTemplateKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			templateKind, err := a.RetrieveByID(tt.id)

			assert.Equal(t, tt.expectedTemplateKind, templateKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestTemplateKindActionCreate(t *testing.T) {
	tests := map[string]struct {
		templateKind                    *resource.TemplateKind
		databaseCreateExpectation       *test.DatabaseCreateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedTemplateKind            *resource.TemplateKind
		expectedError                   error
	}{
		"when successful": {
			templateKind: &resource.TemplateKind{
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				TargetAttr:     "new_target_attr",
				Deleted:        false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedResource: &resource.TemplateKind{
					ID:             uuid.Nil,
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					TargetAttr:     "new_target_attr",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedResource:     test.ArbitraryTemplateKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "templateKind_00000000-0000-0000-0004-000000000000",
				ExpectedResource: test.ArbitraryTemplateKind(t),
				ReturnedResult:   true,
			},
			expectedTemplateKind: test.ArbitraryTemplateKind(t),
			expectedError:        nil,
		},
		"when id and times are modified": {
			templateKind: &resource.TemplateKind{
				ID:             uuid.MustParse("00000000-0000-0000-0004-ffffffffffff"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				TargetAttr:     "new_target_attr",
				Deleted:        false,
				CreateTime:     test.MakeTime("2000-01-01T04:59:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T04:59:59Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedResource: &resource.TemplateKind{
					ID:             uuid.Nil,
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					TargetAttr:     "new_target_attr",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedResource:     test.ArbitraryTemplateKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "templateKind_00000000-0000-0000-0004-000000000000",
				ExpectedResource: test.ArbitraryTemplateKind(t),
				ReturnedResult:   true,
			},
			expectedTemplateKind: test.ArbitraryTemplateKind(t),
			expectedError:        nil,
		},
		"when template kind failed to be created": {
			templateKind: &resource.TemplateKind{
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				TargetAttr:     "new_target_attr",
				Deleted:        false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedResource: &resource.TemplateKind{
					ID:             uuid.Nil,
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					TargetAttr:     "new_target_attr",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedTemplateKind: nil,
			expectedError:        fmt.Errorf("error creating template kind: arbitrary"),
		},
		"when template kind failed to be retrieved": {
			templateKind: &resource.TemplateKind{
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				TargetAttr:     "new_target_attr",
				Deleted:        false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedResource: &resource.TemplateKind{
					ID:             uuid.Nil,
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					TargetAttr:     "new_target_attr",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedTemplateKind: nil,
			expectedError:        fmt.Errorf("error retrieving template kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectCreate(tt.databaseCreateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewTemplateKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			templateKind, err := a.Create(tt.templateKind)

			assert.Equal(t, tt.expectedTemplateKind, templateKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestTemplateKindActionUpdate(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		templateKind                    *resource.TemplateKind
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedTemplateKind            *resource.TemplateKind
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			templateKind: &resource.TemplateKind{
				ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				TargetAttr:     "updated_target_attr",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					TargetAttr:     "updated_target_attr",
					Deleted:        true,
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedResource:     test.ArbitraryTemplateKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "templateKind_00000000-0000-0000-0004-000000000000",
				ExpectedResource: test.ArbitraryTemplateKind(t),
				ReturnedResult:   true,
			},
			expectedTemplateKind: test.ArbitraryTemplateKind(t),
			expectedError:        nil,
		},
		"when id and times are modified": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			templateKind: &resource.TemplateKind{
				ID:             uuid.MustParse("00000000-0000-0000-0004-ffffffffffff"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				TargetAttr:     "updated_target_attr",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T04:59:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T04:59:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					TargetAttr:     "updated_target_attr",
					Deleted:        true,
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedResource:     test.ArbitraryTemplateKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "templateKind_00000000-0000-0000-0004-000000000000",
				ExpectedResource: test.ArbitraryTemplateKind(t),
				ReturnedResult:   true,
			},
			expectedTemplateKind: test.ArbitraryTemplateKind(t),
			expectedError:        nil,
		},
		"when template kind failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			templateKind: &resource.TemplateKind{
				ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				TargetAttr:     "updated_target_attr",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					TargetAttr:     "updated_target_attr",
					Deleted:        true,
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: database.ErrInvalidPrimaryKey,
			},
			expectedTemplateKind: nil,
			expectedError:        action.ErrInvalidResourceID,
		},
		"when template kind failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			templateKind: &resource.TemplateKind{
				ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				TargetAttr:     "updated_target_attr",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					TargetAttr:     "updated_target_attr",
					Deleted:        true,
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedTemplateKind: nil,
			expectedError:        fmt.Errorf("error updating template kind: arbitrary"),
		},
		"when template kind failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			templateKind: &resource.TemplateKind{
				ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				TargetAttr:     "updated_target_attr",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					TargetAttr:     "updated_target_attr",
					Deleted:        true,
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedTemplateKind: nil,
			expectedError:        fmt.Errorf("error retrieving template kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectUpdate(tt.databaseUpdateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewTemplateKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			templateKind, err := a.Update(tt.id, tt.templateKind)

			assert.Equal(t, tt.expectedTemplateKind, templateKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestTemplateKindActionDelete(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		cacheDelExpectation             *test.CacheDelExpectation
		expectedTemplateKind            *resource.TemplateKind
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedResource:     test.ArbitraryTemplateKind(t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					TargetAttr:     "target_attr_000000000000",
					Deleted:        true,
					CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
				},
			},
			cacheDelExpectation: &test.CacheDelExpectation{
				ExpectedKey:    "templateKind_00000000-0000-0000-0004-000000000000",
				ReturnedResult: true,
			},
			expectedTemplateKind: &resource.TemplateKind{
				ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				TargetAttr:     "target_attr_000000000000",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
			},
			expectedError: nil,
		},
		"when template kind failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedTemplateKind: nil,
			expectedError:        action.ErrInvalidResourceID,
		},
		"when template kind failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedTemplateKind: nil,
			expectedError:        fmt.Errorf("error retrieving template kind: arbitrary"),
		},
		"when template kind failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource:     &resource.TemplateKind{},
				ReturnedResource:     test.ArbitraryTemplateKind(t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplateKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedResource: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					TargetAttr:     "target_attr_000000000000",
					Deleted:        true,
					CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedTemplateKind: nil,
			expectedError:        fmt.Errorf("error updating template kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectDel(tt.cacheDelExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)
			db.ExpectUpdate(tt.databaseUpdateExpectation)

			a := action.NewTemplateKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			templateKind, err := a.Delete(tt.id)

			assert.Equal(t, tt.expectedTemplateKind, templateKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}
