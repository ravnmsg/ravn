package action

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/cache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/msgqueue"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type DeliveryActioner interface {
	RetrieveMany(*query.Query) ([]*resource.Delivery, error)
	RetrieveByID(uuid.UUID) (*resource.Delivery, error)
	RetrieveByMessageID(uuid.UUID) ([]*resource.Delivery, error)
	Create(*resource.Delivery) (*resource.Delivery, error)
	Update(uuid.UUID, *resource.Delivery) (*resource.Delivery, error)
	QueueRequest(uuid.UUID) error
}

type DeliveryAction struct {
	mq    msgqueue.Publisher
	cache cache.Cacher
	db    database.ReadWriter
	clk   clock.Clocker
}

// NewDeliveryAction creates a new delivery action.
func NewDeliveryAction(
	mq msgqueue.Publisher,
	c cache.Cacher,
	db database.ReadWriter,
	clk clock.Clocker,
) *DeliveryAction {
	return &DeliveryAction{
		mq:    mq,
		cache: c,
		db:    db,
		clk:   clk,
	}
}

func (a *DeliveryAction) RetrieveMany(q *query.Query) ([]*resource.Delivery, error) {
	deliveries := make([]*resource.Delivery, 0)
	if err := a.db.RetrieveMany(resource.TypeDelivery, q, &deliveries); err != nil {
		return nil, fmt.Errorf("error retrieving deliveries: %s", err)
	}

	return deliveries, nil
}

func (a *DeliveryAction) RetrieveByID(id uuid.UUID) (*resource.Delivery, error) {
	delivery := new(resource.Delivery)

	if ok := a.cache.Get(cachedResourceKey(resource.TypeDelivery, id), delivery); ok {
		return delivery, nil
	}

	err := a.db.RetrieveByID(resource.TypeDelivery, id, delivery)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving delivery: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeDelivery, delivery.ID), delivery)

	return delivery, nil
}

func (a *DeliveryAction) RetrieveByMessageID(messageID uuid.UUID) ([]*resource.Delivery, error) {
	q := query.NewQuery(query.WithFilterArgs("MessageID", query.OpEqual, messageID))

	deliveries := make([]*resource.Delivery, 0)
	if err := a.db.RetrieveMany(resource.TypeDelivery, q, &deliveries); err != nil {
		return nil, fmt.Errorf("error retrieving deliveries: %s", err)
	}

	return deliveries, nil
}

func (a *DeliveryAction) Create(delivery *resource.Delivery) (*resource.Delivery, error) {
	delivery.ID = uuid.Nil
	delivery.CreateTime = a.clk.Now()
	delivery.UpdateTime = a.clk.Now()

	deliveryID, err := a.db.Create(resource.TypeDelivery, delivery)
	if err != nil {
		return nil, fmt.Errorf("error creating delivery: %s", err)
	}

	delivery = new(resource.Delivery)
	if err := a.db.RetrieveByID(resource.TypeDelivery, deliveryID, delivery); err != nil {
		return nil, fmt.Errorf("error retrieving delivery: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeDelivery, delivery.ID), delivery)

	return delivery, nil
}

func (a *DeliveryAction) Update(id uuid.UUID, delivery *resource.Delivery) (*resource.Delivery, error) {
	delivery.ID = id
	delivery.CreateTime = time.Time{}
	delivery.UpdateTime = a.clk.Now()

	err := a.db.Update(resource.TypeDelivery, id, delivery)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error updating delivery: %s", err)
	}

	delivery = new(resource.Delivery)
	if err := a.db.RetrieveByID(resource.TypeDelivery, id, delivery); err != nil {
		return nil, fmt.Errorf("error retrieving delivery: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeDelivery, delivery.ID), delivery)

	return delivery, nil
}

func (a *DeliveryAction) QueueRequest(deliveryID uuid.UUID) error {
	msg := map[string]interface{}{
		"deliveryId": deliveryID.String(),
	}

	data, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	if err := a.mq.Publish(data); err != nil {
		return err
	}

	return nil
}

var _ = DeliveryActioner(new(DeliveryAction))
