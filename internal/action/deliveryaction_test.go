//go:build !integration && !endtoend
// +build !integration,!endtoend

package action_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestDeliveryActionRetrieveMany(t *testing.T) {
	tests := map[string]struct {
		query                           *query.Query
		databaseRetrieveManyExpectation *test.DatabaseRetrieveManyExpectation
		expectedDeliveries              []*resource.Delivery
		expectedError                   error
	}{
		"when successful": {
			query: query.NewQuery(
				query.WithFilterArgs("Status", query.OpEqual, "delivered"),
				query.WithOrderArgs("DeliveryKindID", query.DirDescending),
				query.WithPageArgs(8, 5),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Status", query.OpEqual, "delivered"),
					query.WithOrderArgs("DeliveryKindID", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ExpectedResources: &[]*resource.Delivery{},
				ReturnedResources: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			expectedDeliveries: []*resource.Delivery{
				test.ArbitraryDelivery(t),
			},
			expectedError: nil,
		},
		"when query not specified": {
			query: nil,
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedQuery:        nil,
				ExpectedResources:    &[]*resource.Delivery{},
				ReturnedResources: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			expectedDeliveries: []*resource.Delivery{
				test.ArbitraryDelivery(t),
			},
			expectedError: nil,
		},
		"when deliveries failed to be retrieved": {
			query: query.NewQuery(
				query.WithFilterArgs("Status", query.OpEqual, "delivered"),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Status", query.OpEqual, "delivered"),
				),
				ExpectedResources: &[]*resource.Delivery{},
				ReturnedError:     test.ErrArbitrary,
			},
			expectedDeliveries: nil,
			expectedError:      fmt.Errorf("error retrieving deliveries: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			db := test.NewDatabase(t)
			db.ExpectRetrieveMany(tt.databaseRetrieveManyExpectation)

			a := action.NewDeliveryAction(nil, nil, db, test.NewClock("2000-01-01T00:00:00Z"))
			deliveries, err := a.RetrieveMany(tt.query)

			assert.Equal(t, tt.expectedDeliveries, deliveries)
			assert.Equal(t, tt.expectedError, err)

			db.AssertExpectations(t)
		})
	}
}

func TestDeliveryActionRetrieveByID(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		cacheGetExpectation             *test.CacheGetExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedDelivery                *resource.Delivery
		expectedError                   error
	}{
		"when successful from cache": {
			id: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "delivery_00000000-0000-0000-0007-000000000000",
				ExpectedResource: &resource.Delivery{},
				ReturnedResource: test.ArbitraryDelivery(t),
				ReturnedResult:   true,
			},
			expectedDelivery: test.ArbitraryDelivery(t),
			expectedError:    nil,
		},
		"when successful from database": {
			id: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "delivery_00000000-0000-0000-0007-000000000000",
				ExpectedResource: &resource.Delivery{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ExpectedResource:     &resource.Delivery{},
				ReturnedResource:     test.ArbitraryDelivery(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "delivery_00000000-0000-0000-0007-000000000000",
				ExpectedResource: test.ArbitraryDelivery(t),
				ReturnedResult:   true,
			},
			expectedDelivery: test.ArbitraryDelivery(t),
			expectedError:    nil,
		},
		"when delivery failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "delivery_00000000-0000-0000-0007-000000000000",
				ExpectedResource: &resource.Delivery{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ExpectedResource:     &resource.Delivery{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedDelivery: nil,
			expectedError:    action.ErrInvalidResourceID,
		},
		"when delivery failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "delivery_00000000-0000-0000-0007-000000000000",
				ExpectedResource: &resource.Delivery{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ExpectedResource:     &resource.Delivery{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedDelivery: nil,
			expectedError:    fmt.Errorf("error retrieving delivery: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectGet(tt.cacheGetExpectation)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewDeliveryAction(nil, c, db, test.NewClock("2000-01-01T00:00:00Z"))
			delivery, err := a.RetrieveByID(tt.id)

			assert.Equal(t, tt.expectedDelivery, delivery)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestDeliveryActionRetrieveByMessageID(t *testing.T) {
	tests := map[string]struct {
		messageID                       uuid.UUID
		databaseRetrieveManyExpectation *test.DatabaseRetrieveManyExpectation
		expectedDeliveries              []*resource.Delivery
		expectedError                   error
	}{
		"when successful": {
			messageID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0006-000000000000")),
				),
				ExpectedResources: &[]*resource.Delivery{},
				ReturnedResources: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			expectedDeliveries: []*resource.Delivery{
				test.ArbitraryDelivery(t),
			},
			expectedError: nil,
		},
		"when deliveries failed to be found": {
			messageID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0006-000000000000")),
				),
				ExpectedResources: &[]*resource.Delivery{},
				ReturnedResources: []*resource.Delivery{},
			},
			expectedDeliveries: []*resource.Delivery{},
			expectedError:      nil,
		},
		"when deliveries failed to be retrieved": {
			messageID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0006-000000000000")),
				),
				ExpectedResources: &[]*resource.Delivery{},
				ReturnedError:     test.ErrArbitrary,
			},
			expectedDeliveries: nil,
			expectedError:      fmt.Errorf("error retrieving deliveries: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			db := test.NewDatabase(t)
			db.ExpectRetrieveMany(tt.databaseRetrieveManyExpectation)

			a := action.NewDeliveryAction(nil, nil, db, test.NewClock("2000-01-01T00:00:00Z"))
			deliveries, err := a.RetrieveByMessageID(tt.messageID)

			assert.Equal(t, tt.expectedDeliveries, deliveries)
			assert.Equal(t, tt.expectedError, err)

			db.AssertExpectations(t)
		})
	}
}

func TestDeliveryActionCreate(t *testing.T) {
	tests := map[string]struct {
		delivery                        *resource.Delivery
		databaseCreateExpectation       *test.DatabaseCreateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedDelivery                *resource.Delivery
		expectedError                   error
	}{
		"when successful": {
			delivery: &resource.Delivery{
				MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				LanguageTag:    "new_language_tag",
				Status:         "pending",
				StatusMessage:  "new_status_message",
				DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedResource: &resource.Delivery{
					ID:             uuid.Nil,
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
					StatusMessage:  "new_status_message",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Delivery{},
				ReturnedResource:     test.ArbitraryDelivery(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "delivery_00000000-0000-0000-0007-000000000000",
				ExpectedResource: test.ArbitraryDelivery(t),
				ReturnedResult:   true,
			},
			expectedDelivery: test.ArbitraryDelivery(t),
			expectedError:    nil,
		},
		"when id and times are modified": {
			delivery: &resource.Delivery{
				ID:             uuid.MustParse("00000000-0000-0000-0007-ffffffffffff"),
				MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				LanguageTag:    "new_language_tag",
				Status:         "pending",
				StatusMessage:  "new_status_message",
				DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
				CreateTime:     test.MakeTime("2000-01-01T07:59:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T07:59:59Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedResource: &resource.Delivery{
					ID:             uuid.Nil,
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
					StatusMessage:  "new_status_message",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Delivery{},
				ReturnedResource:     test.ArbitraryDelivery(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "delivery_00000000-0000-0000-0007-000000000000",
				ExpectedResource: test.ArbitraryDelivery(t),
				ReturnedResult:   true,
			},
			expectedDelivery: test.ArbitraryDelivery(t),
			expectedError:    nil,
		},
		"when delivery failed to be created": {
			delivery: &resource.Delivery{
				MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				LanguageTag:    "new_language_tag",
				Status:         "pending",
				StatusMessage:  "new_status_message",
				DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedResource: &resource.Delivery{
					ID:             uuid.Nil,
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
					StatusMessage:  "new_status_message",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedDelivery: nil,
			expectedError:    fmt.Errorf("error creating delivery: arbitrary"),
		},
		"when delivery failed to be retrieved": {
			delivery: &resource.Delivery{
				MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				LanguageTag:    "new_language_tag",
				Status:         "pending",
				StatusMessage:  "new_status_message",
				DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedResource: &resource.Delivery{
					ID:             uuid.Nil,
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
					StatusMessage:  "new_status_message",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Delivery{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedDelivery: nil,
			expectedError:    fmt.Errorf("error retrieving delivery: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectCreate(tt.databaseCreateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewDeliveryAction(nil, c, db, test.NewClock("2000-01-01T00:00:00Z"))
			delivery, err := a.Create(tt.delivery)

			assert.Equal(t, tt.expectedDelivery, delivery)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestDeliveryActionUpdate(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		delivery                        *resource.Delivery
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedDelivery                *resource.Delivery
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			delivery: &resource.Delivery{
				ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				LanguageTag:    "updated_language_tag",
				Status:         "delivered",
				StatusMessage:  "updated_status_message",
				ExternalID:     "updated_external_id",
				DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
				CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ExpectedResource: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "updated_language_tag",
					Status:         "delivered",
					StatusMessage:  "updated_status_message",
					ExternalID:     "updated_external_id",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ExpectedResource:     &resource.Delivery{},
				ReturnedResource:     test.ArbitraryDelivery(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "delivery_00000000-0000-0000-0007-000000000000",
				ExpectedResource: test.ArbitraryDelivery(t),
				ReturnedResult:   true,
			},
			expectedDelivery: test.ArbitraryDelivery(t),
			expectedError:    nil,
		},
		"when id and times are modified": {
			id: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			delivery: &resource.Delivery{
				ID:             uuid.MustParse("00000000-0000-0000-0007-ffffffffffff"),
				MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				LanguageTag:    "updated_language_tag",
				Status:         "delivered",
				StatusMessage:  "updated_status_message",
				ExternalID:     "updated_external_id",
				DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
				CreateTime:     test.MakeTime("2000-01-01T07:59:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T07:59:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ExpectedResource: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "updated_language_tag",
					Status:         "delivered",
					StatusMessage:  "updated_status_message",
					ExternalID:     "updated_external_id",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ExpectedResource:     &resource.Delivery{},
				ReturnedResource:     test.ArbitraryDelivery(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "delivery_00000000-0000-0000-0007-000000000000",
				ExpectedResource: test.ArbitraryDelivery(t),
				ReturnedResult:   true,
			},
			expectedDelivery: test.ArbitraryDelivery(t),
			expectedError:    nil,
		},
		"when delivery failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			delivery: &resource.Delivery{
				ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				LanguageTag:    "updated_language_tag",
				Status:         "delivered",
				StatusMessage:  "updated_status_message",
				ExternalID:     "updated_external_id",
				DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
				CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ExpectedResource: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "updated_language_tag",
					Status:         "delivered",
					StatusMessage:  "updated_status_message",
					ExternalID:     "updated_external_id",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: database.ErrInvalidPrimaryKey,
			},
			expectedDelivery: nil,
			expectedError:    action.ErrInvalidResourceID,
		},
		"when delivery failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			delivery: &resource.Delivery{
				ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				LanguageTag:    "updated_language_tag",
				Status:         "delivered",
				StatusMessage:  "updated_status_message",
				ExternalID:     "updated_external_id",
				DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
				CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ExpectedResource: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "updated_language_tag",
					Status:         "delivered",
					StatusMessage:  "updated_status_message",
					ExternalID:     "updated_external_id",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedDelivery: nil,
			expectedError:    fmt.Errorf("error updating delivery: arbitrary"),
		},
		"when delivery failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			delivery: &resource.Delivery{
				ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				LanguageTag:    "updated_language_tag",
				Status:         "delivered",
				StatusMessage:  "updated_status_message",
				ExternalID:     "updated_external_id",
				DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
				CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ExpectedResource: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "updated_language_tag",
					Status:         "delivered",
					StatusMessage:  "updated_status_message",
					ExternalID:     "updated_external_id",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDelivery,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ExpectedResource:     &resource.Delivery{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedDelivery: nil,
			expectedError:    fmt.Errorf("error retrieving delivery: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectUpdate(tt.databaseUpdateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewDeliveryAction(nil, c, db, test.NewClock("2000-01-01T00:00:00Z"))
			delivery, err := a.Update(tt.id, tt.delivery)

			assert.Equal(t, tt.expectedDelivery, delivery)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestDeliveryActionQueueRequest(t *testing.T) {
	tests := map[string]struct {
		deliveryID                 uuid.UUID
		msgQueuePublishExpectation *test.MSGQueuePublishExpectation
		expectedError              error
	}{
		"when successful": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			msgQueuePublishExpectation: &test.MSGQueuePublishExpectation{
				ExpectedData: []byte(`{"deliveryId":"00000000-0000-0000-0007-000000000000"}`),
			},
			expectedError: nil,
		},
		"when delivery failed to be queued": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			msgQueuePublishExpectation: &test.MSGQueuePublishExpectation{
				ExpectedData:  []byte(`{"deliveryId":"00000000-0000-0000-0007-000000000000"}`),
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: test.ErrArbitrary,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mq := test.NewMSGQueue(t)
			mq.ExpectPublish(tt.msgQueuePublishExpectation)

			a := action.NewDeliveryAction(mq, nil, nil, test.NewClock("2000-01-01T00:00:00Z"))
			err := a.QueueRequest(tt.deliveryID)

			assert.Equal(t, tt.expectedError, err)

			mq.AssertExpectations(t)
		})
	}
}
