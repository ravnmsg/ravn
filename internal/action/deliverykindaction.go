package action

import (
	"fmt"
	"time"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/cache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type DeliveryKindActioner interface {
	RetrieveMany(*query.Query) ([]*resource.DeliveryKind, error)
	RetrieveByID(uuid.UUID) (*resource.DeliveryKind, error)
	RetrieveByIDOrCode(string) (*resource.DeliveryKind, error)
	Create(*resource.DeliveryKind) (*resource.DeliveryKind, error)
	Update(uuid.UUID, *resource.DeliveryKind) (*resource.DeliveryKind, error)
	Delete(uuid.UUID) (*resource.DeliveryKind, error)
}

type DeliveryKindAction struct {
	cache cache.Cacher
	db    database.ReadWriter
	clk   clock.Clocker
}

// NewDeliveryKindAction creates a new delivery kind action.
func NewDeliveryKindAction(c cache.Cacher, db database.ReadWriter, clk clock.Clocker) *DeliveryKindAction {
	return &DeliveryKindAction{
		cache: c,
		db:    db,
		clk:   clk,
	}
}

func (a *DeliveryKindAction) RetrieveMany(q *query.Query) ([]*resource.DeliveryKind, error) {
	deliveryKinds := make([]*resource.DeliveryKind, 0)
	if err := a.db.RetrieveMany(resource.TypeDeliveryKind, q, &deliveryKinds); err != nil {
		return nil, fmt.Errorf("error retrieving delivery kinds: %s", err)
	}

	return deliveryKinds, nil
}

func (a *DeliveryKindAction) RetrieveByID(id uuid.UUID) (*resource.DeliveryKind, error) {
	deliveryKind := new(resource.DeliveryKind)

	if ok := a.cache.Get(cachedResourceKey(resource.TypeDeliveryKind, id), deliveryKind); ok {
		return deliveryKind, nil
	}

	err := a.db.RetrieveByID(resource.TypeDeliveryKind, id, deliveryKind)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving delivery kind: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeDeliveryKind, deliveryKind.ID), deliveryKind)

	return deliveryKind, nil
}

func (a *DeliveryKindAction) RetrieveByIDOrCode(idOrCode string) (*resource.DeliveryKind, error) {
	deliveryKind := new(resource.DeliveryKind)
	err := a.db.RetrieveByIDOrCode(resource.TypeDeliveryKind, idOrCode, deliveryKind)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceIDOrCode
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving delivery kind: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeDeliveryKind, deliveryKind.ID), deliveryKind)

	return deliveryKind, nil
}

func (a *DeliveryKindAction) Create(deliveryKind *resource.DeliveryKind) (*resource.DeliveryKind, error) {
	deliveryKind.ID = uuid.Nil
	deliveryKind.CreateTime = a.clk.Now()
	deliveryKind.UpdateTime = a.clk.Now()

	deliveryKindID, err := a.db.Create(resource.TypeDeliveryKind, deliveryKind)
	if err != nil {
		return nil, fmt.Errorf("error creating delivery kind: %s", err)
	}

	deliveryKind = new(resource.DeliveryKind)
	if err := a.db.RetrieveByID(resource.TypeDeliveryKind, deliveryKindID, deliveryKind); err != nil {
		return nil, fmt.Errorf("error retrieving delivery kind: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeDeliveryKind, deliveryKind.ID), deliveryKind)

	return deliveryKind, nil
}

func (a *DeliveryKindAction) Update(id uuid.UUID, deliveryKind *resource.DeliveryKind) (*resource.DeliveryKind, error) {
	deliveryKind.ID = id
	deliveryKind.CreateTime = time.Time{}
	deliveryKind.UpdateTime = a.clk.Now()

	err := a.db.Update(resource.TypeDeliveryKind, id, deliveryKind)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error updating delivery kind: %s", err)
	}

	deliveryKind = new(resource.DeliveryKind)
	if err := a.db.RetrieveByID(resource.TypeDeliveryKind, id, deliveryKind); err != nil {
		return nil, fmt.Errorf("error retrieving delivery kind: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeDeliveryKind, deliveryKind.ID), deliveryKind)

	return deliveryKind, nil
}

func (a *DeliveryKindAction) Delete(id uuid.UUID) (*resource.DeliveryKind, error) {
	deliveryKind := new(resource.DeliveryKind)
	err := a.db.RetrieveByID(resource.TypeDeliveryKind, id, deliveryKind)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving delivery kind: %s", err)
	}

	deliveryKind.Deleted = true

	if err := a.db.Update(resource.TypeDeliveryKind, id, deliveryKind); err != nil {
		return nil, fmt.Errorf("error updating delivery kind: %s", err)
	}

	a.cache.Del(cachedResourceKey(resource.TypeDeliveryKind, deliveryKind.ID))

	return deliveryKind, nil
}

var _ = DeliveryKindActioner(new(DeliveryKindAction))
