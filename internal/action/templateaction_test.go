//go:build !integration && !endtoend
// +build !integration,!endtoend

package action_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestTemplateActionRetrieveMany(t *testing.T) {
	tests := map[string]struct {
		query                           *query.Query
		databaseRetrieveManyExpectation *test.DatabaseRetrieveManyExpectation
		expectedTemplates               []*resource.Template
		expectedError                   error
	}{
		"when successful": {
			query: query.NewQuery(
				query.WithFilterArgs("MessageKindID", query.OpEqual, "00000000-0000-0000-0003-000000000000"),
				query.WithOrderArgs("LanguageTag", query.DirDescending),
				query.WithPageArgs(8, 5),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpEqual, "00000000-0000-0000-0003-000000000000"),
					query.WithOrderArgs("LanguageTag", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ExpectedResources: &[]*resource.Template{},
				ReturnedResources: []*resource.Template{
					test.ArbitraryTemplate(t),
				},
			},
			expectedTemplates: []*resource.Template{
				test.ArbitraryTemplate(t),
			},
			expectedError: nil,
		},
		"when query not specified": {
			query: nil,
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedQuery:        nil,
				ExpectedResources:    &[]*resource.Template{},
				ReturnedResources: []*resource.Template{
					test.ArbitraryTemplate(t),
				},
			},
			expectedTemplates: []*resource.Template{
				test.ArbitraryTemplate(t),
			},
			expectedError: nil,
		},
		"when templates failed to be retrieved": {
			query: query.NewQuery(
				query.WithFilterArgs("Code", query.OpEqual, "template_000000000000"),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Code", query.OpEqual, "template_000000000000"),
				),
				ExpectedResources: &[]*resource.Template{},
				ReturnedError:     test.ErrArbitrary,
			},
			expectedTemplates: nil,
			expectedError:     fmt.Errorf("error retrieving templates: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			db := test.NewDatabase(t)
			db.ExpectRetrieveMany(tt.databaseRetrieveManyExpectation)

			a := action.NewTemplateAction(nil, db, test.NewClock("2000-01-01T00:00:00Z"))
			templates, err := a.RetrieveMany(tt.query)

			assert.Equal(t, tt.expectedTemplates, templates)
			assert.Equal(t, tt.expectedError, err)

			db.AssertExpectations(t)
		})
	}
}

func TestTemplateActionRetrieveByID(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		cacheGetExpectation             *test.CacheGetExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedTemplate                *resource.Template
		expectedError                   error
	}{
		"when successful from cache": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "template_00000000-0000-0000-0005-000000000000",
				ExpectedResource: &resource.Template{},
				ReturnedResource: test.ArbitraryTemplate(t),
				ReturnedResult:   true,
			},
			expectedTemplate: test.ArbitraryTemplate(t),
			expectedError:    nil,
		},
		"when successful from database": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "template_00000000-0000-0000-0005-000000000000",
				ExpectedResource: &resource.Template{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedResource:     test.ArbitraryTemplate(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "template_00000000-0000-0000-0005-000000000000",
				ExpectedResource: test.ArbitraryTemplate(t),
				ReturnedResult:   true,
			},
			expectedTemplate: test.ArbitraryTemplate(t),
			expectedError:    nil,
		},
		"when template failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "template_00000000-0000-0000-0005-000000000000",
				ExpectedResource: &resource.Template{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedTemplate: nil,
			expectedError:    action.ErrInvalidResourceID,
		},
		"when template failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "template_00000000-0000-0000-0005-000000000000",
				ExpectedResource: &resource.Template{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedTemplate: nil,
			expectedError:    fmt.Errorf("error retrieving template: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectGet(tt.cacheGetExpectation)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewTemplateAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			template, err := a.RetrieveByID(tt.id)

			assert.Equal(t, tt.expectedTemplate, template)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestTemplateActionCreate(t *testing.T) {
	tests := map[string]struct {
		template                        *resource.Template
		databaseCreateExpectation       *test.DatabaseCreateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedTemplate                *resource.Template
		expectedError                   error
	}{
		"when successful": {
			template: &resource.Template{
				MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				LanguageTag:    "new_language_tag",
				Body:           "new_body",
				Deleted:        false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedResource: &resource.Template{
					ID:             uuid.Nil,
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "new_language_tag",
					Body:           "new_body",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedResource:     test.ArbitraryTemplate(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "template_00000000-0000-0000-0005-000000000000",
				ExpectedResource: test.ArbitraryTemplate(t),
				ReturnedResult:   true,
			},
			expectedTemplate: test.ArbitraryTemplate(t),
			expectedError:    nil,
		},
		"when id and times are modified": {
			template: &resource.Template{
				ID:             uuid.MustParse("00000000-0000-0000-0005-ffffffffffff"),
				MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				LanguageTag:    "new_language_tag",
				Body:           "new_body",
				Deleted:        false,
				CreateTime:     test.MakeTime("2000-01-01T05:59:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T05:59:59Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedResource: &resource.Template{
					ID:             uuid.Nil,
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "new_language_tag",
					Body:           "new_body",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedResource:     test.ArbitraryTemplate(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "template_00000000-0000-0000-0005-000000000000",
				ExpectedResource: test.ArbitraryTemplate(t),
				ReturnedResult:   true,
			},
			expectedTemplate: test.ArbitraryTemplate(t),
			expectedError:    nil,
		},
		"when template failed to be created": {
			template: &resource.Template{
				MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				LanguageTag:    "new_language_tag",
				Body:           "new_body",
				Deleted:        false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedResource: &resource.Template{
					ID:             uuid.Nil,
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "new_language_tag",
					Body:           "new_body",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedTemplate: nil,
			expectedError:    fmt.Errorf("error creating template: arbitrary"),
		},
		"when template failed to be retrieved": {
			template: &resource.Template{
				MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				LanguageTag:    "new_language_tag",
				Body:           "new_body",
				Deleted:        false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedResource: &resource.Template{
					ID:             uuid.Nil,
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "new_language_tag",
					Body:           "new_body",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedTemplate: nil,
			expectedError:    fmt.Errorf("error retrieving template: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectCreate(tt.databaseCreateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewTemplateAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			template, err := a.Create(tt.template)

			assert.Equal(t, tt.expectedTemplate, template)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestTemplateActionUpdate(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		template                        *resource.Template
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedTemplate                *resource.Template
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			template: &resource.Template{
				ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				LanguageTag:    "updated_language_tag",
				Body:           "updated_body",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "updated_language_tag",
					Body:           "updated_body",
					Deleted:        true,
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedResource:     test.ArbitraryTemplate(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "template_00000000-0000-0000-0005-000000000000",
				ExpectedResource: test.ArbitraryTemplate(t),
				ReturnedResult:   true,
			},
			expectedTemplate: test.ArbitraryTemplate(t),
			expectedError:    nil,
		},
		"when id and times are modified": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			template: &resource.Template{
				ID:             uuid.MustParse("00000000-0000-0000-0005-ffffffffffff"),
				MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				LanguageTag:    "updated_language_tag",
				Body:           "updated_body",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T05:59:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T05:59:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "updated_language_tag",
					Body:           "updated_body",
					Deleted:        true,
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedResource:     test.ArbitraryTemplate(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "template_00000000-0000-0000-0005-000000000000",
				ExpectedResource: test.ArbitraryTemplate(t),
				ReturnedResult:   true,
			},
			expectedTemplate: test.ArbitraryTemplate(t),
			expectedError:    nil,
		},
		"when template failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			template: &resource.Template{
				ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				LanguageTag:    "updated_language_tag",
				Body:           "updated_body",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "updated_language_tag",
					Body:           "updated_body",
					Deleted:        true,
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: database.ErrInvalidPrimaryKey,
			},
			expectedTemplate: nil,
			expectedError:    action.ErrInvalidResourceID,
		},
		"when template failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			template: &resource.Template{
				ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				LanguageTag:    "updated_language_tag",
				Body:           "updated_body",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "updated_language_tag",
					Body:           "updated_body",
					Deleted:        true,
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedTemplate: nil,
			expectedError:    fmt.Errorf("error updating template: arbitrary"),
		},
		"when template failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			template: &resource.Template{
				ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				LanguageTag:    "updated_language_tag",
				Body:           "updated_body",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "updated_language_tag",
					Body:           "updated_body",
					Deleted:        true,
					CreateTime:     time.Time{},
					UpdateTime:     test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedTemplate: nil,
			expectedError:    fmt.Errorf("error retrieving template: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectUpdate(tt.databaseUpdateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewTemplateAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			template, err := a.Update(tt.id, tt.template)

			assert.Equal(t, tt.expectedTemplate, template)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestTemplateActionDelete(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		cacheDelExpectation             *test.CacheDelExpectation
		expectedTemplate                *resource.Template
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedResource:     test.ArbitraryTemplate(t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Body:           "body_000000000000",
					Deleted:        true,
					CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
				},
			},
			cacheDelExpectation: &test.CacheDelExpectation{
				ExpectedKey:    "template_00000000-0000-0000-0005-000000000000",
				ReturnedResult: true,
			},
			expectedTemplate: &resource.Template{
				ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				LanguageTag:    "language_tag_000000000000",
				Body:           "body_000000000000",
				Deleted:        true,
				CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
			},
			expectedError: nil,
		},
		"when template failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedTemplate: nil,
			expectedError:    action.ErrInvalidResourceID,
		},
		"when template failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedTemplate: nil,
			expectedError:    fmt.Errorf("error retrieving template: arbitrary"),
		},
		"when template failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource:     &resource.Template{},
				ReturnedResource:     test.ArbitraryTemplate(t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeTemplate,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedResource: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Body:           "body_000000000000",
					Deleted:        true,
					CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedTemplate: nil,
			expectedError:    fmt.Errorf("error updating template: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectDel(tt.cacheDelExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)
			db.ExpectUpdate(tt.databaseUpdateExpectation)

			a := action.NewTemplateAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			template, err := a.Delete(tt.id)

			assert.Equal(t, tt.expectedTemplate, template)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}
