//go:build !integration && !endtoend
// +build !integration,!endtoend

package action_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestMessageActionRetrieveMany(t *testing.T) {
	tests := map[string]struct {
		query                           *query.Query
		databaseRetrieveManyExpectation *test.DatabaseRetrieveManyExpectation
		expectedMessages                []*resource.Message
		expectedError                   error
	}{
		"when successful": {
			query: query.NewQuery(
				query.WithFilterArgs("MessageKindID", query.OpEqual, "00000000-0000-0000-0003-000000000000"),
				query.WithOrderArgs("Status", query.DirDescending),
				query.WithPageArgs(8, 5),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpEqual, "00000000-0000-0000-0003-000000000000"),
					query.WithOrderArgs("Status", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ExpectedResources: &[]*resource.Message{},
				ReturnedResources: []*resource.Message{
					test.ArbitraryMessage(t),
				},
			},
			expectedMessages: []*resource.Message{
				test.ArbitraryMessage(t),
			},
			expectedError: nil,
		},
		"when query not specified": {
			query: nil,
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedQuery:        nil,
				ExpectedResources:    &[]*resource.Message{},
				ReturnedResources: []*resource.Message{
					test.ArbitraryMessage(t),
				},
			},
			expectedMessages: []*resource.Message{
				test.ArbitraryMessage(t),
			},
			expectedError: nil,
		},
		"when messages failed to be retrieved": {
			query: query.NewQuery(
				query.WithFilterArgs("Status", query.OpEqual, "succeeded"),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Status", query.OpEqual, "succeeded"),
				),
				ExpectedResources: &[]*resource.Message{},
				ReturnedError:     test.ErrArbitrary,
			},
			expectedMessages: nil,
			expectedError:    fmt.Errorf("error retrieving messages: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			db := test.NewDatabase(t)
			db.ExpectRetrieveMany(tt.databaseRetrieveManyExpectation)

			a := action.NewMessageAction(nil, db, test.NewClock("2000-01-01T00:00:00Z"))
			messages, err := a.RetrieveMany(tt.query)

			assert.Equal(t, tt.expectedMessages, messages)
			assert.Equal(t, tt.expectedError, err)

			db.AssertExpectations(t)
		})
	}
}

func TestMessageActionRetrieveByID(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		cacheGetExpectation             *test.CacheGetExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedMessage                 *resource.Message
		expectedError                   error
	}{
		"when successful from cache": {
			id: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "message_00000000-0000-0000-0006-000000000000",
				ExpectedResource: &resource.Message{},
				ReturnedResource: test.ArbitraryMessage(t),
				ReturnedResult:   true,
			},
			expectedMessage: test.ArbitraryMessage(t),
			expectedError:   nil,
		},
		"when successful from database": {
			id: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "message_00000000-0000-0000-0006-000000000000",
				ExpectedResource: &resource.Message{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Message{},
				ReturnedResource:     test.ArbitraryMessage(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "message_00000000-0000-0000-0006-000000000000",
				ExpectedResource: test.ArbitraryMessage(t),
				ReturnedResult:   true,
			},
			expectedMessage: test.ArbitraryMessage(t),
			expectedError:   nil,
		},
		"when message failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "message_00000000-0000-0000-0006-000000000000",
				ExpectedResource: &resource.Message{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Message{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedMessage: nil,
			expectedError:   action.ErrInvalidResourceID,
		},
		"when message failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "message_00000000-0000-0000-0006-000000000000",
				ExpectedResource: &resource.Message{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Message{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedMessage: nil,
			expectedError:   fmt.Errorf("error retrieving message: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectGet(tt.cacheGetExpectation)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewMessageAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			message, err := a.RetrieveByID(tt.id)

			assert.Equal(t, tt.expectedMessage, message)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestMessageActionCreate(t *testing.T) {
	tests := map[string]struct {
		message                         *resource.Message
		databaseCreateExpectation       *test.DatabaseCreateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedMessage                 *resource.Message
		expectedError                   error
	}{
		"when successful": {
			message: &resource.Message{
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Input:         map[string]interface{}{"key": "new_value"},
				Status:        "active",
				ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedResource: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input:         map[string]interface{}{"key": "new_value"},
					Status:        "active",
					ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Message{},
				ReturnedResource:     test.ArbitraryMessage(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "message_00000000-0000-0000-0006-000000000000",
				ExpectedResource: test.ArbitraryMessage(t),
				ReturnedResult:   true,
			},
			expectedMessage: test.ArbitraryMessage(t),
			expectedError:   nil,
		},
		"when id and times are modified": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-ffffffffffff"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Input:         map[string]interface{}{"key": "new_value"},
				Status:        "active",
				ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:59:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:59:59Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedResource: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input:         map[string]interface{}{"key": "new_value"},
					Status:        "active",
					ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Message{},
				ReturnedResource:     test.ArbitraryMessage(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "message_00000000-0000-0000-0006-000000000000",
				ExpectedResource: test.ArbitraryMessage(t),
				ReturnedResult:   true,
			},
			expectedMessage: test.ArbitraryMessage(t),
			expectedError:   nil,
		},
		"when message failed to be created": {
			message: &resource.Message{
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Input:         map[string]interface{}{"key": "new_value"},
				Status:        "active",
				ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedResource: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input:         map[string]interface{}{"key": "new_value"},
					Status:        "active",
					ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedMessage: nil,
			expectedError:   fmt.Errorf("error creating message: arbitrary"),
		},
		"when message failed to be retrieved": {
			message: &resource.Message{
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Input:         map[string]interface{}{"key": "new_value"},
				Status:        "active",
				ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedResource: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input:         map[string]interface{}{"key": "new_value"},
					Status:        "active",
					ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Message{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedMessage: nil,
			expectedError:   fmt.Errorf("error retrieving message: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectCreate(tt.databaseCreateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewMessageAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			message, err := a.Create(tt.message)

			assert.Equal(t, tt.expectedMessage, message)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestMessageActionUpdate(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		message                         *resource.Message
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedMessage                 *resource.Message
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Input:         map[string]interface{}{"key": "updated_value"},
				Status:        "succeeded",
				ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input:         map[string]interface{}{"key": "updated_value"},
					Status:        "succeeded",
					ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:    time.Time{},
					UpdateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Message{},
				ReturnedResource:     test.ArbitraryMessage(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "message_00000000-0000-0000-0006-000000000000",
				ExpectedResource: test.ArbitraryMessage(t),
				ReturnedResult:   true,
			},
			expectedMessage: test.ArbitraryMessage(t),
			expectedError:   nil,
		},
		"when id and times are modified": {
			id: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-ffffffffffff"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Input:         map[string]interface{}{"key": "updated_value"},
				Status:        "succeeded",
				ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:59:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:59:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input:         map[string]interface{}{"key": "updated_value"},
					Status:        "succeeded",
					ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:    time.Time{},
					UpdateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Message{},
				ReturnedResource:     test.ArbitraryMessage(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "message_00000000-0000-0000-0006-000000000000",
				ExpectedResource: test.ArbitraryMessage(t),
				ReturnedResult:   true,
			},
			expectedMessage: test.ArbitraryMessage(t),
			expectedError:   nil,
		},
		"when message failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Input:         map[string]interface{}{"key": "updated_value"},
				Status:        "succeeded",
				ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input:         map[string]interface{}{"key": "updated_value"},
					Status:        "succeeded",
					ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:    time.Time{},
					UpdateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: database.ErrInvalidPrimaryKey,
			},
			expectedMessage: nil,
			expectedError:   action.ErrInvalidResourceID,
		},
		"when message failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Input:         map[string]interface{}{"key": "updated_value"},
				Status:        "succeeded",
				ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input:         map[string]interface{}{"key": "updated_value"},
					Status:        "succeeded",
					ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:    time.Time{},
					UpdateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedMessage: nil,
			expectedError:   fmt.Errorf("error updating message: arbitrary"),
		},
		"when message failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Input:         map[string]interface{}{"key": "updated_value"},
				Status:        "succeeded",
				ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input:         map[string]interface{}{"key": "updated_value"},
					Status:        "succeeded",
					ScheduleTime:  test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:    time.Time{},
					UpdateTime:    test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeMessage,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedResource:     &resource.Message{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedMessage: nil,
			expectedError:   fmt.Errorf("error retrieving message: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectUpdate(tt.databaseUpdateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewMessageAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			message, err := a.Update(tt.id, tt.message)

			assert.Equal(t, tt.expectedMessage, message)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}
