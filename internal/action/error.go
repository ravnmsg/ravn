package action

import (
	"errors"
)

var (
	// ErrInfiniteLoopDetected defines an error caused when an infinite loop is detected when
	// traversing a namespace tree.
	ErrInfiniteLoopDetected = errors.New("infinite loop detected")

	// ErrInvalidResourceID defines an error caused by an invalid resource id in the provided path.
	ErrInvalidResourceID = errors.New("invalid resource id")

	// ErrInvalidResourceIDOrCode defines an error caused by a value that is supposed to uniquely
	// match a given resource, but which does not match either an ID, nor a code field.
	ErrInvalidResourceIDOrCode = errors.New("invalid resource id or code")

	// ErrNamespaceDeleted defines an error caused by an authorization that was failed because of
	// a namespace, or one of its parents, that was deleted.
	ErrNamespaceDeleted = errors.New("namespace, or parent namespace, is deleted")
)
