package action

import (
	"fmt"
	"time"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/cache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type TemplateKindActioner interface {
	RetrieveMany(*query.Query) ([]*resource.TemplateKind, error)
	RetrieveByID(uuid.UUID) (*resource.TemplateKind, error)
	Create(*resource.TemplateKind) (*resource.TemplateKind, error)
	Update(uuid.UUID, *resource.TemplateKind) (*resource.TemplateKind, error)
	Delete(uuid.UUID) (*resource.TemplateKind, error)
}

type TemplateKindAction struct {
	cache cache.Cacher
	db    database.ReadWriter
	clk   clock.Clocker
}

// NewTemplateKindAction creates a new template kind action.
func NewTemplateKindAction(c cache.Cacher, db database.ReadWriter, clk clock.Clocker) *TemplateKindAction {
	return &TemplateKindAction{
		cache: c,
		db:    db,
		clk:   clk,
	}
}

func (a *TemplateKindAction) RetrieveMany(q *query.Query) ([]*resource.TemplateKind, error) {
	templateKinds := make([]*resource.TemplateKind, 0)
	if err := a.db.RetrieveMany(resource.TypeTemplateKind, q, &templateKinds); err != nil {
		return nil, fmt.Errorf("error retrieving template kinds: %s", err)
	}

	return templateKinds, nil
}

func (a *TemplateKindAction) RetrieveByID(id uuid.UUID) (*resource.TemplateKind, error) {
	templateKind := new(resource.TemplateKind)

	if ok := a.cache.Get(cachedResourceKey(resource.TypeTemplateKind, id), templateKind); ok {
		return templateKind, nil
	}

	err := a.db.RetrieveByID(resource.TypeTemplateKind, id, templateKind)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving template kind: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeTemplateKind, templateKind.ID), templateKind)

	return templateKind, nil
}

func (a *TemplateKindAction) Create(templateKind *resource.TemplateKind) (*resource.TemplateKind, error) {
	templateKind.ID = uuid.Nil
	templateKind.CreateTime = a.clk.Now()
	templateKind.UpdateTime = a.clk.Now()

	templateKindID, err := a.db.Create(resource.TypeTemplateKind, templateKind)
	if err != nil {
		return nil, fmt.Errorf("error creating template kind: %s", err)
	}

	templateKind = new(resource.TemplateKind)
	if err := a.db.RetrieveByID(resource.TypeTemplateKind, templateKindID, templateKind); err != nil {
		return nil, fmt.Errorf("error retrieving template kind: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeTemplateKind, templateKind.ID), templateKind)

	return templateKind, nil
}

func (a *TemplateKindAction) Update(id uuid.UUID, templateKind *resource.TemplateKind) (*resource.TemplateKind, error) {
	templateKind.ID = id
	templateKind.CreateTime = time.Time{}
	templateKind.UpdateTime = a.clk.Now()

	err := a.db.Update(resource.TypeTemplateKind, id, templateKind)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error updating template kind: %s", err)
	}

	templateKind = new(resource.TemplateKind)
	if err := a.db.RetrieveByID(resource.TypeTemplateKind, id, templateKind); err != nil {
		return nil, fmt.Errorf("error retrieving template kind: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeTemplateKind, templateKind.ID), templateKind)

	return templateKind, nil
}

func (a *TemplateKindAction) Delete(id uuid.UUID) (*resource.TemplateKind, error) {
	templateKind := new(resource.TemplateKind)
	err := a.db.RetrieveByID(resource.TypeTemplateKind, id, templateKind)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving template kind: %s", err)
	}

	templateKind.Deleted = true

	if err := a.db.Update(resource.TypeTemplateKind, id, templateKind); err != nil {
		return nil, fmt.Errorf("error updating template kind: %s", err)
	}

	a.cache.Del(cachedResourceKey(resource.TypeTemplateKind, templateKind.ID))

	return templateKind, nil
}

var _ = TemplateKindActioner(new(TemplateKindAction))
