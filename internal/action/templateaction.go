package action

import (
	"fmt"
	"time"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/cache"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type TemplateActioner interface {
	RetrieveMany(*query.Query) ([]*resource.Template, error)
	RetrieveByID(uuid.UUID) (*resource.Template, error)
	Create(*resource.Template) (*resource.Template, error)
	Update(uuid.UUID, *resource.Template) (*resource.Template, error)
	Delete(uuid.UUID) (*resource.Template, error)
}

type TemplateAction struct {
	cache cache.Cacher
	db    database.ReadWriter
	clk   clock.Clocker
}

// NewTemplateAction creates a new template action.
func NewTemplateAction(c cache.Cacher, db database.ReadWriter, clk clock.Clocker) *TemplateAction {
	return &TemplateAction{
		cache: c,
		db:    db,
		clk:   clk,
	}
}

func (a *TemplateAction) RetrieveMany(q *query.Query) ([]*resource.Template, error) {
	templates := make([]*resource.Template, 0)
	if err := a.db.RetrieveMany(resource.TypeTemplate, q, &templates); err != nil {
		return nil, fmt.Errorf("error retrieving templates: %s", err)
	}

	return templates, nil
}

func (a *TemplateAction) RetrieveByID(id uuid.UUID) (*resource.Template, error) {
	template := new(resource.Template)

	if ok := a.cache.Get(cachedResourceKey(resource.TypeTemplate, id), template); ok {
		return template, nil
	}

	err := a.db.RetrieveByID(resource.TypeTemplate, id, template)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving template: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeTemplate, template.ID), template)

	return template, nil
}

func (a *TemplateAction) Create(template *resource.Template) (*resource.Template, error) {
	template.ID = uuid.Nil
	template.CreateTime = a.clk.Now()
	template.UpdateTime = a.clk.Now()

	templateID, err := a.db.Create(resource.TypeTemplate, template)
	if err != nil {
		return nil, fmt.Errorf("error creating template: %s", err)
	}

	template = new(resource.Template)
	if err := a.db.RetrieveByID(resource.TypeTemplate, templateID, template); err != nil {
		return nil, fmt.Errorf("error retrieving template: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeTemplate, template.ID), template)

	return template, nil
}

func (a *TemplateAction) Update(id uuid.UUID, template *resource.Template) (*resource.Template, error) {
	template.ID = id
	template.CreateTime = time.Time{}
	template.UpdateTime = a.clk.Now()

	err := a.db.Update(resource.TypeTemplate, id, template)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error updating template: %s", err)
	}

	template = new(resource.Template)
	if err := a.db.RetrieveByID(resource.TypeTemplate, id, template); err != nil {
		return nil, fmt.Errorf("error retrieving template: %s", err)
	}

	a.cache.Set(cachedResourceKey(resource.TypeTemplate, template.ID), template)

	return template, nil
}

func (a *TemplateAction) Delete(id uuid.UUID) (*resource.Template, error) {
	template := new(resource.Template)
	err := a.db.RetrieveByID(resource.TypeTemplate, id, template)
	if err == database.ErrInvalidPrimaryKey {
		return nil, ErrInvalidResourceID
	} else if err != nil {
		return nil, fmt.Errorf("error retrieving template: %s", err)
	}

	template.Deleted = true

	if err := a.db.Update(resource.TypeTemplate, id, template); err != nil {
		return nil, fmt.Errorf("error updating template: %s", err)
	}

	a.cache.Del(cachedResourceKey(resource.TypeTemplate, template.ID))

	return template, nil
}

var _ = TemplateActioner(new(TemplateAction))
