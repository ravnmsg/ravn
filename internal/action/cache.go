package action

import (
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/resource"
)

func cachedResourceKey(rt resource.Type, id uuid.UUID) string {
	return fmt.Sprintf("%s_%s", rt.Name(), id.String())
}
