//go:build !integration && !endtoend
// +build !integration,!endtoend

package action_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestDeliveryKindActionRetrieveMany(t *testing.T) {
	tests := map[string]struct {
		query                           *query.Query
		databaseRetrieveManyExpectation *test.DatabaseRetrieveManyExpectation
		expectedDeliveryKinds           []*resource.DeliveryKind
		expectedError                   error
	}{
		"when successful": {
			query: query.NewQuery(
				query.WithFilterArgs("Code", query.OpEqual, "delivery_kind_000000000000"),
				query.WithOrderArgs("Code", query.DirDescending),
				query.WithPageArgs(8, 5),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Code", query.OpEqual, "delivery_kind_000000000000"),
					query.WithOrderArgs("Code", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ExpectedResources: &[]*resource.DeliveryKind{},
				ReturnedResources: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			expectedDeliveryKinds: []*resource.DeliveryKind{
				test.ArbitraryDeliveryKind(t),
			},
			expectedError: nil,
		},
		"when query not specified": {
			query: nil,
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedQuery:        nil,
				ExpectedResources:    &[]*resource.DeliveryKind{},
				ReturnedResources: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			expectedDeliveryKinds: []*resource.DeliveryKind{
				test.ArbitraryDeliveryKind(t),
			},
			expectedError: nil,
		},
		"when delivery kinds failed to be retrieved": {
			query: query.NewQuery(
				query.WithFilterArgs("Code", query.OpEqual, "delivery_kind_000000000000"),
			),
			databaseRetrieveManyExpectation: &test.DatabaseRetrieveManyExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Code", query.OpEqual, "delivery_kind_000000000000"),
				),
				ExpectedResources: &[]*resource.DeliveryKind{},
				ReturnedError:     test.ErrArbitrary,
			},
			expectedDeliveryKinds: nil,
			expectedError:         fmt.Errorf("error retrieving delivery kinds: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			db := test.NewDatabase(t)
			db.ExpectRetrieveMany(tt.databaseRetrieveManyExpectation)

			a := action.NewDeliveryKindAction(nil, db, test.NewClock("2000-01-01T00:00:00Z"))
			deliveryKinds, err := a.RetrieveMany(tt.query)

			assert.Equal(t, tt.expectedDeliveryKinds, deliveryKinds)
			assert.Equal(t, tt.expectedError, err)

			db.AssertExpectations(t)
		})
	}
}

func TestDeliveryKindActionRetrieveByID(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		cacheGetExpectation             *test.CacheGetExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedDeliveryKind            *resource.DeliveryKind
		expectedError                   error
	}{
		"when successful from cache": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "deliveryKind_00000000-0000-0000-0002-000000000000",
				ExpectedResource: &resource.DeliveryKind{},
				ReturnedResource: test.ArbitraryDeliveryKind(t),
				ReturnedResult:   true,
			},
			expectedDeliveryKind: test.ArbitraryDeliveryKind(t),
			expectedError:        nil,
		},
		"when successful from database": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "deliveryKind_00000000-0000-0000-0002-000000000000",
				ExpectedResource: &resource.DeliveryKind{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedResource:     test.ArbitraryDeliveryKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "deliveryKind_00000000-0000-0000-0002-000000000000",
				ExpectedResource: test.ArbitraryDeliveryKind(t),
				ReturnedResult:   true,
			},
			expectedDeliveryKind: test.ArbitraryDeliveryKind(t),
			expectedError:        nil,
		},
		"when delivery kind failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "deliveryKind_00000000-0000-0000-0002-000000000000",
				ExpectedResource: &resource.DeliveryKind{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedDeliveryKind: nil,
			expectedError:        action.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			cacheGetExpectation: &test.CacheGetExpectation{
				ExpectedKey:      "deliveryKind_00000000-0000-0000-0002-000000000000",
				ExpectedResource: &resource.DeliveryKind{},
				ReturnedResult:   false,
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedDeliveryKind: nil,
			expectedError:        fmt.Errorf("error retrieving delivery kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectGet(tt.cacheGetExpectation)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewDeliveryKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			deliveryKind, err := a.RetrieveByID(tt.id)

			assert.Equal(t, tt.expectedDeliveryKind, deliveryKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestDeliveryKindActionRetrieveByIDOrCode(t *testing.T) {
	tests := map[string]struct {
		idOrCode                              string
		databaseRetrieveByIDOrCodeExpectation *test.DatabaseRetrieveByIDOrCodeExpectation
		cacheSetExpectation                   *test.CacheSetExpectation
		expectedDeliveryKind                  *resource.DeliveryKind
		expectedError                         error
	}{
		"when id specified as uuid": {
			idOrCode: "00000000-0000-0000-0002-000000000000",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedIDOrCode:     "00000000-0000-0000-0002-000000000000",
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedResource:     test.ArbitraryDeliveryKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "deliveryKind_00000000-0000-0000-0002-000000000000",
				ExpectedResource: test.ArbitraryDeliveryKind(t),
				ReturnedResult:   true,
			},
			expectedDeliveryKind: test.ArbitraryDeliveryKind(t),
			expectedError:        nil,
		},
		"when id specified as code": {
			idOrCode: "code",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedIDOrCode:     "code",
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedResource:     test.ArbitraryDeliveryKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "deliveryKind_00000000-0000-0000-0002-000000000000",
				ExpectedResource: test.ArbitraryDeliveryKind(t),
				ReturnedResult:   true,
			},
			expectedDeliveryKind: test.ArbitraryDeliveryKind(t),
			expectedError:        nil,
		},
		"when delivery kind failed to be found": {
			idOrCode: "00000000-0000-0000-0002-000000000000",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedIDOrCode:     "00000000-0000-0000-0002-000000000000",
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedDeliveryKind: nil,
			expectedError:        action.ErrInvalidResourceIDOrCode,
		},
		"when delivery kind failed to be retrieved": {
			idOrCode: "00000000-0000-0000-0002-000000000000",
			databaseRetrieveByIDOrCodeExpectation: &test.DatabaseRetrieveByIDOrCodeExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedIDOrCode:     "00000000-0000-0000-0002-000000000000",
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedDeliveryKind: nil,
			expectedError:        fmt.Errorf("error retrieving delivery kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByIDOrCode(tt.databaseRetrieveByIDOrCodeExpectation)

			a := action.NewDeliveryKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			deliveryKind, err := a.RetrieveByIDOrCode(tt.idOrCode)

			assert.Equal(t, tt.expectedDeliveryKind, deliveryKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestDeliveryKindActionCreate(t *testing.T) {
	tests := map[string]struct {
		deliveryKind                    *resource.DeliveryKind
		databaseCreateExpectation       *test.DatabaseCreateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedDeliveryKind            *resource.DeliveryKind
		expectedError                   error
	}{
		"when successful": {
			deliveryKind: &resource.DeliveryKind{
				Code:        "new_delivery_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "new_host",
				Disabled:    false,
				Deleted:     false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedResource: &resource.DeliveryKind{
					ID:          uuid.Nil,
					Code:        "new_delivery_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "new_host",
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedResource:     test.ArbitraryDeliveryKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "deliveryKind_00000000-0000-0000-0002-000000000000",
				ExpectedResource: test.ArbitraryDeliveryKind(t),
				ReturnedResult:   true,
			},
			expectedDeliveryKind: test.ArbitraryDeliveryKind(t),
			expectedError:        nil,
		},
		"when id and times are modified": {
			deliveryKind: &resource.DeliveryKind{
				ID:          uuid.MustParse("00000000-0000-0000-0002-ffffffffffff"),
				Code:        "new_delivery_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "new_host",
				Disabled:    false,
				Deleted:     false,
				CreateTime:  test.MakeTime("2000-01-01T02:59:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:59:59Z", t),
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedResource: &resource.DeliveryKind{
					ID:          uuid.Nil,
					Code:        "new_delivery_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "new_host",
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedResource:     test.ArbitraryDeliveryKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "deliveryKind_00000000-0000-0000-0002-000000000000",
				ExpectedResource: test.ArbitraryDeliveryKind(t),
				ReturnedResult:   true,
			},
			expectedDeliveryKind: test.ArbitraryDeliveryKind(t),
			expectedError:        nil,
		},
		"when delivery kind failed to be created": {
			deliveryKind: &resource.DeliveryKind{
				Code:        "new_delivery_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "new_host",
				Disabled:    false,
				Deleted:     false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedResource: &resource.DeliveryKind{
					ID:          uuid.Nil,
					Code:        "new_delivery_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "new_host",
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedDeliveryKind: nil,
			expectedError:        fmt.Errorf("error creating delivery kind: arbitrary"),
		},
		"when delivery kind failed to be retrieved": {
			deliveryKind: &resource.DeliveryKind{
				Code:        "new_delivery_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "new_host",
				Disabled:    false,
				Deleted:     false,
			},
			databaseCreateExpectation: &test.DatabaseCreateExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedResource: &resource.DeliveryKind{
					ID:          uuid.Nil,
					Code:        "new_delivery_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "new_host",
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedDeliveryKind: nil,
			expectedError:        fmt.Errorf("error retrieving delivery kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectCreate(tt.databaseCreateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewDeliveryKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			deliveryKind, err := a.Create(tt.deliveryKind)

			assert.Equal(t, tt.expectedDeliveryKind, deliveryKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestDeliveryKindActionUpdate(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		deliveryKind                    *resource.DeliveryKind
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		cacheSetExpectation             *test.CacheSetExpectation
		expectedDeliveryKind            *resource.DeliveryKind
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			deliveryKind: &resource.DeliveryKind{
				ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				Code:        "updated_delivery_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "host_000000000000",
				Disabled:    true,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "host_000000000000",
					Disabled:    true,
					Deleted:     true,
					CreateTime:  time.Time{},
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedResource:     test.ArbitraryDeliveryKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "deliveryKind_00000000-0000-0000-0002-000000000000",
				ExpectedResource: test.ArbitraryDeliveryKind(t),
				ReturnedResult:   true,
			},
			expectedDeliveryKind: test.ArbitraryDeliveryKind(t),
			expectedError:        nil,
		},
		"when id and times are modified": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			deliveryKind: &resource.DeliveryKind{
				ID:          uuid.MustParse("00000000-0000-0000-0002-ffffffffffff"),
				Code:        "updated_delivery_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "host_000000000000",
				Disabled:    true,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T02:59:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:59:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "host_000000000000",
					Disabled:    true,
					Deleted:     true,
					CreateTime:  time.Time{},
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedResource:     test.ArbitraryDeliveryKind(t),
			},
			cacheSetExpectation: &test.CacheSetExpectation{
				ExpectedKey:      "deliveryKind_00000000-0000-0000-0002-000000000000",
				ExpectedResource: test.ArbitraryDeliveryKind(t),
				ReturnedResult:   true,
			},
			expectedDeliveryKind: test.ArbitraryDeliveryKind(t),
			expectedError:        nil,
		},
		"when delivery kind failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			deliveryKind: &resource.DeliveryKind{
				ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				Code:        "updated_delivery_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "host_000000000000",
				Disabled:    true,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "host_000000000000",
					Disabled:    true,
					Deleted:     true,
					CreateTime:  time.Time{},
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: database.ErrInvalidPrimaryKey,
			},
			expectedDeliveryKind: nil,
			expectedError:        action.ErrInvalidResourceID,
		},
		"when delivery kind failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			deliveryKind: &resource.DeliveryKind{
				ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				Code:        "updated_delivery_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "host_000000000000",
				Disabled:    true,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "host_000000000000",
					Disabled:    true,
					Deleted:     true,
					CreateTime:  time.Time{},
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedDeliveryKind: nil,
			expectedError:        fmt.Errorf("error updating delivery kind: arbitrary"),
		},
		"when delivery kind failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			deliveryKind: &resource.DeliveryKind{
				ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				Code:        "updated_delivery_kind",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "host_000000000000",
				Disabled:    true,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "host_000000000000",
					Disabled:    true,
					Deleted:     true,
					CreateTime:  time.Time{},
					UpdateTime:  test.MakeTime("2000-01-01T00:00:00Z", t),
				},
			},
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedDeliveryKind: nil,
			expectedError:        fmt.Errorf("error retrieving delivery kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectSet(tt.cacheSetExpectation)

			db := test.NewDatabase(t)
			db.ExpectUpdate(tt.databaseUpdateExpectation)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)

			a := action.NewDeliveryKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			deliveryKind, err := a.Update(tt.id, tt.deliveryKind)

			assert.Equal(t, tt.expectedDeliveryKind, deliveryKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}

func TestDeliveryKindActionDelete(t *testing.T) {
	tests := map[string]struct {
		id                              uuid.UUID
		databaseRetrieveByIDExpectation *test.DatabaseRetrieveByIDExpectation
		databaseUpdateExpectation       *test.DatabaseUpdateExpectation
		cacheDelExpectation             *test.CacheDelExpectation
		expectedDeliveryKind            *resource.DeliveryKind
		expectedError                   error
	}{
		"when successful": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedResource:     test.ArbitraryDeliveryKind(t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "delivery_kind_000000000000",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "host_000000000000",
					Disabled:    false,
					Deleted:     true,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
			},
			expectedDeliveryKind: &resource.DeliveryKind{
				ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				Code:        "delivery_kind_000000000000",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "host_000000000000",
				Disabled:    false,
				Deleted:     true,
				CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
			},
			cacheDelExpectation: &test.CacheDelExpectation{
				ExpectedKey:    "deliveryKind_00000000-0000-0000-0002-000000000000",
				ReturnedResult: true,
			},
			expectedError: nil,
		},
		"when delivery kind failed to be found": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedError:        database.ErrInvalidPrimaryKey,
			},
			expectedDeliveryKind: nil,
			expectedError:        action.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedDeliveryKind: nil,
			expectedError:        fmt.Errorf("error retrieving delivery kind: arbitrary"),
		},
		"when delivery kind failed to be updated": {
			id: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
			databaseRetrieveByIDExpectation: &test.DatabaseRetrieveByIDExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource:     &resource.DeliveryKind{},
				ReturnedResource:     test.ArbitraryDeliveryKind(t),
			},
			databaseUpdateExpectation: &test.DatabaseUpdateExpectation{
				ExpectedResourceType: resource.TypeDeliveryKind,
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedResource: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "delivery_kind_000000000000",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "host_000000000000",
					Disabled:    false,
					Deleted:     true,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedDeliveryKind: nil,
			expectedError:        fmt.Errorf("error updating delivery kind: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			c := test.NewCache(t)
			c.ExpectDel(tt.cacheDelExpectation)

			db := test.NewDatabase(t)
			db.ExpectRetrieveByID(tt.databaseRetrieveByIDExpectation)
			db.ExpectUpdate(tt.databaseUpdateExpectation)

			a := action.NewDeliveryKindAction(c, db, test.NewClock("2000-01-01T00:00:00Z"))
			deliveryKind, err := a.Delete(tt.id)

			assert.Equal(t, tt.expectedDeliveryKind, deliveryKind)
			assert.Equal(t, tt.expectedError, err)

			c.AssertExpectations(t)
			db.AssertExpectations(t)
		})
	}
}
