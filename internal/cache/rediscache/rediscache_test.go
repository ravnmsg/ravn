//go:build !unit && !endtoend
// +build !unit,!endtoend

package rediscache_test

import (
	"context"
	"fmt"
	"os"
	"testing"

	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/cache/rediscache"
)

func TestGet(t *testing.T) {
	type testValue struct {
		Value int
	}

	var key = fmt.Sprintf("test_%s", uuid.New())
	var res testValue

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	c := connectCache(ctx, t)

	assert.False(t, c.Get(key, &res))

	assert.True(t, c.Set(key, testValue{Value: 1}))
	assert.True(t, c.Get(key, &res))

	assert.Equal(t, testValue{Value: 1}, res)
}

func TestSet(t *testing.T) {
	var key = fmt.Sprintf("test_%s", uuid.New())
	var res int

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	c := connectCache(ctx, t)

	assert.False(t, c.Get(key, &res))
	assert.True(t, c.Set(key, 1))

	assert.True(t, c.Get(key, &res))
	assert.Equal(t, 1, res)
}

func TestDel(t *testing.T) {
	var key = fmt.Sprintf("test_%s", uuid.New())
	var res interface{}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	c := connectCache(ctx, t)

	assert.False(t, c.Del(key))

	assert.True(t, c.Set(key, 1))
	assert.True(t, c.Get(key, res))
	assert.True(t, c.Del(key))

	assert.False(t, c.Get(key, res))
}

func connectCache(ctx context.Context, t *testing.T) *rediscache.Cache {
	c, err := rediscache.New(ctx, redisURI())
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	return c
}

func redisURI() string {
	host := os.Getenv("RAVN_REDIS_URI")
	if host == "" {
		host = "localhost:6379"
	}

	return host
}
