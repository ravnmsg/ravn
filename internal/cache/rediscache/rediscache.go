package rediscache

import (
	"bytes"
	"context"
	"encoding/gob"
	"fmt"
	"log"
	"time"

	"github.com/gomodule/redigo/redis"
	"gitlab.com/ravnmsg/ravn/internal/cache"
)

type Cache struct {
	conn       redis.Conn
	defaultTTL int
}

func New(ctx context.Context, uri string) (*Cache, error) {
	redisPool := &redis.Pool{
		MaxActive:   12000,
		MaxIdle:     80,
		IdleTimeout: 240 * time.Second,
		Dial:        func() (redis.Conn, error) { return redis.Dial("tcp", uri) },
	}

	conn, err := redisPool.GetContext(ctx)
	if err != nil {
		return nil, fmt.Errorf("error connecting to Redis: %s", err)
	}

	// Shutdowns the cache connection when the application context ends.
	go func() {
		<-ctx.Done()

		if err := conn.Close(); err != nil {
			log.Printf("Error closing connection to Redis: %s", err)
		}
	}()

	return &Cache{
		conn:       conn,
		defaultTTL: int((5 * time.Minute).Seconds()),
	}, nil
}

func (c *Cache) Get(key string, res interface{}) bool {
	reply, err := redis.Bytes(c.conn.Do("GET", key))
	if err == redis.ErrNil {
		return false
	} else if err != nil {
		log.Printf("Error retrieving key `%s` from cache: %s", key, err)
		return false
	}

	if err := gob.NewDecoder(bytes.NewReader(reply)).Decode(res); err != nil {
		log.Printf("Error saving key `%s` to cache: error decoding value: %s", key, err)
		return false
	}

	return true
}

func (c *Cache) Set(key string, res interface{}) bool {
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(res); err != nil {
		log.Printf("Error saving key `%s` to cache: error encoding value: %s", key, err)
		return false
	}

	args := []interface{}{key, buf.Bytes()}
	v, err := redis.Int(c.conn.Do("EXISTS", key))
	switch {
	case err != nil:
		log.Printf("Error saving key `%s` to cache: error looking up key: %s", key, err)
		return false
	case v == 0:
		// Key was not found in the cache -- set it with the full TTL.
		args = append(args, "EX", c.defaultTTL)
	default:
		// If key already exists, keeps the current TTL. This prevents the cache from rotting by
		// forcing all keys to expire.
		args = append(args, "KEEPTTL")
	}

	_, err = c.conn.Do("SET", args...)
	if err != nil {
		log.Printf("Error saving key `%s` to cache: %s", key, err)
		return false
	}

	return true
}

func (c *Cache) Del(key string) bool {
	v, err := redis.Int(c.conn.Do("DEL", key))
	if err != nil {
		log.Printf("Error deleting key `%s` from cache: %s", key, err)
		return false
	} else if v == 0 {
		return false
	}

	return true
}

var _ = cache.Cacher(new(Cache))
