package cache

// Cacher is the interface that wraps the basic Get, Set, and Del methods used to cache resources.
type Cacher interface {
	// Get retrieves the value associated with the specified key. If successful, res will contain
	// the cached value.
	Get(key string, res interface{}) bool

	// Set caches the value res and associates it with the specified key.
	Set(key string, res interface{}) bool

	// Del removes the specified key and associated value from the cache.
	Del(key string) bool
}
