//go:build !integration && !endtoend
// +build !integration,!endtoend

package retryer_test

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/retryer"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestRetrieveFailedMessages(t *testing.T) {
}

//nolint:gocritic // Keeping this one for future reference.
// func TestDueTimes(t *testing.T) {
// 	tests := map[string]struct {
// 		deliveryCount   int
// 		deliverTime     *time.Time
// 		expectedToBeDue bool
// 	}{
// 		"whenMessageHasFailedOnceButIsNotDueYet": {
// 			deliveryCount:   1,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T02:59:56Z", t),
// 			expectedToBeDue: false,
// 		},
// 		"whenMessageHasFailedOnceAndIsDue": {
// 			deliveryCount:   1,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T02:59:55Z", t),
// 			expectedToBeDue: true,
// 		},
// 		"whenMessageHasFailedTwiceButIsNotDueYet": {
// 			deliveryCount:   2,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T02:59:44Z", t),
// 			expectedToBeDue: false,
// 		},
// 		"whenMessageHasFailedTwiceAndIsDue": {
// 			deliveryCount:   2,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T02:59:43Z", t),
// 			expectedToBeDue: true,
// 		},
// 		"whenMessageHasFailedThreeTimesButIsNotDueYet": {
// 			deliveryCount:   3,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T02:58:56Z", t),
// 			expectedToBeDue: false,
// 		},
// 		"whenMessageHasFailedThreeTimesAndIsDue": {
// 			deliveryCount:   3,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T02:58:55Z", t),
// 			expectedToBeDue: true,
// 		},
// 		"whenMessageHasFailedFourTimesButIsNotDueYet": {
// 			deliveryCount:   4,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T02:55:44Z", t),
// 			expectedToBeDue: false,
// 		},
// 		"whenMessageHasFailedFourTimesAndIsDue": {
// 			deliveryCount:   4,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T02:55:43Z", t),
// 			expectedToBeDue: true,
// 		},
// 		"whenMessageHasFailedFiveTimesButIsNotDueYet": {
// 			deliveryCount:   5,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T02:42:56Z", t),
// 			expectedToBeDue: false,
// 		},
// 		"whenMessageHasFailedFiveTimesAndIsDue": {
// 			deliveryCount:   5,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T02:42:55Z", t),
// 			expectedToBeDue: true,
// 		},
// 		"whenMessageHasFailedSixTimesButIsNotDueYet": {
// 			deliveryCount:   6,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T00:51:44Z", t),
// 			expectedToBeDue: false,
// 		},
// 		"whenMessageHasFailedSixTimesAndIsDue": {
// 			deliveryCount:   6,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T00:51:43Z", t),
// 			expectedToBeDue: true,
// 		},
// 		"whenMessageHasFailedMoreThanSixTimes": {
// 			deliveryCount:   7,
// 			deliverTime:     test.MakeTimeRef("2000-01-01T00:00:00Z", t),
// 			expectedToBeDue: false,
// 		},
// 		"whenMessageHasFailedButDoesntHaveAnyDeliveries": {
// 			deliveryCount:   0,
// 			deliverTime:     nil,
// 			expectedToBeDue: true,
// 		},
// 	}

// 	for name, tt := range tests {
// 		tt := tt

// 		t.Run(name, func(t *testing.T) {
// 			dbResultAllExpt := &testadapter.ResultAllExpectation{
// 				ExpectedSlice: &[]*resource.Message{},
// 				ReturnedSlice: []*resource.Message{},
// 			}

// 			dbSelectorAllExpt := &testadapter.SelectorAllExpectation{
// 				ExpectedSlice: &[]*MessageMetadata{},
// 				ReturnedSlice: []*MessageMetadata{
// 					{ID: uuid.MustParse("00000000-0000-0000-0006-000000000000"), DCount: tt.deliveryCount, DTime: tt.deliverTime},
// 				},
// 			}

// 			sess := testadapter.NewSession(t)
// 			sess.Collection("messages").Find(db.Cond{"status": "scheduled"}).And("schedule_time >= NOW()").Select("id").(*testadapter.Result).ExpectAll(dbResultAllExpt)
// 			sess.SQL().Select("m.id", db.Raw("COUNT(d.message_id) AS dcount"), db.Raw("MAX(d.deliver_time) AS dtime")).From("messages m").LeftJoin("deliveries d ON m.id = d.message_id").Where("m.status = 'failed' AND m.create_time >= NOW() - INTERVAL '2h'").GroupBy("m.id").(*testadapter.Selector).ExpectAll(dbSelectorAllExpt)

// 			clock := test.NewClock("2000-01-01T03:00:00Z")
// 			scheduler := New(sess, nil, "", clock)
// 			ids, err := scheduler.RetrieveMessageIDs()
// 			if err != nil {
// 				t.Fatalf("unexpected error: %s", err)
// 			}

// 			var expectedIDs []uuid.UUID
// 			if tt.expectedToBeDue {
// 				expectedIDs = []uuid.UUID{uuid.MustParse("00000000-0000-0000-0006-000000000000")}
// 			} else {
// 				expectedIDs = []uuid.UUID{}
// 			}

// 			if !cmp.Equal(expectedIDs, ids) {
// 				t.Fatalf(
// 					"expected message ids to be different:\n%s",
// 					cmp.Diff(expectedIDs, ids),
// 				)
// 			}
// 		})
// 	}
// }

func TestDeliverMessage(t *testing.T) {
	tests := map[string]struct {
		message                                *resource.Message
		deliveryRetrieveByMessageIDExpectation *test.DeliveryActionRetrieveByMessageIDExpectation
		deliveryCreateExpectation              *test.DeliveryActionCreateExpectation
		deliveryQueueRequestExpectation        *test.DeliveryActionQueueRequestExpectation
		expectedError                          error
	}{
		"when successful": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "active",
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveByMessageIDExpectation: &test.DeliveryActionRetrieveByMessageIDExpectation{
				ExpectedMessageID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			deliveryCreateExpectation: &test.DeliveryActionCreateExpectation{
				ExpectedDelivery: &resource.Delivery{
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000001"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			deliveryQueueRequestExpectation: &test.DeliveryActionQueueRequestExpectation{
				ExpectedDeliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000001"),
				ReturnedError:      nil,
			},
			expectedError: nil,
		},
		"when message is not active": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "succeeded",
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			expectedError: fmt.Errorf("expected message to be active, but was `succeeded`"),
		},
		"when deliveries failed to be retrieved": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "active",
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveByMessageIDExpectation: &test.DeliveryActionRetrieveByMessageIDExpectation{
				ExpectedMessageID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedError:     test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error retrieving deliveries: arbitrary"),
		},
		"when message has no deliveries": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "active",
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveByMessageIDExpectation: &test.DeliveryActionRetrieveByMessageIDExpectation{
				ExpectedMessageID:  uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedDeliveries: []*resource.Delivery{},
			},
			expectedError: fmt.Errorf("expected at least one delivery on message"),
		},
		"when delivery failed to be created": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "active",
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveByMessageIDExpectation: &test.DeliveryActionRetrieveByMessageIDExpectation{
				ExpectedMessageID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			deliveryCreateExpectation: &test.DeliveryActionCreateExpectation{
				ExpectedDelivery: &resource.Delivery{
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error creating delivery: arbitrary"),
		},
		"when delivery failed to be queued": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "active",
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveByMessageIDExpectation: &test.DeliveryActionRetrieveByMessageIDExpectation{
				ExpectedMessageID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			deliveryCreateExpectation: &test.DeliveryActionCreateExpectation{
				ExpectedDelivery: &resource.Delivery{
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000001"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			deliveryQueueRequestExpectation: &test.DeliveryActionQueueRequestExpectation{
				ExpectedDeliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000001"),
				ReturnedError:      test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error queuing delivery: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			da := test.NewDeliveryAction(t)
			da.ExpectRetrieveByMessageID(tt.deliveryRetrieveByMessageIDExpectation)
			da.ExpectCreate(tt.deliveryCreateExpectation)
			da.ExpectQueueRequest(tt.deliveryQueueRequestExpectation)

			ret := retryer.New(da, nil, test.NewClock("2000-01-01T00:00:00Z"))
			err := ret.DeliverMessage(tt.message)

			assert.Equal(t, tt.expectedError, err)

			da.AssertExpectations(t)
		})
	}
}
