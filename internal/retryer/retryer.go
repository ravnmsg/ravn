package retryer

import (
	"fmt"
	"time"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// Retryer defines the service in charge of finding failed deliveries and trying them again.
type Retryer struct {
	da  action.DeliveryActioner
	ma  action.MessageActioner
	clk clock.Clocker
}

// New creates a Retryer instance.
func New(
	da action.DeliveryActioner,
	ma action.MessageActioner,
	clk clock.Clocker,
) *Retryer {
	return &Retryer{
		da:  da,
		ma:  ma,
		clk: clk,
	}
}

// retrieveFailedMessageIDs retrieves from the database all failed messages that were created in the
// last two hours, then calculates whether each one is due to be retried.
//
// Failed messages are retried on an exponential backoff schedule, waiting 4^i seconds with each
// retry. Messages that have been tried six times will be considered as permanently failed.
//
// The following table lists the wait time of each retry:
//
//      deliveries |    wait time    | cumul. wait time
//     -------------------------------------------------
//               0 |    1s,       1s |               1s
//               1 |    4s,       4s |               5s
//               2 |   16s,      16s |              21s
//               3 |   64s,    1m04s |             1m25
//               4 |  256s,    4m16s |             5m41
//               5 | 1024s,   17m04s |            22m45
//               6 | 4096s, 1h08m16s |          1h31m01
//
// Notes:
// * It should not happen for a failed message to have no deliveries. In this case, the message will
//   still be added to the list of messages to deliver.
// * The cumulative wait time column is an approximation. The Scheduler service is run periodically,
//   and the exact wait time cannot be calculated beforehand.
func (r *Retryer) RetrieveFailedMessages() ([]*resource.Message, error) {
	messages, err := r.ma.RetrievePotentiallyFailed(2 * time.Hour)
	if err != nil {
		return nil, err
	}

	expBackoffs := make([]time.Duration, 7)
	for i := range expBackoffs {
		expBackoffs[i] = 1 << (2 * i) * time.Second
	}

	// failedMessages := make([]*resource.Message, 0, len(messages))
	// for _, m := range messages {
	// A failing message will be delivered a maximum of 6 times.
	// if m.DCount > 6 {
	// 	continue
	// }

	// if m.DTime == nil || m.DTime.Before(r.clock.Now().Add(-expBackoffs[m.DCount])) {
	// failedMessages = append(failedMessages, m)
	// }
	// }

	return messages, nil
}

// DeliverMessage queues the message to be delivered.
func (r *Retryer) DeliverMessage(message *resource.Message) error {
	if message.Status != "active" {
		return fmt.Errorf("expected message to be active, but was `%s`", message.Status)
	}

	// Finds any of the message's deliveries as a base.
	deliveries, err := r.da.RetrieveByMessageID(message.ID)
	if err != nil {
		return fmt.Errorf("error retrieving deliveries: %s", err)
	} else if len(deliveries) == 0 {
		return fmt.Errorf("expected at least one delivery on message")
	}

	baseDelivery := deliveries[0]

	delivery := &resource.Delivery{
		MessageID:      message.ID,
		DeliveryKindID: baseDelivery.DeliveryKindID,
		LanguageTag:    baseDelivery.LanguageTag,
		Status:         "pending",
	}

	delivery, err = r.da.Create(delivery)
	if err != nil {
		return fmt.Errorf("error creating delivery: %s", err)
	}

	if err := r.da.QueueRequest(delivery.ID); err != nil {
		return fmt.Errorf("error queuing delivery: %s", err)
	}

	return nil
}
