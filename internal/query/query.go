package query

// Query defines a group of modifiers that can change the way resources are retrieved and displayed.
type Query struct {
	Filters []*Filter
	Orders  []*Order
	Page    *Page
}

// NewQuery creates a new query. Options can be specified to provide initial values to the query.
//
// The following example shows how different types of options can be used:
//
//     NewQuery(
//         WithFilters(
//             NewFilter("FieldName1", OpEqual, "operand1"),
//             NewFilter("FieldName2", OpNotEqual, "operand2"),
//         ),
//         WithOrder(NewOrder("FieldName1", DirAscending)),
//         WithOrder(NewOrder("FieldName2", DirDescending)),
//         WithPageArgs(10, 20),
//     )
func NewQuery(options ...Option) *Query {
	q := &Query{
		Filters: []*Filter{},
		Orders:  []*Order{},
		Page:    nil,
	}

	for _, o := range options {
		o(q)
	}

	return q
}

// AddFilters adds the specified filters to the query.
func (q *Query) AddFilters(filters ...*Filter) *Query {
	if len(filters) > 0 {
		q.Filters = append(q.Filters, filters...)
	}

	return q
}

// AddOrders adds the specified orders to the query.
func (q *Query) AddOrders(orders ...*Order) *Query {
	if len(orders) > 0 {
		q.Orders = append(q.Orders, orders...)
	}

	return q
}

// SetPage sets the specified page to the query. This will override the current page.
func (q *Query) SetPage(page *Page) *Query {
	q.Page = page
	return q
}

// Option defines a way to pass build options to a new query.
type Option func(q *Query)

// WithFilters adds the specified filters to the query.
func WithFilters(filters ...*Filter) Option {
	return func(q *Query) {
		q.AddFilters(filters...)
	}
}

// WithFilter adds the specified filter to the query.
func WithFilter(filter *Filter) Option {
	return WithFilters(filter)
}

// WithFilterArgs builds a new Filter instance composed of the specified fieldName, op, and operand
// arguments, and adds it to the query.
func WithFilterArgs(fieldName string, op Operator, operand interface{}) Option {
	f := NewFilter(fieldName, op, operand)
	return WithFilters(f)
}

// WithOrders adds the specified orders to the query.
func WithOrders(orders ...*Order) Option {
	return func(q *Query) {
		q.AddOrders(orders...)
	}
}

// WithOrder adds the specified order to the query.
func WithOrder(order *Order) Option {
	return WithOrders(order)
}

// WithOrderArgs builds a new Order instance composed of the specified fieldName and dir arguments,
// and adds it to the query.
func WithOrderArgs(fieldName string, dir Direction) Option {
	o := NewOrder(fieldName, dir)
	return WithOrders(o)
}

// WithPage sets the specified page to the query.
func WithPage(page *Page) Option {
	return func(q *Query) {
		q.SetPage(page)
	}
}

// WithPageArgs builds a new Page instance composed of the specified startIndex and capacity
// arguments, and adds it to the query.
func WithPageArgs(startIndex, capacity int) Option {
	return func(q *Query) {
		q.SetPage(NewPage(startIndex, capacity))
	}
}

// Filter defines a single filter.
type Filter struct {
	Operator  Operator
	FieldName string
	Operand   interface{}
}

// Operator defines a logical operator, eg. equal, greater than, or in.
type Operator string

// Enumeration of possible operators.
const (
	OpNone               = ""
	OpEqual              = "=="
	OpNotEqual           = "!="
	OpGreaterThan        = ">"
	OpGreaterThanOrEqual = ">="
	OpLessThan           = "<"
	OpLessThanOrEqual    = "<="
	OpIn                 = "IN"
)

// NewFilter creates a new filter composed of the specified fieldName, op, and operand arguments.
func NewFilter(fieldName string, op Operator, operand interface{}) *Filter {
	return &Filter{Operator: op, FieldName: fieldName, Operand: operand}
}

// Order defines a single order.
type Order struct {
	Direction Direction
	FieldName string
}

// Direction defines a direction of ordering.
type Direction string

// Enumeration of possible order directions.
const (
	DirNone       = ""
	DirAscending  = "asc"
	DirDescending = "desc"
)

// NewOrder creates an order composed of the specified fieldName and dir arguments.
func NewOrder(fieldName string, dir Direction) *Order {
	return &Order{Direction: dir, FieldName: fieldName}
}

// Page defines pagination configs in a collection of items.
type Page struct {
	StartIndex int
	Capacity   int
}

// NewPage creates a page composed of the specified startIndex and capacity arguments.
func NewPage(startIndex, capacity int) *Page {
	return &Page{StartIndex: startIndex, Capacity: capacity}
}
