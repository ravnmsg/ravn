//go:build !integration && !endtoend
// +build !integration,!endtoend

package query_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	. "gitlab.com/ravnmsg/ravn/internal/query"
)

func TestNew(t *testing.T) {
	tests := map[string]struct {
		options        []Option
		expectedResult *Query
	}{
		"when no options are provided": {
			options: []Option{},
			expectedResult: &Query{
				Filters: []*Filter{},
				Orders:  []*Order{},
				Page:    nil,
			},
		},
		"when filters are provided": {
			options: []Option{
				WithFilters(
					NewFilter("FieldName1", OpEqual, "operand1"),
					NewFilter("FieldName2", OpNotEqual, "operand2"),
				),
			},
			expectedResult: &Query{
				Filters: []*Filter{
					{Operator: OpEqual, FieldName: "FieldName1", Operand: "operand1"},
					{Operator: OpNotEqual, FieldName: "FieldName2", Operand: "operand2"},
				},
				Orders: []*Order{},
				Page:   nil,
			},
		},
		"when a filter is provided": {
			options: []Option{
				WithFilter(NewFilter("FieldName", OpEqual, "operand")),
			},
			expectedResult: &Query{
				Filters: []*Filter{
					{Operator: OpEqual, FieldName: "FieldName", Operand: "operand"},
				},
				Orders: []*Order{},
				Page:   nil,
			},
		},
		"when filter arguments are provided": {
			options: []Option{
				WithFilterArgs("FieldName", OpEqual, "operand"),
			},
			expectedResult: &Query{
				Filters: []*Filter{
					{Operator: OpEqual, FieldName: "FieldName", Operand: "operand"},
				},
				Orders: []*Order{},
				Page:   nil,
			},
		},
		"when orders are provided": {
			options: []Option{
				WithOrders(
					NewOrder("FieldName1", DirAscending),
					NewOrder("FieldName2", DirDescending),
				),
			},
			expectedResult: &Query{
				Filters: []*Filter{},
				Orders: []*Order{
					{Direction: DirAscending, FieldName: "FieldName1"},
					{Direction: DirDescending, FieldName: "FieldName2"},
				},
				Page: nil,
			},
		},
		"when an order is provided": {
			options: []Option{
				WithOrder(NewOrder("FieldName", DirAscending)),
			},
			expectedResult: &Query{
				Filters: []*Filter{},
				Orders: []*Order{
					{Direction: DirAscending, FieldName: "FieldName"},
				},
				Page: nil,
			},
		},
		"when order arguments are provided": {
			options: []Option{
				WithOrderArgs("FieldName", DirAscending),
			},
			expectedResult: &Query{
				Filters: []*Filter{},
				Orders: []*Order{
					{Direction: DirAscending, FieldName: "FieldName"},
				},
				Page: nil,
			},
		},
		"when a page is provided": {
			options: []Option{
				WithPage(NewPage(10, 20)),
			},
			expectedResult: &Query{
				Filters: []*Filter{},
				Orders:  []*Order{},
				Page:    &Page{StartIndex: 10, Capacity: 20},
			},
		},
		"when page arguments are provided": {
			options: []Option{
				WithPageArgs(10, 20),
			},
			expectedResult: &Query{
				Filters: []*Filter{},
				Orders:  []*Order{},
				Page:    &Page{StartIndex: 10, Capacity: 20},
			},
		},
		"when multiple options are provided": {
			options: []Option{
				WithFilters(
					NewFilter("FieldName1", OpEqual, "operand1"),
					NewFilter("FieldName2", OpNotEqual, "operand2"),
				),
				WithOrder(NewOrder("FieldName1", DirAscending)),
				WithOrder(NewOrder("FieldName2", DirDescending)),
				WithPageArgs(10, 20),
			},
			expectedResult: &Query{
				Filters: []*Filter{
					{Operator: OpEqual, FieldName: "FieldName1", Operand: "operand1"},
					{Operator: OpNotEqual, FieldName: "FieldName2", Operand: "operand2"},
				},
				Orders: []*Order{
					{Direction: DirAscending, FieldName: "FieldName1"},
					{Direction: DirDescending, FieldName: "FieldName2"},
				},
				Page: &Page{StartIndex: 10, Capacity: 20},
			},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			q := NewQuery(tt.options...)
			assert.Equal(t, tt.expectedResult, q)
		})
	}
}

func TestAddFilters(t *testing.T) {
	q := NewQuery(WithFilterArgs("CurrentAttr", OpEqual, "current operand"))

	filters := []*Filter{
		{Operator: OpEqual, FieldName: "Attr1", Operand: "operand1"},
		{Operator: OpNotEqual, FieldName: "Attr2", Operand: "operand2"},
	}

	q.AddFilters(filters...)

	assert.Equal(t, 3, len(q.Filters))

	expectedFilters := []*Filter{
		{Operator: OpEqual, FieldName: "CurrentAttr", Operand: "current operand"},
		{Operator: OpEqual, FieldName: "Attr1", Operand: "operand1"},
		{Operator: OpNotEqual, FieldName: "Attr2", Operand: "operand2"},
	}

	assert.Equal(t, expectedFilters, q.Filters)
}

func TestAddOrders(t *testing.T) {
	q := NewQuery(WithOrderArgs("CurrentAttr", DirAscending))

	orders := []*Order{
		{Direction: DirAscending, FieldName: "Attr1"},
		{Direction: DirDescending, FieldName: "Attr2"},
	}

	q.AddOrders(orders...)

	assert.Equal(t, 3, len(q.Orders))

	expectedOrders := []*Order{
		{Direction: DirAscending, FieldName: "CurrentAttr"},
		{Direction: DirAscending, FieldName: "Attr1"},
		{Direction: DirDescending, FieldName: "Attr2"},
	}

	assert.Equal(t, expectedOrders, q.Orders)
}

func TestSetPage(t *testing.T) {
	q := NewQuery(WithPageArgs(4, 2))

	page := &Page{StartIndex: 20, Capacity: 10}

	q.SetPage(page)

	expectedPage := &Page{StartIndex: 20, Capacity: 10}

	assert.Equal(t, expectedPage, q.Page)
}

func TestNewFilter(t *testing.T) {
	tests := map[string]struct {
		fieldName      string
		operator       Operator
		operand        interface{}
		expectedFilter *Filter
	}{
		"when successful": {
			fieldName:      "FieldName",
			operator:       OpEqual,
			operand:        "operand",
			expectedFilter: &Filter{Operator: OpEqual, FieldName: "FieldName", Operand: "operand"},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			f := NewFilter(tt.fieldName, tt.operator, tt.operand)
			assert.Equal(t, tt.expectedFilter, f)
		})
	}
}

func TestNewOrder(t *testing.T) {
	tests := map[string]struct {
		fieldName     string
		direction     Direction
		expectedOrder *Order
	}{
		"when successful": {
			fieldName:     "FieldName",
			direction:     DirAscending,
			expectedOrder: &Order{Direction: DirAscending, FieldName: "FieldName"},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			o := NewOrder(tt.fieldName, tt.direction)
			assert.Equal(t, tt.expectedOrder, o)
		})
	}
}

func TestNewPage(t *testing.T) {
	tests := map[string]struct {
		startIndex   int
		capacity     int
		expectedPage *Page
	}{
		"when successful": {
			startIndex:   20,
			capacity:     10,
			expectedPage: &Page{StartIndex: 20, Capacity: 10},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			p := NewPage(tt.startIndex, tt.capacity)
			assert.Equal(t, tt.expectedPage, p)
		})
	}
}

func TestOperatorString(t *testing.T) {
	tests := map[string]struct {
		operator       Operator
		expectedResult string
	}{
		"when operator is equal": {
			operator:       OpEqual,
			expectedResult: "==",
		},
		"when operator is not equal": {
			operator:       OpNotEqual,
			expectedResult: "!=",
		},
		"when operator is greater than": {
			operator:       OpGreaterThan,
			expectedResult: ">",
		},
		"when operator is greater than or equal": {
			operator:       OpGreaterThanOrEqual,
			expectedResult: ">=",
		},
		"when operator is less than": {
			operator:       OpLessThan,
			expectedResult: "<",
		},
		"when operator is less than or equal": {
			operator:       OpLessThanOrEqual,
			expectedResult: "<=",
		},
		"when operator is in": {
			operator:       OpIn,
			expectedResult: "IN",
		},
		"when operator is invalid": {
			operator:       OpNone,
			expectedResult: "",
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			result := string(tt.operator)
			assert.Equal(t, tt.expectedResult, result)
		})
	}
}

func TestDirectionString(t *testing.T) {
	tests := map[string]struct {
		direction      Direction
		expectedResult string
	}{
		"when direction is ascending": {
			direction:      DirAscending,
			expectedResult: "asc",
		},
		"when direction is descending": {
			direction:      DirDescending,
			expectedResult: "desc",
		},
		"when direction is invalid": {
			direction:      DirNone,
			expectedResult: "",
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			result := string(tt.direction)
			assert.Equal(t, tt.expectedResult, result)
		})
	}
}
