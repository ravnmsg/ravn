package test

import (
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type MessageAction struct {
	mock.Mock
	t *testing.T
}

type MessageActionRetrieveManyExpectation struct {
	ExpectedQuery    *query.Query
	ReturnedMessages []*resource.Message
	ReturnedError    error
}

type MessageActionRetrieveByIDExpectation struct {
	ExpectedID      uuid.UUID
	ReturnedMessage *resource.Message
	ReturnedError   error
}

type MessageActionRetrievePotentiallyFailedExpectation struct {
	ExpectedSince    time.Duration
	ReturnedMessages []*resource.Message
	ReturnedError    error
}

type MessageActionCreateExpectation struct {
	ExpectedMessage *resource.Message
	ReturnedMessage *resource.Message
	ReturnedError   error
}

type MessageActionUpdateExpectation struct {
	ExpectedID      uuid.UUID
	ExpectedMessage *resource.Message
	ReturnedMessage *resource.Message
	ReturnedError   error
}

type MessageActionDeleteExpectation struct {
	ExpectedID    uuid.UUID
	ReturnedError error
}

func NewMessageAction(t *testing.T) *MessageAction {
	return &MessageAction{
		t: t,
	}
}

func (a *MessageAction) RetrieveMany(q *query.Query) ([]*resource.Message, error) {
	args := a.Called(q)
	return args.Get(0).([]*resource.Message), args.Error(1)
}

func (a *MessageAction) RetrieveByID(id uuid.UUID) (*resource.Message, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.Message), args.Error(1)
}

func (a *MessageAction) RetrievePotentiallyFailed(since time.Duration) ([]*resource.Message, error) {
	args := a.Called(since)
	return args.Get(0).([]*resource.Message), args.Error(1)
}

func (a *MessageAction) Create(message *resource.Message) (*resource.Message, error) {
	args := a.Called(message)

	returnedMessage, ok := args.Get(0).(*resource.Message)
	if (!ok || returnedMessage == nil) && args.Error(1) == nil {
		return message, nil
	}

	return returnedMessage, args.Error(1)
}

func (a *MessageAction) Update(id uuid.UUID, message *resource.Message) (*resource.Message, error) {
	args := a.Called(id, message)

	returnedMessage, ok := args.Get(0).(*resource.Message)
	if (!ok || returnedMessage == nil) && args.Error(1) == nil {
		return message, nil
	}

	return returnedMessage, args.Error(1)
}

func (a *MessageAction) Delete(id uuid.UUID) error {
	args := a.Called(id)
	return args.Error(0)
}

func (a *MessageAction) ExpectRetrieveMany(e *MessageActionRetrieveManyExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveMany",
		e.ExpectedQuery,
	).Return(
		e.ReturnedMessages,
		e.ReturnedError,
	).Once()
}

func (a *MessageAction) ExpectRetrieveByID(e *MessageActionRetrieveByIDExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveByID",
		e.ExpectedID,
	).Return(
		e.ReturnedMessage,
		e.ReturnedError,
	).Once()
}

func (a *MessageAction) ExpectRetrievePotentiallyFailed(e *MessageActionRetrievePotentiallyFailedExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrievePotentiallyFailed",
		e.ExpectedSince,
	).Return(
		e.ReturnedMessages,
		e.ReturnedError,
	).Once()
}

func (a *MessageAction) ExpectCreate(e *MessageActionCreateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Create",
		e.ExpectedMessage,
	).Return(
		e.ReturnedMessage,
		e.ReturnedError,
	).Once()
}

func (a *MessageAction) ExpectUpdate(e *MessageActionUpdateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Update",
		e.ExpectedID,
		e.ExpectedMessage,
	).Return(
		e.ReturnedMessage,
		e.ReturnedError,
	).Once()
}

func (a *MessageAction) ExpectDelete(e *MessageActionDeleteExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Delete",
		e.ExpectedID,
	).Return(
		e.ReturnedError,
	).Once()
}

var _ = action.MessageActioner(new(MessageAction))
