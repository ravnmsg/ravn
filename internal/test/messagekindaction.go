package test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type MessageKindAction struct {
	mock.Mock
	t *testing.T
}

type MessageKindActionRetrieveManyExpectation struct {
	ExpectedQuery        *query.Query
	ReturnedMessageKinds []*resource.MessageKind
	ReturnedError        error
}

type MessageKindActionRetrieveByIDExpectation struct {
	ExpectedID          uuid.UUID
	ReturnedMessageKind *resource.MessageKind
	ReturnedError       error
}

type MessageKindActionRetrieveByIDOrCodeExpectation struct {
	ExpectedIDOrCode    string
	ReturnedMessageKind *resource.MessageKind
	ReturnedError       error
}

type MessageKindActionCreateExpectation struct {
	ExpectedMessageKind *resource.MessageKind
	ReturnedMessageKind *resource.MessageKind
	ReturnedError       error
}

type MessageKindActionUpdateExpectation struct {
	ExpectedID          uuid.UUID
	ExpectedMessageKind *resource.MessageKind
	ReturnedMessageKind *resource.MessageKind
	ReturnedError       error
}

type MessageKindActionDeleteExpectation struct {
	ExpectedID          uuid.UUID
	ReturnedMessageKind *resource.MessageKind
	ReturnedError       error
}

func NewMessageKindAction(t *testing.T) *MessageKindAction {
	return &MessageKindAction{
		t: t,
	}
}

func (a *MessageKindAction) RetrieveMany(q *query.Query) ([]*resource.MessageKind, error) {
	args := a.Called(q)
	return args.Get(0).([]*resource.MessageKind), args.Error(1)
}

func (a *MessageKindAction) RetrieveByID(id uuid.UUID) (*resource.MessageKind, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.MessageKind), args.Error(1)
}

func (a *MessageKindAction) RetrieveByIDOrCode(idOrCode string) (*resource.MessageKind, error) {
	args := a.Called(idOrCode)
	return args.Get(0).(*resource.MessageKind), args.Error(1)
}

func (a *MessageKindAction) Create(messageKind *resource.MessageKind) (*resource.MessageKind, error) {
	args := a.Called(messageKind)

	returnedMessageKind, ok := args.Get(0).(*resource.MessageKind)
	if (!ok || returnedMessageKind == nil) && args.Error(1) == nil {
		return messageKind, nil
	}

	return returnedMessageKind, args.Error(1)
}

func (a *MessageKindAction) Update(id uuid.UUID, messageKind *resource.MessageKind) (*resource.MessageKind, error) {
	args := a.Called(id, messageKind)

	returnedMessageKind, ok := args.Get(0).(*resource.MessageKind)
	if (!ok || returnedMessageKind == nil) && args.Error(1) == nil {
		return messageKind, nil
	}

	return returnedMessageKind, args.Error(1)
}

func (a *MessageKindAction) Delete(id uuid.UUID) (*resource.MessageKind, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.MessageKind), args.Error(1)
}

func (a *MessageKindAction) ExpectRetrieveMany(e *MessageKindActionRetrieveManyExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveMany",
		e.ExpectedQuery,
	).Return(
		e.ReturnedMessageKinds,
		e.ReturnedError,
	).Once()
}

func (a *MessageKindAction) ExpectRetrieveByID(e *MessageKindActionRetrieveByIDExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveByID",
		e.ExpectedID,
	).Return(
		e.ReturnedMessageKind,
		e.ReturnedError,
	).Once()
}

func (a *MessageKindAction) ExpectRetrieveByIDOrCode(e *MessageKindActionRetrieveByIDOrCodeExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveByIDOrCode",
		e.ExpectedIDOrCode,
	).Return(
		e.ReturnedMessageKind,
		e.ReturnedError,
	).Once()
}

func (a *MessageKindAction) ExpectCreate(e *MessageKindActionCreateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Create",
		e.ExpectedMessageKind,
	).Return(
		e.ReturnedMessageKind,
		e.ReturnedError,
	).Once()
}

func (a *MessageKindAction) ExpectUpdate(e *MessageKindActionUpdateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Update",
		e.ExpectedID,
		e.ExpectedMessageKind,
	).Return(
		e.ReturnedMessageKind,
		e.ReturnedError,
	).Once()
}

func (a *MessageKindAction) ExpectDelete(e *MessageKindActionDeleteExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Delete",
		e.ExpectedID,
	).Return(
		e.ReturnedMessageKind,
		e.ReturnedError,
	).Once()
}

var _ = action.MessageKindActioner(new(MessageKindAction))
