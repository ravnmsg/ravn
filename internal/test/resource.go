package test

import (
	"testing"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/resource"
)

func ArbitraryNamespace(t *testing.T) *resource.Namespace {
	return &resource.Namespace{
		ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
		Code:       "namespace_000000000001",
		ParentID:   MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
		Deleted:    false,
		CreateTime: MakeTime("2000-01-01T01:01:58Z", t),
		UpdateTime: MakeTime("2000-01-01T01:01:59Z", t),
	}
}

func ArbitraryDeliveryKind(t *testing.T) *resource.DeliveryKind {
	return &resource.DeliveryKind{
		ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
		Code:        "delivery_kind_000000000000",
		NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
		Host:        "host_000000000000",
		Disabled:    false,
		Deleted:     false,
		CreateTime:  MakeTime("2000-01-01T02:00:58Z", t),
		UpdateTime:  MakeTime("2000-01-01T02:00:59Z", t),
	}
}

func ArbitraryMessageKind(t *testing.T) *resource.MessageKind {
	return &resource.MessageKind{
		ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
		Code:        "message_kind_000000000000",
		NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
		Disabled:    false,
		Deleted:     false,
		CreateTime:  MakeTime("2000-01-01T03:00:58Z", t),
		UpdateTime:  MakeTime("2000-01-01T03:00:59Z", t),
	}
}

func ArbitraryTemplateKind(t *testing.T) *resource.TemplateKind {
	return &resource.TemplateKind{
		ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
		DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
		TargetAttr:     "target_attr_000000000000",
		Deleted:        false,
		CreateTime:     MakeTime("2000-01-01T04:00:58Z", t),
		UpdateTime:     MakeTime("2000-01-01T04:00:59Z", t),
	}
}

func ArbitraryTemplate(t *testing.T) *resource.Template {
	return &resource.Template{
		ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
		MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
		TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
		LanguageTag:    "language_tag_000000000000",
		Body:           "body_000000000000",
		Deleted:        false,
		CreateTime:     MakeTime("2000-01-01T05:00:58Z", t),
		UpdateTime:     MakeTime("2000-01-01T05:00:59Z", t),
	}
}

func ArbitraryMessage(t *testing.T) *resource.Message {
	return &resource.Message{
		ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
		MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
		Input: map[string]interface{}{
			"key": "value_000000000000",
		},
		Status:       "succeeded",
		ScheduleTime: MakeTimeRef("2000-01-01T06:00:00Z", t),
		CreateTime:   MakeTime("2000-01-01T06:00:58Z", t),
		UpdateTime:   MakeTime("2000-01-01T06:00:59Z", t),
	}
}

func ArbitraryDelivery(t *testing.T) *resource.Delivery {
	return &resource.Delivery{
		ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
		MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
		DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
		LanguageTag:    "language_tag_000000000000",
		Status:         "delivered",
		StatusMessage:  "status_message_000000000000",
		DeliverTime:    MakeTimeRef("2000-01-01T07:00:00Z", t),
		Input:          map[string]interface{}{"input_key": "input_value_000000000000"},
		Output:         map[string]interface{}{"output_key": "output_value_000000000000"},
		ExternalID:     "external_id_000000000000",
		CreateTime:     MakeTime("2000-01-01T07:00:58Z", t),
		UpdateTime:     MakeTime("2000-01-01T07:00:59Z", t),
	}
}
