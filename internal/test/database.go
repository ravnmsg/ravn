package test

import (
	"reflect"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"

	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// Database defines a mock of a database.
type Database struct {
	mock.Mock
	t *testing.T
}

type DatabaseCountExpectation struct {
	ExpectedResourceType resource.Type
	ExpectedFilters      []*query.Filter
	ReturnedResponse     int
	ReturnedError        error
}

type DatabaseRetrieveManyExpectation struct {
	ExpectedResourceType resource.Type
	ExpectedQuery        *query.Query
	ExpectedResources    interface{}
	ReturnedResources    interface{}
	ReturnedError        error
}

type DatabaseRetrieveByIDExpectation struct {
	ExpectedResourceType resource.Type
	ExpectedID           uuid.UUID
	ExpectedResource     interface{}
	ReturnedResource     interface{}
	ReturnedError        error
}

type DatabaseRetrieveByIDOrCodeExpectation struct {
	ExpectedResourceType resource.Type
	ExpectedIDOrCode     string
	ExpectedResource     interface{}
	ReturnedResource     interface{}
	ReturnedError        error
}

type DatabaseCreateExpectation struct {
	ExpectedResourceType resource.Type
	ExpectedResource     interface{}
	ReturnedID           uuid.UUID
	ReturnedError        error
}

type DatabaseUpdateExpectation struct {
	ExpectedResourceType resource.Type
	ExpectedID           uuid.UUID
	ExpectedResource     interface{}
	ReturnedError        error
}

type DatabaseDeleteExpectation struct {
	ExpectedResourceType resource.Type
	ExpectedID           uuid.UUID
	ReturnedError        error
}

// NewDatabase defines a mock of a database.
func NewDatabase(t *testing.T) *Database {
	return &Database{
		t: t,
	}
}

// Count mocks the Count function.
func (db *Database) Count(rt resource.Type, filters []*query.Filter) (int, error) {
	args := db.Called(rt, filters)

	if err := args.Error(1); err != nil {
		return 0, err
	}

	return args.Int(0), nil
}

// RetrieveMany mocks the RetrieveMany function.
func (db *Database) RetrieveMany(rt resource.Type, q *query.Query, resources interface{}) error {
	args := db.Called(rt, q, resources)

	if err := args.Error(1); err != nil {
		return err
	}

	dstv := reflect.ValueOf(resources)

	if dstv.IsNil() || dstv.Kind() != reflect.Ptr || dstv.Elem().Kind() != reflect.Slice {
		db.t.Errorf("unexpected error: RetrieveMany: slice pointer expected")
		return nil
	}

	returnedValue := reflect.ValueOf(args.Get(0))
	dstv.Elem().Set(returnedValue)

	return nil
}

// Retrieve mocks the Retrieve function.
func (db *Database) RetrieveByID(rt resource.Type, id uuid.UUID, res interface{}) error {
	args := db.Called(rt, id, res)

	if err := args.Error(1); err != nil {
		return err
	}

	dstv := reflect.ValueOf(res)

	if dstv.IsNil() || dstv.Kind() != reflect.Ptr {
		db.t.Errorf("unexpected error: RetrieveByID: slice pointer expected")
		return nil
	}

	itemV := dstv.Elem()
	itemT := itemV.Type()

	returnedValue := reflect.ValueOf(args.Get(0))

	if itemT.Kind() == reflect.Ptr {
		itemV.Set(returnedValue)
	} else {
		itemV.Set(reflect.Indirect(returnedValue))
	}

	return nil
}

func (db *Database) RetrieveByIDOrCode(rt resource.Type, idOrCode string, res interface{}) error {
	args := db.Called(rt, idOrCode, res)

	if err := args.Error(1); err != nil {
		return err
	}

	dstv := reflect.ValueOf(res)

	if dstv.IsNil() || dstv.Kind() != reflect.Ptr {
		db.t.Errorf("unexpected error: RetrieveByIDOrCode: slice pointer expected")
		return nil
	}

	itemV := dstv.Elem()
	itemT := itemV.Type()

	returnedValue := reflect.ValueOf(args.Get(0))

	if itemT.Kind() == reflect.Ptr {
		itemV.Set(returnedValue)
	} else {
		itemV.Set(reflect.Indirect(returnedValue))
	}

	return nil
}

// Create mocks the Create function.
func (db *Database) Create(rt resource.Type, res resource.Resource) (uuid.UUID, error) {
	args := db.Called(rt, res)

	if err := args.Error(1); err != nil {
		return uuid.Nil, err
	}

	return args.Get(0).(uuid.UUID), nil
}

// Update mocks the Update function.
func (db *Database) Update(rt resource.Type, id uuid.UUID, res resource.Resource) error {
	args := db.Called(rt, id, res)

	if err := args.Error(0); err != nil {
		return err
	}

	return nil
}

// Delete mocks the Delete function.
func (db *Database) Delete(rt resource.Type, id uuid.UUID) error {
	args := db.Called(rt, id)

	if err := args.Error(0); err != nil {
		return err
	}

	return nil
}

// ExpectCount adds the mock expectation.
func (db *Database) ExpectCount(e *DatabaseCountExpectation) {
	if e == nil {
		return
	}

	db.On(
		"Count",
		e.ExpectedResourceType,
		e.ExpectedFilters,
	).Return(
		e.ReturnedResponse,
		e.ReturnedError,
	).Once()
}

// ExpectRetrieveMany adds the mock expectation.
func (db *Database) ExpectRetrieveMany(e *DatabaseRetrieveManyExpectation) {
	if e == nil {
		return
	}

	db.On(
		"RetrieveMany",
		e.ExpectedResourceType,
		e.ExpectedQuery,
		e.ExpectedResources,
	).Return(
		e.ReturnedResources,
		e.ReturnedError,
	).Once()
}

// ExpectRetrieve adds the mock expectation.
func (db *Database) ExpectRetrieveByID(e *DatabaseRetrieveByIDExpectation) {
	if e == nil {
		return
	}

	db.On(
		"RetrieveByID",
		e.ExpectedResourceType,
		e.ExpectedID,
		e.ExpectedResource,
	).Return(
		e.ReturnedResource,
		e.ReturnedError,
	).Once()
}

func (db *Database) ExpectRetrieveByIDOrCode(e *DatabaseRetrieveByIDOrCodeExpectation) {
	if e == nil {
		return
	}

	db.On(
		"RetrieveByIDOrCode",
		e.ExpectedResourceType,
		e.ExpectedIDOrCode,
		e.ExpectedResource,
	).Return(
		e.ReturnedResource,
		e.ReturnedError,
	).Once()
}

// ExpectCreate adds the mock expectation.
func (db *Database) ExpectCreate(e *DatabaseCreateExpectation) {
	if e == nil {
		return
	}

	db.On(
		"Create",
		e.ExpectedResourceType,
		e.ExpectedResource,
	).Return(
		e.ReturnedID,
		e.ReturnedError,
	).Once()
}

// ExpectUpdate adds the mock expectation.
func (db *Database) ExpectUpdate(e *DatabaseUpdateExpectation) {
	if e == nil {
		return
	}

	db.On(
		"Update",
		e.ExpectedResourceType,
		e.ExpectedID,
		e.ExpectedResource,
	).Return(
		e.ReturnedError,
	).Once()
}

// ExpectDelete adds the mock expectation.
func (db *Database) ExpectDelete(e *DatabaseDeleteExpectation) {
	if e == nil {
		return
	}

	db.On(
		"Delete",
		e.ExpectedResourceType,
		e.ExpectedID,
	).Return(
		e.ReturnedError,
	).Once()
}

var _ = database.ReadWriter(new(Database))
