package test

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type Permission struct {
	NamespaceID  uuid.UUID
	ResourceType resource.Type
	Operation    string
}

type Authorizer struct {
	permissions []Permission
}

func NewAuthorizer(permissions []Permission) *Authorizer {
	return &Authorizer{
		permissions: permissions,
	}
}

func (a *Authorizer) AuthorizedNamespaceIDs(_ context.Context, rt resource.Type, op string) ([]uuid.UUID, error) {
	namespaceIDs := make([]uuid.UUID, 0)
	for _, p := range a.permissions {
		if p.ResourceType == rt && p.Operation == op {
			namespaceIDs = append(namespaceIDs, p.NamespaceID)
		}
	}

	return namespaceIDs, nil
}

func (a *Authorizer) AuthorizeOperation(_ context.Context, namespaceID uuid.UUID, rt resource.Type, op string) error {
	for _, p := range a.permissions {
		if p.NamespaceID == namespaceID && p.ResourceType == rt && p.Operation == op {
			return nil
		}
	}

	return authorizer.ErrUnauthorized
}

var _ = authorizer.Authorizer(new(Authorizer))
