package test

import (
	"testing"

	"github.com/stretchr/testify/mock"

	"gitlab.com/ravnmsg/ravn/internal/msgqueue"
	"gitlab.com/ravnmsg/ravn/internal/msgqueue/handler"
)

// MSGQueue defines a mock of a message queue.
type MSGQueue struct {
	mock.Mock
	t *testing.T
}

type MSGQueuePublishExpectation struct {
	ExpectedData  []byte
	ReturnedError error
}

type MSGQueueSubscribeExpectation struct {
	ExpectedHandler handler.MSGHandlerFunc
	ReturnedError   error
}

// NewMSGQueue creates a new message queue usable during tests.
func NewMSGQueue(t *testing.T) *MSGQueue {
	return &MSGQueue{
		t: t,
	}
}

// Publish mocks the Publish function.
func (mq *MSGQueue) Publish(data []byte) error {
	args := mq.Called(data)
	return args.Error(0)
}

// Subscribe mocks the Subscribe function.
func (mq *MSGQueue) Subscribe(h handler.MSGHandlerFunc) error {
	args := mq.Called(h)
	return args.Error(0)
}

// ExpectPublish adds the mock expectation.
func (mq *MSGQueue) ExpectPublish(e *MSGQueuePublishExpectation) {
	if e == nil {
		return
	}

	mq.On(
		"Publish",
		e.ExpectedData,
	).Return(
		e.ReturnedError,
	).Once()
}

// ExpectSubscribe adds the mock expectation.
func (mq *MSGQueue) ExpectSubscribe(e *MSGQueueSubscribeExpectation) {
	if e == nil {
		return
	}

	mq.On(
		"Subscribe",
		e.ExpectedHandler,
	).Return(
		e.ReturnedError,
	).Once()
}

var _ = msgqueue.Publisher(new(MSGQueue))
var _ = msgqueue.Subscriber(new(MSGQueue))
