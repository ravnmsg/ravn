package test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type TemplateAction struct {
	mock.Mock
	t *testing.T
}

type TemplateActionRetrieveManyExpectation struct {
	ExpectedQuery     *query.Query
	ReturnedTemplates []*resource.Template
	ReturnedError     error
}

type TemplateActionRetrieveByIDExpectation struct {
	ExpectedID       uuid.UUID
	ReturnedTemplate *resource.Template
	ReturnedError    error
}

type TemplateActionCreateExpectation struct {
	ExpectedTemplate *resource.Template
	ReturnedTemplate *resource.Template
	ReturnedError    error
}

type TemplateActionUpdateExpectation struct {
	ExpectedID       uuid.UUID
	ExpectedTemplate *resource.Template
	ReturnedTemplate *resource.Template
	ReturnedError    error
}

type TemplateActionDeleteExpectation struct {
	ExpectedID       uuid.UUID
	ReturnedTemplate *resource.Template
	ReturnedError    error
}

func NewTemplateAction(t *testing.T) *TemplateAction {
	return &TemplateAction{
		t: t,
	}
}

func (a *TemplateAction) RetrieveMany(q *query.Query) ([]*resource.Template, error) {
	args := a.Called(q)
	return args.Get(0).([]*resource.Template), args.Error(1)
}

func (a *TemplateAction) RetrieveByID(id uuid.UUID) (*resource.Template, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.Template), args.Error(1)
}

func (a *TemplateAction) Create(template *resource.Template) (*resource.Template, error) {
	args := a.Called(template)

	returnedTemplate, ok := args.Get(0).(*resource.Template)
	if (!ok || returnedTemplate == nil) && args.Error(1) == nil {
		return template, nil
	}

	return returnedTemplate, args.Error(1)
}

func (a *TemplateAction) Update(id uuid.UUID, template *resource.Template) (*resource.Template, error) {
	args := a.Called(id, template)

	returnedTemplate, ok := args.Get(0).(*resource.Template)
	if (!ok || returnedTemplate == nil) && args.Error(1) == nil {
		return template, nil
	}

	return returnedTemplate, args.Error(1)
}

func (a *TemplateAction) Delete(id uuid.UUID) (*resource.Template, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.Template), args.Error(1)
}

func (a *TemplateAction) ExpectRetrieveMany(e *TemplateActionRetrieveManyExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveMany",
		e.ExpectedQuery,
	).Return(
		e.ReturnedTemplates,
		e.ReturnedError,
	).Once()
}

func (a *TemplateAction) ExpectRetrieveByID(e *TemplateActionRetrieveByIDExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveByID",
		e.ExpectedID,
	).Return(
		e.ReturnedTemplate,
		e.ReturnedError,
	).Once()
}

func (a *TemplateAction) ExpectCreate(e *TemplateActionCreateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Create",
		e.ExpectedTemplate,
	).Return(
		e.ReturnedTemplate,
		e.ReturnedError,
	).Once()
}

func (a *TemplateAction) ExpectUpdate(e *TemplateActionUpdateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Update",
		e.ExpectedID,
		e.ExpectedTemplate,
	).Return(
		e.ReturnedTemplate,
		e.ReturnedError,
	).Once()
}

func (a *TemplateAction) ExpectDelete(e *TemplateActionDeleteExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Delete",
		e.ExpectedID,
	).Return(
		e.ReturnedTemplate,
		e.ReturnedError,
	).Once()
}

var _ = action.TemplateActioner(new(TemplateAction))
