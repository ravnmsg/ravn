package test

import (
	"errors"
	"testing"
	"time"

	"github.com/google/uuid"
)

// MakeUUID parses s into a UUID and returns it.
func MakeUUID(s string, t *testing.T) uuid.UUID {
	result, err := uuid.Parse(s)
	if err != nil {
		t.Fatalf("unexpected error parsing uuid `%s`", s)
	}

	return result
}

// MakeUUIDRef parses s into a UUID and returns its reference.
func MakeUUIDRef(s string, t *testing.T) *uuid.UUID {
	result := MakeUUID(s, t)
	return &result
}

// MakeTime parses s into a Time and returns it.
func MakeTime(s string, t *testing.T) time.Time {
	result, err := time.Parse(time.RFC3339, s)
	if err != nil {
		t.Fatalf("unexpected error parsing time `%s`", s)
	}

	return result
}

// MakeTimeRef parses s into a Time and returns its reference.
func MakeTimeRef(s string, t *testing.T) *time.Time {
	result := MakeTime(s, t)
	return &result
}

// ErrorExpectation defines some data about an expected error.
type ErrorExpectation struct {
	Error        error
	ErrorMessage string
}

var (
	// ErrArbitrary defines an arbitrary error. Usable during tests when the error's actual value
	// is of no importance.
	ErrArbitrary = errors.New("arbitrary")
)
