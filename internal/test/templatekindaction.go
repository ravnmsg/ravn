package test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type TemplateKindAction struct {
	mock.Mock
	t *testing.T
}

type TemplateKindActionRetrieveManyExpectation struct {
	ExpectedQuery         *query.Query
	ReturnedTemplateKinds []*resource.TemplateKind
	ReturnedError         error
}

type TemplateKindActionRetrieveByIDExpectation struct {
	ExpectedID           uuid.UUID
	ReturnedTemplateKind *resource.TemplateKind
	ReturnedError        error
}

type TemplateKindActionCreateExpectation struct {
	ExpectedTemplateKind *resource.TemplateKind
	ReturnedTemplateKind *resource.TemplateKind
	ReturnedError        error
}

type TemplateKindActionUpdateExpectation struct {
	ExpectedID           uuid.UUID
	ExpectedTemplateKind *resource.TemplateKind
	ReturnedTemplateKind *resource.TemplateKind
	ReturnedError        error
}

type TemplateKindActionDeleteExpectation struct {
	ExpectedID           uuid.UUID
	ReturnedTemplateKind *resource.TemplateKind
	ReturnedError        error
}

func NewTemplateKindAction(t *testing.T) *TemplateKindAction {
	return &TemplateKindAction{
		t: t,
	}
}

func (a *TemplateKindAction) RetrieveMany(q *query.Query) ([]*resource.TemplateKind, error) {
	args := a.Called(q)
	return args.Get(0).([]*resource.TemplateKind), args.Error(1)
}

func (a *TemplateKindAction) RetrieveByID(id uuid.UUID) (*resource.TemplateKind, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.TemplateKind), args.Error(1)
}

func (a *TemplateKindAction) Create(templateKind *resource.TemplateKind) (*resource.TemplateKind, error) {
	args := a.Called(templateKind)

	returnedTemplateKind, ok := args.Get(0).(*resource.TemplateKind)
	if (!ok || returnedTemplateKind == nil) && args.Error(1) == nil {
		return templateKind, nil
	}

	return returnedTemplateKind, args.Error(1)
}

func (a *TemplateKindAction) Update(id uuid.UUID, templateKind *resource.TemplateKind) (*resource.TemplateKind, error) {
	args := a.Called(id, templateKind)

	returnedTemplateKind, ok := args.Get(0).(*resource.TemplateKind)
	if (!ok || returnedTemplateKind == nil) && args.Error(1) == nil {
		return templateKind, nil
	}

	return returnedTemplateKind, args.Error(1)
}

func (a *TemplateKindAction) Delete(id uuid.UUID) (*resource.TemplateKind, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.TemplateKind), args.Error(1)
}

func (a *TemplateKindAction) ExpectRetrieveMany(e *TemplateKindActionRetrieveManyExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveMany",
		e.ExpectedQuery,
	).Return(
		e.ReturnedTemplateKinds,
		e.ReturnedError,
	).Once()
}

func (a *TemplateKindAction) ExpectRetrieveByID(e *TemplateKindActionRetrieveByIDExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveByID",
		e.ExpectedID,
	).Return(
		e.ReturnedTemplateKind,
		e.ReturnedError,
	).Once()
}

func (a *TemplateKindAction) ExpectCreate(e *TemplateKindActionCreateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Create",
		e.ExpectedTemplateKind,
	).Return(
		e.ReturnedTemplateKind,
		e.ReturnedError,
	).Once()
}

func (a *TemplateKindAction) ExpectUpdate(e *TemplateKindActionUpdateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Update",
		e.ExpectedID,
		e.ExpectedTemplateKind,
	).Return(
		e.ReturnedTemplateKind,
		e.ReturnedError,
	).Once()
}

func (a *TemplateKindAction) ExpectDelete(e *TemplateKindActionDeleteExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Delete",
		e.ExpectedID,
	).Return(
		e.ReturnedTemplateKind,
		e.ReturnedError,
	).Once()
}

var _ = action.TemplateKindActioner(new(TemplateKindAction))
