package test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type NamespaceAction struct {
	mock.Mock
	t *testing.T
}

type NamespaceActionRetrieveManyExpectation struct {
	ExpectedQuery      *query.Query
	ReturnedNamespaces []*resource.Namespace
	ReturnedError      error
}

type NamespaceActionRetrieveByIDExpectation struct {
	ExpectedID        uuid.UUID
	ReturnedNamespace *resource.Namespace
	ReturnedError     error
}

type NamespaceActionRetrieveByIDOrCodeExpectation struct {
	ExpectedIDOrCode  string
	ReturnedNamespace *resource.Namespace
	ReturnedError     error
}

type NamespaceActionRetrieveParentsExpectation struct {
	ExpectedID         uuid.UUID
	ReturnedNamespaces []*resource.Namespace
	ReturnedError      error
}

type NamespaceActionRetrieveChildrenExpectation struct {
	ExpectedID         uuid.UUID
	ReturnedNamespaces []*resource.Namespace
	ReturnedError      error
}

type NamespaceActionCreateExpectation struct {
	ExpectedNamespace *resource.Namespace
	ReturnedNamespace *resource.Namespace
	ReturnedError     error
}

type NamespaceActionUpdateExpectation struct {
	ExpectedID        uuid.UUID
	ExpectedNamespace *resource.Namespace
	ReturnedNamespace *resource.Namespace
	ReturnedError     error
}

type NamespaceActionDeleteExpectation struct {
	ExpectedID        uuid.UUID
	ReturnedNamespace *resource.Namespace
	ReturnedError     error
}

func NewNamespaceAction(t *testing.T) *NamespaceAction {
	return &NamespaceAction{
		t: t,
	}
}

func (a *NamespaceAction) RetrieveMany(q *query.Query) ([]*resource.Namespace, error) {
	args := a.Called(q)
	return args.Get(0).([]*resource.Namespace), args.Error(1)
}

func (a *NamespaceAction) RetrieveByID(id uuid.UUID) (*resource.Namespace, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.Namespace), args.Error(1)
}

func (a *NamespaceAction) RetrieveByIDOrCode(idOrCode string) (*resource.Namespace, error) {
	args := a.Called(idOrCode)
	return args.Get(0).(*resource.Namespace), args.Error(1)
}

func (a *NamespaceAction) RetrieveParents(id uuid.UUID) ([]*resource.Namespace, error) {
	args := a.Called(id)
	return args.Get(0).([]*resource.Namespace), args.Error(1)
}

func (a *NamespaceAction) RetrieveChildren(id uuid.UUID) ([]*resource.Namespace, error) {
	args := a.Called(id)
	return args.Get(0).([]*resource.Namespace), args.Error(1)
}

func (a *NamespaceAction) Create(namespace *resource.Namespace) (*resource.Namespace, error) {
	args := a.Called(namespace)

	returnedNamespace, ok := args.Get(0).(*resource.Namespace)
	if (!ok || returnedNamespace == nil) && args.Error(1) == nil {
		return namespace, nil
	}

	return returnedNamespace, args.Error(1)
}

func (a *NamespaceAction) Update(id uuid.UUID, namespace *resource.Namespace) (*resource.Namespace, error) {
	args := a.Called(id, namespace)

	returnedNamespace, ok := args.Get(0).(*resource.Namespace)
	if (!ok || returnedNamespace == nil) && args.Error(1) == nil {
		return namespace, nil
	}

	return returnedNamespace, args.Error(1)
}

func (a *NamespaceAction) Delete(id uuid.UUID) (*resource.Namespace, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.Namespace), args.Error(1)
}

func (a *NamespaceAction) ExpectRetrieveMany(e *NamespaceActionRetrieveManyExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveMany",
		e.ExpectedQuery,
	).Return(
		e.ReturnedNamespaces,
		e.ReturnedError,
	).Once()
}

func (a *NamespaceAction) ExpectRetrieveByID(e *NamespaceActionRetrieveByIDExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveByID",
		e.ExpectedID,
	).Return(
		e.ReturnedNamespace,
		e.ReturnedError,
	).Once()
}

func (a *NamespaceAction) ExpectRetrieveByIDOrCode(e *NamespaceActionRetrieveByIDOrCodeExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveByIDOrCode",
		e.ExpectedIDOrCode,
	).Return(
		e.ReturnedNamespace,
		e.ReturnedError,
	).Once()
}

func (a *NamespaceAction) ExpectRetrieveParents(e *NamespaceActionRetrieveParentsExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveParents",
		e.ExpectedID,
	).Return(
		e.ReturnedNamespaces,
		e.ReturnedError,
	).Once()
}

func (a *NamespaceAction) ExpectRetrieveChildren(e *NamespaceActionRetrieveChildrenExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveChildren",
		e.ExpectedID,
	).Return(
		e.ReturnedNamespaces,
		e.ReturnedError,
	).Once()
}

func (a *NamespaceAction) ExpectCreate(e *NamespaceActionCreateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Create",
		e.ExpectedNamespace,
	).Return(
		e.ReturnedNamespace,
		e.ReturnedError,
	).Once()
}

func (a *NamespaceAction) ExpectUpdate(e *NamespaceActionUpdateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Update",
		e.ExpectedID,
		e.ExpectedNamespace,
	).Return(
		e.ReturnedNamespace,
		e.ReturnedError,
	).Once()
}

func (a *NamespaceAction) ExpectDelete(e *NamespaceActionDeleteExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Delete",
		e.ExpectedID,
	).Return(
		e.ReturnedNamespace,
		e.ReturnedError,
	).Once()
}

var _ = action.NamespaceActioner(new(NamespaceAction))
