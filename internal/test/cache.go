package test

import (
	"reflect"
	"testing"

	"github.com/stretchr/testify/mock"

	"gitlab.com/ravnmsg/ravn/internal/cache"
)

// Cache defines a mock of a cache.
type Cache struct {
	mock.Mock
	t *testing.T
}

type CacheGetExpectation struct {
	ExpectedKey      string
	ExpectedResource interface{}
	ReturnedResource interface{}
	ReturnedResult   bool
}

type CacheSetExpectation struct {
	ExpectedKey      string
	ExpectedResource interface{}
	ReturnedResult   bool
}

type CacheDelExpectation struct {
	ExpectedKey    string
	ReturnedResult bool
}

// NewCache defines a mock of a cache.
func NewCache(t *testing.T) *Cache {
	return &Cache{
		t: t,
	}
}

// Get mocks the Get function.
func (c *Cache) Get(key string, res interface{}) bool {
	args := c.Called(key, res)

	dstv := reflect.ValueOf(res)

	if dstv.IsNil() || dstv.Kind() != reflect.Ptr {
		c.t.Errorf("unexpected error: Get: slice pointer expected")
		return false
	}

	itemV := dstv.Elem()
	itemT := itemV.Type()

	if returnedValue := reflect.ValueOf(args.Get(0)); returnedValue != reflect.ValueOf(nil) {
		if itemT.Kind() == reflect.Ptr {
			itemV.Set(returnedValue)
		} else {
			itemV.Set(reflect.Indirect(returnedValue))
		}
	}

	return args.Bool(1)
}

// Set mocks the Set function.
func (c *Cache) Set(key string, res interface{}) bool {
	args := c.Called(key, res)

	return args.Bool(0)
}

// Del mocks the Del function.
func (c *Cache) Del(key string) bool {
	args := c.Called(key)

	return args.Bool(0)
}

// ExpectGet adds the mock expectation.
func (c *Cache) ExpectGet(e *CacheGetExpectation) {
	if e == nil {
		return
	}

	c.On(
		"Get",
		e.ExpectedKey,
		e.ExpectedResource,
	).Return(
		e.ReturnedResource,
		e.ReturnedResult,
	).Once()
}

// ExpectSet adds the mock expectation.
func (c *Cache) ExpectSet(e *CacheSetExpectation) {
	if e == nil {
		return
	}

	c.On(
		"Set",
		e.ExpectedKey,
		e.ExpectedResource,
	).Return(
		e.ReturnedResult,
	).Once()
}

// ExpectDel adds the mock expectation.
func (c *Cache) ExpectDel(e *CacheDelExpectation) {
	if e == nil {
		return
	}

	c.On(
		"Del",
		e.ExpectedKey,
	).Return(
		e.ReturnedResult,
	).Once()
}

var _ = cache.Cacher(new(Cache))
