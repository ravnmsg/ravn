package test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type DeliveryKindAction struct {
	mock.Mock
	t *testing.T
}

type DeliveryKindActionRetrieveManyExpectation struct {
	ExpectedQuery         *query.Query
	ReturnedDeliveryKinds []*resource.DeliveryKind
	ReturnedError         error
}

type DeliveryKindActionRetrieveByIDExpectation struct {
	ExpectedID           uuid.UUID
	ReturnedDeliveryKind *resource.DeliveryKind
	ReturnedError        error
}

type DeliveryKindActionRetrieveByIDOrCodeExpectation struct {
	ExpectedIDOrCode     string
	ReturnedDeliveryKind *resource.DeliveryKind
	ReturnedError        error
}

type DeliveryKindActionCreateExpectation struct {
	ExpectedDeliveryKind *resource.DeliveryKind
	ReturnedDeliveryKind *resource.DeliveryKind
	ReturnedError        error
}

type DeliveryKindActionUpdateExpectation struct {
	ExpectedID           uuid.UUID
	ExpectedDeliveryKind *resource.DeliveryKind
	ReturnedDeliveryKind *resource.DeliveryKind
	ReturnedError        error
}

type DeliveryKindActionDeleteExpectation struct {
	ExpectedID           uuid.UUID
	ReturnedDeliveryKind *resource.DeliveryKind
	ReturnedError        error
}

func NewDeliveryKindAction(t *testing.T) *DeliveryKindAction {
	return &DeliveryKindAction{
		t: t,
	}
}

func (a *DeliveryKindAction) RetrieveMany(q *query.Query) ([]*resource.DeliveryKind, error) {
	args := a.Called(q)
	return args.Get(0).([]*resource.DeliveryKind), args.Error(1)
}

func (a *DeliveryKindAction) RetrieveByID(id uuid.UUID) (*resource.DeliveryKind, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.DeliveryKind), args.Error(1)
}

func (a *DeliveryKindAction) RetrieveByIDOrCode(idOrCode string) (*resource.DeliveryKind, error) {
	args := a.Called(idOrCode)
	return args.Get(0).(*resource.DeliveryKind), args.Error(1)
}

func (a *DeliveryKindAction) Create(deliveryKind *resource.DeliveryKind) (*resource.DeliveryKind, error) {
	args := a.Called(deliveryKind)

	returnedDeliveryKind, ok := args.Get(0).(*resource.DeliveryKind)
	if (!ok || returnedDeliveryKind == nil) && args.Error(1) == nil {
		return deliveryKind, nil
	}

	return returnedDeliveryKind, args.Error(1)
}

func (a *DeliveryKindAction) Update(id uuid.UUID, deliveryKind *resource.DeliveryKind) (*resource.DeliveryKind, error) {
	args := a.Called(id, deliveryKind)

	returnedDeliveryKind, ok := args.Get(0).(*resource.DeliveryKind)
	if (!ok || returnedDeliveryKind == nil) && args.Error(1) == nil {
		return deliveryKind, nil
	}

	return returnedDeliveryKind, args.Error(1)
}

func (a *DeliveryKindAction) Delete(id uuid.UUID) (*resource.DeliveryKind, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.DeliveryKind), args.Error(1)
}

func (a *DeliveryKindAction) ExpectRetrieveMany(e *DeliveryKindActionRetrieveManyExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveMany",
		e.ExpectedQuery,
	).Return(
		e.ReturnedDeliveryKinds,
		e.ReturnedError,
	).Once()
}

func (a *DeliveryKindAction) ExpectRetrieveByID(e *DeliveryKindActionRetrieveByIDExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveByID",
		e.ExpectedID,
	).Return(
		e.ReturnedDeliveryKind,
		e.ReturnedError,
	).Once()
}

func (a *DeliveryKindAction) ExpectRetrieveByIDOrCode(e *DeliveryKindActionRetrieveByIDOrCodeExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveByIDOrCode",
		e.ExpectedIDOrCode,
	).Return(
		e.ReturnedDeliveryKind,
		e.ReturnedError,
	).Once()
}

func (a *DeliveryKindAction) ExpectCreate(e *DeliveryKindActionCreateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Create",
		e.ExpectedDeliveryKind,
	).Return(
		e.ReturnedDeliveryKind,
		e.ReturnedError,
	).Once()
}

func (a *DeliveryKindAction) ExpectUpdate(e *DeliveryKindActionUpdateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Update",
		e.ExpectedID,
		e.ExpectedDeliveryKind,
	).Return(
		e.ReturnedDeliveryKind,
		e.ReturnedError,
	).Once()
}

func (a *DeliveryKindAction) ExpectDelete(e *DeliveryKindActionDeleteExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Delete",
		e.ExpectedID,
	).Return(
		e.ReturnedDeliveryKind,
		e.ReturnedError,
	).Once()
}

var _ = action.DeliveryKindActioner(new(DeliveryKindAction))
