package test

import (
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/mock"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type DeliveryAction struct {
	mock.Mock
	t *testing.T
}

type DeliveryActionRetrieveManyExpectation struct {
	ExpectedQuery      *query.Query
	ReturnedDeliveries []*resource.Delivery
	ReturnedError      error
}

type DeliveryActionRetrieveByIDExpectation struct {
	ExpectedID       uuid.UUID
	ReturnedDelivery *resource.Delivery
	ReturnedError    error
}

type DeliveryActionRetrieveByMessageIDExpectation struct {
	ExpectedMessageID  uuid.UUID
	ReturnedDeliveries []*resource.Delivery
	ReturnedError      error
}

type DeliveryActionCreateExpectation struct {
	ExpectedDelivery *resource.Delivery
	ReturnedDelivery *resource.Delivery
	ReturnedError    error
}

type DeliveryActionUpdateExpectation struct {
	ExpectedID       uuid.UUID
	ExpectedDelivery *resource.Delivery
	ReturnedDelivery *resource.Delivery
	ReturnedError    error
}

type DeliveryActionQueueRequestExpectation struct {
	ExpectedDeliveryID uuid.UUID
	ReturnedError      error
}

func NewDeliveryAction(t *testing.T) *DeliveryAction {
	return &DeliveryAction{
		t: t,
	}
}

func (a *DeliveryAction) RetrieveMany(q *query.Query) ([]*resource.Delivery, error) {
	args := a.Called(q)
	return args.Get(0).([]*resource.Delivery), args.Error(1)
}

func (a *DeliveryAction) RetrieveByID(id uuid.UUID) (*resource.Delivery, error) {
	args := a.Called(id)
	return args.Get(0).(*resource.Delivery), args.Error(1)
}

func (a *DeliveryAction) RetrieveByMessageID(id uuid.UUID) ([]*resource.Delivery, error) {
	args := a.Called(id)
	return args.Get(0).([]*resource.Delivery), args.Error(1)
}

func (a *DeliveryAction) Create(delivery *resource.Delivery) (*resource.Delivery, error) {
	args := a.Called(delivery)

	returnedDelivery, ok := args.Get(0).(*resource.Delivery)
	if (!ok || returnedDelivery == nil) && args.Error(1) == nil {
		return delivery, nil
	}

	return returnedDelivery, args.Error(1)
}

func (a *DeliveryAction) Update(id uuid.UUID, delivery *resource.Delivery) (*resource.Delivery, error) {
	args := a.Called(id, delivery)

	returnedDelivery, ok := args.Get(0).(*resource.Delivery)
	if (!ok || returnedDelivery == nil) && args.Error(1) == nil {
		return delivery, nil
	}

	return returnedDelivery, args.Error(1)
}

func (a *DeliveryAction) QueueRequest(deliveryID uuid.UUID) error {
	args := a.Called(deliveryID)
	return args.Error(0)
}

func (a *DeliveryAction) ExpectRetrieveMany(e *DeliveryActionRetrieveManyExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveMany",
		e.ExpectedQuery,
	).Return(
		e.ReturnedDeliveries,
		e.ReturnedError,
	).Once()
}

func (a *DeliveryAction) ExpectRetrieveByID(e *DeliveryActionRetrieveByIDExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveByID",
		e.ExpectedID,
	).Return(
		e.ReturnedDelivery,
		e.ReturnedError,
	).Once()
}

func (a *DeliveryAction) ExpectRetrieveByMessageID(e *DeliveryActionRetrieveByMessageIDExpectation) {
	if e == nil {
		return
	}

	a.On(
		"RetrieveByMessageID",
		e.ExpectedMessageID,
	).Return(
		e.ReturnedDeliveries,
		e.ReturnedError,
	).Once()
}

func (a *DeliveryAction) ExpectCreate(e *DeliveryActionCreateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Create",
		e.ExpectedDelivery,
	).Return(
		e.ReturnedDelivery,
		e.ReturnedError,
	).Once()
}

func (a *DeliveryAction) ExpectUpdate(e *DeliveryActionUpdateExpectation) {
	if e == nil {
		return
	}

	a.On(
		"Update",
		e.ExpectedID,
		e.ExpectedDelivery,
	).Return(
		e.ReturnedDelivery,
		e.ReturnedError,
	).Once()
}
func (a *DeliveryAction) ExpectQueueRequest(e *DeliveryActionQueueRequestExpectation) {
	if e == nil {
		return
	}

	a.On(
		"QueueRequest",
		e.ExpectedDeliveryID,
	).Return(
		e.ReturnedError,
	).Once()
}

var _ = action.DeliveryActioner(new(DeliveryAction))
