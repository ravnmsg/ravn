package test

import (
	"time"
)

// Clock defines a clock with a constant time. It is used during tests to predict values that
// change, such as creation time attributes.
type Clock struct {
	CurrentTime time.Time
}

// NewClock creates a new instance of a test clock with the specified value as current time.
func NewClock(value string) *Clock {
	t, err := time.Parse(time.RFC3339, value)
	if err != nil {
		panic(err)
	}

	return &Clock{CurrentTime: t}
}

// Now returns the configured time.
func (c *Clock) Now() time.Time {
	return c.CurrentTime
}
