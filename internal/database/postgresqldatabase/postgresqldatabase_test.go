//go:build !unit && !endtoend
// +build !unit,!endtoend

package postgresqldatabase_test

import (
	"context"
	"fmt"
	"os"
	"reflect"
	"testing"

	"github.com/google/go-cmp/cmp"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"github.com/stretchr/testify/assert"
	upperdb "github.com/upper/db/v4"

	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/database/postgresqldatabase"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestCount(t *testing.T) {
	tests := map[string]struct {
		resourceType  resource.Type
		filters       []*query.Filter
		expectedCount int
		expectedError error
	}{
		"deliveries": {
			resourceType:  resource.TypeDelivery,
			filters:       []*query.Filter{},
			expectedCount: 5,
		},
		"deliveriesFilteredByMessageID": {
			resourceType: resource.TypeDelivery,
			filters: []*query.Filter{
				query.NewFilter("MessageID", query.OpEqual, "00000000-0000-0000-0006-000000000000"),
			},
			expectedCount: 1,
		},
		"deliveryKinds": {
			resourceType:  resource.TypeDeliveryKind,
			filters:       []*query.Filter{},
			expectedCount: 4,
		},
		"messages": {
			resourceType:  resource.TypeMessage,
			filters:       []*query.Filter{},
			expectedCount: 5,
		},
		"messageKinds": {
			resourceType:  resource.TypeMessageKind,
			filters:       []*query.Filter{},
			expectedCount: 5,
		},
		"namespaces": {
			resourceType:  resource.TypeNamespace,
			filters:       []*query.Filter{},
			expectedCount: 4,
		},
		"templates": {
			resourceType:  resource.TypeTemplate,
			filters:       []*query.Filter{},
			expectedCount: 4,
		},
		"templateKinds": {
			resourceType:  resource.TypeTemplateKind,
			filters:       []*query.Filter{},
			expectedCount: 3,
		},
		"resourcesFilteredByNonexistantCode": {
			resourceType: resource.TypeDeliveryKind,
			filters: []*query.Filter{
				query.NewFilter("Code", query.OpEqual, "nonexistant"),
			},
			expectedCount: 0,
		},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	db := connectDatabase(ctx, t)

	for name, tt := range tests {
		name := name
		tt := tt

		err := db.Tx(func(sess upperdb.Session) error {
			t.Run(name, func(t *testing.T) {
				db := postgresqldatabase.NewWithSession(db, sess)
				count, err := db.Count(tt.resourceType, tt.filters)

				assert.Equal(t, tt.expectedError, err)
				assert.Equal(t, tt.expectedCount, count)
			})

			return fmt.Errorf("rollback")
		})

		if err == nil {
			t.Fatalf("unexpected transaction success - database was probably modified")
		}
	}
}

func TestRetrieveMany(t *testing.T) {
	tests := map[string]struct {
		resourceType      resource.Type
		resourceQuery     *query.Query
		resources         interface{}
		expectedResources interface{}
		expectedError     error
	}{
		"deliveries": {
			resourceType:  resource.TypeDelivery,
			resourceQuery: query.NewQuery(),
			resources:     &[]*resource.Delivery{},
			expectedResources: &[]*resource.Delivery{
				{
					ID:             test.MakeUUID("00000000-0000-0000-0007-000000000000", t),
					MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000000", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					LanguageTag:    "language_tag_000000000000",
					Status:         "delivered",
					StatusMessage:  "",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					Input:          map[string]interface{}{"key": "value_000000000000"},
					Output:         map[string]interface{}{"externalId": "external_id_000000000000"},
					ExternalID:     "external_id_000000000000",
					CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0007-000000000001", t),
					MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000001", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000001", t),
					LanguageTag:    "language_tag_000000000001",
					Status:         "pending",
					StatusMessage:  "",
					DeliverTime:    nil,
					Input:          map[string]interface{}{},
					Output:         map[string]interface{}{},
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:01:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:01:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0007-000000000002", t),
					MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000002", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000001", t),
					LanguageTag:    "language_tag_000000000002",
					Status:         "delivering",
					StatusMessage:  "",
					DeliverTime:    nil,
					Input:          map[string]interface{}{},
					Output:         map[string]interface{}{},
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:02:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:02:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0007-000000000003", t),
					MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000003", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					LanguageTag:    "language_tag_000000000003",
					Status:         "undelivered",
					StatusMessage:  "failed",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:03:00Z", t),
					Input:          map[string]interface{}{"key": "value_000000000000"},
					Output:         map[string]interface{}{},
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:03:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:03:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0007-000000000004", t),
					MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000003", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					LanguageTag:    "language_tag_000000000004",
					Status:         "undelivered",
					StatusMessage:  "failed again",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:04:01Z", t),
					Input:          map[string]interface{}{"key": "value_000000000000"},
					Output:         map[string]interface{}{},
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:04:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:04:59Z", t),
				},
			},
		},
		"deliveriesFilteredByMessageID": {
			resourceType: resource.TypeDelivery,
			resourceQuery: query.NewQuery(
				query.WithFilterArgs("MessageID", query.OpEqual, "00000000-0000-0000-0006-000000000003"),
			),
			resources: &[]*resource.Delivery{},
			expectedResources: &[]*resource.Delivery{
				{
					ID:             test.MakeUUID("00000000-0000-0000-0007-000000000003", t),
					MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000003", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					LanguageTag:    "language_tag_000000000003",
					Status:         "undelivered",
					StatusMessage:  "failed",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:03:00Z", t),
					Input:          map[string]interface{}{"key": "value_000000000000"},
					Output:         map[string]interface{}{},
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:03:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:03:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0007-000000000004", t),
					MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000003", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					LanguageTag:    "language_tag_000000000004",
					Status:         "undelivered",
					StatusMessage:  "failed again",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:04:01Z", t),
					Input:          map[string]interface{}{"key": "value_000000000000"},
					Output:         map[string]interface{}{},
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:04:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:04:59Z", t),
				},
			},
		},
		"deliveriesOrderedByDeliverTime": {
			resourceType: resource.TypeDelivery,
			resourceQuery: query.NewQuery(
				query.WithFilterArgs("DeliverTime", query.OpNotEqual, nil),
				query.WithOrderArgs("DeliverTime", query.DirDescending),
			),
			resources: &[]*resource.Delivery{},
			expectedResources: &[]*resource.Delivery{
				{
					ID:             test.MakeUUID("00000000-0000-0000-0007-000000000004", t),
					MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000003", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					LanguageTag:    "language_tag_000000000004",
					Status:         "undelivered",
					StatusMessage:  "failed again",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:04:01Z", t),
					Input:          map[string]interface{}{"key": "value_000000000000"},
					Output:         map[string]interface{}{},
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:04:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:04:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0007-000000000003", t),
					MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000003", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					LanguageTag:    "language_tag_000000000003",
					Status:         "undelivered",
					StatusMessage:  "failed",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:03:00Z", t),
					Input:          map[string]interface{}{"key": "value_000000000000"},
					Output:         map[string]interface{}{},
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:03:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:03:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0007-000000000000", t),
					MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000000", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					LanguageTag:    "language_tag_000000000000",
					Status:         "delivered",
					StatusMessage:  "",
					DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					Input:          map[string]interface{}{"key": "value_000000000000"},
					Output:         map[string]interface{}{"externalId": "external_id_000000000000"},
					ExternalID:     "external_id_000000000000",
					CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
				},
			},
		},
		"deliveryKinds": {
			resourceType:  resource.TypeDeliveryKind,
			resourceQuery: query.NewQuery(),
			resources:     &[]*resource.DeliveryKind{},
			expectedResources: &[]*resource.DeliveryKind{
				{
					ID:          test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					Code:        "delivery_kind_000000000000",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000000", t),
					Host:        "000000000000.example.com",
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
				{
					ID:          test.MakeUUID("00000000-0000-0000-0002-000000000001", t),
					Code:        "noop",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "000000000001.example.com",
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T02:01:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:01:59Z", t),
				},
				{
					ID:          test.MakeUUID("00000000-0000-0000-0002-000000000002", t),
					Code:        "delivery_kind_000000000002",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000000", t),
					Host:        "000000000002.example.com",
					Disabled:    true,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T02:02:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:02:59Z", t),
				},
				{
					ID:          test.MakeUUID("00000000-0000-0000-0002-000000000003", t),
					Code:        "delivery_kind_000000000003",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000000", t),
					Host:        "000000000003.example.com",
					Disabled:    false,
					Deleted:     true,
					CreateTime:  test.MakeTime("2000-01-01T02:03:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:03:59Z", t),
				},
			},
		},
		"deliveryKindsFilteredByCode": {
			resourceType: resource.TypeDeliveryKind,
			resourceQuery: query.NewQuery(
				query.WithFilterArgs("Code", query.OpEqual, "delivery_kind_000000000000"),
			),
			resources: &[]*resource.DeliveryKind{},
			expectedResources: &[]*resource.DeliveryKind{
				{
					ID:          test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					Code:        "delivery_kind_000000000000",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000000", t),
					Host:        "000000000000.example.com",
					Disabled:    false,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
			},
		},
		"messages": {
			resourceType:  resource.TypeMessage,
			resourceQuery: query.NewQuery(),
			resources:     &[]*resource.Message{},
			expectedResources: &[]*resource.Message{
				{
					ID:            test.MakeUUID("00000000-0000-0000-0006-000000000000", t),
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Status:        "succeeded",
					Input:         map[string]interface{}{"key": "value_000000000000"},
					CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
				},
				{
					ID:            test.MakeUUID("00000000-0000-0000-0006-000000000001", t),
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
					Status:        "active",
					Input:         map[string]interface{}{"key": "value_000000000001"},
					CreateTime:    test.MakeTime("2000-01-01T06:01:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:01:59Z", t),
				},
				{
					ID:            test.MakeUUID("00000000-0000-0000-0006-000000000002", t),
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
					Status:        "active",
					Input:         map[string]interface{}{"key": "value_000000000002"},
					CreateTime:    test.MakeTime("2000-01-01T06:02:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:02:59Z", t),
				},
				{
					ID:            test.MakeUUID("00000000-0000-0000-0006-000000000003", t),
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000002", t),
					Status:        "failed",
					Input:         map[string]interface{}{"key": "value_000000000003"},
					CreateTime:    test.MakeTime("2000-01-01T06:03:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:03:59Z", t),
				},
				{
					ID:            test.MakeUUID("00000000-0000-0000-0006-000000000004", t),
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Status:        "scheduled",
					Input:         map[string]interface{}{"key": "value_000000000004"},
					ScheduleTime:  test.MakeTimeRef("2000-01-01T06:04:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T06:04:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:04:59Z", t),
				},
			},
		},
		"messagesFilteredByCreateTime": {
			resourceType: resource.TypeMessage,
			resourceQuery: query.NewQuery(
				query.WithFilterArgs("CreateTime", query.OpLessThan, "2000-01-01T06:02:00Z"),
			),
			resources: &[]*resource.Message{},
			expectedResources: &[]*resource.Message{
				{
					ID:            test.MakeUUID("00000000-0000-0000-0006-000000000000", t),
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Status:        "succeeded",
					Input:         map[string]interface{}{"key": "value_000000000000"},
					CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
				},
				{
					ID:            test.MakeUUID("00000000-0000-0000-0006-000000000001", t),
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
					Status:        "active",
					Input:         map[string]interface{}{"key": "value_000000000001"},
					CreateTime:    test.MakeTime("2000-01-01T06:01:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:01:59Z", t),
				},
			},
		},
		"messagesPaged": {
			resourceType: resource.TypeMessage,
			resourceQuery: query.NewQuery(
				query.WithPageArgs(1, 2),
			),
			resources: &[]*resource.Message{},
			expectedResources: &[]*resource.Message{
				{
					ID:            test.MakeUUID("00000000-0000-0000-0006-000000000001", t),
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
					Status:        "active",
					Input:         map[string]interface{}{"key": "value_000000000001"},
					CreateTime:    test.MakeTime("2000-01-01T06:01:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:01:59Z", t),
				},
				{
					ID:            test.MakeUUID("00000000-0000-0000-0006-000000000002", t),
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
					Status:        "active",
					Input:         map[string]interface{}{"key": "value_000000000002"},
					CreateTime:    test.MakeTime("2000-01-01T06:02:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:02:59Z", t),
				},
			},
		},
		"messageKinds": {
			resourceType:  resource.TypeMessageKind,
			resourceQuery: query.NewQuery(),
			resources:     &[]*resource.MessageKind{},
			expectedResources: &[]*resource.MessageKind{
				{
					ID:          test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Code:        "message_kind_000000000000",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000000", t),
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
				},
				{
					ID:          test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
					Code:        "message_kind_000000000001",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T03:01:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:01:59Z", t),
				},
				{
					ID:          test.MakeUUID("00000000-0000-0000-0003-000000000002", t),
					Code:        "message_kind_000000000002",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000000", t),
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T03:02:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:02:59Z", t),
				},
				{
					ID:          test.MakeUUID("00000000-0000-0000-0003-000000000003", t),
					Code:        "message_kind_000000000003",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000000", t),
					Disabled:    true,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T03:03:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:03:59Z", t),
				},
				{
					ID:          test.MakeUUID("00000000-0000-0000-0003-000000000004", t),
					Code:        "message_kind_000000000004",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000000", t),
					Disabled:    false,
					Deleted:     true,
					CreateTime:  test.MakeTime("2000-01-01T03:04:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:04:59Z", t),
				},
			},
		},
		"messageKindsFilteredByCode": {
			resourceType: resource.TypeMessageKind,
			resourceQuery: query.NewQuery(
				query.WithFilterArgs("Code", query.OpEqual, "message_kind_000000000001"),
			),
			resources: &[]*resource.MessageKind{},
			expectedResources: &[]*resource.MessageKind{
				{
					ID:          test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
					Code:        "message_kind_000000000001",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    false,
					CreateTime:  test.MakeTime("2000-01-01T03:01:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:01:59Z", t),
				},
			},
		},
		"namespaces": {
			resourceType:  resource.TypeNamespace,
			resourceQuery: query.NewQuery(),
			resources:     &[]*resource.Namespace{},
			expectedResources: &[]*resource.Namespace{
				{
					ID:         test.MakeUUID("00000000-0000-0000-0001-000000000000", t),
					Code:       "namespace_000000000000",
					ParentID:   nil,
					Deleted:    false,
					CreateTime: test.MakeTime("2000-01-01T01:00:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:00:59Z", t),
				},
				{
					ID:         test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Code:       "namespace_000000000001",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					Deleted:    false,
					CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
				},
				{
					ID:         test.MakeUUID("00000000-0000-0000-0001-000000000002", t),
					Code:       "namespace_000000000002",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
					Deleted:    false,
					CreateTime: test.MakeTime("2000-01-01T01:02:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:02:59Z", t),
				},
				{
					ID:         test.MakeUUID("00000000-0000-0000-0001-000000000003", t),
					Code:       "namespace_000000000003",
					ParentID:   nil,
					Deleted:    true,
					CreateTime: test.MakeTime("2000-01-01T01:03:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:03:59Z", t),
				},
			},
		},
		"namespacesFilteredByCode": {
			resourceType: resource.TypeNamespace,
			resourceQuery: query.NewQuery(
				query.WithFilterArgs("Code", query.OpEqual, "namespace_000000000001"),
			),
			resources: &[]*resource.Namespace{},
			expectedResources: &[]*resource.Namespace{
				{
					ID:         test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Code:       "namespace_000000000001",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
				},
			},
		},
		"templates": {
			resourceType:  resource.TypeTemplate,
			resourceQuery: query.NewQuery(),
			resources:     &[]*resource.Template{},
			expectedResources: &[]*resource.Template{
				{
					ID:             test.MakeUUID("00000000-0000-0000-0005-000000000000", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000000", t),
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					LanguageTag:    "language_tag_000000000000",
					Body:           "body_000000000000",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0005-000000000001", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
					LanguageTag:    "language_tag_000000000001",
					Body:           "Body {{.key}}, in language_tag_000000000001.",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T05:01:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:01:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0005-000000000002", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
					LanguageTag:    "language_tag_000000000002",
					Body:           "Body {{.key}}, in language_tag_000000000002.",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T05:02:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:02:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0005-000000000003", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000000", t),
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					LanguageTag:    "language_tag_000000000003",
					Body:           "body_000000000003",
					Deleted:        false,
					CreateTime:     test.MakeTime("2000-01-01T05:03:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:03:59Z", t),
				},
			},
		},
		"templatesFilteredByMessageKindIdAndLanguageTag": {
			resourceType: resource.TypeTemplate,
			resourceQuery: query.NewQuery(
				query.WithFilterArgs("MessageKindID", query.OpEqual, "00000000-0000-0000-0003-000000000001"),
				query.WithFilterArgs("LanguageTag", query.OpEqual, "language_tag_000000000001"),
			),
			resources: &[]*resource.Template{},
			expectedResources: &[]*resource.Template{
				{
					ID:             test.MakeUUID("00000000-0000-0000-0005-000000000001", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
					LanguageTag:    "language_tag_000000000001",
					Body:           "Body {{.key}}, in language_tag_000000000001.",
					CreateTime:     test.MakeTime("2000-01-01T05:01:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:01:59Z", t),
				},
			},
		},
		"templateKinds": {
			resourceType:  resource.TypeTemplateKind,
			resourceQuery: query.NewQuery(),
			resources:     &[]*resource.TemplateKind{},
			expectedResources: &[]*resource.TemplateKind{
				{
					ID:             test.MakeUUID("00000000-0000-0000-0004-000000000000", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					TargetAttr:     "target_attr_000000000000",
					CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000002", t),
					TargetAttr:     "target_attr_000000000001",
					CreateTime:     test.MakeTime("2000-01-01T04:01:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:01:59Z", t),
				},
				{
					ID:             test.MakeUUID("00000000-0000-0000-0004-000000000002", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					TargetAttr:     "target_attr_000000000002",
					CreateTime:     test.MakeTime("2000-01-01T04:02:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:02:59Z", t),
				},
			},
		},
		"templateKindsFilteredByDeliveryKindId": {
			resourceType: resource.TypeTemplateKind,
			resourceQuery: query.NewQuery(
				query.WithFilterArgs("DeliveryKindID", query.OpEqual, "00000000-0000-0000-0002-000000000002"),
			),
			resources: &[]*resource.TemplateKind{},
			expectedResources: &[]*resource.TemplateKind{
				{
					ID:             test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000002", t),
					TargetAttr:     "target_attr_000000000001",
					CreateTime:     test.MakeTime("2000-01-01T04:01:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:01:59Z", t),
				},
			},
		},
		"resourcesFilteredByNonexistantCode": {
			resourceType: resource.TypeDeliveryKind,
			resourceQuery: query.NewQuery(
				query.WithFilterArgs("Code", query.OpEqual, "nonexistant_code"),
			),
			resources:         &[]*resource.DeliveryKind{},
			expectedResources: &[]*resource.DeliveryKind{},
		},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	db := connectDatabase(ctx, t)

	for name, tt := range tests {
		name := name
		tt := tt

		err := db.Tx(func(sess upperdb.Session) error {
			t.Run(name, func(t *testing.T) {
				db := postgresqldatabase.NewWithSession(db, sess)
				err := db.RetrieveMany(tt.resourceType, tt.resourceQuery, tt.resources)

				assert.Equal(t, tt.expectedError, err)
				if !cmp.Equal(tt.expectedResources, tt.resources) {
					t.Fatalf(
						"expected resource to be different:\n%s",
						cmp.Diff(tt.expectedResources, tt.resources),
					)
				}
			})

			return fmt.Errorf("rollback")
		})

		if err == nil {
			t.Fatalf("unexpected transaction success - database was probably modified")
		}
	}
}

func TestRetrieveByID(t *testing.T) {
	tests := map[string]struct {
		resourceType     resource.Type
		resourceID       uuid.UUID
		expectedResource resource.Resource
		expectedError    error
	}{
		"delivery": {
			resourceType: resource.TypeDelivery,
			resourceID:   test.MakeUUID("00000000-0000-0000-0007-000000000001", t),
			expectedResource: &resource.Delivery{
				ID:             test.MakeUUID("00000000-0000-0000-0007-000000000001", t),
				MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000001", t),
				DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000001", t),
				LanguageTag:    "language_tag_000000000001",
				Status:         "pending",
				StatusMessage:  "",
				DeliverTime:    nil,
				Input:          map[string]interface{}{},
				Output:         map[string]interface{}{},
				ExternalID:     "",
				CreateTime:     test.MakeTime("2000-01-01T07:01:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T07:01:59Z", t),
			},
		},
		"deliveryKind": {
			resourceType: resource.TypeDeliveryKind,
			resourceID:   test.MakeUUID("00000000-0000-0000-0002-000000000001", t),
			expectedResource: &resource.DeliveryKind{
				ID:          test.MakeUUID("00000000-0000-0000-0002-000000000001", t),
				Code:        "noop",
				NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
				Host:        "000000000001.example.com",
				Disabled:    false,
				Deleted:     false,
				CreateTime:  test.MakeTime("2000-01-01T02:01:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:01:59Z", t),
			},
		},
		"message": {
			resourceType: resource.TypeMessage,
			resourceID:   test.MakeUUID("00000000-0000-0000-0006-000000000001", t),
			expectedResource: &resource.Message{
				ID:            test.MakeUUID("00000000-0000-0000-0006-000000000001", t),
				MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
				Status:        "active",
				Input:         map[string]interface{}{"key": "value_000000000001"},
				CreateTime:    test.MakeTime("2000-01-01T06:01:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:01:59Z", t),
			},
		},
		"messageKind": {
			resourceType: resource.TypeMessageKind,
			resourceID:   test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
			expectedResource: &resource.MessageKind{
				ID:          test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
				Code:        "message_kind_000000000001",
				NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
				Disabled:    false,
				Deleted:     false,
				CreateTime:  test.MakeTime("2000-01-01T03:01:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T03:01:59Z", t),
			},
		},
		"namespace": {
			resourceType: resource.TypeNamespace,
			resourceID:   test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
			expectedResource: &resource.Namespace{
				ID:         test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
				Code:       "namespace_000000000001",
				ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:    false,
				CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
				UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
			},
		},
		"template": {
			resourceType: resource.TypeTemplate,
			resourceID:   test.MakeUUID("00000000-0000-0000-0005-000000000001", t),
			expectedResource: &resource.Template{
				ID:             test.MakeUUID("00000000-0000-0000-0005-000000000001", t),
				TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
				MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
				LanguageTag:    "language_tag_000000000001",
				Body:           "Body {{.key}}, in language_tag_000000000001.",
				Deleted:        false,
				CreateTime:     test.MakeTime("2000-01-01T05:01:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T05:01:59Z", t),
			},
		},
		"templateKind": {
			resourceType: resource.TypeTemplateKind,
			resourceID:   test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
			expectedResource: &resource.TemplateKind{
				ID:             test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
				DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000002", t),
				TargetAttr:     "target_attr_000000000001",
				Deleted:        false,
				CreateTime:     test.MakeTime("2000-01-01T04:01:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T04:01:59Z", t),
			},
		},
		"resourceWithNonexistantID": {
			resourceType:     resource.TypeDeliveryKind,
			resourceID:       test.MakeUUID("00000000-0000-0000-0002-ffffffffffff", t),
			expectedResource: &resource.DeliveryKind{},
			expectedError:    database.ErrInvalidPrimaryKey,
		},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	db := connectDatabase(ctx, t)

	for name, tt := range tests {
		name := name
		tt := tt

		err := db.Tx(func(sess upperdb.Session) error {
			t.Run(name, func(t *testing.T) {
				db := postgresqldatabase.NewWithSession(db, sess)
				res := tt.resourceType.New()
				err := db.RetrieveByID(tt.resourceType, tt.resourceID, res)

				assert.Equal(t, tt.expectedError, err)
				if !cmp.Equal(tt.expectedResource, res) {
					t.Fatalf(
						"expected resource to be different:\n%s",
						cmp.Diff(tt.expectedResource, res),
					)
				}
			})

			return fmt.Errorf("rollback")
		})

		if err == nil {
			t.Fatalf("unexpected transaction success - database was probably modified")
		}
	}
}

func TestRetrieveByIDOrCode(t *testing.T) {
	tests := map[string]struct {
		resourceType     resource.Type
		resourceIDOrCode string
		expectedResource resource.Resource
		expectedError    error
	}{
		"deliveryKind": {
			resourceType:     resource.TypeDeliveryKind,
			resourceIDOrCode: "delivery_kind_000000000002",
			expectedResource: &resource.DeliveryKind{
				ID:          test.MakeUUID("00000000-0000-0000-0002-000000000002", t),
				Code:        "delivery_kind_000000000002",
				NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000000", t),
				Host:        "000000000002.example.com",
				Disabled:    true,
				Deleted:     false,
				CreateTime:  test.MakeTime("2000-01-01T02:02:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:02:59Z", t),
			},
		},
		"messageKind": {
			resourceType:     resource.TypeMessageKind,
			resourceIDOrCode: "message_kind_000000000001",
			expectedResource: &resource.MessageKind{
				ID:          test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
				Code:        "message_kind_000000000001",
				NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
				Disabled:    false,
				Deleted:     false,
				CreateTime:  test.MakeTime("2000-01-01T03:01:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T03:01:59Z", t),
			},
		},
		"namespace": {
			resourceType:     resource.TypeNamespace,
			resourceIDOrCode: "namespace_000000000001",
			expectedResource: &resource.Namespace{
				ID:         test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
				Code:       "namespace_000000000001",
				ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:    false,
				CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
				UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
			},
		},
		"resourceWithNonexistantIDOrCode": {
			resourceType:     resource.TypeDeliveryKind,
			resourceIDOrCode: "00000000-0000-0000-0002-ffffffffffff",
			expectedResource: &resource.DeliveryKind{},
			expectedError:    database.ErrInvalidPrimaryKey,
		},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	db := connectDatabase(ctx, t)

	for name, tt := range tests {
		name := name
		tt := tt

		err := db.Tx(func(sess upperdb.Session) error {
			t.Run(name, func(t *testing.T) {
				db := postgresqldatabase.NewWithSession(db, sess)
				res := tt.resourceType.New()
				err := db.RetrieveByIDOrCode(tt.resourceType, tt.resourceIDOrCode, res)

				assert.Equal(t, tt.expectedError, err)
				if !cmp.Equal(tt.expectedResource, res) {
					t.Fatalf(
						"expected resource to be different:\n%s",
						cmp.Diff(tt.expectedResource, res),
					)
				}
			})

			return fmt.Errorf("rollback")
		})

		if err == nil {
			t.Fatalf("unexpected transaction success - database was probably modified")
		}
	}
}

func TestCreate(t *testing.T) {
	tests := map[string]struct {
		resourceType  resource.Type
		res           resource.Resource
		expectedError error
	}{
		"delivery": {
			resourceType: resource.TypeDelivery,
			res: &resource.Delivery{
				MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000000", t),
				DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
				LanguageTag:    "new_language_tag",
				Status:         "delivered",
				StatusMessage:  "",
				DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
				ExternalID:     "new_external_id",
				CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
			},
		},
		"deliveryKind": {
			resourceType: resource.TypeDeliveryKind,
			res: &resource.DeliveryKind{
				Code:        "new_delivery_kind",
				NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
				Host:        "new.example.com",
				Disabled:    false,
				Deleted:     false,
				CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
			},
		},
		"message": {
			resourceType: resource.TypeMessage,
			res: &resource.Message{
				MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
				Status:        "active",
				Input:         map[string]interface{}{"key": "new_value"},
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
		},
		"messageKind": {
			resourceType: resource.TypeMessageKind,
			res: &resource.MessageKind{
				Code:        "new_message_kind",
				NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
				Disabled:    true,
				Deleted:     false,
				CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
			},
		},
		"namespace": {
			resourceType: resource.TypeNamespace,
			res: &resource.Namespace{
				Code:       "new_namespace",
				ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:    false,
				CreateTime: test.MakeTime("2000-01-01T01:00:58Z", t),
				UpdateTime: test.MakeTime("2000-01-01T01:00:59Z", t),
			},
		},
		"template": {
			resourceType: resource.TypeTemplate,
			res: &resource.Template{
				TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
				MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
				LanguageTag:    "new_language_tag",
				Body:           "new_body",
				Deleted:        false,
				CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
			},
		},
		"templateKind": {
			resourceType: resource.TypeTemplateKind,
			res: &resource.TemplateKind{
				DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
				TargetAttr:     "new_target_attr",
				Deleted:        false,
				CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
			},
		},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	db := connectDatabase(ctx, t)

	for name, tt := range tests {
		name := name
		tt := tt

		err := db.Tx(func(sess upperdb.Session) error {
			t.Run(name, func(t *testing.T) {
				db := postgresqldatabase.NewWithSession(db, sess)
				id, err := db.Create(tt.resourceType, tt.res)

				assert.Equal(t, tt.expectedError, err)
				if err != nil {
					return
				}

				res := tt.resourceType.New()
				if err := db.RetrieveByID(tt.resourceType, id, res); err != nil {
					t.Fatalf("unexpected error: %s", err)
				}

				// Removes the ID field from the comparison, since the actual value is not important,
				// except if it is a zero UUID. Also removes the CreateTime and UpdateTime fields, set
				// to the current time.
				reflectedResource := reflect.ValueOf(res).Elem()
				idField := reflectedResource.FieldByName("ID")

				if idField == reflect.ValueOf(uuid.UUID{}) {
					t.Fatalf("expected ID field to be a random UUID, but was a zero UUID")
				}

				idField.Set(reflect.ValueOf(uuid.UUID{}))

				if !cmp.Equal(tt.res, res) {
					t.Fatalf(
						"expected resource to be different:\n%s",
						cmp.Diff(tt.res, res),
					)
				}
			})

			return fmt.Errorf("rollback")
		})

		if err == nil {
			t.Fatalf("unexpected transaction success - database was probably modified")
		}
	}
}

func TestUpdate(t *testing.T) {
	tests := map[string]struct {
		resourceType  resource.Type
		id            uuid.UUID
		res           resource.Resource
		expectedError error
	}{
		"delivery": {
			resourceType: resource.TypeDelivery,
			id:           test.MakeUUID("00000000-0000-0000-0007-000000000001", t),
			res: &resource.Delivery{
				ID:             test.MakeUUID("00000000-0000-0000-0007-000000000001", t),
				MessageID:      test.MakeUUID("00000000-0000-0000-0006-000000000001", t),
				DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000001", t),
				LanguageTag:    "updated_language_tag",
				Status:         "pending",
				StatusMessage:  "updated_status_message",
				DeliverTime:    test.MakeTimeRef("2000-01-01T07:01:00Z", t),
				ExternalID:     "updated_external_id",
				CreateTime:     test.MakeTime("2000-01-01T07:01:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T07:01:59Z", t),
			},
		},
		"deliveryKind": {
			resourceType: resource.TypeDeliveryKind,
			id:           test.MakeUUID("00000000-0000-0000-0002-000000000001", t),
			res: &resource.DeliveryKind{
				ID:          test.MakeUUID("00000000-0000-0000-0002-000000000001", t),
				Code:        "updated_delivery_kind",
				NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
				Host:        "updated.example.com",
				Disabled:    false,
				Deleted:     false,
				CreateTime:  test.MakeTime("2000-01-01T02:01:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:01:59Z", t),
			},
		},
		"message": {
			resourceType: resource.TypeMessage,
			id:           test.MakeUUID("00000000-0000-0000-0006-000000000001", t),
			res: &resource.Message{
				ID:            test.MakeUUID("00000000-0000-0000-0006-000000000001", t),
				MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
				Status:        "active",
				Input:         map[string]interface{}{"key": "updated_value"},
				CreateTime:    test.MakeTime("2000-01-01T06:01:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:01:59Z", t),
			},
		},
		"messageKind": {
			resourceType: resource.TypeMessageKind,
			id:           test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
			res: &resource.MessageKind{
				ID:          test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
				Code:        "updated_message_kind",
				NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
				Disabled:    false,
				Deleted:     false,
				CreateTime:  test.MakeTime("2000-01-01T03:01:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T03:01:59Z", t),
			},
		},
		"namespace": {
			resourceType: resource.TypeNamespace,
			id:           test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
			res: &resource.Namespace{
				ID:         test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
				Code:       "updated_namespace",
				ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
				Deleted:    false,
				CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
				UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
			},
		},
		"template": {
			resourceType: resource.TypeTemplate,
			id:           test.MakeUUID("00000000-0000-0000-0005-000000000001", t),
			res: &resource.Template{
				ID:             test.MakeUUID("00000000-0000-0000-0005-000000000001", t),
				TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
				MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000001", t),
				LanguageTag:    "updated_language_tag",
				Body:           "updated_body",
				Deleted:        false,
				CreateTime:     test.MakeTime("2000-01-01T05:01:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T05:01:59Z", t),
			},
		},
		"templateKind": {
			resourceType: resource.TypeTemplateKind,
			id:           test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
			res: &resource.TemplateKind{
				ID:             test.MakeUUID("00000000-0000-0000-0004-000000000001", t),
				DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000001", t),
				TargetAttr:     "updated_target_attr",
				Deleted:        false,
				CreateTime:     test.MakeTime("2000-01-01T04:01:58Z", t),
				UpdateTime:     test.MakeTime("2000-01-01T04:01:59Z", t),
			},
		},
		"resourceWithNonexistantID": {
			resourceType: resource.TypeDeliveryKind,
			id:           test.MakeUUID("00000000-0000-0000-0001-ffffffffffff", t),
			res: &resource.DeliveryKind{
				ID:         test.MakeUUID("00000000-0000-0000-0001-ffffffffffff", t),
				Code:       "delivery_kind_updated",
				Host:       "updated.example.com",
				Disabled:   true,
				CreateTime: test.MakeTime("2000-01-01T04:00:58Z", t),
			},
			expectedError: database.ErrInvalidPrimaryKey,
		},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	db := connectDatabase(ctx, t)

	for name, tt := range tests {
		name := name
		tt := tt

		err := db.Tx(func(sess upperdb.Session) error {
			t.Run(name, func(t *testing.T) {
				db := postgresqldatabase.NewWithSession(db, sess)
				err := db.Update(tt.resourceType, tt.id, tt.res)

				assert.Equal(t, tt.expectedError, err)
				if err != nil {
					return
				}

				res := tt.resourceType.New()
				if err := db.RetrieveByID(tt.resourceType, tt.id, res); err != nil {
					t.Fatalf("unexpected error: %s", err)
				}

				if !cmp.Equal(tt.res, res) {
					t.Fatalf(
						"expected resource to be different:\n%s",
						cmp.Diff(tt.res, res),
					)
				}
			})

			return fmt.Errorf("rollback")
		})

		if err == nil {
			t.Fatalf("unexpected transaction success - database was probably modified")
		}
	}
}

func TestDelete(t *testing.T) {
	tests := map[string]struct {
		resourceType  resource.Type
		id            uuid.UUID
		expectedError error
	}{
		"deliveryKind": {
			resourceType:  resource.TypeDeliveryKind,
			id:            test.MakeUUID("00000000-0000-0000-0002-000000000003", t),
			expectedError: nil,
		},
		"messageKind": {
			resourceType:  resource.TypeMessageKind,
			id:            test.MakeUUID("00000000-0000-0000-0003-000000000004", t),
			expectedError: nil,
		},
		"namespace": {
			resourceType:  resource.TypeNamespace,
			id:            test.MakeUUID("00000000-0000-0000-0001-000000000003", t),
			expectedError: nil,
		},
		"template": {
			resourceType:  resource.TypeTemplate,
			id:            test.MakeUUID("00000000-0000-0000-0005-000000000003", t),
			expectedError: nil,
		},
		"templateKind": {
			resourceType:  resource.TypeTemplateKind,
			id:            test.MakeUUID("00000000-0000-0000-0004-000000000002", t),
			expectedError: nil,
		},
		"resourceWithNonexistantID": {
			resourceType:  resource.TypeDeliveryKind,
			id:            test.MakeUUID("00000000-0000-0000-0001-ffffffffffff", t),
			expectedError: database.ErrInvalidPrimaryKey,
		},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	db := connectDatabase(ctx, t)

	for name, tt := range tests {
		name := name
		tt := tt

		err := db.Tx(func(sess upperdb.Session) error {
			t.Run(name, func(t *testing.T) {
				db := postgresqldatabase.NewWithSession(db, sess)
				err := db.Delete(tt.resourceType, tt.id)

				assert.Equal(t, tt.expectedError, err)
				if err != nil {
					return
				}

				res := tt.resourceType.New()
				if err := db.RetrieveByID(tt.resourceType, tt.id, res); err == nil {
					t.Fatalf("expected resource to be deleted, but wasn't")
				}
			})

			return fmt.Errorf("rollback")
		})

		if err == nil {
			t.Fatalf("unexpected transaction success - database was probably modified")
		}
	}
}

func connectDatabase(ctx context.Context, t *testing.T) *postgresqldatabase.Database {
	db, err := postgresqldatabase.New(ctx, postgresqlURI())
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	return db
}

func postgresqlURI() string {
	uri := os.Getenv("RAVN_POSTGRESQL_URI")
	if uri != "" {
		return uri
	}

	host := os.Getenv("RAVN_POSTGRESQL_HOST")
	if host == "" {
		host = "localhost:5432"
	}

	return fmt.Sprintf("postgres://ravn:ravn@%s/ravntest?sslmode=disable", host)
}
