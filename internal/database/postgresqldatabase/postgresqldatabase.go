package postgresqldatabase

import (
	"context"
	"fmt"
	"log"
	"reflect"
	"strings"

	"github.com/google/uuid"
	upperdb "github.com/upper/db/v4"
	"github.com/upper/db/v4/adapter/postgresql"

	"gitlab.com/ravnmsg/ravn/internal/database"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// Database defines a connection to a PostgreSQL database.
type Database struct {
	upperdb.Session
}

// New creates and opens a connection to a PostgreSQL database. It closes the connection when the
// provided context is terminated.
func New(ctx context.Context, uri string) (*Database, error) {
	connURL, err := postgresql.ParseURL(uri)
	if err != nil {
		return nil, err
	}

	session, err := postgresql.Open(connURL)
	if err != nil {
		return nil, err
	}

	db := &Database{
		Session: session,
	}

	// Shutdowns the database connection when the application context ends.
	go func() {
		<-ctx.Done()

		if err := db.Close(); err != nil {
			log.Printf("Error with database connection shutdown: %s", err)
		}
	}()

	return db, nil
}

// NewWithSession is used during tests to instantiate a new database with the specified session.
func NewWithSession(db *Database, session upperdb.Session) *Database {
	return &Database{
		Session: session,
	}
}

// Count retrieves the number of resources of the specified resource type, based on the filters.
func (db *Database) Count(rt resource.Type, filters []*query.Filter) (int, error) {
	coll := db.Collection(db.tableName(rt))
	result := coll.Find()

	result, err := db.applyFilters(result, rt, filters)
	if err != nil {
		return 0, fmt.Errorf("invalid filters: %s", err)
	}

	count, err := result.Count()
	if err != nil {
		return 0, err
	}

	return int(count), nil
}

// RetrieveMany retrieves a collection of resources, based on the specified database.
func (db *Database) RetrieveMany(rt resource.Type, q *query.Query, resources interface{}) error {
	coll := db.Collection(db.tableName(rt))
	result := coll.Find()

	if q != nil {
		var err error
		result, err = db.applyFilters(result, rt, q.Filters)
		if err != nil {
			return fmt.Errorf("invalid filters: %s", err)
		}

		result, err = db.applyOrders(result, rt, q.Orders)
		if err != nil {
			return fmt.Errorf("invalid orders: %s", err)
		}

		result, err = db.applyPage(result, q.Page)
		if err != nil {
			return fmt.Errorf("invalid page: %s", err)
		}
	}

	if err := result.All(resources); err != nil {
		return fmt.Errorf("error retrieving from db: %s", err)
	}

	return nil
}

// Retrieve retrieves the resource matching the specified id.
func (db *Database) RetrieveByID(rt resource.Type, id uuid.UUID, res interface{}) error {
	coll := db.Collection(db.tableName(rt))
	result := coll.Find(upperdb.Cond{"id": id.String()})

	return db.retrieve(result, res)
}

func (db *Database) RetrieveByIDOrCode(rt resource.Type, idOrCode string, res interface{}) error {
	coll := db.Collection(db.tableName(rt))

	var result upperdb.Result
	if id, err := uuid.Parse(idOrCode); err == nil {
		result = coll.Find(upperdb.Or(upperdb.Cond{"id": id}, upperdb.Cond{"code": idOrCode}))
	} else {
		result = coll.Find(upperdb.Cond{"code": idOrCode})
	}

	return db.retrieve(result, res)
}

func (db *Database) retrieve(result upperdb.Result, res interface{}) error {
	if exists, err := result.Exists(); err != nil {
		return err
	} else if !exists {
		return database.ErrInvalidPrimaryKey
	}

	if err := result.One(res); err != nil {
		return err
	}

	return nil
}

// Create creates a new resource based on the provided res. The function returns its new id.
func (db *Database) Create(rt resource.Type, res resource.Resource) (uuid.UUID, error) {
	// The resource is converted to a map to allow the id to be cleared. This is because upperdb
	// does not correctly handle omitempty on uuid.UUIDs. The actions could be in charge of
	// generating the new UUID, but I prefer the database to handle that.
	cols, err := db.resourceToColumns(res)
	if err != nil {
		return uuid.Nil, err
	}

	// The id attribute is removed to prevent an empty UUID from being saved in the database. This
	// ensures that PostgreSQL will generate a new UUID when inserting the new row. The create_time
	// and update_time attributes are set to the current time.
	delete(cols, "id")

	coll := db.Collection(db.tableName(rt))
	pgID, err := coll.Insert(cols)
	if err != nil {
		return uuid.Nil, err
	}

	id, err := uuid.Parse(pgID.ID().(string))
	if err != nil {
		return uuid.Nil, fmt.Errorf("error with returned ID `%v`: %w", pgID, err)
	}

	return id, nil
}

// Update updates an existing resource matching the provided res by its ID field and updates all of
// its attributes.
func (db *Database) Update(rt resource.Type, id uuid.UUID, res resource.Resource) error {
	coll := db.Collection(db.tableName(rt))
	result := coll.Find(upperdb.Cond{"id": id.String()})

	if exists, err := result.Exists(); err != nil {
		return err
	} else if !exists {
		return database.ErrInvalidPrimaryKey
	}

	if err := result.Update(res); err != nil {
		return err
	}

	return nil
}

// Delete deletes the resource matching the provided res by its ID field.
func (db *Database) Delete(rt resource.Type, id uuid.UUID) error {
	coll := db.Collection(db.tableName(rt))
	result := coll.Find(upperdb.Cond{"id": id.String()})

	if exists, err := result.Exists(); err != nil {
		return err
	} else if !exists {
		return database.ErrInvalidPrimaryKey
	}

	if err := result.Delete(); err != nil {
		return err
	}

	return nil
}

func (*Database) tableName(rt resource.Type) string {
	switch rt {
	case resource.TypeDelivery:
		return "deliveries"
	case resource.TypeDeliveryKind:
		return "delivery_kinds"
	case resource.TypeMessage:
		return "messages"
	case resource.TypeMessageKind:
		return "message_kinds"
	case resource.TypeNamespace:
		return "namespaces"
	case resource.TypeTemplate:
		return "templates"
	case resource.TypeTemplateKind:
		return "template_kinds"
	default:
		panic(fmt.Sprintf("table name for resource type `%v` is not configured", rt))
	}
}

func (db *Database) applyFilters(
	result upperdb.Result,
	rt resource.Type,
	filters []*query.Filter,
) (upperdb.Result, error) {
	if len(filters) == 0 {
		return result, nil
	}

	cond := upperdb.Cond{}
	for _, f := range filters {
		col, ok := db.fieldToColumn(rt, f.FieldName)
		if !ok {
			return nil, fmt.Errorf("invalid field name `%s` on resource type `%s`", f.FieldName, rt.Name())
		}

		var condKey string
		switch f.Operator {
		case query.OpEqual:
			condKey = col
		case query.OpNotEqual:
			if f.Operand != nil {
				condKey = fmt.Sprintf("%s <>", col)
			} else {
				condKey = fmt.Sprintf("%s IS NOT", col)
			}
		case query.OpGreaterThan:
			condKey = fmt.Sprintf("%s >", col)
		case query.OpGreaterThanOrEqual:
			condKey = fmt.Sprintf("%s >=", col)
		case query.OpLessThan:
			condKey = fmt.Sprintf("%s <", col)
		case query.OpLessThanOrEqual:
			condKey = fmt.Sprintf("%s <=", col)
		case query.OpIn:
			condKey = fmt.Sprintf("%s IN", col)
		default:
			return nil, fmt.Errorf("invalid operator `%v`", f.Operator)
		}

		cond[condKey] = f.Operand
	}

	return result.And(cond), nil
}

func (db *Database) applyOrders(
	result upperdb.Result,
	rt resource.Type,
	orders []*query.Order,
) (upperdb.Result, error) {
	if len(orders) == 0 {
		return result, nil
	}

	orderbys := make([]interface{}, 0, len(orders))
	for _, o := range orders {
		col, ok := db.fieldToColumn(rt, o.FieldName)
		if !ok {
			return nil, fmt.Errorf("invalid field name `%s`", o.FieldName)
		}

		var prefix string
		switch o.Direction {
		case query.DirAscending:
			prefix = ""
		case query.DirDescending:
			prefix = "-"
		default:
			return nil, fmt.Errorf("invalid direction `%v`", o.Direction)
		}

		orderby := fmt.Sprintf("%s%s", prefix, col)
		orderbys = append(orderbys, orderby)
	}

	return result.OrderBy(orderbys...), nil
}

func (*Database) applyPage(result upperdb.Result, page *query.Page) (upperdb.Result, error) {
	if page == nil {
		return result, nil
	}

	if page.StartIndex < 0 {
		return nil, fmt.Errorf("invalid page start index")
	}

	if page.Capacity < 0 {
		return nil, fmt.Errorf("invalid page capacity")
	}

	result = result.Offset(page.StartIndex)

	if page.Capacity > 0 {
		result = result.Limit(page.Capacity)
	}

	return result, nil
}

func (*Database) resourceToColumns(res resource.Resource) (map[string]interface{}, error) {
	columns := make(map[string]interface{})

	v := reflect.ValueOf(res).Elem()
	for i := 0; i < v.NumField(); i++ {
		f := v.Type().Field(i)

		col := strings.Split(f.Tag.Get("db"), ",")[0]
		if col == "" {
			return nil, fmt.Errorf("invalid database column")
		}

		columns[col] = v.Field(i).Interface()
	}

	return columns, nil
}

func (*Database) fieldToColumn(rt resource.Type, fieldName string) (colName string, ok bool) {
	t := reflect.TypeOf(rt.New()).Elem()
	field, ok := t.FieldByName(fieldName)
	if !ok {
		return "", false
	}

	return strings.Split(field.Tag.Get("db"), ",")[0], true
}

// // fkError wraps the specified dbErr in an InvalidForeignKeyError if dbErr is related to a foreign
// // key contraint violation. The original error is otherwise returned.
// func (db *PostgresqlDatabase) fkError(dbErr error) error {
// 	re := regexp.MustCompile("violates foreign key constraint \"(.*?)\"")

// 	matches := re.FindStringSubmatch(dbErr.Error())
// 	if len(matches) < 2 {
// 		return dbErr
// 	}

// 	var targetResourceType resource.Type
// 	switch matches[1] {
// 	case "deliveries_delivery_kind_id_fkey":
// 		targetResourceType = resource.TypeDeliveryKind
// 	case "deliveries_message_id_fkey":
// 		targetResourceType = resource.TypeMessage
// 	case "delivery_kinds_adapter_id_fkey":
// 		targetResourceType = resource.TypeAdapter
// 	case "delivery_steps_delivery_id_idx":
// 		targetResourceType = resource.TypeDelivery
// 	case "delivery_steps_delivery_step_kind_id_idx":
// 		targetResourceType = resource.TypeDeliveryStepKind
// 	case "messages_message_kind_id_fkey":
// 		targetResourceType = resource.TypeMessageKind
// 	case "messages_delivery_step_kind_id_idx":
// 		targetResourceType = resource.TypeDeliveryStepKind
// 	case "message_kinds_adapter_id_fkey":
// 		targetResourceType = resource.TypeAdapter
// 	}

// 	if targetResourceType == resource.TypeNone {
// 		return dbErr
// 	}

// 	return fmt.Errorf("%w: error using resource %s", database.InvalidForeignKeyError, resource.Name(targetResourceType))
// }

var _ = database.ReadWriter(new(Database))
