package database

import (
	"errors"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// Reader is the interface that wraps the basic Count, RetrieveMany, and Retrieve methods.
type Reader interface {
	// Count retrieves the number of resources of the specified resource type, based on the provided
	// filters.
	//
	//     filter := NewFilter("CreateTime", OpLessThan, time.Now().Add(-4 * time.Hour))
	//     count, err := reader.Count(resource.TypeDelivery, Filters{filter})
	Count(
		resourceType resource.Type,
		filters []*query.Filter,
	) (int, error)

	// RetrieveMany retrieves a collection of resources. It takes a resource type and a query. The
	// result is stored in the provided resources, a pointer to a slice of resources.
	//
	//     deliveries := make([]*resource.Delivery, 0)
	//     q := NewQuery(
	//         WithFilter("Status", Equal, "delivered"),
	//     )
	//     err := reader.RetrieveMany(resource.TypeDelivery, q, &deliveries)
	RetrieveMany(
		resourceType resource.Type,
		q *query.Query,
		resources interface{},
	) error

	// Retrieve retrieves the resource matching the specified resource type and ID. The result is
	// stored in the provided res, a pointer to a resource.
	//
	//     delivery := new(resource.Delivery)
	//     err := reader.RetrieveByID(resource.TypeDelivery, deliveryID, delivery)
	//
	// An ErrInvalidPrimaryKey error is returned when the specified id was not found in the
	// database.
	RetrieveByID(
		resourceType resource.Type,
		id uuid.UUID,
		res interface{},
	) error

	// TODO: Comment
	RetrieveByIDOrCode(
		resourceType resource.Type,
		idOrCode string,
		res interface{},
	) error
}

// Writer is the interface that wraps the basic Create, Update and Delete methods.
type Writer interface {
	// Create creates a new resource of type resourceType with data provided in res. The function
	// returns the new resource's id.
	//
	//     delivery := &resource.Delivery{
	//         MessageID: messageID,
	//         Status: "delivered",
	//     }
	//     id, err := writer.Create(resource.TypeDelivery, delivery)
	Create(
		resourceType resource.Type,
		res resource.Resource,
	) (uuid.UUID, error)

	// Update updates an existing resource of type resourceType matching the specified id with data
	// provided in res. All relevant fields in res will be updated in the database.
	//
	//     delivery.Status = "delivered"
	//     err := writer.Update(resource.TypeDelivery, deliveryID, delivery)
	//
	// An ErrInvalidPrimaryKey error is returned when the specified id was not found in the
	// database.
	Update(
		resourceType resource.Type,
		id uuid.UUID,
		res resource.Resource,
	) error

	// Delete deletes the resource of type resourceType matching the specified id.
	//
	//     err := writer.Delete(resource.TypeDelivery, deliveryID)
	//
	// An ErrInvalidPrimaryKey error is returned when the specified id was not found in the
	// database.
	Delete(
		resourceType resource.Type,
		id uuid.UUID,
	) error
}

// ReadWriter is the interface that wraps the Reader and Writer interfaces.
type ReadWriter interface {
	Reader
	Writer
}

var (
	// ErrInvalidCreatedUUID defines an error caused when a new resource is created, but the
	// database returned an ID that is not a valid UUID.
	// ErrInvalidCreatedUUID = errors.New("invalid created UUID")

	// ErrInvalidForeignKey defines an error caused by an invalid foreign key.
	// ErrInvalidForeignKey = errors.New("invalid foreign key")

	// ErrInvalidPrimaryKey defines an error caused by an invalid primary key.
	ErrInvalidPrimaryKey = errors.New("invalid primary key")

	// ErrInvalidRequest defines an error caused by an invalid request. This can be because of a
	// malformed payload, an unknown field, or invalid filters.
	// ErrInvalidRequest = errors.New("invalid request")
)
