//go:build !integration && !endtoend
// +build !integration,!endtoend

package resource_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestTemplateJSON(t *testing.T) {
	template := test.ArbitraryTemplate(t)

	expectedJSON := "{" +
		`"id":"00000000-0000-0000-0005-000000000000",` +
		`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
		`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
		`"languageTag":"language_tag_000000000000",` +
		`"body":"body_000000000000",` +
		`"deleted":false,` +
		`"createTime":"2000-01-01T05:00:58Z",` +
		`"updateTime":"2000-01-01T05:00:59Z"` +
		"}"

	actualJSON, err := json.Marshal(template)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	assert.Equal(t, expectedJSON, string(actualJSON))
}
