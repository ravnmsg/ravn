package resource

import (
	"time"

	"github.com/google/uuid"
)

// DeliveryKind defines the attributes related to a delivery kind.
type DeliveryKind struct {
	ID uuid.UUID `json:"id" db:"id"`

	// A human-readable identifier. Only one delivery kind should be active for a given code.
	Code string `json:"code" db:"code"`

	// A reference to the delivery kind's namespace.
	NamespaceID uuid.UUID `json:"namespaceId" db:"namespace_id"`

	// The deliverer host where it can be reached.
	Host string `json:"host" db:"host"`

	// Whether delivering messages using this delivery kind is blocked.
	Disabled bool `json:"disabled" db:"disabled"`

	// Whether the delivery kind has been deleted.
	Deleted bool `json:"deleted" db:"deleted"`

	CreateTime time.Time `json:"createTime" db:"create_time,omitempty"`
	UpdateTime time.Time `json:"updateTime" db:"update_time,omitempty"`
}
