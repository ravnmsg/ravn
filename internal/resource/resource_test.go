//go:build !integration && !endtoend
// +build !integration,!endtoend

package resource_test

import (
	"testing"

	"github.com/google/go-cmp/cmp"

	"gitlab.com/ravnmsg/ravn/internal/resource"
)

func TestResourceName(t *testing.T) {
	tests := map[string]struct {
		resourceType resource.Type
		expectedName string
	}{
		"delivery": {
			resourceType: resource.TypeDelivery,
			expectedName: "delivery",
		},
		"deliveryKind": {
			resourceType: resource.TypeDeliveryKind,
			expectedName: "deliveryKind",
		},
		"message": {
			resourceType: resource.TypeMessage,
			expectedName: "message",
		},
		"messageKind": {
			resourceType: resource.TypeMessageKind,
			expectedName: "messageKind",
		},
		"namespace": {
			resourceType: resource.TypeNamespace,
			expectedName: "namespace",
		},
		"template": {
			resourceType: resource.TypeTemplate,
			expectedName: "template",
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			name := tt.resourceType.Name()
			if tt.expectedName != name {
				t.Fatalf("expected name to be `%s`, but was `%s`", tt.expectedName, name)
			}
		})
	}
}

func TestResourceNew(t *testing.T) {
	tests := map[string]struct {
		resourceType     resource.Type
		expectedResource resource.Resource
	}{
		"delivery": {
			resourceType:     resource.TypeDelivery,
			expectedResource: &resource.Delivery{},
		},
		"deliveryKind": {
			resourceType:     resource.TypeDeliveryKind,
			expectedResource: &resource.DeliveryKind{},
		},
		"message": {
			resourceType:     resource.TypeMessage,
			expectedResource: &resource.Message{},
		},
		"messageKind": {
			resourceType:     resource.TypeMessageKind,
			expectedResource: &resource.MessageKind{},
		},
		"namespace": {
			resourceType:     resource.TypeNamespace,
			expectedResource: &resource.Namespace{},
		},
		"template": {
			resourceType:     resource.TypeTemplate,
			expectedResource: &resource.Template{},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			res := tt.resourceType.New()
			if !cmp.Equal(tt.expectedResource, res) {
				t.Fatalf(
					"expected resource to be different:\n%s",
					cmp.Diff(tt.expectedResource, res),
				)
			}
		})
	}
}
