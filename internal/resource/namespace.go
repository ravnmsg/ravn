package resource

import (
	"time"

	"github.com/google/uuid"
)

// Namespace defines the attributes related to a namespace.
type Namespace struct {
	ID uuid.UUID `json:"id" db:"id"`

	// A human-readable identifier. Only one namespace should be active for a given code.
	Code string `json:"code" db:"code"`

	// The ID of the parent namespace. A root namespace has no parent IDs.
	ParentID *uuid.UUID `json:"parentId" db:"parent_id"`

	// Whether the namespace has been deleted.
	Deleted bool `json:"deleted" db:"deleted"`

	CreateTime time.Time `json:"createTime" db:"create_time,omitempty"`
	UpdateTime time.Time `json:"updateTime" db:"update_time,omitempty"`
}
