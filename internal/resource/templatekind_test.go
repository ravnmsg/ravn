//go:build !integration && !endtoend
// +build !integration,!endtoend

package resource_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestTemplateKindJSON(t *testing.T) {
	templateKind := test.ArbitraryTemplateKind(t)

	expectedJSON := "{" +
		`"id":"00000000-0000-0000-0004-000000000000",` +
		`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
		`"targetAttr":"target_attr_000000000000",` +
		`"deleted":false,` +
		`"createTime":"2000-01-01T04:00:58Z",` +
		`"updateTime":"2000-01-01T04:00:59Z"` +
		"}"

	actualJSON, err := json.Marshal(templateKind)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	assert.Equal(t, expectedJSON, string(actualJSON))
}
