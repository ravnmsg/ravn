//go:build !integration && !endtoend
// +build !integration,!endtoend

package resource_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestDeliveryJSON(t *testing.T) {
	delivery := test.ArbitraryDelivery(t)

	expectedJSON := "{" +
		`"id":"00000000-0000-0000-0007-000000000000",` +
		`"messageId":"00000000-0000-0000-0006-000000000000",` +
		`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
		`"languageTag":"language_tag_000000000000",` +
		`"status":"delivered",` +
		`"statusMessage":"status_message_000000000000",` +
		`"deliverTime":"2000-01-01T07:00:00Z",` +
		`"input":{"input_key":"input_value_000000000000"},` +
		`"output":{"output_key":"output_value_000000000000"},` +
		`"externalId":"external_id_000000000000",` +
		`"createTime":"2000-01-01T07:00:58Z",` +
		`"updateTime":"2000-01-01T07:00:59Z"` +
		"}"

	actualJSON, err := json.Marshal(delivery)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	assert.Equal(t, expectedJSON, string(actualJSON))
}
