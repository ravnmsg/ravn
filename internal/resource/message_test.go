//go:build !integration && !endtoend
// +build !integration,!endtoend

package resource_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestMessageJSON(t *testing.T) {
	message := test.ArbitraryMessage(t)

	expectedJSON := "{" +
		`"id":"00000000-0000-0000-0006-000000000000",` +
		`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
		`"input":{"key":"value_000000000000"},` +
		`"status":"succeeded",` +
		`"scheduleTime":"2000-01-01T06:00:00Z",` +
		`"createTime":"2000-01-01T06:00:58Z",` +
		`"updateTime":"2000-01-01T06:00:59Z"` +
		"}"

	actualJSON, err := json.Marshal(message)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	assert.Equal(t, expectedJSON, string(actualJSON))
}
