package resource

import (
	"time"

	"github.com/google/uuid"
)

// Template defines the attributes related to a template that can generate a message.
type Template struct {
	ID uuid.UUID `json:"id" db:"id"`

	// A reference to the message kind.
	MessageKindID uuid.UUID `json:"messageKindId" db:"message_kind_id"`

	// A reference to the template kind.
	TemplateKindID uuid.UUID `json:"templateKindId" db:"template_kind_id"`

	// The IETF language tag into which a content should be generated.
	LanguageTag string `json:"languageTag" db:"language_tag"`

	// The template body that will be provided to the template engine.
	Body string `json:"body" db:"body"`

	// Whether the template has been deleted.
	Deleted bool `json:"deleted" db:"deleted"`

	CreateTime time.Time `json:"createTime" db:"create_time,omitempty"`
	UpdateTime time.Time `json:"updateTime" db:"update_time,omitempty"`
}
