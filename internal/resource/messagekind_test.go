//go:build !integration && !endtoend
// +build !integration,!endtoend

package resource_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestMessageKindJSON(t *testing.T) {
	messageKind := test.ArbitraryMessageKind(t)

	expectedJSON := "{" +
		`"id":"00000000-0000-0000-0003-000000000000",` +
		`"code":"message_kind_000000000000",` +
		`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
		`"disabled":false,` +
		`"deleted":false,` +
		`"createTime":"2000-01-01T03:00:58Z",` +
		`"updateTime":"2000-01-01T03:00:59Z"` +
		"}"

	actualJSON, err := json.Marshal(messageKind)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	assert.Equal(t, expectedJSON, string(actualJSON))
}
