package resource

import (
	"fmt"
)

// Resource defines the interface for a resource.
type Resource interface{}

// Type defines the enumerator for resource types.
type Type int

// Enumeration of all resource types.
const (
	TypeNone = Type(iota)
	TypeDelivery
	TypeDeliveryKind
	TypeMessage
	TypeMessageKind
	TypeNamespace
	TypeTemplate
	TypeTemplateKind
)

// Name returns the resource name for the specified resource.
func (t Type) Name() string {
	switch t {
	case TypeDelivery:
		return "delivery"
	case TypeDeliveryKind:
		return "deliveryKind"
	case TypeMessage:
		return "message"
	case TypeMessageKind:
		return "messageKind"
	case TypeNamespace:
		return "namespace"
	case TypeTemplate:
		return "template"
	case TypeTemplateKind:
		return "templateKind"
	default:
		panic(fmt.Sprintf("name for resource `%v` is not configured", t))
	}
}

// New returns an empty instance of the resource type.
func (t Type) New() Resource {
	switch t {
	case TypeDelivery:
		return new(Delivery)
	case TypeDeliveryKind:
		return new(DeliveryKind)
	case TypeMessage:
		return new(Message)
	case TypeMessageKind:
		return new(MessageKind)
	case TypeNamespace:
		return new(Namespace)
	case TypeTemplate:
		return new(Template)
	case TypeTemplateKind:
		return new(TemplateKind)
	default:
		panic(fmt.Sprintf("instance for resource type `%s` not configured", t.Name()))
	}
}
