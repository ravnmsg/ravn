//go:build !integration && !endtoend
// +build !integration,!endtoend

package resource_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestDeliveryKindJSON(t *testing.T) {
	deliveryKind := test.ArbitraryDeliveryKind(t)

	expectedJSON := "{" +
		`"id":"00000000-0000-0000-0002-000000000000",` +
		`"code":"delivery_kind_000000000000",` +
		`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
		`"host":"host_000000000000",` +
		`"disabled":false,` +
		`"deleted":false,` +
		`"createTime":"2000-01-01T02:00:58Z",` +
		`"updateTime":"2000-01-01T02:00:59Z"` +
		"}"

	actualJSON, err := json.Marshal(deliveryKind)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	assert.Equal(t, expectedJSON, string(actualJSON))
}
