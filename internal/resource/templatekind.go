package resource

import (
	"time"

	"github.com/google/uuid"
)

// TemplateKind defines the attributes related to a template kind.
type TemplateKind struct {
	ID uuid.UUID `json:"id" db:"id"`

	// A reference to the delivery kind.
	DeliveryKindID uuid.UUID `json:"deliveryKindId" db:"delivery_kind_id"`

	// The attribute name into which the result will be stored.
	TargetAttr string `json:"targetAttr" db:"target_attr"`

	// Whether the template kind has been deleted.
	Deleted bool `json:"deleted" db:"deleted"`

	CreateTime time.Time `json:"createTime" db:"create_time,omitempty"`
	UpdateTime time.Time `json:"updateTime" db:"update_time,omitempty"`
}
