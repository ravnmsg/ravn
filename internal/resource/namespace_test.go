//go:build !integration && !endtoend
// +build !integration,!endtoend

package resource_test

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestNamespaceJSON(t *testing.T) {
	namespace := test.ArbitraryNamespace(t)

	expectedJSON := "{" +
		`"id":"00000000-0000-0000-0001-000000000001",` +
		`"code":"namespace_000000000001",` +
		`"parentId":"00000000-0000-0000-0001-000000000000",` +
		`"deleted":false,` +
		`"createTime":"2000-01-01T01:01:58Z",` +
		`"updateTime":"2000-01-01T01:01:59Z"` +
		"}"

	actualJSON, err := json.Marshal(namespace)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	assert.Equal(t, expectedJSON, string(actualJSON))
}
