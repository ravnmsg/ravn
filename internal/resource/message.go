package resource

import (
	"time"

	"github.com/google/uuid"
)

// Message defines the attributes related to a message.
type Message struct {
	ID uuid.UUID `json:"id" db:"id"`

	// A reference to the message's kind.
	MessageKindID uuid.UUID `json:"messageKindId" db:"message_kind_id"`

	// The initial input provided by the user.
	Input map[string]interface{} `json:"input" db:"input"`

	// The current status of the message, either `active`, `scheduled`, `succeeded`, or `failed`.
	Status string `json:"status" db:"status"`

	// The time at which the message is scheduled to be delivered.
	ScheduleTime *time.Time `json:"scheduleTime" db:"schedule_time"`

	CreateTime time.Time `json:"createTime" db:"create_time,omitempty"`
	UpdateTime time.Time `json:"updateTime" db:"update_time,omitempty"`
}
