package resource

import (
	"time"

	"github.com/google/uuid"
)

// Delivery defines the attributes related to a delivery.
type Delivery struct {
	ID uuid.UUID `json:"id" db:"id"`

	// A reference to the requested message.
	MessageID uuid.UUID `json:"messageId" db:"message_id"`

	// A reference to how the message was delivered.
	DeliveryKindID uuid.UUID `json:"deliveryKindId" db:"delivery_kind_id"`

	// The IETF language tag into which the message content was generated.
	LanguageTag string `json:"languageTag" db:"language_tag"`

	// The current status of the delivery, either `pending`, `delivering`, `delivered`,
	// `undelivered`, or `blocked`.
	Status string `json:"status" db:"status"`

	// Any information about the delivery's current status.
	StatusMessage string `json:"statusMessage" db:"status_message"`

	// The time when the delivery was actually done. A value does not indicate that the delivery was
	// successful - the status field must instead be used for this purpose.
	DeliverTime *time.Time `json:"deliverTime" db:"deliver_time"`

	// The input provided to the requester when the delivery was made, and its resulting output.
	// These may contain sensitive information.
	Input  map[string]interface{} `json:"input" db:"input"`
	Output map[string]interface{} `json:"output" db:"output"`

	// The delivery's identifier from the point of view of the third-party service that
	// performed the actual delivery. This is used when that service performs a callback with more
	// information about the delivery.
	ExternalID string `json:"externalId" db:"external_id"`

	CreateTime time.Time `json:"createTime" db:"create_time,omitempty"`
	UpdateTime time.Time `json:"updateTime" db:"update_time,omitempty"`
}
