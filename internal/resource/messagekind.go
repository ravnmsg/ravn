package resource

import (
	"time"

	"github.com/google/uuid"
)

// MessageKind defines the attributes related to a message kind.
type MessageKind struct {
	ID uuid.UUID `json:"id" db:"id"`

	// A human-readable identifier. Only one message kind should be active for a given code.
	Code string `json:"code" db:"code"`

	// A reference to the message kind's namespace.
	NamespaceID uuid.UUID `json:"namespaceId" db:"namespace_id"`

	// Whether delivering messages using this message kind is blocked.
	Disabled bool `json:"disabled" db:"disabled"`

	// Whether the message kind has been deleted.
	Deleted bool `json:"deleted" db:"deleted"`

	CreateTime time.Time `json:"createTime" db:"create_time,omitempty"`
	UpdateTime time.Time `json:"updateTime" db:"update_time,omitempty"`
}
