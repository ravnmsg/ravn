//go:build !unit && !endtoend
// +build !unit,!endtoend

package natsmsgqueue_test

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/nats-io/nats.go"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/msgqueue/natsmsgqueue"
)

func TestNew(t *testing.T) {
	tests := map[string]struct {
		uri           string
		subject       string
		expectedError error
	}{
		"when successful": {
			uri:     natsURI(),
			subject: "subject",
		},
		"when URI is missing": {
			uri:           "",
			subject:       "subject",
			expectedError: fmt.Errorf("URI missing"),
		},
		"when URI is invalid": {
			uri:           "invalid",
			subject:       "subject",
			expectedError: fmt.Errorf("error connecting to NATS: URI invalid or no such host"),
		},
		"when subject is missing": {
			uri:           natsURI(),
			subject:       "",
			expectedError: fmt.Errorf("invalid subject ``"),
		},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			_, err := natsmsgqueue.New(ctx, tt.uri, tt.subject)
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestPublish(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	baseNATS, err := nats.Connect(natsURI())
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	defer baseNATS.Close()

	subject := uuid.New().String()
	msg := uuid.New().String()
	mq := natsmq(ctx, subject, t)

	testDone := false

	go func(t *testing.T) {
		_, err = baseNATS.Subscribe(subject, func(m *nats.Msg) {
			if string(m.Data) != msg {
				t.Errorf("expected message to be `%s`, but was `%s`", msg, string(m.Data))
			}

			testDone = true
			cancel()
		})
		if err != nil {
			t.Errorf("unexpected error: %s", err)
		}
	}(t)

	time.Sleep(10 * time.Millisecond)

	if err := mq.Publish([]byte(msg)); err != nil {
		t.Fatalf("expected not to fail, but did with error `%s`", err)
	}

	<-ctx.Done()

	if !testDone {
		t.Fatalf("test timeout")
	}
}

func TestSubscribe(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	baseNATS, err := nats.Connect(natsURI())
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}
	defer baseNATS.Close()

	subject := uuid.New().String()
	msg := uuid.New().String()
	mq := natsmq(ctx, subject, t)

	testDone := false

	go func(t *testing.T) {
		err = mq.Subscribe(func(data []byte) error {
			if string(data) != msg {
				t.Errorf("expected message to be `%s`, but was `%s`", msg, string(data))
			}

			testDone = true
			cancel()

			return nil
		})
		if err != nil {
			t.Errorf("expected not to fail, but did with error `%s`", err)
		}
	}(t)

	time.Sleep(10 * time.Millisecond)

	if err := baseNATS.Publish(subject, []byte(msg)); err != nil {
		t.Fatalf("expected not to fail, but did with error `%s`", err)
	}

	<-ctx.Done()

	if !testDone {
		t.Fatalf("test timeout")
	}
}

func natsmq(ctx context.Context, subject string, t *testing.T) *natsmsgqueue.MSGQueue {
	mq, err := natsmsgqueue.New(ctx, natsURI(), subject)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	return mq
}

func natsURI() string {
	uri := os.Getenv("RAVN_NATS_URI")
	if uri != "" {
		return uri
	}

	host := os.Getenv("RAVN_NATS_HOST")
	if host == "" {
		host = "localhost:4222"
	}

	return fmt.Sprintf("nats://ravn:ravn@%s", host)
}
