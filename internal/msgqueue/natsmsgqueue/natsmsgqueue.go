package natsmsgqueue

import (
	"context"
	"fmt"
	"log"

	"github.com/nats-io/nats.go"

	"gitlab.com/ravnmsg/ravn/internal/msgqueue"
	"gitlab.com/ravnmsg/ravn/internal/msgqueue/handler"
)

// MSGQueue defines a messaging queue handled by the NATS server.
type MSGQueue struct {
	ctx     context.Context
	conn    *nats.Conn
	subject string
}

// New creates and opens a NATS messaging queue connection.
func New(ctx context.Context, uri, subject string) (*MSGQueue, error) {
	if uri == "" {
		return nil, fmt.Errorf("URI missing")
	}

	nc, err := nats.Connect(uri)
	if err != nil {
		// TODO: add explanation to this error. On localhost, this gives
		//
		//     error connecting to NATS: dial tcp: lookup invalid: no such host
		//
		// while on another server, it results in
		//
		//     error connecting to NATS: dial tcp: lookup invalid on {host ip:port}: no such host
		//
		// Since the error message is not in Ravn's control, and can also change based on the host
		// IP address and port, this should'nt be in the main error message.
		return nil, fmt.Errorf("error connecting to NATS: URI invalid or no such host")
	}

	if subject == "" {
		return nil, fmt.Errorf("invalid subject `%s`", subject)
	}

	mq := MSGQueue{
		ctx:     ctx,
		conn:    nc,
		subject: subject,
	}

	// Closes the NATS mesasging queue connection.
	go func() {
		<-mq.ctx.Done()

		if err := mq.conn.Drain(); err != nil {
			return
		}
	}()

	return &mq, nil
}

// Publish publishes a message to the specified subject.
func (mq *MSGQueue) Publish(data []byte) error {
	if err := mq.conn.Publish(mq.subject, data); err != nil {
		return fmt.Errorf("error publishing to subject `%s`: %w", mq.subject, err)
	}

	return nil
}

// Subscribe opens a subscriber to the specified subject. Incoming messages will be forwarded to the
// handler. This will block until NATS's context ends.
func (mq *MSGQueue) Subscribe(h handler.MSGHandlerFunc) error {
	if _, err := mq.conn.QueueSubscribe(
		mq.subject,
		"workers",
		func(msg *nats.Msg) {
			if err := h(msg.Data); err != nil {
				log.Printf("Error handling message: %s", err)
			}
		},
	); err != nil {
		return fmt.Errorf("error subscribing to subject `%s`: %w", mq.subject, err)
	}

	// This blocks until the context is canceled.
	<-mq.ctx.Done()

	return nil
}

var _ = msgqueue.Publisher(new(MSGQueue))
var _ = msgqueue.Subscriber(new(MSGQueue))
