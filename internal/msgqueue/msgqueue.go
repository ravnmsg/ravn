package msgqueue

import (
	"gitlab.com/ravnmsg/ravn/internal/msgqueue/handler"
)

// Publisher is the interface that wraps the basic Publish method.
type Publisher interface {
	// Publish publishes the message data to the specified queue.
	Publish([]byte) error
}

// Subscriber is the interface that wraps the basic Subscribe method.
type Subscriber interface {
	// Subscribe opens a connection to the specified queue, and triggers the handler function on
	// each received message data. It is expected to block.
	Subscribe(handler.MSGHandlerFunc) error
}
