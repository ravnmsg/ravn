//go:build !integration && !endtoend
// +build !integration,!endtoend

package scheduler_test

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/scheduler"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestRetrieveScheduledMessages(t *testing.T) {
	tests := map[string]struct {
		messageRetrieveManyExpectation *test.MessageActionRetrieveManyExpectation
		expectedMessages               []*resource.Message
		expectedError                  error
	}{
		"when successful": {
			messageRetrieveManyExpectation: &test.MessageActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Status", query.OpEqual, "scheduled"),
					query.WithFilterArgs("ScheduleTime", query.OpLessThanOrEqual, test.MakeTime("2000-01-01T00:00:00Z", t)),
				),
				ReturnedMessages: []*resource.Message{
					{
						ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
						Status:        "scheduled",
						ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
						CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
						UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
					},
					{
						ID:            uuid.MustParse("00000000-0000-0000-0006-000000000001"),
						MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
						Status:        "scheduled",
						ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
						CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
						UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
					},
				},
			},
			expectedMessages: []*resource.Message{
				{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Status:        "scheduled",
					ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
				},
				{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000001"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Status:        "scheduled",
					ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
				},
			},
			expectedError: nil,
		},
		"when messages failed to be retrieved": {
			messageRetrieveManyExpectation: &test.MessageActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Status", query.OpEqual, "scheduled"),
					query.WithFilterArgs("ScheduleTime", query.OpLessThanOrEqual, test.MakeTime("2000-01-01T00:00:00Z", t)),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedMessages: nil,
			expectedError:    fmt.Errorf("error retrieving messages: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			ma := test.NewMessageAction(t)
			ma.ExpectRetrieveMany(tt.messageRetrieveManyExpectation)

			sched := scheduler.New(nil, ma, test.NewClock("2000-01-01T00:00:00Z"))
			messages, err := sched.RetrieveScheduledMessages()

			assert.Equal(t, tt.expectedMessages, messages)
			assert.Equal(t, tt.expectedError, err)

			ma.AssertExpectations(t)
		})
	}
}

func TestDeliverMessage(t *testing.T) {
	tests := map[string]struct {
		message                         *resource.Message
		deliveryRetrieveManyExpectation *test.DeliveryActionRetrieveManyExpectation
		messageUpdateExpectation        *test.MessageActionUpdateExpectation
		deliveryQueueRequestExpectation *test.DeliveryActionQueueRequestExpectation
		expectedError                   error
	}{
		"when successful": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "scheduled",
				ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0006-000000000000")),
				),
				ReturnedDeliveries: []*resource.Delivery{
					{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "pending",
						CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
						UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
					},
				},
			},
			messageUpdateExpectation: &test.MessageActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Status:        "active",
					ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
				},
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Status:        "active",
					ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
				},
			},
			deliveryQueueRequestExpectation: &test.DeliveryActionQueueRequestExpectation{
				ExpectedDeliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedError:      nil,
			},
			expectedError: nil,
		},
		"when message is not scheduled": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "succeeded",
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			expectedError: fmt.Errorf("expected message to be scheduled, but was `succeeded`"),
		},
		"when deliveries failed to be retrieved": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "scheduled",
				ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0006-000000000000")),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error retrieving deliveries: arbitrary"),
		},
		"when message has no deliveries": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "scheduled",
				ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0006-000000000000")),
				),
				ReturnedDeliveries: []*resource.Delivery{},
			},
			expectedError: fmt.Errorf("expected one delivery"),
		},
		"when message has multiple deliveries": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "scheduled",
				ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0006-000000000000")),
				),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
					test.ArbitraryDelivery(t),
				},
			},
			expectedError: fmt.Errorf("expected one delivery"),
		},
		"when delivery is not pending": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "scheduled",
				ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0006-000000000000")),
				),
				ReturnedDeliveries: []*resource.Delivery{
					{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivered",
						StatusMessage:  "status_message_000000000000",
						DeliverTime:    test.MakeTimeRef("2000-01-01T07:00:00Z", t),
						ExternalID:     "external_id_000000000000",
						CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
						UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
					},
				},
			},
			expectedError: fmt.Errorf("expected delivery to be pending, but was `delivered`"),
		},
		"when message failed to be updated": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "scheduled",
				ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0006-000000000000")),
				),
				ReturnedDeliveries: []*resource.Delivery{
					{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "pending",
						CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
						UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
					},
				},
			},
			messageUpdateExpectation: &test.MessageActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Status:        "active",
					ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error updating message: arbitrary"),
		},
		"when delivery failed to be queued": {
			message: &resource.Message{
				ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				Status:        "scheduled",
				ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
				CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
				UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0006-000000000000")),
				),
				ReturnedDeliveries: []*resource.Delivery{
					{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "pending",
						CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
						UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
					},
				},
			},
			messageUpdateExpectation: &test.MessageActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Status:        "active",
					ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
				},
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Status:        "active",
					ScheduleTime:  test.MakeTimeRef("2100-01-01T06:00:00Z", t),
					CreateTime:    test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:    test.MakeTime("2000-01-01T06:00:59Z", t),
				},
			},
			deliveryQueueRequestExpectation: &test.DeliveryActionQueueRequestExpectation{
				ExpectedDeliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedError:      test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error queuing delivery: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			da := test.NewDeliveryAction(t)
			da.ExpectRetrieveMany(tt.deliveryRetrieveManyExpectation)
			da.ExpectQueueRequest(tt.deliveryQueueRequestExpectation)

			ma := test.NewMessageAction(t)
			ma.ExpectUpdate(tt.messageUpdateExpectation)

			sched := scheduler.New(da, ma, test.NewClock("2000-01-01T00:00:00Z"))
			err := sched.DeliverMessage(tt.message)

			assert.Equal(t, tt.expectedError, err)

			da.AssertExpectations(t)
			ma.AssertExpectations(t)
		})
	}
}
