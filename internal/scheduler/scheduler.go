package scheduler

import (
	"fmt"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// Scheduler defines the service in charge of retrieving the next batch of scheduled messages.
type Scheduler struct {
	da  action.DeliveryActioner
	ma  action.MessageActioner
	clk clock.Clocker
}

// New creates a Scheduler instance.
func New(
	da action.DeliveryActioner,
	ma action.MessageActioner,
	clk clock.Clocker,
) *Scheduler {
	return &Scheduler{
		da:  da,
		ma:  ma,
		clk: clk,
	}
}

func (s *Scheduler) RetrieveScheduledMessages() ([]*resource.Message, error) {
	q := query.NewQuery(
		query.WithFilterArgs("Status", query.OpEqual, "scheduled"),
		query.WithFilterArgs("ScheduleTime", query.OpLessThanOrEqual, s.clk.Now()),
	)
	messages, err := s.ma.RetrieveMany(q)
	if err != nil {
		return nil, fmt.Errorf("error retrieving messages: %s", err)
	}

	return messages, nil
}

// DeliverMessage queues the message with the specified id to be delivered.
func (s *Scheduler) DeliverMessage(message *resource.Message) error {
	if message.Status != "scheduled" {
		return fmt.Errorf("expected message to be scheduled, but was `%s`", message.Status)
	}

	q := query.NewQuery(
		query.WithFilterArgs("MessageID", query.OpEqual, message.ID),
	)
	deliveries, err := s.da.RetrieveMany(q)
	if err != nil {
		return fmt.Errorf("error retrieving deliveries: %s", err)
	} else if len(deliveries) != 1 {
		return fmt.Errorf("expected one delivery")
	}

	delivery := deliveries[0]
	if delivery.Status != "pending" {
		return fmt.Errorf("expected delivery to be pending, but was `%s`", delivery.Status)
	}

	message.Status = "active"
	if _, err = s.ma.Update(message.ID, message); err != nil {
		return fmt.Errorf("error updating message: %s", err)
	}

	if err := s.da.QueueRequest(delivery.ID); err != nil {
		return fmt.Errorf("error queuing delivery: %s", err)
	}

	return nil
}
