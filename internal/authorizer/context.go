package authorizer

import "context"

// ctxKey defines a context key. See https://blog.golang.org/context for more information.
type ctxKey int

// List of keys that can be added to a context.
const (
	ctxKeyUserRoles ctxKey = iota
)

func UserRoles(ctx context.Context) []*Role {
	return ctx.Value(ctxKeyUserRoles).([]*Role)
}

func SetUserRoles(ctx context.Context, roles []*Role) context.Context {
	return context.WithValue(ctx, ctxKeyUserRoles, roles)
}
