//go:build !integration && !endtoend
// +build !integration,!endtoend

package authorizer_test

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
)

func TestParseAuthToken(t *testing.T) {
	tests := map[string]struct {
		header        string
		payload       string
		signature     string
		expectedRoles []*authorizer.Role
		expectedError error
	}{
		"when successful": {
			header:    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
			payload:   "eyJyb2xlcyI6W3siY29kZSI6Im5zX2VkaXRvciIsIm5hbWVzcGFjZUlkIjoiMDAwMDAwMDAtMDAwMC0wMDAwLTAwMDEtMDAwMDAwMDAwMDAwIn1dfQ",
			signature: "uK6i6qh6QnCgQz4n6jdM7k5bzDvbvLCdWh2eBilyZPc",
			expectedRoles: []*authorizer.Role{
				{Code: "ns_editor", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
			},
		},
		"when parts are empty": {
			header:        "",
			payload:       "",
			signature:     "",
			expectedError: authorizer.ErrInvalidJWT,
		},
		"when payload is invalid": {
			header:        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
			payload:       "aW52YWxpZA", // invalid
			signature:     "J4zSoiDdGTZ5JhqFZNpFrRCdFs9xrqunmKOcTLNzxG4",
			expectedError: authorizer.ErrInvalidJWT,
		},
		"when payload is a string": {
			header:        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
			payload:       "ImludmFsaWQi", // "invalid"
			signature:     "M_xoQR9i4TUm_RcsVXnIQvCTz4vtb9FkEMbBIWj9OBA",
			expectedError: authorizer.ErrInvalidJWT,
		},
		"when signature does not match payload": {
			header:        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
			payload:       "eyJyb2xlcyI6W3siY29kZSI6Im5zX2VkaXRvciIsIm5hbWVzcGFjZUlkIjoiMDAwMDAwMDAtMDAwMC0wMDAwLTAwMDEtMDAwMDAwMDAwMDAwIn1dfQ",
			signature:     "aW52YWxpZA",
			expectedError: authorizer.ErrInvalidJWT,
		},
		"when payload does not have any roles": {
			header:        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
			payload:       "e30",
			signature:     "E5urBL-NPzmEQTG5hJQSyAaIxNdhbGiClIoMCE7B0n4",
			expectedError: authorizer.ErrInvalidJWTPayload,
		},
		"when payload has invalid roles": {
			header:        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
			payload:       "eyJyb2xlcyI6ImludmFsaWQifQ",
			signature:     "86TNseW5clGMkcjVgCUKCiV2mUPcdPaDfItVmaMe0bA",
			expectedError: authorizer.ErrInvalidJWTPayload,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			jwt := fmt.Sprintf("%s.%s.%s", tt.header, tt.payload, tt.signature)
			roles, err := authorizer.ParseAuthToken(jwt)

			assert.Equal(t, tt.expectedRoles, roles)
			assert.Equal(t, tt.expectedError, err)
		})
	}
}
