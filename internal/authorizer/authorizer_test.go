//go:build !integration && !endtoend
// +build !integration,!endtoend

package authorizer_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestAuthorizedNamespaceIDs(t *testing.T) {
	tests := map[string]struct {
		resourceType                          resource.Type
		op                                    string
		userRoles                             []*authorizer.Role
		namespaceRetrieveParentsExpectations  []*test.NamespaceActionRetrieveParentsExpectation
		namespaceRetrieveChildrenExpectations []*test.NamespaceActionRetrieveChildrenExpectation
		expectedNamespaceIDs                  []uuid.UUID
		expectedError                         error
	}{
		"when successful": {
			resourceType: resource.TypeDeliveryKind,
			op:           "read",
			userRoles: []*authorizer.Role{
				{Code: "ns_writer", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
				{Code: "ns_reader", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},

				// This role should be skipped in both namespaceRetrieveParentsExpectations and
				// namespaceRetrieveChildrenExpectations because the namespace ID is already in the
				// list.
				{Code: "ns_editor", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000003")},

				// This role should be skipped because the it doesn't have the correct permissions.
				{Code: "message_reader", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000004")},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000000"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			namespaceRetrieveChildrenExpectations: []*test.NamespaceActionRetrieveChildrenExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000003")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000000"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000003")},
					},
				},
			},
			expectedNamespaceIDs: []uuid.UUID{
				uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				uuid.MustParse("00000000-0000-0000-0001-000000000003"),
				uuid.MustParse("00000000-0000-0000-0001-000000000000"),
			},
			expectedError: nil,
		},
		"when not authorized": {
			resourceType:         resource.TypeDeliveryKind,
			op:                   "read",
			userRoles:            []*authorizer.Role{},
			expectedNamespaceIDs: nil,
			expectedError:        authorizer.ErrUnauthorized,
		},
		"when namespace tree is deleted": {
			resourceType: resource.TypeDeliveryKind,
			op:           "read",
			userRoles: []*authorizer.Role{
				{Code: "ns_reader", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
				{Code: "ns_reader", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
					},
				},
			},
			namespaceRetrieveChildrenExpectations: []*test.NamespaceActionRetrieveChildrenExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
					},
				},
			},
			expectedNamespaceIDs: []uuid.UUID{
				uuid.MustParse("00000000-0000-0000-0001-000000000002"),
			},
			expectedError: nil,
		},
		"when namespace parents failed to be retrieved": {
			resourceType: resource.TypeDeliveryKind,
			op:           "read",
			userRoles: []*authorizer.Role{
				{Code: "ns_reader", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
				{Code: "ns_reader", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			namespaceRetrieveChildrenExpectations: []*test.NamespaceActionRetrieveChildrenExpectation{
				{
					ExpectedID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{},
				},
			},
			expectedNamespaceIDs: nil,
			expectedError:        fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when namespace children failed to be retrieved": {
			resourceType: resource.TypeDeliveryKind,
			op:           "read",
			userRoles: []*authorizer.Role{
				{Code: "ns_reader", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
				{Code: "ns_reader", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
					},
				},
			},
			namespaceRetrieveChildrenExpectations: []*test.NamespaceActionRetrieveChildrenExpectation{
				{
					ExpectedID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedNamespaceIDs: nil,
			expectedError:        fmt.Errorf("error retrieving children namespaces: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			na := test.NewNamespaceAction(t)
			for _, e := range tt.namespaceRetrieveParentsExpectations {
				na.ExpectRetrieveParents(e)
			}
			for _, e := range tt.namespaceRetrieveChildrenExpectations {
				na.ExpectRetrieveChildren(e)
			}

			ctx := context.Background()
			ctx = authorizer.SetUserRoles(ctx, tt.userRoles)

			a := authorizer.NewAuthorizer(na)
			namespaceIDs, err := a.AuthorizedNamespaceIDs(ctx, tt.resourceType, tt.op)

			assert.Equal(t, tt.expectedNamespaceIDs, namespaceIDs)
			assert.Equal(t, tt.expectedError, err)

			na.AssertExpectations(t)
		})
	}
}

func TestAuthorizeOperation(t *testing.T) {
	tests := map[string]struct {
		namespaceID                         uuid.UUID
		resourceType                        resource.Type
		op                                  string
		userRoles                           []*authorizer.Role
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		expectedError                       error
	}{
		"when authorized on specified namespaces": {
			namespaceID:  uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			resourceType: resource.TypeDeliveryKind,
			op:           "read",
			userRoles: []*authorizer.Role{
				{Code: "ns_writer", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedError: nil,
		},
		"when authorized on parent namespace": {
			namespaceID:  uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			resourceType: resource.TypeDeliveryKind,
			op:           "read",
			userRoles: []*authorizer.Role{
				{Code: "ns_writer", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedError: nil,
		},
		"when not authorized": {
			namespaceID:  uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			resourceType: resource.TypeDeliveryKind,
			op:           "read",
			userRoles:    []*authorizer.Role{},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when namespace parents failed to be retrieved": {
			namespaceID:  uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			resourceType: resource.TypeDeliveryKind,
			op:           "read",
			userRoles: []*authorizer.Role{
				{Code: "ns_writer", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			ctx := context.Background()
			ctx = authorizer.SetUserRoles(ctx, tt.userRoles)

			a := authorizer.NewAuthorizer(na)
			err := a.AuthorizeOperation(ctx, tt.namespaceID, tt.resourceType, tt.op)

			assert.Equal(t, tt.expectedError, err)

			na.AssertExpectations(t)
		})
	}
}
