//go:build !integration && !endtoend
// +build !integration,!endtoend

package authorizer_test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/authorizer"
)

func TestUserRoles(t *testing.T) {
	ctx := context.Background()
	userRoles := []*authorizer.Role{
		{Code: "code_1", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
		{Code: "code_2", NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
	}

	ctx = authorizer.SetUserRoles(ctx, userRoles)

	assert.Equal(t, userRoles, authorizer.UserRoles(ctx))
}
