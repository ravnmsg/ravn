package authorizer

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type UnsecuredAuthorizer struct {
	na action.NamespaceActioner
}

func NewUnsecuredAuthorizer(na action.NamespaceActioner) *UnsecuredAuthorizer {
	return &UnsecuredAuthorizer{
		na: na,
	}
}

// AuthorizedNamespaceIDs returns all namespace IDs that are not part of deleted namespace branches.
func (a *UnsecuredAuthorizer) AuthorizedNamespaceIDs(
	_ context.Context,
	_ resource.Type,
	_ string,
) ([]uuid.UUID, error) {
	q := query.NewQuery(
		query.WithFilterArgs("ParentID", query.OpEqual, nil),
		query.WithFilterArgs("Deleted", query.OpEqual, false),
	)
	rootNamespaces, err := a.na.RetrieveMany(q)
	if err != nil {
		return nil, fmt.Errorf("error retrieving root namespaces: %s", err)
	}

	// Retrieves all children of the root namespaces. The namespaceChildren function will take care
	// to ignore deleted namespace branches.
	namespaces := make([]*resource.Namespace, 0)
	for _, n := range rootNamespaces {
		childNamespaces, err := a.na.RetrieveChildren(n.ID)
		if err != nil {
			return nil, fmt.Errorf("error retrieving children namespaces: %s", err)
		}

		namespaces = append(namespaces, childNamespaces...)
	}

	if len(namespaces) == 0 {
		return nil, ErrUnauthorized
	}

	return uniqueNamespaceIDs(namespaces), nil
}

// AuthorizeOperation authorizes all operations, given that the provided namespace, or one of its
// parents, is deleted.
func (a *UnsecuredAuthorizer) AuthorizeOperation(
	_ context.Context,
	namespaceID uuid.UUID,
	_ resource.Type,
	_ string,
) error {
	if _, err := a.na.RetrieveParents(namespaceID); err != nil {
		return fmt.Errorf("error retrieving parent namespaces: %s", err)
	}

	return nil
}

var _ = Authorizer(new(UnsecuredAuthorizer))
