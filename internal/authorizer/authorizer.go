package authorizer

import (
	"context"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type Authorizer interface {
	AuthorizedNamespaceIDs(ctx context.Context, rt resource.Type, op string) ([]uuid.UUID, error)
	AuthorizeOperation(ctx context.Context, namespaceID uuid.UUID, rt resource.Type, op string) error
}

type AppAuthorizer struct {
	na action.NamespaceActioner
}

func NewAuthorizer(na action.NamespaceActioner) *AppAuthorizer {
	return &AppAuthorizer{
		na: na,
	}
}

func (a *AppAuthorizer) AuthorizedNamespaceIDs(ctx context.Context, rt resource.Type, op string) ([]uuid.UUID, error) {
	namespaces := make([]*resource.Namespace, 0)

	userRoles := UserRoles(ctx)
	for _, ur := range userRoles {
		for _, permission := range RolePermissions[ur.Code] {
			if permission.ResourceType != rt || permission.Operation != op {
				continue
			} else if namespaceIDAlreadyDone(ur.NamespaceID, namespaces) {
				continue
			}

			// Validates that ur.NamespaceID and all of its parents are valid and not deleted. If
			// the namespace, or one of its parents, is deleted, the current namespace is simply
			// ignored.
			if _, err := a.na.RetrieveParents(ur.NamespaceID); err == action.ErrNamespaceDeleted {
				continue
			} else if err != nil {
				return nil, fmt.Errorf("error retrieving parent namespaces: %s", err)
			}

			// A namespace's permissions are inherited down to its children, so they must all be
			// retrieved.
			childNamespaces, err := a.na.RetrieveChildren(ur.NamespaceID)
			if err != nil {
				return nil, fmt.Errorf("error retrieving children namespaces: %s", err)
			}

			namespaces = append(namespaces, childNamespaces...)
		}
	}

	if len(namespaces) == 0 {
		return nil, ErrUnauthorized
	}

	return uniqueNamespaceIDs(namespaces), nil
}

// AuthorizeOperation validates that an operation on a given resource type is authorized in the
// specified namespace. Since a namespace's permissions are inherited down to its children, all
// parent namespaces are searched.
func (a *AppAuthorizer) AuthorizeOperation(
	ctx context.Context,
	namespaceID uuid.UUID,
	rt resource.Type,
	op string,
) error {
	namespaces, err := a.na.RetrieveParents(namespaceID)
	if err != nil {
		return fmt.Errorf("error retrieving parent namespaces: %s", err)
	}

	userRoles := UserRoles(ctx)

	for _, n := range namespaces {
		if a.isOperationAuthorizedInNamespace(n, rt, op, userRoles) {
			return nil
		}
	}

	return ErrUnauthorized
}

func (a *AppAuthorizer) isOperationAuthorizedInNamespace(
	namespace *resource.Namespace,
	rt resource.Type,
	op string,
	userRoles []*Role,
) bool {
	for _, ur := range userRoles {
		if ur.NamespaceID != namespace.ID {
			continue
		}

		for _, permission := range RolePermissions[ur.Code] {
			if permission.ResourceType == rt && permission.Operation == op {
				return true
			}
		}
	}

	return false
}

func namespaceIDAlreadyDone(namespaceID uuid.UUID, namespaces []*resource.Namespace) bool {
	for _, n := range namespaces {
		if n.ID == namespaceID {
			return true
		}
	}
	return false
}

// uniqueNamespaceIDs only keeps the namespaces' IDs, while at the same time making sure that the ID
// is not yet in the list.
func uniqueNamespaceIDs(namespaces []*resource.Namespace) []uuid.UUID {
	namespaceIDs := make([]uuid.UUID, 0, len(namespaces))
	for _, n := range namespaces {
		found := false
		for _, nID := range namespaceIDs {
			if nID == n.ID {
				found = true
				break
			}
		}

		if !found {
			namespaceIDs = append(namespaceIDs, n.ID)
		}
	}

	return namespaceIDs
}

var _ = Authorizer(new(AppAuthorizer))
