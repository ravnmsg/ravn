package authorizer

import (
	"errors"
)

var (
	// ErrInvalidJWT defines an error caused by an invalid JWT. This does not include authorization
	// issues, which are handled by ErrUnauthorized.
	ErrInvalidJWT = errors.New("invalid JWT")

	// ErrInvalidJWTPayload defines an error caused by an invalid JWT payload, such as missing
	// roles.
	ErrInvalidJWTPayload = errors.New("invalid JWT payload")

	// ErrUnauthenticated defines an error caused by an invalid username.
	ErrUnauthenticated = errors.New("unauthenticated")

	// ErrUnauthorized defines an error caused by a resource operation that was not permitted for
	// a specific user.
	ErrUnauthorized = errors.New("unauthorized")
)
