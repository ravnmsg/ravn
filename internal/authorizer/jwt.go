package authorizer

import (
	"encoding/json"
	"fmt"

	"github.com/golang-jwt/jwt"
)

func ParseAuthToken(token string) ([]*Role, error) {
	jwtClaims, err := parseJWTClaims(token)
	if err != nil {
		return nil, ErrInvalidJWT
	}

	// TODO: verify claims and signature

	roles, err := parseUserRoles(jwtClaims)
	if err != nil {
		return nil, ErrInvalidJWTPayload
	}

	return roles, nil
}

func parseJWTClaims(t string) (jwt.MapClaims, error) {
	// TODO: Generate signing key. Best practices? Store in env var?
	var signingKey = []byte("arbitrary key")

	token, err := jwt.Parse(t, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("error with JWT: not HMAC")
		}
		return signingKey, nil
	})
	if err != nil {
		return nil, fmt.Errorf("error parsing JWT: %s", err)
	} else if !token.Valid {
		return nil, fmt.Errorf("invalid JWT")
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, fmt.Errorf("unexpected error: invalid JWT claims")
	}

	return claims, nil
}

func parseUserRoles(claims jwt.MapClaims) ([]*Role, error) {
	claimsRoles, ok := claims["roles"]
	if !ok || claimsRoles == "" {
		return nil, fmt.Errorf("no roles")
	}

	// Lazy way to convert a []map[string]interface{} to a []*Role

	bytes, err := json.Marshal(claimsRoles)
	if err != nil {
		return nil, fmt.Errorf("error parsing user roles")
	}

	var roles []*Role
	if err := json.Unmarshal(bytes, &roles); err != nil {
		return nil, fmt.Errorf("error parsing user roles")
	}

	return roles, nil
}
