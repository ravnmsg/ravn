package authorizer

import (
	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type Role struct {
	Code        string    `json:"code"`
	NamespaceID uuid.UUID `json:"namespaceId"`
}

// Permission represents an operation on a resource type.
type Permission struct {
	ResourceType resource.Type
	Operation    string
}

// role permissions should be assignable to a specific resource type.
//
// also need namespace admin (and tenant admin).

// RolePermissions defines the permissions that each roles have.
var RolePermissions = map[string][]Permission{
	// Namespace editors manage a namespace's resources. They also have read access to some global
	// resources like delivery kinds.
	"ns_editor": {
		{resource.TypeNamespace, "read"},
		{resource.TypeNamespace, "create"},
		{resource.TypeNamespace, "update"},
		{resource.TypeNamespace, "delete"},
		{resource.TypeDeliveryKind, "read"},
		{resource.TypeDeliveryKind, "create"},
		{resource.TypeDeliveryKind, "update"},
		{resource.TypeDeliveryKind, "delete"},
		{resource.TypeMessageKind, "read"},
		{resource.TypeMessageKind, "create"},
		{resource.TypeMessageKind, "update"},
		{resource.TypeMessageKind, "delete"},
		{resource.TypeTemplate, "read"},
		{resource.TypeTemplate, "create"},
		{resource.TypeTemplate, "update"},
		{resource.TypeTemplate, "delete"},
		{resource.TypeTemplateKind, "read"},
		{resource.TypeMessage, "read"},
		{resource.TypeMessage, "create"},
	},

	"pre_ns_editor": {
		{resource.TypeNamespace, "read"},
		{resource.TypeNamespace, "create"},
		{resource.TypeNamespace, "update"},
		{resource.TypeNamespace, "delete"},
		{resource.TypeDeliveryKind, "read"},
		{resource.TypeMessageKind, "read"},
		{resource.TypeMessageKind, "create"},
		{resource.TypeMessageKind, "update"},
		{resource.TypeMessageKind, "delete"},
		{resource.TypeTemplate, "read"},
		{resource.TypeTemplate, "create"},
		{resource.TypeTemplate, "update"},
		{resource.TypeTemplate, "delete"},
		{resource.TypeTemplateKind, "read"},
		{resource.TypeMessage, "read"},
		{resource.TypeMessage, "create"},
	},

	// Namespace writers can manage templates and send messages.
	"ns_writer": {
		{resource.TypeNamespace, "read"},
		{resource.TypeDeliveryKind, "read"},
		{resource.TypeMessageKind, "read"},
		{resource.TypeTemplate, "read"},
		{resource.TypeTemplate, "create"},
		{resource.TypeTemplate, "update"},
		{resource.TypeMessage, "read"},
		{resource.TypeMessage, "create"},
	},

	// Namespace readers have read access to all of a namespace's resources.
	"ns_reader": {
		{resource.TypeNamespace, "read"},
		{resource.TypeDeliveryKind, "read"},
		{resource.TypeMessageKind, "read"},
		{resource.TypeTemplate, "read"},
		{resource.TypeMessage, "read"},
	},

	// Message writer is typically given to applications that need to send messages.
	"message_writer": {
		{resource.TypeDeliveryKind, "read"},
		{resource.TypeMessageKind, "read"},
		{resource.TypeMessage, "create"},
	},

	// Message reader is typically given to applications that only need to retrieve messages.
	"message_reader": {
		{resource.TypeMessage, "read"},
	},
}
