//go:build !integration && !endtoend
// +build !integration,!endtoend

package authorizer_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestUnsecuredAuthorizedNamespaceIDs(t *testing.T) {
	tests := map[string]struct {
		namespaceRetrieveManyExpectation      *test.NamespaceActionRetrieveManyExpectation
		namespaceRetrieveChildrenExpectations []*test.NamespaceActionRetrieveChildrenExpectation
		expectedNamespaceIDs                  []uuid.UUID
		expectedError                         error
	}{
		"when successful": {
			namespaceRetrieveManyExpectation: &test.NamespaceActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ParentID", query.OpEqual, nil),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
				},
			},
			namespaceRetrieveChildrenExpectations: []*test.NamespaceActionRetrieveChildrenExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000000"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
					},
				},
			},
			expectedNamespaceIDs: []uuid.UUID{
				uuid.MustParse("00000000-0000-0000-0001-000000000000"),
				uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				uuid.MustParse("00000000-0000-0000-0001-000000000002"),
			},
			expectedError: nil,
		},
		"when the root namespaces failed to be retrieved": {
			namespaceRetrieveManyExpectation: &test.NamespaceActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ParentID", query.OpEqual, nil),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedNamespaceIDs: nil,
			expectedError:        fmt.Errorf("error retrieving root namespaces: arbitrary"),
		},
		"when namespace children failed to be retrieved": {
			namespaceRetrieveManyExpectation: &test.NamespaceActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ParentID", query.OpEqual, nil),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
				},
			},
			namespaceRetrieveChildrenExpectations: []*test.NamespaceActionRetrieveChildrenExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000000"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedNamespaceIDs: nil,
			expectedError:        fmt.Errorf("error retrieving children namespaces: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveMany(tt.namespaceRetrieveManyExpectation)
			for _, e := range tt.namespaceRetrieveChildrenExpectations {
				na.ExpectRetrieveChildren(e)
			}

			a := authorizer.NewUnsecuredAuthorizer(na)
			namespaceIDs, err := a.AuthorizedNamespaceIDs(context.Background(), resource.TypeNone, "arbitrary")

			assert.Equal(t, tt.expectedNamespaceIDs, namespaceIDs)
			assert.Equal(t, tt.expectedError, err)

			na.AssertExpectations(t)
		})
	}
}

func TestUnsecuredAuthorizeOperation(t *testing.T) {
	tests := map[string]struct {
		namespaceID                         uuid.UUID
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		expectedError                       error
	}{
		"when successful": {
			namespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedError: nil,
		},
		"when the namespace parents failed to be retrieved": {
			namespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			a := authorizer.NewUnsecuredAuthorizer(na)
			err := a.AuthorizeOperation(context.Background(), tt.namespaceID, resource.TypeNone, "arbitrary")

			assert.Equal(t, tt.expectedError, err)

			na.AssertExpectations(t)
		})
	}
}
