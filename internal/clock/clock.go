package clock

import (
	"time"
)

// Clocker defines the interface that clock dependencies must follow. This is used so that packages
// are deterministic and reproductable.
type Clocker interface {
	Now() time.Time
}

// Clock defines the application's clock.
type Clock struct{}

// New creates a new Clock instance.
func New() *Clock {
	return &Clock{}
}

// Now returns the current time, as provided by time.Now().
func (*Clock) Now() time.Time {
	return time.Now()
}
