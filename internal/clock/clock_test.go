//go:build !integration && !endtoend
// +build !integration,!endtoend

package clock_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/clock"
)

func TestNow(t *testing.T) {
	clk := clock.New()
	clockTime := clk.Now()
	actualTime := time.Now()

	assert.True(t, actualTime.Sub(clockTime) <= 100*time.Millisecond)
}
