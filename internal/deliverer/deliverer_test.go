//go:build !integration && !endtoend
// +build !integration,!endtoend

package deliverer_test

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/deliverer"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestDeliver(t *testing.T) {
	tests := map[string]struct {
		deliveryID                          uuid.UUID
		deliveryRetrieveByIDExpectation     *test.DeliveryActionRetrieveByIDExpectation
		messageRetrieveByIDExpectation      *test.MessageActionRetrieveByIDExpectation
		deliveryUpdateExpectations          []*test.DeliveryActionUpdateExpectation
		messageKindRetrieveByIDExpectation  *test.MessageKindActionRetrieveByIDExpectation
		deliveryKindRetrieveByIDExpectation *test.DeliveryKindActionRetrieveByIDExpectation
		templateKindRetrieveManyExpectation *test.TemplateKindActionRetrieveManyExpectation
		templateRetrieveManyExpectation     *test.TemplateActionRetrieveManyExpectation
		messageUpdateExpectation            *test.MessageActionUpdateExpectation
		expectedError                       error
	}{
		"when successful": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "active",
				},
			},
			deliveryUpdateExpectations: []*test.DeliveryActionUpdateExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivering",
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivered",
						DeliverTime:    test.MakeTimeRef("2000-01-01T00:00:00Z", t),
						Input: map[string]interface{}{
							"key":                      "value_000000000000",
							"target_attr_000000000000": "body_000000000000",
						},
						Output: map[string]interface{}{
							"externalId": "noop",
						},
						ExternalID: "noop",
					},
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{
					ID:       uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Disabled: false,
				},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:       uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:     "noop",
					Disabled: false,
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0002-000000000000")),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			templateRetrieveManyExpectation: &test.TemplateActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("TemplateKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0004-000000000000")}),
					query.WithFilterArgs("MessageKindID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0003-000000000000")),
					query.WithFilterArgs("LanguageTag", query.OpEqual, "language_tag_000000000000"),
				),
				ReturnedTemplates: []*resource.Template{
					test.ArbitraryTemplate(t),
				},
			},
			messageUpdateExpectation: &test.MessageActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "succeeded",
				},
			},
			expectedError: nil,
		},
		"when delivery failed to be retrieved": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error during pre-delivery: error retrieving delivery: arbitrary"),
		},
		"when delivery is not pending": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "delivered",
				},
			},
			expectedError: fmt.Errorf("delivery aborted: delivery status is `delivered`, but should be `pending`"),
		},
		"when message failed to be retrieved": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error during pre-delivery: error retrieving message: arbitrary"),
		},
		"when message is not active": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "succeeded",
				},
			},
			expectedError: fmt.Errorf("delivery aborted: message status is `succeeded`, but should be `active`"),
		},
		"when delivery failed to be updated during pre-delivery": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "active",
				},
			},
			deliveryUpdateExpectations: []*test.DeliveryActionUpdateExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivering",
					},
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedError: fmt.Errorf("error during pre-delivery: error updating delivery: arbitrary"),
		},
		"when message kind failed to be retrieved": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "active",
				},
			},
			deliveryUpdateExpectations: []*test.DeliveryActionUpdateExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivering",
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "undelivered",
						StatusMessage:  "error retrieving message kind: arbitrary",
						DeliverTime:    test.MakeTimeRef("2000-01-01T00:00:00Z", t),
					},
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error during delivery: error retrieving message kind: arbitrary"),
		},
		"when message kind is disabled": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "active",
				},
			},
			deliveryUpdateExpectations: []*test.DeliveryActionUpdateExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivering",
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "blocked",
						StatusMessage:  "message kind is disabled",
						DeliverTime:    test.MakeTimeRef("2000-01-01T00:00:00Z", t),
					},
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{
					ID:       uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Disabled: true,
				},
			},
			expectedError: nil,
		},
		"when delivery kind failed to be retrieved": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "active",
				},
			},
			deliveryUpdateExpectations: []*test.DeliveryActionUpdateExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivering",
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "undelivered",
						StatusMessage:  "error retrieving delivery kind: arbitrary",
						DeliverTime:    test.MakeTimeRef("2000-01-01T00:00:00Z", t),
					},
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{
					ID:       uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Disabled: false,
				},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error during delivery: error retrieving delivery kind: arbitrary"),
		},
		"when delivery kind is disabled": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "active",
				},
			},
			deliveryUpdateExpectations: []*test.DeliveryActionUpdateExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivering",
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "blocked",
						StatusMessage:  "delivery kind is disabled",
						DeliverTime:    test.MakeTimeRef("2000-01-01T00:00:00Z", t),
					},
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{
					ID:       uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Disabled: false,
				},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:       uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:     "noop",
					Disabled: true,
				},
			},
			expectedError: nil,
		},
		"when template kinds failed to be retrieved": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "active",
				},
			},
			deliveryUpdateExpectations: []*test.DeliveryActionUpdateExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivering",
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "undelivered",
						StatusMessage:  "error retrieving template kinds: arbitrary",
						DeliverTime:    test.MakeTimeRef("2000-01-01T00:00:00Z", t),
					},
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{
					ID:       uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Disabled: false,
				},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:       uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:     "noop",
					Disabled: false,
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0002-000000000000")),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error during delivery: error retrieving template kinds: arbitrary"),
		},
		"when templates failed to be retrieved": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "active",
				},
			},
			deliveryUpdateExpectations: []*test.DeliveryActionUpdateExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivering",
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "undelivered",
						StatusMessage:  "error retrieving templates: arbitrary",
						DeliverTime:    test.MakeTimeRef("2000-01-01T00:00:00Z", t),
					},
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{
					ID:       uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Disabled: false,
				},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:       uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:     "noop",
					Disabled: false,
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0002-000000000000")),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			templateRetrieveManyExpectation: &test.TemplateActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("TemplateKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0004-000000000000")}),
					query.WithFilterArgs("MessageKindID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0003-000000000000")),
					query.WithFilterArgs("LanguageTag", query.OpEqual, "language_tag_000000000000"),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error during delivery: error retrieving templates: arbitrary"),
		},
		"when delivery failed to be updated during post-delivery": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "active",
				},
			},
			deliveryUpdateExpectations: []*test.DeliveryActionUpdateExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivering",
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivered",
						DeliverTime:    test.MakeTimeRef("2000-01-01T00:00:00Z", t),
						Input: map[string]interface{}{
							"key":                      "value_000000000000",
							"target_attr_000000000000": "body_000000000000",
						},
						Output: map[string]interface{}{
							"externalId": "noop",
						},
						ExternalID: "noop",
					},
					ReturnedError: test.ErrArbitrary,
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{
					ID:       uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Disabled: false,
				},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:       uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:     "noop",
					Disabled: false,
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0002-000000000000")),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			templateRetrieveManyExpectation: &test.TemplateActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("TemplateKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0004-000000000000")}),
					query.WithFilterArgs("MessageKindID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0003-000000000000")),
					query.WithFilterArgs("LanguageTag", query.OpEqual, "language_tag_000000000000"),
				),
				ReturnedTemplates: []*resource.Template{
					test.ArbitraryTemplate(t),
				},
			},
			expectedError: fmt.Errorf("error during post-delivery: error updating delivery: arbitrary"),
		},
		"when message failed to be updated during post-delivery": {
			deliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			deliveryRetrieveByIDExpectation: &test.DeliveryActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "language_tag_000000000000",
					Status:         "pending",
				},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "active",
				},
			},
			deliveryUpdateExpectations: []*test.DeliveryActionUpdateExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivering",
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					ExpectedDelivery: &resource.Delivery{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
						LanguageTag:    "language_tag_000000000000",
						Status:         "delivered",
						DeliverTime:    test.MakeTimeRef("2000-01-01T00:00:00Z", t),
						Input: map[string]interface{}{
							"key":                      "value_000000000000",
							"target_attr_000000000000": "body_000000000000",
						},
						Output: map[string]interface{}{
							"externalId": "noop",
						},
						ExternalID: "noop",
					},
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{
					ID:       uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Disabled: false,
				},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:       uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:     "noop",
					Disabled: false,
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0002-000000000000")),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			templateRetrieveManyExpectation: &test.TemplateActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("TemplateKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0004-000000000000")}),
					query.WithFilterArgs("MessageKindID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0003-000000000000")),
					query.WithFilterArgs("LanguageTag", query.OpEqual, "language_tag_000000000000"),
				),
				ReturnedTemplates: []*resource.Template{
					test.ArbitraryTemplate(t),
				},
			},
			messageUpdateExpectation: &test.MessageActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ExpectedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status: "succeeded",
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedError: fmt.Errorf("error during post-delivery: error updating message: arbitrary"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			da := test.NewDeliveryAction(t)
			da.ExpectRetrieveByID(tt.deliveryRetrieveByIDExpectation)

			for _, e := range tt.deliveryUpdateExpectations {
				da.ExpectUpdate(e)
			}

			ma := test.NewMessageAction(t)
			ma.ExpectRetrieveByID(tt.messageRetrieveByIDExpectation)
			ma.ExpectUpdate(tt.messageUpdateExpectation)

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveMany(tt.templateKindRetrieveManyExpectation)

			ta := test.NewTemplateAction(t)
			ta.ExpectRetrieveMany(tt.templateRetrieveManyExpectation)

			del := deliverer.New(da, ma, ta, tka, mka, dka, test.NewClock("2000-01-01T00:00:00Z"))
			err := del.Deliver(tt.deliveryID)

			assert.Equal(t, tt.expectedError, err)

			da.AssertExpectations(t)
			ma.AssertExpectations(t)
			ta.AssertExpectations(t)
			tka.AssertExpectations(t)
			mka.AssertExpectations(t)
			dka.AssertExpectations(t)
		})
	}
}
