package requester

import (
	"fmt"
	"log"

	"gitlab.com/ravnmsg/ravn-requester/pkg/client/httpclient"
	"gitlab.com/ravnmsg/ravn-requester/pkg/requester"

	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// HTTPRequester performs an HTTP request to the requester service at the configured host.
type HTTPRequester struct {
	deliveryKind *resource.DeliveryKind
}

func NewHTTPRequester(deliveryKind *resource.DeliveryKind) *HTTPRequester {
	return &HTTPRequester{
		deliveryKind: deliveryKind,
	}
}

func (r *HTTPRequester) Request(input map[string]interface{}) (map[string]interface{}, error) {
	log.Printf("HTTP requester: calling requester service at `%s`...", r.deliveryKind.Host)

	requesterClient := httpclient.New(r.deliveryKind.Host)

	output, err := requesterClient.Request(requester.NewValuesFromMap(input))
	if err != nil {
		return nil, fmt.Errorf("error during HTTP request: %s", err)
	}

	// TODO: return everything
	externalID := map[string]interface{}{"externalId": output.GetString("externalId")}

	return externalID, nil
}
