package requester

// NoopRequester does nothing. It is typically used when a message should not be delivered, eg. when
// testing or debugging.
type NoopRequester struct{}

func NewNoopRequester() *NoopRequester {
	return &NoopRequester{}
}

func (r *NoopRequester) Request(_ map[string]interface{}) (map[string]interface{}, error) {
	return map[string]interface{}{"externalId": "noop"}, nil
}
