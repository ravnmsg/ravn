package requester

import (
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// Requester defines a request to deliver a message.
type Requester interface {
	Request(map[string]interface{}) (map[string]interface{}, error)
}

// New creates a new requester for the specified delivery kind. Most requesters are callable by
// HTTP, but there are some built-in requesters.
func New(deliveryKind *resource.DeliveryKind) Requester {
	switch deliveryKind.Code {
	case "log":
		return NewLogRequester(deliveryKind)
	case "noop":
		return NewNoopRequester()
	default:
		return NewHTTPRequester(deliveryKind)
	}
}
