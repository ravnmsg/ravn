//go:build !integration && !endtoend
// +build !integration,!endtoend

package requester_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/deliverer/requester"
)

func TestNoopRequest(t *testing.T) {
	input := map[string]interface{}{
		"key": "value",
	}

	r := requester.NewNoopRequester()
	output, err := r.Request(input)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	expectedOutput := map[string]interface{}{
		"externalId": "noop",
	}

	assert.Equal(t, expectedOutput, output)
}
