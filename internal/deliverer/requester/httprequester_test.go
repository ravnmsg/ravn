//go:build !unit && !endtoend
// +build !unit,!endtoend

package requester_test

import (
	"context"
	"net"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/deliverer/requester"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

func TestHTTPRequest(t *testing.T) {
	testDone := false

	m := http.NewServeMux()
	s := http.Server{Handler: m}

	m.HandleFunc("/request", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
		if _, err := w.Write([]byte(`{"externalId":"http"}`)); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
		}
	})

	l, err := net.Listen("tcp", "localhost:0")
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	// If the test takes more than a second to run, this will end the context, which will shutdown
	// the server. This prevents anything from happening which would lock the test.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	// Fails the test if it took more than a second to run.
	go func(t *testing.T) {
		<-ctx.Done()

		if err := s.Shutdown(context.Background()); err != nil {
			t.Errorf("unexpected error: %s", err)
		}

		if !testDone {
			t.Errorf("test timeout")
		}
	}(t)

	go func(t *testing.T) {
		if err := s.Serve(l); err != http.ErrServerClosed {
			t.Errorf("unexpected error: %s", err)
		}
	}(t)

	time.Sleep(10 * time.Millisecond)

	deliveryKind := &resource.DeliveryKind{
		Code: "any_unreserved_code",
		Host: l.Addr().String(),
	}

	input := map[string]interface{}{
		"key": "value",
	}

	r := requester.NewHTTPRequester(deliveryKind)
	output, err := r.Request(input)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	expectedOutput := map[string]interface{}{
		"externalId": "http",
	}

	assert.Equal(t, expectedOutput, output)

	testDone = true
	ctx.Done()
}
