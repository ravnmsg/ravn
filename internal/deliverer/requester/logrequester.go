package requester

import (
	"log"

	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// LogRequester logs the input and output values. It is typically used during development when the
// actual message delivery is not important.
type LogRequester struct {
	deliveryKind *resource.DeliveryKind
}

func NewLogRequester(deliveryKind *resource.DeliveryKind) *LogRequester {
	return &LogRequester{
		deliveryKind: deliveryKind,
	}
}

func (r *LogRequester) Request(input map[string]interface{}) (map[string]interface{}, error) {
	log.Println()
	log.Printf("Log requester: calling log requester...")
	log.Printf("Log requester:   |- delivery kind: `%s` (id `%s`)", r.deliveryKind.Code, r.deliveryKind.ID)
	log.Printf("Log requester:   |- input: %v", input)

	output := map[string]interface{}{
		"externalId": "log",
	}

	log.Printf("Log requester:   \\- output: %v", output)
	log.Println()

	return output, nil
}
