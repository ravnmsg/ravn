//go:build !integration && !endtoend
// +build !integration,!endtoend

package requester_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/deliverer/requester"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

func TestLogRequest(t *testing.T) {
	deliveryKind := &resource.DeliveryKind{Code: "log"}
	input := map[string]interface{}{
		"key": "value",
	}

	r := requester.NewLogRequester(deliveryKind)
	output, err := r.Request(input)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	expectedOutput := map[string]interface{}{
		"externalId": "log",
	}

	assert.Equal(t, expectedOutput, output)
}
