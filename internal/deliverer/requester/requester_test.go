//go:build !integration && !endtoend
// +build !integration,!endtoend

package requester_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/deliverer/requester"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

func TestNew(t *testing.T) {
	tests := map[string]struct {
		deliveryKind          *resource.DeliveryKind
		expectedRequesterType string
	}{
		"when delivery kind is noop": {
			deliveryKind:          &resource.DeliveryKind{Code: "noop"},
			expectedRequesterType: "*requester.NoopRequester",
		},
		"when delivery kind is log": {
			deliveryKind:          &resource.DeliveryKind{Code: "log"},
			expectedRequesterType: "*requester.LogRequester",
		},
		"when delivery kind is anything else": {
			deliveryKind:          &resource.DeliveryKind{Code: "any_unreserved_code"},
			expectedRequesterType: "*requester.HTTPRequester",
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			r := requester.New(tt.deliveryKind)
			assert.Equal(t, tt.expectedRequesterType, fmt.Sprintf("%T", r))
		})
	}
}
