package deliverer

import (
	"errors"
)

var (
	// ErrDisabledDeliveryKind defines an error caused by a disabled delivery kind.
	ErrDisabledDeliveryKind = errors.New("disabled delivery kind")

	// ErrDisabledMessageKind defines an error caused by a disabled message kind.
	ErrDisabledMessageKind = errors.New("disabled message kind")
)
