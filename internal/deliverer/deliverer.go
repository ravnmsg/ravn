package deliverer

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/deliverer/processor"
	"gitlab.com/ravnmsg/ravn/internal/deliverer/requester"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type Deliverer interface {
	Deliver(deliveryID uuid.UUID) error
}

type AppDeliverer struct {
	da  action.DeliveryActioner
	ma  action.MessageActioner
	ta  action.TemplateActioner
	tka action.TemplateKindActioner
	mka action.MessageKindActioner
	dka action.DeliveryKindActioner
	clk clock.Clocker
}

// New creates a Deliverer that can perform the delivery of a message by calling a Requester
// service.
func New(
	da action.DeliveryActioner,
	ma action.MessageActioner,
	ta action.TemplateActioner,
	tka action.TemplateKindActioner,
	mka action.MessageKindActioner,
	dka action.DeliveryKindActioner,
	clk clock.Clocker,
) *AppDeliverer {
	return &AppDeliverer{
		da:  da,
		ma:  ma,
		ta:  ta,
		tka: tka,
		mka: mka,
		dka: dka,
		clk: clk,
	}
}

// Deliver delivers the delivery with the specified delivery ID. In order to do so, it calls the
// Requester service configured for the delivery. It returns the requester's input and output.
// nolint:funlen // This method is straightforward and should be easy to read.
func (deliverer *AppDeliverer) Deliver(deliveryID uuid.UUID) error {
	delivery, message, err := deliverer.preDeliver(deliveryID)
	if err != nil {
		return err
	}

	input, output, deliveryErr := deliverer.deliver(delivery, message)

	if err := deliverer.postDeliver(delivery, message, input, output, deliveryErr); err != nil {
		return err
	}

	return nil
}

func (deliverer *AppDeliverer) preDeliver(
	deliveryID uuid.UUID,
) (
	delivery *resource.Delivery,
	message *resource.Message,
	err error,
) {
	delivery, err = deliverer.da.RetrieveByID(deliveryID)
	if err != nil {
		return nil, nil, fmt.Errorf("error during pre-delivery: error retrieving delivery: %s", err)
	} else if delivery.Status != "pending" {
		// Verifies that the delivery is currently in a pending state. This prevents the same
		// delivery from being processed multiple times in a row.
		return nil, nil, fmt.Errorf("delivery aborted: delivery status is `%s`, but should be `pending`", delivery.Status)
	}

	message, err = deliverer.ma.RetrieveByID(delivery.MessageID)
	if err != nil {
		return nil, nil, fmt.Errorf("error during pre-delivery: error retrieving message: %s", err)
	} else if message.Status != "active" {
		// Verifies that the message is currently in an active state. This makes sure that the
		// resources are in the correct state.
		return nil, nil, fmt.Errorf("delivery aborted: message status is `%s`, but should be `active`", message.Status)
	}

	delivery.Status = "delivering"
	delivery, err = deliverer.da.Update(delivery.ID, delivery)
	if err != nil {
		return nil, nil, fmt.Errorf("error during pre-delivery: error updating delivery: %s", err)
	}

	return delivery, message, nil
}

func (deliverer *AppDeliverer) deliver(
	delivery *resource.Delivery,
	message *resource.Message,
) (
	input map[string]interface{},
	output map[string]interface{},
	deliveryErr error,
) {
	messageKind, err := deliverer.mka.RetrieveByID(message.MessageKindID)
	if err != nil {
		return nil, nil, fmt.Errorf("error retrieving message kind: %s", err)
	} else if messageKind.Disabled {
		return nil, nil, ErrDisabledMessageKind
	}

	deliveryKind, err := deliverer.dka.RetrieveByID(delivery.DeliveryKindID)
	if err != nil {
		return nil, nil, fmt.Errorf("error retrieving delivery kind: %s", err)
	} else if deliveryKind.Disabled {
		return nil, nil, ErrDisabledDeliveryKind
	}

	templatesByTemplateKind, err := deliverer.findTemplates(deliveryKind.ID, messageKind.ID, delivery.LanguageTag)
	if err != nil {
		return nil, nil, err
	}

	input, err = deepCopy(message.Input)
	if err != nil {
		return input, nil, fmt.Errorf("error processing input: %s", err)
	}

	inputMods, err := deliverer.process(templatesByTemplateKind, message.Input)
	if err != nil {
		return input, nil, fmt.Errorf("error processing delivery: %s", err)
	}

	// Applies the processor's modifications to the input.
	for k, v := range inputMods {
		input[k] = v
	}

	req := requester.New(deliveryKind)
	output, err = req.Request(input)
	if err != nil {
		return input, nil, fmt.Errorf("error requesting delivery: %s", err)
	}

	return input, output, nil
}

func (deliverer *AppDeliverer) findTemplates(
	deliveryKindID uuid.UUID,
	messageKindID uuid.UUID,
	languageTag string,
) (map[*resource.TemplateKind]*resource.Template, error) {
	q := query.NewQuery(query.WithFilterArgs("DeliveryKindID", query.OpEqual, deliveryKindID))
	templateKinds, err := deliverer.tka.RetrieveMany(q)
	if err != nil {
		return nil, fmt.Errorf("error retrieving template kinds: %s", err)
	}

	templateKindIDs := make([]uuid.UUID, len(templateKinds))
	for i, tk := range templateKinds {
		templateKindIDs[i] = tk.ID
	}

	q = query.NewQuery(
		query.WithFilterArgs("TemplateKindID", query.OpIn, templateKindIDs),
		query.WithFilterArgs("MessageKindID", query.OpEqual, messageKindID),
		query.WithFilterArgs("LanguageTag", query.OpEqual, languageTag),
	)
	templates, err := deliverer.ta.RetrieveMany(q)
	if err != nil {
		return nil, fmt.Errorf("error retrieving templates: %s", err)
	}

	templatesByTemplateKind := make(map[*resource.TemplateKind]*resource.Template)
	for _, tk := range templateKinds {
		foundTemplate := false

		for _, t := range templates {
			if t.TemplateKindID != tk.ID {
				continue
			}

			// Undefined behavior if multiple templates.
			templatesByTemplateKind[tk] = t
			foundTemplate = true
		}

		if !foundTemplate {
			return nil, fmt.Errorf("error retrieving templates: none were found for a given template kind")
		}
	}

	return templatesByTemplateKind, nil
}

func (deliverer *AppDeliverer) process(
	templatesByTemplateKind map[*resource.TemplateKind]*resource.Template,
	input map[string]interface{},
) (map[string]interface{}, error) {
	inputMods := make(map[string]interface{})

	for tk, t := range templatesByTemplateKind {
		p := processor.NewTemplateProcessor()
		value, err := p.Process(t, input)
		if err != nil {
			return nil, err
		}

		inputMods[tk.TargetAttr] = value
	}

	return inputMods, nil
}

func (deliverer *AppDeliverer) postDeliver(
	delivery *resource.Delivery,
	message *resource.Message,
	input map[string]interface{},
	output map[string]interface{},
	deliveryErr error,
) error {
	currentTime := deliverer.clk.Now()
	delivery.DeliverTime = &currentTime
	delivery.Input = input
	delivery.Output = output

	externalID := output["externalId"]
	if externalID, ok := externalID.(string); ok {
		delivery.ExternalID = externalID
	}

	switch {
	case errors.Is(deliveryErr, ErrDisabledMessageKind):
		delivery.Status = "blocked"
		delivery.StatusMessage = "message kind is disabled"

	case errors.Is(deliveryErr, ErrDisabledDeliveryKind):
		delivery.Status = "blocked"
		delivery.StatusMessage = "delivery kind is disabled"

	case deliveryErr != nil:
		delivery.Status = "undelivered"
		delivery.StatusMessage = deliveryErr.Error()

	default:
		delivery.Status = "delivered"
	}

	delivery, err := deliverer.da.Update(delivery.ID, delivery)
	if err != nil {
		return fmt.Errorf("error during post-delivery: error updating delivery: %s", err)
	}

	if delivery.Status == "delivered" {
		message.Status = "succeeded"
		if _, err := deliverer.ma.Update(message.ID, message); err != nil {
			return fmt.Errorf("error during post-delivery: error updating message: %s", err)
		}
	} else if delivery.Status == "undelivered" {
		// Message is left in an active state.
		return fmt.Errorf("error during delivery: %s", delivery.StatusMessage)
	}

	return nil
}

// Lazy way to deep-copy a map.
func deepCopy(sourceMap map[string]interface{}) (map[string]interface{}, error) {
	bytes, err := json.Marshal(sourceMap)
	if err != nil {
		return nil, fmt.Errorf("error marshaling map")
	}

	targetMap := make(map[string]interface{})
	if err := json.Unmarshal(bytes, &targetMap); err != nil {
		return nil, fmt.Errorf("error unmarshaling map")
	}

	return targetMap, nil
}

var _ = Deliverer(new(AppDeliverer))
