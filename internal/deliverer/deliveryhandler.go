package deliverer

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/google/uuid"
)

// DeliveryHandler defines a handler for incoming delivery requests.
func DeliveryHandler(deliverer Deliverer) func([]byte) error {
	fn := func(data []byte) error {
		msg := &deliveryRequest{}
		if err := json.Unmarshal(data, &msg); err != nil {
			return fmt.Errorf("error converting message: %s", err)
		} else if msg.DeliveryID == uuid.Nil {
			return fmt.Errorf("error converting message: invalid delivery ID")
		}

		err := deliverer.Deliver(msg.DeliveryID)
		if err != nil {
			log.Printf("deliverer: %s", err)
		}

		return nil
	}

	return fn
}

// deliveryRequest defines the body of a message related to a delivery request.
type deliveryRequest struct {
	DeliveryID uuid.UUID `json:"deliveryId"`
	RequestID  string    `json:"requestId"`
}
