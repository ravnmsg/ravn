package processor

import (
	"bytes"
	"fmt"
	tmpl "text/template"
	"time"

	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// TemplateProcessor defines a template processor.
type TemplateProcessor struct {
}

// New creates a processor that uses templates to generate content.
func NewTemplateProcessor() *TemplateProcessor {
	return &TemplateProcessor{}
}

func (p *TemplateProcessor) Process(
	template *resource.Template,
	input map[string]interface{},
) (interface{}, error) {
	funcMap := tmpl.FuncMap{
		"trDate": trDateFunc(template.LanguageTag),
	}

	g, err := tmpl.New("templateProcessor").Funcs(funcMap).Parse(template.Body)
	if err != nil {
		return nil, fmt.Errorf("error parsing template: %s", err)
	}

	// g.Execute outputs to an io writer, which will later be transformed into a string.
	result := new(bytes.Buffer)
	if err := g.Execute(result, input); err != nil {
		return nil, fmt.Errorf("error executing template: %s", err)
	}

	return result.String(), nil
}

func trDateFunc(languageTag string) func(time.Time) string {
	switch languageTag {
	case "en":
		return func(t time.Time) string {
			if t == (time.Time{}) {
				return ""
			}

			return fmt.Sprintf(
				"%s %d%s, %d",
				t.Format("January"),
				t.Day(),
				ordinalSuffix(t.Day(), languageTag),
				t.Year(),
			)
		}

	case "fr":
		var months = [...]string{
			"Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
			"Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre",
		}

		return func(t time.Time) string {
			if t == (time.Time{}) {
				return ""
			}

			return fmt.Sprintf(
				"%d %s %d",
				t.Day(),
				months[t.Month()-1],
				t.Year(),
			)
		}

	default:
		return func(t time.Time) string {
			if t == (time.Time{}) {
				return ""
			}

			return t.Format("2006-01-02")
		}
	}
}

func ordinalSuffix(n int, locale string) string {
	if locale != "en" {
		return ""
	}

	defaultSuffix := "th"

	switch n % 10 {
	case 1:
		if n == 11 {
			return defaultSuffix
		}

		return "st"

	case 2:
		if n == 12 {
			return defaultSuffix
		}

		return "nd"

	case 3:
		if n == 13 {
			return defaultSuffix
		}

		return "rd"

	default:
		return defaultSuffix
	}
}
