package processor

import (
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// Processor defines a template processor.
type Processor interface {
	Process(
		template *resource.Template,
		messageInput map[string]interface{},
	) (
		requesterInput map[string]interface{},
		requesterOutput map[string]interface{},
		err error,
	)
}
