//go:build !integration && !endtoend
// +build !integration,!endtoend

package processor_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/deliverer/processor"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

func TestTemplateProcessor(t *testing.T) {
	tests := map[string]struct {
		template       *resource.Template
		input          map[string]interface{}
		expectedResult interface{}
		expectedError  error
	}{
		"when template is static": {
			template: &resource.Template{
				LanguageTag: "language_tag",
				Body:        "body",
			},
			input:          map[string]interface{}{},
			expectedResult: "body",
		},
		"when template is dynamic": {
			template: &resource.Template{
				LanguageTag: "language_tag",
				Body:        "body {{.key}}.",
			},
			input:          map[string]interface{}{"key": "value"},
			expectedResult: "body value.",
		},
		"when input key is missing": {
			template: &resource.Template{
				LanguageTag: "language_tag",
				Body:        "body {{.key}}.",
			},
			input:          map[string]interface{}{},
			expectedResult: "body <no value>.",
		},
		"when template body is invalid": {
			template: &resource.Template{
				LanguageTag: "language_tag",
				Body:        "body {{.key.",
			},
			input:         map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing template: template: templateProcessor:1: unexpected \".\" in operand"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			p := processor.NewTemplateProcessor()
			result, err := p.Process(tt.template, tt.input)

			assert.Equal(t, tt.expectedResult, result)
			assert.Equal(t, tt.expectedError, err)
		})
	}
}
