//go:build !integration && !endtoend
// +build !integration,!endtoend

package deliverer_test

import (
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/deliverer"
)

type testDeliverer struct {
	actualDeliveryID uuid.UUID
}

func (d *testDeliverer) Deliver(deliveryID uuid.UUID) error {
	d.actualDeliveryID = deliveryID
	return nil
}

func TestDeliveryHandler(t *testing.T) {
	tests := map[string]struct {
		msgData            []byte
		expectedDeliveryID uuid.UUID
		expectedError      error
	}{
		"when successful": {
			msgData: []byte("{" +
				`"deliveryId":"00000000-0000-0000-0007-000000000000"` +
				"}"),
			expectedDeliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
			expectedError:      nil,
		},
		"when delivery ID is not provided": {
			msgData:            []byte("{}"),
			expectedDeliveryID: uuid.Nil,
			expectedError:      fmt.Errorf("error converting message: invalid delivery ID"),
		},
		"when delivery ID is invalid": {
			msgData: []byte("{" +
				`"deliveryId":"invalid"` +
				"}"),
			expectedDeliveryID: uuid.Nil,
			expectedError:      fmt.Errorf("error converting message: invalid UUID length: 7"),
		},
		"when message is invalid": {
			msgData:            []byte("invalid"),
			expectedDeliveryID: uuid.Nil,
			expectedError:      fmt.Errorf("error converting message: invalid character 'i' looking for beginning of value"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			del := &testDeliverer{}
			handlerFn := deliverer.DeliveryHandler(del)

			err := handlerFn(tt.msgData)

			assert.Equal(t, tt.expectedDeliveryID, del.actualDeliveryID)
			assert.Equal(t, tt.expectedError, err)
		})
	}
}
