//go:build !integration && !endtoend
// +build !integration,!endtoend

package handler_test

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestErrorStatus(t *testing.T) {
	tests := map[string]struct {
		err                error
		expectedStatusCode int
	}{
		"when resource deleted": {
			err:                handler.ErrDeletedResource,
			expectedStatusCode: http.StatusBadRequest,
		},
		"when resource disabled": {
			err:                handler.ErrDisabledResource,
			expectedStatusCode: http.StatusBadRequest,
		},
		"when invalid request body": {
			err:                handler.ErrInvalidRequestBody,
			expectedStatusCode: http.StatusBadRequest,
		},
		"when invalid resource scope": {
			err:                handler.ErrInvalidResourceScope,
			expectedStatusCode: http.StatusBadRequest,
		},
		"when unauthenticated": {
			err:                authorizer.ErrUnauthenticated,
			expectedStatusCode: http.StatusUnauthorized,
		},
		"when unauthorized": {
			err:                authorizer.ErrUnauthorized,
			expectedStatusCode: http.StatusForbidden,
		},
		"when invalid api version": {
			err:                handler.ErrInvalidAPIVersion,
			expectedStatusCode: http.StatusNotFound,
		},
		"when invalid method or path": {
			err:                handler.ErrInvalidMethodOrPath,
			expectedStatusCode: http.StatusNotFound,
		},
		"when invalid resource id": {
			err:                handler.ErrInvalidResourceID,
			expectedStatusCode: http.StatusNotFound,
		},
		"when invalid resource id or code": {
			err:                handler.ErrInvalidResourceIDOrCode,
			expectedStatusCode: http.StatusNotFound,
		},
		"when any other error": {
			err:                test.ErrArbitrary,
			expectedStatusCode: http.StatusInternalServerError,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			statusCode := handler.ErrorStatus(tt.err)

			assert.Equal(t, tt.expectedStatusCode, statusCode)
		})
	}
}
