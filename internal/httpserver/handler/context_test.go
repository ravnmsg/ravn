//go:build !integration && !endtoend
// +build !integration,!endtoend

package handler_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
)

func TestAPIVersion(t *testing.T) {
	ctx := context.Background()
	apiVersion := "api_version"

	ctx = handler.SetAPIVersion(ctx, apiVersion)

	assert.Equal(t, apiVersion, handler.APIVersion(ctx))
}

func TestRequestID(t *testing.T) {
	ctx := context.Background()
	requestID := "request_id"

	ctx = handler.SetRequestID(ctx, requestID)

	assert.Equal(t, requestID, handler.RequestID(ctx))
}
