//go:build !integration && !endtoend
// +build !integration,!endtoend

package handler_test

import (
	"bytes"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	. "gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestParseRequestBody(t *testing.T) {
	tests := map[string]struct {
		body                 []byte
		expectedDeliveryKind *resource.DeliveryKind
		expectedError        error
	}{
		"when successful": {
			body: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"delivery_kind_000000000000",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"host_000000000000",` +
				`"disabled":false,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			expectedDeliveryKind: &resource.DeliveryKind{
				ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				Code:        "delivery_kind_000000000000",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "host_000000000000",
				Disabled:    false,
				CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
			},
		},
		"when request body is empty": {
			body:          []byte{},
			expectedError: fmt.Errorf("error parsing body"),
		},
		"when request body failed to be unmarshaled": {
			body:          []byte("invalid"),
			expectedError: fmt.Errorf("error parsing body"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			deliveryKind := new(resource.DeliveryKind)
			err := ParseRequestBody(bytes.NewReader(tt.body), deliveryKind)
			if err != nil {
				assert.Equal(t, tt.expectedError, err)
				return
			}

			assert.Equal(t, tt.expectedDeliveryKind, deliveryKind)
		})
	}
}

func TestParseOData(t *testing.T) {
	tests := map[string]struct {
		params        map[string]string
		expectedQuery *query.Query
		expectedError error
	}{
		"when successful": {
			params: map[string]string{
				"$filter":  "code eq delivery_kind_000000000000 and createTime ge 2000-01-01T00:00:00Z",
				"$orderby": "code desc",
				"$top":     "5",
				"$skip":    "8",
			},
			expectedQuery: &query.Query{
				Filters: []*query.Filter{
					{Operator: query.OpEqual, FieldName: "Code", Operand: "delivery_kind_000000000000"},
					{Operator: query.OpGreaterThanOrEqual, FieldName: "CreateTime", Operand: "2000-01-01T00:00:00Z"},
				},
				Orders: []*query.Order{
					{FieldName: "Code", Direction: query.DirDescending},
				},
				Page: &query.Page{StartIndex: 8, Capacity: 5},
			},
		},
		"when no parameters are provided": {
			params: map[string]string{},
			expectedQuery: &query.Query{
				Filters: []*query.Filter{},
				Orders:  []*query.Order{{FieldName: "CreateTime", Direction: query.DirAscending}},
				Page:    &query.Page{StartIndex: 0, Capacity: 20},
			},
		},
		"when filters failed to be parsed": {
			params: map[string]string{
				"$filter": "id invalid value",
			},
			expectedError: fmt.Errorf("error parsing $filter: invalid operator `invalid`"),
		},
		"when orders failed to be parsed": {
			params: map[string]string{
				"$orderby": "id invalid",
			},
			expectedError: fmt.Errorf("error parsing $order: invalid direction `invalid`"),
		},
		"when capacity failed to be parsed": {
			params: map[string]string{
				"$top": "invalid",
			},
			expectedError: fmt.Errorf("error parsing $top: invalid value `invalid`"),
		},
		"when start index failed to be parsed": {
			params: map[string]string{
				"$skip": "invalid",
			},
			expectedError: fmt.Errorf("error parsing $skip: invalid value `invalid`"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			q, err := ParseOData(tt.params, resource.TypeDeliveryKind)
			if err != nil {
				assert.Equal(t, tt.expectedError, err)
				return
			}

			assert.Equal(t, tt.expectedQuery, q)
		})
	}
}

func TestParseODataFilters(t *testing.T) {
	tests := map[string]struct {
		value           string
		expectedFilters []*query.Filter
		expectedError   error
	}{
		"when filter attribute is invalid": {
			value:         "invalid eq token",
			expectedError: fmt.Errorf("invalid attribute `invalid`"),
		},
		"when filter operator is eq": {
			value:           "id eq token",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpEqual, "token")},
		},
		"when filter operator is ne": {
			value:           "id ne token",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpNotEqual, "token")},
		},
		"when filter operator is gt": {
			value:           "id gt token",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpGreaterThan, "token")},
		},
		"when filter operator is ge": {
			value:           "id ge token",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpGreaterThanOrEqual, "token")},
		},
		"when filter operator is lt": {
			value:           "id lt token",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpLessThan, "token")},
		},
		"when filter operator is le": {
			value:           "id le token",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpLessThanOrEqual, "token")},
		},
		"when filter operator is invalid": {
			value:         "id invalid token",
			expectedError: fmt.Errorf("invalid operator `invalid`"),
		},
		"when filter operand is single-quoted": {
			value:           "id eq 'quoted token'",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpEqual, "quoted token")},
		},
		"when filter operand is double-quoted": {
			value:           "id eq \"quoted token\"",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpEqual, "quoted token")},
		},
		"when filter operand contains a quote": {
			value:           "id eq 'quoted\" token'",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpEqual, "quoted\" token")},
		},
		"when filter operand is the null value": {
			value:           "id eq null",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpEqual, nil)},
		},
		"when filter operand is the null string": {
			value:           "id eq 'null'",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpEqual, "null")},
		},
		"when filter starts with and": {
			value:           "and id eq token",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpEqual, "token")},
		},
		"when filter ends with and": {
			value:           "id eq token and",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpEqual, "token")},
		},
		"when filter is invalid": {
			value:         "invalid",
			expectedError: fmt.Errorf("invalid token count"),
		},
		"when filter is empty": {
			value:           "",
			expectedFilters: []*query.Filter{},
		},
		"when filter contains multiple filters": {
			value:           "id eq token1 and namespaceId ne token2",
			expectedFilters: []*query.Filter{query.NewFilter("ID", query.OpEqual, "token1"), query.NewFilter("NamespaceID", query.OpNotEqual, "token2")},
		},
		"when filter is invalid on later filter": {
			value:         "id eq token and invalid",
			expectedError: fmt.Errorf("invalid token count"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			q, err := ParseODataFilters(tt.value, resource.TypeDeliveryKind)
			if err != nil {
				assert.Equal(t, tt.expectedError, err)
				return
			}

			assert.Equal(t, tt.expectedFilters, q)
		})
	}
}

func TestParseODataOrderBy(t *testing.T) {
	tests := map[string]struct {
		value          string
		expectedOrders []*query.Order
		expectedError  error
	}{
		"when orderby attribute is invalid": {
			value:         "invalid asc",
			expectedError: fmt.Errorf("invalid attribute `invalid`"),
		},
		"when orderby direction is ascending": {
			value:          "id asc",
			expectedOrders: []*query.Order{query.NewOrder("ID", query.DirAscending)},
		},
		"when orderby direction is descending": {
			value:          "id desc",
			expectedOrders: []*query.Order{query.NewOrder("ID", query.DirDescending)},
		},
		"when orderby has no directions": {
			value:          "id",
			expectedOrders: []*query.Order{query.NewOrder("ID", query.DirAscending)},
		},
		"when orderby direction is uppercase": {
			value:          "id DESC",
			expectedOrders: []*query.Order{query.NewOrder("ID", query.DirDescending)},
		},
		"when orderby attribute is quoted": {
			value:          "'id' asc",
			expectedOrders: []*query.Order{query.NewOrder("ID", query.DirAscending)},
		},
		"when orderby direction is quoted": {
			value:          "id 'asc'",
			expectedOrders: []*query.Order{query.NewOrder("ID", query.DirAscending)},
		},
		"when orderby direction is invalid": {
			value:         "id invalid",
			expectedError: fmt.Errorf("invalid direction `invalid`"),
		},
		"when orderby is invalid": {
			value:         "id asc invalid",
			expectedError: fmt.Errorf("invalid token count"),
		},
		"when orderby is empty": {
			value:          "",
			expectedOrders: []*query.Order{},
		},
		"when orderby has multiple orders": {
			value:          "id asc, namespaceId desc",
			expectedOrders: []*query.Order{query.NewOrder("ID", query.DirAscending), query.NewOrder("NamespaceID", query.DirDescending)},
		},
		"when orderby contains empty orders": {
			value:          ",,id asc,",
			expectedOrders: []*query.Order{query.NewOrder("ID", query.DirAscending)},
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			q, err := ParseODataOrderBy(tt.value, resource.TypeDeliveryKind)
			if err != nil {
				assert.Equal(t, tt.expectedError, err)
				return
			}

			assert.Equal(t, tt.expectedOrders, q)
		})
	}
}

func TestParseODataTop(t *testing.T) {
	tests := map[string]struct {
		value              string
		expectedStartIndex int
		expectedError      error
	}{
		"when top value is positive": {
			value:              "1",
			expectedStartIndex: 1,
		},
		"when top value is zero": {
			value:              "0",
			expectedStartIndex: 0,
		},
		"when top value is negative": {
			value:         "-1",
			expectedError: fmt.Errorf("invalid value `-1`"),
		},
		"when top value is invalid": {
			value:         "invalid",
			expectedError: fmt.Errorf("invalid value `invalid`"),
		},
		"when top value is empty": {
			value:              "",
			expectedStartIndex: 0,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			q, err := ParseODataTop(tt.value)
			if err != nil {
				assert.Equal(t, tt.expectedError, err)
				return
			}

			assert.Equal(t, tt.expectedStartIndex, q)
		})
	}
}

func TestParseODataSkip(t *testing.T) {
	tests := map[string]struct {
		value            string
		expectedCapacity int
		expectedError    error
	}{
		"when skip value is positive": {
			value:            "1",
			expectedCapacity: 1,
		},
		"when skip value is zero": {
			value:            "0",
			expectedCapacity: 0,
		},
		"when skip value is negative": {
			value:         "-1",
			expectedError: fmt.Errorf("invalid value `-1`"),
		},
		"when skip value is invalid": {
			value:         "invalid",
			expectedError: fmt.Errorf("invalid value `invalid`"),
		},
		"when skip value is empty": {
			value:            "",
			expectedCapacity: 0,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			q, err := ParseODataSkip(tt.value)
			if err != nil {
				assert.Equal(t, tt.expectedError, err)
				return
			}

			assert.Equal(t, tt.expectedCapacity, q)
		})
	}
}
