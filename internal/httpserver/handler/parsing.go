package handler

import (
	"encoding/json"
	"fmt"
	"io"
	"reflect"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"

	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

func ParseRequestBody(body io.Reader, out interface{}) error {
	reqBody, err := io.ReadAll(body)
	if err != nil {
		return fmt.Errorf("error reading body")
	}

	if err := json.Unmarshal(reqBody, out); err != nil {
		return fmt.Errorf("error parsing body")
	}

	return nil
}

func ParseOData(params map[string]string, rt resource.Type) (*query.Query, error) {
	q := query.NewQuery()

	if odataFilter, ok := params["$filter"]; ok && odataFilter != "" {
		filters, err := ParseODataFilters(odataFilter, rt)
		if err != nil {
			return nil, fmt.Errorf("error parsing $filter: %s", err)
		}

		q.AddFilters(filters...)
	}

	if odataOrderby, ok := params["$orderby"]; ok && odataOrderby != "" {
		orders, err := ParseODataOrderBy(odataOrderby, rt)
		if err != nil {
			return nil, fmt.Errorf("error parsing $order: %s", err)
		}

		q.AddOrders(orders...)
	} else {
		q.AddOrders(query.NewOrder("CreateTime", query.DirAscending))
	}

	odataTop := params["$top"]
	odataSkip := params["$skip"]
	if odataTop != "" || odataSkip != "" {
		capacity, err := ParseODataTop(odataTop)
		if err != nil {
			return nil, fmt.Errorf("error parsing $top: %s", err)
		}

		startIndex, err := ParseODataSkip(odataSkip)
		if err != nil {
			return nil, fmt.Errorf("error parsing $skip: %s", err)
		}

		q.SetPage(query.NewPage(startIndex, capacity))
	} else {
		q.SetPage(query.NewPage(0, 20))
	}

	return q, nil
}

// ParseODataFilters parses the specified string value into an array of *Filter structs.
//
// The $filter system query option restricts the set of items returned.
//
// Example 46: return all Products whose Price is less than $10.00
//
//     GET http://host/service/Products?$filter=Price lt 10.00
//
// As a more concrete example, when receiving the following string
//
//     "status eq 'delivered' and deliverTime le '2000-01-02T03:04:05'"
//
// this function is expected to return the following filters, in the same order:
//
// * { Operator: OpEqual, FieldName: "status", Operand: "delivered" }
// * { Operator: OpLessThan, FieldName: "deliverTime", Operand: "2000-01-02T03:04:05" }
//
// Only the following operators are currently supported:
//
// Comparison Operators
//
// Operator|Description|Example
// ---|---|---
// eq|Equal|Address/City eq 'Redmond'
// ne|Not equal|Address/City ne 'London'
// gt|Greater than|Price gt 20
// ge|Greater than or equal|Price ge 10
// lt|Less than|Price lt 20
// le|Less than or equal|Price le 100
// in|Is a member of|Address/City in ('Redmond', 'London')
//
// Logical Operators
//
// Operator|Description|Example
// ---|---|---
// and|Logical and|Price le 200 and Price gt 3.5
//
// As this algorithm is currently coded, the following queries are valid, even though they
// probably shouldn't be:
//
//     "and status eq 'pending'"
//     "status eq 'pending' and"
//
// This function will return an error if the value could not be parsed, or if a resulting filter is
// invalid.
//
// ref. https://docs.oasis-open.org/odata/odata/v4.01/odata-v4.01-part1-protocol.html#sec_SystemQueryOptionfilter
// ref. https://docs.oasis-open.org/odata/odata/v4.01/odata-v4.01-part2-url-conventions.html#_Toc31360954
// ref. https://docs.oasis-open.org/odata/odata/v4.01/odata-v4.01-part1-protocol.html#ABNF
func ParseODataFilters(value string, rt resource.Type) ([]*query.Filter, error) {
	filters := make([]*query.Filter, 0)
	tokens := tokenize(value)

	for {
		if len(tokens) > 0 && tokens[0] == "and" {
			tokens = tokens[1:]
		}

		if len(tokens) == 0 {
			break
		}

		// Current subset of implemented operators can only group tokens by three.
		if len(tokens) < 3 {
			return nil, fmt.Errorf("invalid token count")
		}

		fieldName, err := parseODataAttribute(tokens[0], rt)
		if err != nil {
			return nil, err
		}

		op, err := parseODataFilterOperator(tokens[1])
		if err != nil {
			return nil, err
		}

		filter := &query.Filter{
			FieldName: fieldName,
			Operator:  op,
			Operand:   parseODataFilterOperand(tokens[2]),
		}

		tokens = tokens[3:]
		filters = append(filters, filter)
	}

	return filters, nil
}

func parseODataFilterOperator(v string) (query.Operator, error) {
	switch v {
	case "eq":
		return query.OpEqual, nil
	case "ne":
		return query.OpNotEqual, nil
	case "gt":
		return query.OpGreaterThan, nil
	case "ge":
		return query.OpGreaterThanOrEqual, nil
	case "lt":
		return query.OpLessThan, nil
	case "le":
		return query.OpLessThanOrEqual, nil
	default:
		return query.OpNone, fmt.Errorf("invalid operator `%s`", v)
	}
}

func parseODataFilterOperand(v string) interface{} {
	if v == "null" {
		return nil
	}

	return trimQuotes(v)
}

// ParseODataOrderBy parses the specified string value into an array of *Order structs.
//
// The $orderby System Query option specifies the order in which items are returned from the
// service.
//
// The value of the $orderby System Query option contains a comma-separated list of expressions
// whose primitive result values are used to sort the items. A special case of such an expression is
// a property path terminating on a primitive property. A type cast using the qualified entity type
// name is required to order by a property defined on a derived type. Only aliases defined in the
// metadata document of the service can be used in URLs.
//
// The expression can include the suffix asc for ascending or desc for descending, separated from
// the property name by one or more spaces. If asc or desc is not specified, the service MUST order
// by the specified property in ascending order. 4.01 services MUST support case-insensitive values
// for asc and desc. Clients that want to work with 4.0 services MUST use lower case values.
//
// Null values come before non-null values when sorting in ascending order and after non-null values
// when sorting in descending order.
//
// Items are sorted by the result values of the first expression, and then items with the same value
// for the first expression are sorted by the result value of the second expression, and so on.
//
// The Boolean value false comes before the value true in ascending order.
//
// Services SHOULD order language-dependent strings according to the content-language of the
// response, and SHOULD annotate string properties with language-dependent order with the term
// Core.IsLanguageDependent, see [OData-VocCore].
//
// Values of type Edm.Stream or any of the Geo types cannot be sorted.
//
// Example 50: return all Products ordered by release date in ascending order, then by rating in
// descending order
//
//     GET http://host/service/Products?$orderby=ReleaseDate asc, Rating desc
//
// Related entities may be ordered by specifying $orderby within the $expand clause.
//
// Example 51: return all Categories, and their Products ordered according to release date and in
// descending order of rating
//
//     GET http://host/service/Categories?$expand=Products($orderby=ReleaseDate asc, Rating desc)
//
// $count may be used within a $orderby expression to order the returned items according to the
// exact count of related entities or items within a collection-valued property.
//
// Example 52: return all Categories ordered by the number of Products within each category
//
//     GET http://host/service/Categories?$orderby=Products/$count
//
// ref. https://docs.oasis-open.org/odata/odata/v4.01/odata-v4.01-part1-protocol.html#sec_SystemQueryOptionorderby
func ParseODataOrderBy(value string, rt resource.Type) ([]*query.Order, error) {
	orders := make([]*query.Order, 0)
	for _, v := range strings.Split(value, ",") {
		if v == "" {
			continue
		}

		op, v := popToken(v)
		fieldName, err := parseODataAttribute(op, rt)
		if err != nil {
			return nil, err
		}

		d, v := popToken(v)
		dir, err := parseODataOrderByDirection(d)
		if err != nil {
			return nil, err
		}

		if v != "" {
			return nil, fmt.Errorf("invalid token count")
		}

		order := &query.Order{
			FieldName: fieldName,
			Direction: dir,
		}

		orders = append(orders, order)
	}

	return orders, nil
}

func parseODataOrderByDirection(v string) (query.Direction, error) {
	switch {
	case strings.EqualFold(v, "asc"), v == "":
		return query.DirAscending, nil
	case strings.EqualFold(v, "desc"):
		return query.DirDescending, nil
	default:
		return query.DirNone, fmt.Errorf("invalid direction `%s`", v)
	}
}

// parseODataTop parses the specified string value into an integer.
//
// The $top system query option specifies a non-negative integer n that limits the number of items
// returned from a collection. The service returns the number of available items up to but not
// greater than the specified value n.
//
// Example 53: return only the first five products of the Products entity set
//
//     GET http://host/service/Products?$top=5
//
// If no unique ordering is imposed through an $orderby query option, the service MUST impose a
// stable ordering across requests that include $top.
//
// ref. https://docs.oasis-open.org/odata/odata/v4.01/odata-v4.01-part1-protocol.html#sec_SystemQueryOptiontop
func ParseODataTop(value string) (int, error) {
	if value == "" {
		return 0, nil
	}

	v, err := strconv.Atoi(value)
	if err != nil {
		return 0, fmt.Errorf("invalid value `%s`", value)
	}

	if v < 0 {
		return 0, fmt.Errorf("invalid value `%s`", value)
	}

	return v, nil
}

// parseODataSkip parses the specified string value into an integer.
//
// The $skip system query option specifies a non-negative integer n that excludes the first n items
// of the queried collection from the result. The service returns items starting at position n+1.
//
// Example 54: return products starting with the 6th product of the Products entity set
//
//     GET http://host/service/Products?$skip=5
//
// Where $top and $skip are used together, $skip MUST be applied before $top, regardless of the
// order in which they appear in the request.
//
// Example 55: return the third through seventh products of the Products entity set
//
//     GET http://host/service/Products?$top=5&$skip=2
//
// If no unique ordering is imposed through an $orderby query option, the service MUST impose a
// stable ordering across requests that include $skip.
//
// ref. https://docs.oasis-open.org/odata/odata/v4.01/odata-v4.01-part1-protocol.html#sec_SystemQueryOptionskip
func ParseODataSkip(value string) (int, error) {
	if value == "" {
		return 0, nil
	}

	v, err := strconv.Atoi(value)
	if err != nil {
		return 0, fmt.Errorf("invalid value `%s`", value)
	}

	if v < 0 {
		return 0, fmt.Errorf("invalid value `%s`", value)
	}

	return v, nil
}

func parseODataAttribute(v string, rt resource.Type) (string, error) {
	fieldName := fieldNameFromAttribute(v, rt)
	if fieldName == "" {
		return "", fmt.Errorf("invalid attribute `%s`", v)
	}

	return fieldName, nil
}

func fieldNameFromAttribute(attr string, rt resource.Type) string {
	resType := reflect.TypeOf(rt.New()).Elem()
	for i := 0; i < resType.NumField(); i++ {
		field := resType.Field(i)

		if attr == field.Tag.Get("json") {
			return field.Name
		}
	}

	return ""
}

// tokenize breaks the string value into tokens, respecting quoted strings. The quoted strings will
// contain the quotes themselves. For example, a value of "status eq 'pending'" will return
//
//     ["status", "eq", "'pending'"]
func tokenize(value string) []string {
	nullRune := rune(0)
	quoteRune := nullRune

	fn := func(r rune) bool {
		switch {
		case r == quoteRune:
			// Found a matching ending quote.
			quoteRune = nullRune
			return false

		case quoteRune != nullRune:
			// Currently inside a quote.
			return false

		case unicode.In(r, unicode.Quotation_Mark):
			// Found an opening quote.
			quoteRune = r
			return false

		default:
			// Otherwise break on all spaces.
			return unicode.IsSpace(r)
		}
	}

	return strings.FieldsFunc(value, fn)
}

// trimQuotes removes enclosing and matching quotes around the specified value.
func trimQuotes(value string) string {
	firstRune, firstRuneSize := utf8.DecodeRuneInString(value)
	if !unicode.In(firstRune, unicode.Quotation_Mark) {
		return value
	}

	lastRune, lastRuneSize := utf8.DecodeLastRuneInString(value)

	// Does nothing if the last rune does not match the first quote rune.
	if firstRune != lastRune {
		return value
	}

	return value[firstRuneSize : len(value)-lastRuneSize]
}

// popToken removes the first token, based on a space separator, from the specified string s, and
// returns both.
//
// Tokens will not contain spaces, unless surrounded by matching quotes. If the returned token is
// empty, it means that there are no more tokens in the provided string s.
func popToken(value string) (token, result string) {
	nullRune := rune(0)
	quoteRune := nullRune

	length := 0

	for {
		if value == "" {
			break
		}

		addRune := false
		tokenFound := false

		r, l := utf8.DecodeRuneInString(value)
		value = value[l:]
		length += l

		switch {
		case r == quoteRune:
			// Closing quote.
			quoteRune = nullRune

		case quoteRune != nullRune:
			// Inside quote, do nothing special.
			addRune = true

		case unicode.In(r, unicode.Quotation_Mark):
			// Opening quote.
			quoteRune = r

		case unicode.IsSpace(r):
			// Token is found, unless currently empty. This prevents beginning spaces from returning
			// empty tokens.
			tokenFound = token != ""

		default:
			addRune = true
		}

		if addRune {
			token += string(r)
		}

		if tokenFound {
			break
		}
	}

	return token, value
}
