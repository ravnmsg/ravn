package handler

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/google/uuid"
	"github.com/gorilla/mux"

	"gitlab.com/ravnmsg/ravn/internal/authorizer"
)

// RequestIDMiddleware retrieves the request ID from the HTTP headers, or generates a new one. The
// resulting request ID is placed into the request's context.
func RequestIDMiddleware(h http.Handler) http.Handler {
	f := func(w http.ResponseWriter, r *http.Request) {
		requestID := r.Header.Get("X-Request-ID")
		if requestID == "" {
			requestID = uuid.New().String()
		}

		ctx := SetRequestID(r.Context(), requestID)

		h.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(f)
}

// APIVersionMiddleware validates that the specified api version, if any, is equal to v1.
func APIVersionMiddleware(h http.Handler) http.Handler {
	f := func(w http.ResponseWriter, r *http.Request) {
		apiVersion := mux.Vars(r)["api_version"]
		if apiVersion != "" && apiVersion != "v1" {
			WriteErrorResponse(w, ErrInvalidAPIVersion, nil)
			return
		}

		ctx := SetAPIVersion(r.Context(), apiVersion)

		h.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(f)
}

// JWTMiddleware parses and validates the request's JWT token.
//
// JWT claims should contain something similar to the following:
//
//     {
//       "roles": [
//         { "namespaceId": "00000000-0000-0000-0001-000000000001", "code": "ns_writer" }
//         { "namespaceId": "00000000-0000-0000-0001-000000000003", "code": "ns_reader" }
//       ]
//     }
func JWTMiddleware(h http.Handler) http.Handler {
	f := func(w http.ResponseWriter, r *http.Request) {
		token, err := readAuthToken(r)
		if err != nil {
			WriteErrorResponse(w, ErrInvalidAuthToken, nil)
			return
		}

		userRoles, err := authorizer.ParseAuthToken(token)
		if err != nil {
			WriteErrorResponse(w, err, nil)
			return
		}

		ctx := r.Context()
		ctx = authorizer.SetUserRoles(ctx, userRoles)

		h.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(f)
}

func readAuthToken(r *http.Request) (string, error) {
	token := r.Header.Get("Authorization")

	tokenParts := strings.Split(token, " ")
	if len(tokenParts) != 2 {
		return "", fmt.Errorf("invalid token with value `%s`", token)
	} else if tokenParts[0] != "Bearer" {
		return "", fmt.Errorf("invalid token prefix `%s`", tokenParts[0])
	}

	return tokenParts[1], nil
}
