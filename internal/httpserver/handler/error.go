package handler

import (
	"errors"
	"net/http"

	"gitlab.com/ravnmsg/ravn/internal/authorizer"
)

// ErrorStatus maps an error to its HTTP status code.
func ErrorStatus(err error) int {
	switch {
	// 400 Bad Request
	case errors.Is(err, ErrDeletedResource),
		errors.Is(err, ErrDisabledResource),
		errors.Is(err, ErrInvalidRequestBody),
		errors.Is(err, ErrInvalidResourceScope):
		return http.StatusBadRequest

	// 401 Unauthorized
	case errors.Is(err, authorizer.ErrUnauthenticated):
		return http.StatusUnauthorized

	// 403 Forbidden
	case errors.Is(err, ErrInvalidAuthToken),
		errors.Is(err, authorizer.ErrInvalidJWT),
		errors.Is(err, authorizer.ErrInvalidJWTPayload),
		errors.Is(err, authorizer.ErrUnauthorized):
		return http.StatusForbidden

	// 404 Not Found
	case errors.Is(err, ErrInvalidAPIVersion),
		errors.Is(err, ErrInvalidMethodOrPath),
		errors.Is(err, ErrInvalidResourceID),
		errors.Is(err, ErrInvalidResourceIDOrCode):
		return http.StatusNotFound

	// 500 Internal Server Error
	// All other errors are considered as "not caused by the user".
	default:
		return http.StatusInternalServerError
	}
}

var (
	// ErrDeletedResource defines an error caused by a resource that is either directly deleted, or
	// that is under a deleted namespace branch.
	ErrDeletedResource = errors.New("deleted resource")

	// ErrDisabledResource defines an error caused by a disabled resource. A disabled resource is
	// still accessible, but cannot be used in processes. Disabled resources are not hidden, like
	// deleted resources are.
	ErrDisabledResource = errors.New("disabled resource")

	// ErrInvalidAPIVersion defines an error caused by a request to an API endpoint with an invalid
	// version ID.
	ErrInvalidAPIVersion = errors.New("invalid API version")

	// ErrInvalidAuthToken defines an error caused by a malformed Authorization header, for example
	// a basic instead of a bearer token. This is not the same as authorizer.ErrInvalidJWT, which
	// handles the actual JWT and leads to an actual 403 FORBIDDEN.
	ErrInvalidAuthToken = errors.New("invalid auth token")

	// ErrInvalidMethodOrPath defines an error caused by a request that doesn't match anything, for
	// example a request to an invalid path, or a POST request to a specific resource ID.
	ErrInvalidMethodOrPath = errors.New("invalid method or path")

	// ErrInvalidQuery defines an error caused by an invalid or malformed query. This does not
	// include validation errors.
	ErrInvalidQuery = errors.New("invalid query")

	ErrInvalidRequestBody = errors.New("invalid request body")

	// ErrInvalidResource defines an error caused by an invalid resource in the provided path, for
	// example because of an invalid namespace id or code, or a resource that is in another
	// namespace.
	ErrInvalidResource = errors.New("invalid resource")

	// ErrInvalidResourceID defines an error caused by an invalid resource id in the provided path.
	// This only includes the main resource id, not a sub-resource (eg. a namespace id).
	ErrInvalidResourceID = errors.New("invalid resource id")

	// ErrInvalidResourceIDOrCode defines an error caused by a value that is supposed to uniquely
	// match a given resource, but which does not match either an ID, nor a code field.
	ErrInvalidResourceIDOrCode = errors.New("invalid resource id or code")

	// ErrInvalidResourceScope defines an error caused by a mismatch between the user scope
	// provided in the path, and the namespace related to the current resource.
	ErrInvalidResourceScope = errors.New("invalid resource scope")
)
