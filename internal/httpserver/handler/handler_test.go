//go:build !integration && !endtoend
// +build !integration,!endtoend

package handler_test

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestRequestParams(t *testing.T) {
	req := httptest.NewRequest(http.MethodGet, "/path?arg1=qs&arg2=qs", nil)
	req = mux.SetURLVars(req, map[string]string{"arg2": "mux", "arg3": "mux"})

	params := handler.RequestParams(req)

	expectedParams := map[string]string{
		"arg1": "qs",
		"arg2": "mux",
		"arg3": "mux",
	}
	assert.Equal(t, expectedParams, params)
}

func TestWriteResponse(t *testing.T) {
	tests := map[string]struct {
		status         int
		data           interface{}
		meta           map[string]interface{}
		expectedStatus int
		expectedBody   []byte
	}{
		"when data is an array of bytes": {
			status:         http.StatusOK,
			data:           []byte("file content"),
			meta:           map[string]interface{}{"key": "value"},
			expectedStatus: http.StatusOK,
			expectedBody:   []byte("file content"),
		},
		"when data is a resource": {
			status: http.StatusOK,
			data: &resource.DeliveryKind{
				ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				Code:        "delivery_kind_000000000000",
				NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				Host:        "host_000000000000",
				Disabled:    true,
				Deleted:     false,
				CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
				UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
			},
			meta:           map[string]interface{}{"key": "value"},
			expectedStatus: http.StatusOK,
			expectedBody: []byte(`{"data":{` +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"delivery_kind_000000000000",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"host_000000000000",` +
				`"disabled":true,` +
				`"deleted":false,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				`},"meta":{"key":"value"}}`),
		},
		"when data is an array of resources": {
			status: http.StatusOK,
			data: []*resource.DeliveryKind{
				{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "delivery_kind_000000000000",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "host_000000000000",
					Disabled:    true,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
				{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000001"),
					Code:        "delivery_kind_000000000001",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Host:        "host_000000000001",
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T02:01:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:01:59Z", t),
				},
			},
			meta:           map[string]interface{}{"key": "value"},
			expectedStatus: http.StatusOK,
			expectedBody: []byte(`{"data":[{` +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"delivery_kind_000000000000",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"host_000000000000",` +
				`"disabled":true,` +
				`"deleted":false,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				`},{` +
				`"id":"00000000-0000-0000-0002-000000000001",` +
				`"code":"delivery_kind_000000000001",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"host_000000000001",` +
				`"disabled":false,` +
				`"deleted":false,` +
				`"createTime":"2000-01-01T02:01:58Z",` +
				`"updateTime":"2000-01-01T02:01:59Z"` +
				`}],"meta":{"key":"value"}}`),
		},
		"when data is a map": {
			status:         http.StatusOK,
			data:           map[string]interface{}{"dataKey": "dataValue"},
			meta:           map[string]interface{}{"metaKey": "metaValue"},
			expectedStatus: http.StatusOK,
			expectedBody:   []byte(`{"data":{"dataKey":"dataValue"},"meta":{"metaKey":"metaValue"}}`),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			w := httptest.NewRecorder()
			handler.WriteResponse(w, tt.status, tt.data, tt.meta)

			resp := w.Result()

			assert.Equal(t, tt.expectedStatus, resp.StatusCode)

			respBody, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("unexpected error: %s", err)
			}
			defer resp.Body.Close()

			assert.Equal(t, tt.expectedBody, respBody)
		})
	}
}

func TestWriteErrorResponse(t *testing.T) {
	tests := map[string]struct {
		err            error
		meta           map[string]interface{}
		expectedStatus int
		expectedBody   []byte
	}{
		"when data is an array of bytes": {
			err:            handler.ErrDeletedResource,
			meta:           map[string]interface{}{"key": "value"},
			expectedStatus: http.StatusBadRequest,
			expectedBody: []byte("{" +
				`"errors":["deleted resource"],` +
				`"meta":{"key":"value"}` +
				"}"),
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			w := httptest.NewRecorder()
			handler.WriteErrorResponse(w, tt.err, tt.meta)

			resp := w.Result()

			assert.Equal(t, tt.expectedStatus, resp.StatusCode)

			respBody, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("unexpected error: %s", err)
			}
			defer resp.Body.Close()

			assert.Equal(t, tt.expectedBody, respBody)
		})
	}
}
