package handler

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func RequestParams(r *http.Request) map[string]string {
	reqParams := make(map[string]string)

	query := r.URL.Query()
	for k := range query {
		reqParams[k] = query.Get(k)
	}

	// mux.Vars takes precedence over query string values.
	for k, v := range mux.Vars(r) {
		reqParams[k] = v
	}

	return reqParams
}

// WriteResponse builds the successful response body and writes it to the provided w writer.
func WriteResponse(
	w http.ResponseWriter,
	status int,
	data interface{},
	meta map[string]interface{},
) {
	var respBody []byte
	var respErr error

	switch data := data.(type) {
	case []byte:
		// The meta map will be ignored when sending back files.
		respBody = data
		w.Header().Set("Content-Type", "application/octet-stream")

	default:
		output := map[string]interface{}{
			"data": data,
			"meta": meta,
		}

		var err error
		respBody, err = json.Marshal(output)
		if err != nil {
			respErr = fmt.Errorf("request succeeded, but error building response: %s", err)
		}

		w.Header().Set("Content-Type", "application/json")
	}

	w.WriteHeader(status)

	if _, err := w.Write(respBody); err != nil {
		respErr = fmt.Errorf("request succeeded, but error writing response: %s", err)
	}

	if respErr != nil {
		WriteErrorResponse(w, respErr, meta)
		log.Printf("Unexpected failure writing successful response: %s", respErr)
	}
}

// WriteErrorResponse writes the body of a failed request's response.
func WriteErrorResponse(w http.ResponseWriter, reqErr error, meta map[string]interface{}) {
	if meta == nil {
		meta = make(map[string]interface{})
	}

	// TODO: Maybe new error type for multiple errors?
	status := ErrorStatus(reqErr)

	output := map[string]interface{}{
		"errors": []string{reqErr.Error()},
		"meta":   meta,
	}

	respBody, err := json.Marshal(output)
	if err != nil {
		status = http.StatusInternalServerError
		wrappedErr := fmt.Errorf("unexpected error while building the error response: %w", err)

		respBody = []byte("{" +
			fmt.Sprintf(`"errors":[%q],`, wrappedErr) +
			`"meta":"{}"` +
			"}")
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)

	if _, err := w.Write(respBody); err != nil {
		panic(fmt.Sprintf("unexpected problem writing to an io.Writer: %s", err))
	}
}
