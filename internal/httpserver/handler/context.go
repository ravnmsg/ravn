package handler

import (
	"context"
)

// ctxKey defines a context key. See https://blog.golang.org/context for more information.
type ctxKey int

// List of keys that can be added to a request context.
const (
	ctxKeyAPIVersion ctxKey = iota
	ctxKeyRequestID
)

func APIVersion(ctx context.Context) string {
	return ctx.Value(ctxKeyAPIVersion).(string)
}

func SetAPIVersion(ctx context.Context, apiVersion string) context.Context {
	return context.WithValue(ctx, ctxKeyAPIVersion, apiVersion)
}

func RequestID(ctx context.Context) string {
	return ctx.Value(ctxKeyRequestID).(string)
}

func SetRequestID(ctx context.Context, requestID string) context.Context {
	return context.WithValue(ctx, ctxKeyRequestID, requestID)
}
