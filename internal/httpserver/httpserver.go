package httpserver

import (
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"time"

	"github.com/gorilla/mux"

	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
)

// Server defines a server that listens on HTTP requests.
type Server struct {
	server    *http.Server
	unsecured bool
	listener  net.Listener
	router    *mux.Router
	Host      string
}

// New creates an instance of an HTTP server. When the provided context terminates, the server will
// gracefully shutdown.
func New(ctx context.Context, port string, unsecured bool) (*Server, error) {
	l, err := net.Listen("tcp", fmt.Sprintf(":%s", port))
	if err != nil {
		return nil, fmt.Errorf("error listening to port `%s`: %w", port, err)
	}

	s := http.Server{
		ReadTimeout: 2 * time.Minute,
		Handler:     http.NewServeMux(),
	}

	srv := Server{
		server:    &s,
		unsecured: unsecured,
		listener:  l,
		router:    mux.NewRouter(),
		Host:      l.Addr().String(),
	}

	// Shutdowns the server when the application context ends.
	go func() {
		<-ctx.Done()
		srv.stop()
	}()

	return &srv, nil
}

// Serve starts the HTTP server and listens on the configured port. It will block forever unless the
// server fails.
func (srv *Server) Serve() error {
	// Registers the root endpoint that will catch all invalid paths.
	srv.registerRootEndpoint()

	serveMux, ok := srv.server.Handler.(*http.ServeMux)
	if !ok {
		panic("server mux was not of type *http.ServeMux")
	}

	serveMux.Handle("/", srv.router)

	if err := srv.server.Serve(srv.listener); err != http.ErrServerClosed {
		return fmt.Errorf("error serving: %w", err)
	}

	return nil
}

func (srv *Server) registerRootEndpoint() {
	f := func(w http.ResponseWriter, r *http.Request) {
		if r.Method == http.MethodOptions {
			allowCORS(w)
			return
		}

		handler.WriteErrorResponse(w, handler.ErrInvalidMethodOrPath, nil)
	}

	srv.router.PathPrefix("/").HandlerFunc(f)
}

// TODO: better handling of CORS
func allowCORS(w http.ResponseWriter) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "*")
	w.Header().Set("Access-Control-Allow-Headers", "*")
}

// RegisterEndpoint registers the specified method and path endpoint with the request handler
// reqHandler. The resource type rt is provided for shared handlers.
func (srv *Server) RegisterHandler(method, path string, h http.Handler) {
	// Middlewares are appended in reverse order of execution.

	if !srv.unsecured {
		h = handler.JWTMiddleware(h)
	}

	h = handler.APIVersionMiddleware(h)
	h = handler.RequestIDMiddleware(h)

	srv.router.Handle(path, h).Methods(method)
}

func (srv *Server) stop() {
	// Server has two minutes to finish all current connections.
	timedCtx, cancel := context.WithTimeout(context.Background(), 2*time.Minute)
	defer cancel()

	if err := srv.server.Shutdown(timedCtx); err != nil {
		log.Printf("Error with server shutdown: %s", err)
	}
}
