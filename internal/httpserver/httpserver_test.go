//go:build !unit && !endtoend
// +build !unit,!endtoend

package httpserver_test

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/httpserver"
	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
)

func TestServe(t *testing.T) {
	testDone := false

	// If the test takes more than a second to run, this will end the context, which will shutdown
	// the server. This prevents anything from happening which would lock the test.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	srv, err := httpserver.New(ctx, "0", false)
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	// Fails the test if it took more than a second to run.
	go func(t *testing.T) {
		<-ctx.Done()

		if !testDone {
			t.Errorf("test timeout")
		}
	}(t)

	go func(t *testing.T) {
		if err := srv.Serve(); err != nil {
			t.Errorf("unexpected error: %s", err)
		}
	}(t)

	// Makes a GET request to "/", which is expected to respond with a 404 Not Found. This
	// validates that a server is really listening.
	resp, err := http.Get(fmt.Sprintf("http://%s/", srv.Host))
	if err != nil {
		t.Fatalf("unexpected error: %s", err)
	}

	if resp.StatusCode != 404 {
		t.Fatalf(
			"expected response to have status code `404`, but status was `%s`", resp.Status)
	}

	resp.Body.Close()

	testDone = true
}

// TestRegisterHandler creates a new server and calls its RegisterHandler function. It validates
// that the expected middlewares are called.
func TestRegisterHandler(t *testing.T) {
	validJWT := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlcyI6W3siY29kZSI6Im5zX2VkaXRvciIsIm5hbWVzcGFjZUlkIjoiMDAwMDAwMDAtMDAwMC0wMDAwLTAwMDAtMDAwMDAwMDAwMDAyIn1dfQ.H-CNZwZZkfUFcw8dyzpdci8Mesn2SLbd57Ug-Cpjafk"

	tests := map[string]struct {
		requestID               string
		apiVersion              string
		jwt                     string
		unsecured               bool
		expectedResponseStatus  int
		expectedRequestID       string
		expectedRandomRequestID bool
		expectedAPIVersion      string
		expectedError           string
	}{
		"when successful": {
			requestID:              "request_id",
			apiVersion:             "v1",
			jwt:                    fmt.Sprintf("Bearer %s", validJWT),
			expectedResponseStatus: http.StatusOK,
			expectedRequestID:      "request_id",
			expectedAPIVersion:     "v1",
		},
		"when the request id is empty": {
			requestID:               "",
			apiVersion:              "v1",
			jwt:                     fmt.Sprintf("Bearer %s", validJWT),
			expectedResponseStatus:  http.StatusOK,
			expectedRandomRequestID: true,
			expectedAPIVersion:      "v1",
		},
		"when the api version is invalid": {
			requestID:              "request_id",
			apiVersion:             "invalid",
			jwt:                    fmt.Sprintf("Bearer %s", validJWT),
			expectedResponseStatus: http.StatusNotFound,
			expectedError:          "invalid API version",
		},
		"when JWT is empty": {
			requestID:              "request_id",
			apiVersion:             "v1",
			jwt:                    "",
			expectedResponseStatus: http.StatusForbidden,
			expectedError:          "invalid auth token",
		},
		"when JWT prefix is invalid": {
			requestID:              "request_id",
			apiVersion:             "v1",
			jwt:                    fmt.Sprintf("Basic %s", validJWT),
			expectedResponseStatus: http.StatusForbidden,
			expectedError:          "invalid auth token",
		},
		"when JWT is invalid": {
			requestID:              "request_id",
			apiVersion:             "v1",
			jwt:                    "invalid",
			expectedResponseStatus: http.StatusForbidden,
			expectedError:          "invalid auth token",
		},
		"when unsecured": {
			requestID:              "request_id",
			apiVersion:             "v1",
			unsecured:              true,
			expectedResponseStatus: http.StatusOK,
			expectedRequestID:      "request_id",
			expectedAPIVersion:     "v1",
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			testDone := false

			// If the test takes more than a second to run, this will end the context, which will
			// shutdown the server. This prevents anything from happening which would lock the test.
			ctx, cancel := context.WithTimeout(context.Background(), time.Second)
			defer cancel()

			srv, err := httpserver.New(ctx, "0", tt.unsecured)
			if err != nil {
				t.Fatalf("unexpected error: %s", err)
			}

			// Fails the test if it took more than a second to run.
			go func(t *testing.T) {
				<-ctx.Done()

				if !t.Failed() && !testDone {
					t.Errorf("test timeout")
				}
			}(t)

			h := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				// Reaching this point means that no middlewares failed.
				// The request context should then have the correct request ID and api version.
				ctx := r.Context()
				requestID := handler.RequestID(ctx)
				apiVersion := handler.APIVersion(ctx)

				w.WriteHeader(http.StatusOK)

				body := fmt.Sprintf("%s\n%s", requestID, apiVersion)
				_, err := w.Write([]byte(body))
				if err != nil {
					t.Fatalf("unexpected error: %s", err)
				}
			})

			srv.RegisterHandler(http.MethodGet, "/{api_version}/arbitrary", h)

			go func(t *testing.T) {
				if err := srv.Serve(); err != nil {
					t.Errorf("unexpected error: %s", err)
				}
			}(t)

			url := fmt.Sprintf("http://%s/%s/arbitrary", srv.Host, tt.apiVersion)
			req, err := http.NewRequest(http.MethodGet, url, http.NoBody)
			if err != nil {
				t.Errorf("unexpected error: %s", err)
			}

			req.Header.Set("X-Request-ID", tt.requestID)
			req.Header.Set("Authorization", tt.jwt)

			resp, err := http.DefaultClient.Do(req)
			if err != nil {
				t.Fatalf("unexpected error: %s", err)
			}

			respBody, err := io.ReadAll(resp.Body)
			if err != nil {
				t.Fatalf("unexpected error: %s", err)
			}
			defer resp.Body.Close()

			assert.Equal(t, tt.expectedResponseStatus, resp.StatusCode)

			if tt.expectedError != "" {
				var resp map[string]interface{}
				if err := json.Unmarshal(respBody, &resp); err != nil {
					t.Fatalf("unexpected error: %s", err)
				}

				errorsAttr, ok := resp["errors"]
				if !ok {
					assert.Fail(t, "expected response body to contain the attribute `errors`")
				}

				errorsArray, ok := errorsAttr.([]interface{})
				if !ok {
					assert.Fail(t, "expected response body's `errors` attribute to be an array")
				}

				assert.Equal(t, 1, len(errorsArray))
				assert.Equal(t, tt.expectedError, errorsArray[0].(string))
			} else {
				lines := strings.Split(string(respBody), "\n")

				assert.Equal(t, 2, len(lines))

				if tt.expectedRandomRequestID {
					assert.NotEmpty(t, lines[0])
				} else {
					assert.Equal(t, tt.expectedRequestID, lines[0])
				}

				assert.Equal(t, tt.expectedAPIVersion, lines[1])
			}

			testDone = true
		})
	}
}
