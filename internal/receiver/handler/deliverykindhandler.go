package handler

import (
	"context"
	"fmt"
	"io"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// DeliveryKindHandler defines an HTTP server handler for delivery kind endpoints.
type DeliveryKindHandler struct {
	dka   action.DeliveryKindActioner
	na    action.NamespaceActioner
	authz authorizer.Authorizer
}

// NewDeliveryKindHandler creates a new delivery kind handler.
func NewDeliveryKindHandler(
	dka action.DeliveryKindActioner,
	na action.NamespaceActioner,
	authz authorizer.Authorizer,
) *DeliveryKindHandler {
	return &DeliveryKindHandler{
		dka:   dka,
		na:    na,
		authz: authz,
	}
}

// GetMany handles a request to retrieve multiple delivery kinds. The params can contain OData
// values to filter, sort, and paginate the results.
func (h *DeliveryKindHandler) GetMany(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	q, err := h.getManyQuery(ctx, params)
	if err != nil {
		return nil, meta, err
	}

	deliveryKinds, err := h.dka.RetrieveMany(q)
	if err != nil {
		return nil, meta, err
	}

	return deliveryKinds, meta, nil
}

// GetByID handles a request to retrieve an existing delivery kind, specified by its ID.
func (h *DeliveryKindHandler) GetByID(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	deliveryKind, err := h.retrieveDeliveryKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, deliveryKind.NamespaceID, resource.TypeDeliveryKind, "read"); err != nil {
		return nil, meta, err
	}

	return deliveryKind, meta, nil
}

// Post handles a request to create a new delivery kind.
func (h *DeliveryKindHandler) Post(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	deliveryKind := new(resource.DeliveryKind)
	if err := handler.ParseRequestBody(body, deliveryKind); err != nil {
		return nil, meta, fmt.Errorf("error parsing delivery kind: %w", handler.ErrInvalidRequestBody)
	}

	if pathScope := PathScope(ctx); pathScope != nil {
		if deliveryKind.NamespaceID == uuid.Nil {
			deliveryKind.NamespaceID = pathScope.ID
		} else if deliveryKind.NamespaceID != pathScope.ID {
			// If parent namespace is specified in both path and request body, they must match.
			return nil, meta, handler.ErrInvalidResource
		}
	} else if deliveryKind.NamespaceID == uuid.Nil {
		// If parent namespace is not specified anywhere.
		return nil, meta, handler.ErrInvalidResource
	}

	if err := isNamespaceDeleted(h.na, deliveryKind.NamespaceID); err == handler.ErrDeletedResource {
		return nil, meta, handler.ErrInvalidResource
	} else if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, deliveryKind.NamespaceID, resource.TypeDeliveryKind, "create"); err != nil {
		return nil, meta, err
	}

	deliveryKind, err = h.dka.Create(deliveryKind)
	if err != nil {
		return nil, meta, err
	}

	return deliveryKind, meta, nil
}

// Put handles a request to update an existing delivery kind, specified by its ID, by providing all
// of its attributes.
func (h *DeliveryKindHandler) Put(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	currentDeliveryKind, err := h.retrieveDeliveryKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	err = h.authz.AuthorizeOperation(ctx, currentDeliveryKind.NamespaceID, resource.TypeDeliveryKind, "update")
	if err != nil {
		return nil, meta, err
	}

	deliveryKind := new(resource.DeliveryKind)
	if err := handler.ParseRequestBody(body, deliveryKind); err != nil {
		return nil, meta, fmt.Errorf("error parsing delivery kind: %w", handler.ErrInvalidRequestBody)
	}

	if deliveryKind.NamespaceID == uuid.Nil {
		deliveryKind.NamespaceID = currentDeliveryKind.NamespaceID
	} else if deliveryKind.NamespaceID != currentDeliveryKind.NamespaceID {
		return nil, meta, fmt.Errorf("error validating delivery kind: namespace cannot be modified")
	}

	deliveryKind, err = h.dka.Update(currentDeliveryKind.ID, deliveryKind)
	if err != nil {
		return nil, meta, err
	}

	return deliveryKind, meta, nil
}

// Patch handles a request to update an existing delivery kind, specified by its ID, by only
// providing its changed attributes.
func (h *DeliveryKindHandler) Patch(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	deliveryKind, err := h.retrieveDeliveryKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, deliveryKind.NamespaceID, resource.TypeDeliveryKind, "update"); err != nil {
		return nil, meta, err
	}

	currentDeliveryKindNamespaceID := deliveryKind.NamespaceID

	if err := handler.ParseRequestBody(body, deliveryKind); err != nil {
		return nil, meta, fmt.Errorf("error parsing delivery kind: %w", handler.ErrInvalidRequestBody)
	}

	if deliveryKind.NamespaceID != currentDeliveryKindNamespaceID {
		return nil, meta, fmt.Errorf("error validating delivery kind: namespace cannot be modified")
	}

	deliveryKind, err = h.dka.Update(deliveryKind.ID, deliveryKind)
	if err != nil {
		return nil, meta, err
	}

	return deliveryKind, meta, nil
}

// Delete handles a request to delete an existing namespace, specified by its ID.
func (h *DeliveryKindHandler) Delete(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	deliveryKind, err := h.retrieveDeliveryKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, deliveryKind.NamespaceID, resource.TypeDeliveryKind, "delete"); err != nil {
		return nil, meta, err
	}

	if _, err := h.dka.Delete(deliveryKind.ID); err != nil {
		return nil, meta, err
	}

	return nil, meta, nil
}

func (h *DeliveryKindHandler) getManyQuery(ctx context.Context, params map[string]string) (*query.Query, error) {
	q, err := handler.ParseOData(params, resource.TypeDeliveryKind)
	if err != nil {
		return nil, fmt.Errorf("error parsing query: %s", err)
	}

	authorizedNamespaceIDs, err := h.authz.AuthorizedNamespaceIDs(ctx, resource.TypeDeliveryKind, "read")
	if err != nil {
		return nil, err
	}

	if pathScope := PathScope(ctx); pathScope != nil {
		authorizedNamespaceIDs, err = forcePathScope(authorizedNamespaceIDs, pathScope.ID)
		if err != nil {
			return nil, err
		}
	}

	q.Filters = append(
		q.Filters,
		query.NewFilter("NamespaceID", query.OpIn, authorizedNamespaceIDs),
		query.NewFilter("Deleted", query.OpEqual, false),
	)

	return q, nil
}

func (h *DeliveryKindHandler) retrieveDeliveryKind(ctx context.Context, id uuid.UUID) (*resource.DeliveryKind, error) {
	deliveryKind, namespaceID, err := retrieveDeliveryKind(h.dka, h.na, id)
	if err == handler.ErrDeletedResource {
		return nil, handler.ErrInvalidResourceID
	} else if err != nil {
		return nil, err
	}

	// Validates the delivery kind's scope with the one defined in the path, if any.
	if pathScope := PathScope(ctx); pathScope != nil && pathScope.ID != namespaceID {
		return nil, handler.ErrInvalidResourceID
	}

	return deliveryKind, nil
}
