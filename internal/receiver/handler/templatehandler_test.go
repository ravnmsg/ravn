//go:build !integration && !endtoend
// +build !integration,!endtoend

package handler_test

import (
	"bytes"
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	serverhandler "gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/receiver/handler"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestTemplateHandlerGetMany(t *testing.T) {
	tests := map[string]struct {
		queryFilters                        string
		queryOrderBy                        string
		queryPageTop                        string
		queryPageSkip                       string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		messageKindRetrieveManyExpectation  *test.MessageKindActionRetrieveManyExpectation
		deliveryKindRetrieveManyExpectation *test.DeliveryKindActionRetrieveManyExpectation
		templateKindRetrieveManyExpectation *test.TemplateKindActionRetrieveManyExpectation
		templateRetrieveManyExpectation     *test.TemplateActionRetrieveManyExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			templateRetrieveManyExpectation: &test.TemplateActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithFilterArgs("TemplateKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0004-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplates: []*resource.Template{
					test.ArbitraryTemplate(t),
				},
			},
			expectedData: []*resource.Template{
				test.ArbitraryTemplate(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when user has permission on multiple namespaces": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"), ResourceType: resource.TypeTemplate, Operation: "read"},

				// This ID should not appear in the filters because it is for another resource type.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000003"), ResourceType: resource.TypeMessageKind, Operation: "read"},

				// This ID should not appear in the filters because it is for the create operation.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000004"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001"), uuid.MustParse("00000000-0000-0000-0001-000000000002")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
					{ID: uuid.MustParse("00000000-0000-0000-0003-000000000001")},
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001"), uuid.MustParse("00000000-0000-0000-0001-000000000002")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
					{ID: uuid.MustParse("00000000-0000-0000-0002-000000000001")},
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000"), uuid.MustParse("00000000-0000-0000-0002-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
					{ID: uuid.MustParse("00000000-0000-0000-0004-000000000001")},
				},
			},
			templateRetrieveManyExpectation: &test.TemplateActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000"), uuid.MustParse("00000000-0000-0000-0003-000000000001")}),
					query.WithFilterArgs("TemplateKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0004-000000000000"), uuid.MustParse("00000000-0000-0000-0004-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplates: []*resource.Template{
					test.ArbitraryTemplate(t),
				},
			},
			expectedData: []*resource.Template{
				test.ArbitraryTemplate(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query specified": {
			queryFilters:  "languageTag eq language_tag_000000000000",
			queryOrderBy:  "messageKindId desc",
			queryPageTop:  "5",
			queryPageSkip: "8",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			templateRetrieveManyExpectation: &test.TemplateActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("LanguageTag", query.OpEqual, "language_tag_000000000000"),
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithFilterArgs("TemplateKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0004-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("MessageKindID", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ReturnedTemplates: []*resource.Template{
					test.ArbitraryTemplate(t),
				},
			},
			expectedData: []*resource.Template{
				test.ArbitraryTemplate(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query invalid": {
			queryFilters: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing query: error parsing $filter: invalid token count"),
		},
		"when not authorized": {
			userPermissions: []test.Permission{},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{},
			},
			templateRetrieveManyExpectation: &test.TemplateActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{}),
					query.WithFilterArgs("TemplateKindID", query.OpIn, []uuid.UUID{}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplates: []*resource.Template{},
			},
			expectedData:  []*resource.Template{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when message kinds failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kinds failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kinds failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when templates failed to be found": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			templateRetrieveManyExpectation: &test.TemplateActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithFilterArgs("TemplateKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0004-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplates: []*resource.Template{},
			},
			expectedData:  []*resource.Template{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when templates failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			templateRetrieveManyExpectation: &test.TemplateActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithFilterArgs("TemplateKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0004-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplates: []*resource.Template{},
				ReturnedError:     test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope is specified": {
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000000"), ResourceType: resource.TypeTemplate, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			templateRetrieveManyExpectation: &test.TemplateActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithFilterArgs("TemplateKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0004-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplates: []*resource.Template{
					test.ArbitraryTemplate(t),
				},
			},
			expectedData: []*resource.Template{
				test.ArbitraryTemplate(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ta := test.NewTemplateAction(t)
			ta.ExpectRetrieveMany(tt.templateRetrieveManyExpectation)

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveMany(tt.templateKindRetrieveManyExpectation)

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveMany(tt.messageKindRetrieveManyExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveMany(tt.deliveryKindRetrieveManyExpectation)

			params := map[string]string{
				"$filter":  tt.queryFilters,
				"$orderby": tt.queryOrderBy,
				"$top":     tt.queryPageTop,
				"$skip":    tt.queryPageSkip,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateHandler(ta, tka, mka, dka, nil, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.GetMany(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			ta.AssertExpectations(t)
			tka.AssertExpectations(t)
			mka.AssertExpectations(t)
			dka.AssertExpectations(t)
		})
	}
}

func TestTemplateHandlerGetByID(t *testing.T) {
	tests := map[string]struct {
		id                                   string
		pathScope                            *resource.Namespace
		userPermissions                      []test.Permission
		templateRetrieveByIDExpectation      *test.TemplateActionRetrieveByIDExpectation
		messageKindRetrieveByIDExpectation   *test.MessageKindActionRetrieveByIDExpectation
		templateKindRetrieveByIDExpectation  *test.TemplateKindActionRetrieveByIDExpectation
		deliveryKindRetrieveByIDExpectation  *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectations []*test.NamespaceActionRetrieveParentsExpectation
		expectedData                         interface{}
		expectedMeta                         map[string]interface{}
		expectedError                        error
	}{
		"when successful": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  test.ArbitraryTemplate(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when message kind and template kind are in different namespaces": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
					},
				},
			},
			expectedData:  test.ArbitraryTemplate(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id:              "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when template failed to be found": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: &resource.Template{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when message kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: &resource.TemplateKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when delivery kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when path scope is the parent namespace": {
			id:        "00000000-0000-0000-0005-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  test.ArbitraryTemplate(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope is not the parent namespace": {
			id:        "00000000-0000-0000-0005-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			}, expectedData: nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ta := test.NewTemplateAction(t)
			ta.ExpectRetrieveByID(tt.templateRetrieveByIDExpectation)

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveByID(tt.templateKindRetrieveByIDExpectation)

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			for _, e := range tt.namespaceRetrieveParentsExpectations {
				na.ExpectRetrieveParents(e)
			}

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateHandler(ta, tka, mka, dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.GetByID(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			ta.AssertExpectations(t)
			tka.AssertExpectations(t)
			mka.AssertExpectations(t)
			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestTemplateHandlerPost(t *testing.T) {
	tests := map[string]struct {
		requestBody                          []byte
		pathScope                            *resource.Namespace
		userPermissions                      []test.Permission
		messageKindRetrieveByIDExpectation   *test.MessageKindActionRetrieveByIDExpectation
		templateKindRetrieveByIDExpectation  *test.TemplateKindActionRetrieveByIDExpectation
		deliveryKindRetrieveByIDExpectation  *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectations []*test.NamespaceActionRetrieveParentsExpectation
		templateCreateExpectation            *test.TemplateActionCreateExpectation
		expectedData                         interface{}
		expectedMeta                         map[string]interface{}
		expectedError                        error
	}{
		"when successful": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateCreateExpectation: &test.TemplateActionCreateExpectation{
				ExpectedTemplate: &resource.Template{
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000000", t),
					LanguageTag:    "new_language_tag",
					Body:           "new_body",
				},
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			expectedData:  test.ArbitraryTemplate(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when message kind and template kind are in different namespaces": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
					},
				},
			},
			templateCreateExpectation: &test.TemplateActionCreateExpectation{
				ExpectedTemplate: &resource.Template{
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000000", t),
					LanguageTag:    "new_language_tag",
					Body:           "new_body",
				},
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			expectedData:  test.ArbitraryTemplate(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when not authorized": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			userPermissions: []test.Permission{},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing template: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when message kind failed to be retrieved": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind is deleted": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when message kind is in a deleted namespace branch": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when message kind parent namespaces failed to be retrieved": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when template kind failed to be retrieved": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kind is deleted": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: &resource.TemplateKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when delivery kind failed to be retrieved": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when delivery kind is in a deleted namespace branch": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when delivery kind parent namespaces failed to be retrieved": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when template failed to be created": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateCreateExpectation: &test.TemplateActionCreateExpectation{
				ExpectedTemplate: &resource.Template{
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000000", t),
					LanguageTag:    "new_language_tag",
					Body:           "new_body",
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateCreateExpectation: &test.TemplateActionCreateExpectation{
				ExpectedTemplate: &resource.Template{
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000000", t),
					LanguageTag:    "new_language_tag",
					Body:           "new_body",
				},
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			expectedData:  test.ArbitraryTemplate(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"body":"new_body"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "create"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ta := test.NewTemplateAction(t)
			ta.ExpectCreate(tt.templateCreateExpectation)

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveByID(tt.templateKindRetrieveByIDExpectation)

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			for _, e := range tt.namespaceRetrieveParentsExpectations {
				na.ExpectRetrieveParents(e)
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateHandler(ta, tka, mka, dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Post(ctx, make(map[string]string), bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			ta.AssertExpectations(t)
			tka.AssertExpectations(t)
			mka.AssertExpectations(t)
			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestTemplateHandlerPut(t *testing.T) {
	tests := map[string]struct {
		id                                   string
		requestBody                          []byte
		pathScope                            *resource.Namespace
		userPermissions                      []test.Permission
		templateRetrieveByIDExpectation      *test.TemplateActionRetrieveByIDExpectation
		messageKindRetrieveByIDExpectation   *test.MessageKindActionRetrieveByIDExpectation
		templateKindRetrieveByIDExpectation  *test.TemplateKindActionRetrieveByIDExpectation
		deliveryKindRetrieveByIDExpectation  *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectations []*test.NamespaceActionRetrieveParentsExpectation
		templateUpdateExpectation            *test.TemplateActionUpdateExpectation
		expectedData                         interface{}
		expectedMeta                         map[string]interface{}
		expectedError                        error
	}{
		"when successful": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateUpdateExpectation: &test.TemplateActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedTemplate: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000000", t),
					LanguageTag:    "updated_language_tag",
					Body:           "updated_body",
					CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
				},
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			expectedData:  test.ArbitraryTemplate(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when message kind and delivery kind are in different namespaces": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
					},
				},
			},
			templateUpdateExpectation: &test.TemplateActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedTemplate: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000000", t),
					LanguageTag:    "updated_language_tag",
					Body:           "updated_body",
					CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
				},
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			expectedData:  test.ArbitraryTemplate(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			id:          "00000000-0000-0000-0005-000000000000",
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing template: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with modified parent message kind id": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-ffffffffffff",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating template: message kind cannot be modified"),
		},
		"when validation failed with modified parent template kind id": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-ffffffffffff",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating template: template kind cannot be modified"),
		},
		"when template failed to be found": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: &resource.Template{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when message kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: &resource.TemplateKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when delivery kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template failed to be updated": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateUpdateExpectation: &test.TemplateActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedTemplate: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000000", t),
					LanguageTag:    "updated_language_tag",
					Body:           "updated_body",
					CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateUpdateExpectation: &test.TemplateActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedTemplate: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					TemplateKindID: test.MakeUUID("00000000-0000-0000-0004-000000000000", t),
					LanguageTag:    "updated_language_tag",
					Body:           "updated_body",
					CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
				},
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			expectedData:  test.ArbitraryTemplate(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0005-000000000000",` +
				`"messageKindId":"00000000-0000-0000-0003-000000000000",` +
				`"templateKindId":"00000000-0000-0000-0004-000000000000",` +
				`"languageTag":"updated_language_tag",` +
				`"body":"updated_body",` +
				`"createTime":"2000-01-01T05:00:58Z",` +
				`"updateTime":"2000-01-01T05:00:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ta := test.NewTemplateAction(t)
			ta.ExpectRetrieveByID(tt.templateRetrieveByIDExpectation)
			ta.ExpectUpdate(tt.templateUpdateExpectation)

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveByID(tt.templateKindRetrieveByIDExpectation)

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			for _, e := range tt.namespaceRetrieveParentsExpectations {
				na.ExpectRetrieveParents(e)
			}

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateHandler(ta, tka, mka, dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Put(ctx, params, bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			ta.AssertExpectations(t)
			mka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestTemplateHandlerPatch(t *testing.T) {
	tests := map[string]struct {
		id                                   string
		requestBody                          []byte
		pathScope                            *resource.Namespace
		userPermissions                      []test.Permission
		templateRetrieveByIDExpectation      *test.TemplateActionRetrieveByIDExpectation
		messageKindRetrieveByIDExpectation   *test.MessageKindActionRetrieveByIDExpectation
		templateKindRetrieveByIDExpectation  *test.TemplateKindActionRetrieveByIDExpectation
		deliveryKindRetrieveByIDExpectation  *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectations []*test.NamespaceActionRetrieveParentsExpectation
		templateUpdateExpectation            *test.TemplateActionUpdateExpectation
		expectedData                         interface{}
		expectedMeta                         map[string]interface{}
		expectedError                        error
	}{
		"when successful": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateUpdateExpectation: &test.TemplateActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedTemplate: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "updated_language_tag",
					Body:           "body_000000000000",
					CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
				},
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			expectedData:  test.ArbitraryTemplate(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			id:          "00000000-0000-0000-0005-000000000000",
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing template: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with modified parent message kind id": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"messageKindId":"00000000-0000-0000-0003-ffffffffffff"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating template: message kind cannot be modified"),
		},
		"when validation failed with modified parent template kind id": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"templateKindId":"00000000-0000-0000-0004-ffffffffffff"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating template: template kind cannot be modified"),
		},
		"when template failed to be found": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: &resource.Template{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when message kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: &resource.TemplateKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when delivery kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template failed to be updated": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateUpdateExpectation: &test.TemplateActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedTemplate: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "updated_language_tag",
					Body:           "body_000000000000",
					CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id: "00000000-0000-0000-0005-000000000000",
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateUpdateExpectation: &test.TemplateActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ExpectedTemplate: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "updated_language_tag",
					Body:           "body_000000000000",
					CreateTime:     test.MakeTime("2000-01-01T05:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T05:00:59Z", t),
				},
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			expectedData:  test.ArbitraryTemplate(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id:        "00000000-0000-0000-0005-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			requestBody: []byte("{" +
				`"languageTag":"updated_language_tag"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "update"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ta := test.NewTemplateAction(t)
			ta.ExpectRetrieveByID(tt.templateRetrieveByIDExpectation)
			ta.ExpectUpdate(tt.templateUpdateExpectation)

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveByID(tt.templateKindRetrieveByIDExpectation)

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			for _, e := range tt.namespaceRetrieveParentsExpectations {
				na.ExpectRetrieveParents(e)
			}

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateHandler(ta, tka, mka, dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Patch(ctx, params, bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			ta.AssertExpectations(t)
			tka.AssertExpectations(t)
			mka.AssertExpectations(t)
			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestTemplateHandlerDelete(t *testing.T) {
	tests := map[string]struct {
		id                                   string
		pathScope                            *resource.Namespace
		userPermissions                      []test.Permission
		templateRetrieveByIDExpectation      *test.TemplateActionRetrieveByIDExpectation
		messageKindRetrieveByIDExpectation   *test.MessageKindActionRetrieveByIDExpectation
		templateKindRetrieveByIDExpectation  *test.TemplateKindActionRetrieveByIDExpectation
		deliveryKindRetrieveByIDExpectation  *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectations []*test.NamespaceActionRetrieveParentsExpectation
		templateDeleteExpectation            *test.TemplateActionDeleteExpectation
		expectedData                         interface{}
		expectedMeta                         map[string]interface{}
		expectedError                        error
	}{
		"when successful": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateDeleteExpectation: &test.TemplateActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: nil,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when message kind and template kind are in different namespaces": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
					},
				},
			},
			templateDeleteExpectation: &test.TemplateActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: nil,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id:              "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when template failed to be found": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: &resource.Template{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when message kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: &resource.TemplateKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when delivery kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template failed to be deleted": {
			id: "00000000-0000-0000-0005-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateDeleteExpectation: &test.TemplateActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id:        "00000000-0000-0000-0005-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			templateDeleteExpectation: &test.TemplateActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: nil,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id:        "00000000-0000-0000-0005-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "delete"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			}, expectedData: nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ta := test.NewTemplateAction(t)
			ta.ExpectRetrieveByID(tt.templateRetrieveByIDExpectation)
			ta.ExpectDelete(tt.templateDeleteExpectation)

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveByID(tt.templateKindRetrieveByIDExpectation)

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			for _, e := range tt.namespaceRetrieveParentsExpectations {
				na.ExpectRetrieveParents(e)
			}

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateHandler(ta, tka, mka, dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Delete(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			ta.AssertExpectations(t)
			tka.AssertExpectations(t)
			mka.AssertExpectations(t)
			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestTemplateHandlerPreviewByTemplateID(t *testing.T) {
	tests := map[string]struct {
		requestBody                          []byte
		userPermissions                      []test.Permission
		templateRetrieveByIDExpectation      *test.TemplateActionRetrieveByIDExpectation
		messageKindRetrieveByIDExpectation   *test.MessageKindActionRetrieveByIDExpectation
		templateKindRetrieveByIDExpectation  *test.TemplateKindActionRetrieveByIDExpectation
		deliveryKindRetrieveByIDExpectation  *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectations []*test.NamespaceActionRetrieveParentsExpectation
		expectedData                         interface{}
		expectedMeta                         map[string]interface{}
		expectedError                        error
	}{
		"when successful": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "language_tag",
					Body:           "body {{.key}}",
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  "body value",
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when not authorized": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "language_tag",
					Body:           "body {{.key}}",
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing preview request: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when input is empty": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: &resource.Template{
					ID:             uuid.MustParse("00000000-0000-0000-0005-000000000000"),
					MessageKindID:  uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					TemplateKindID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					LanguageTag:    "language_tag",
					Body:           "body",
				},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  "body",
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when template failed to be found": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template failed to be retrieved": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template is deleted": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: &resource.Template{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind failed to be retrieved": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind is deleted": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind parent namespaces failed to be retrieved": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when message kind is in a deleted namespace branch": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template kind failed to be retrieved": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kind is deleted": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: &resource.TemplateKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind parent namespaces failed to be retrieved": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when delivery kind is in a deleted namespace branch": {
			requestBody: []byte("{" +
				`"templateId":"00000000-0000-0000-0005-000000000000",` +
				`"input":{"key":"value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplate, Operation: "read"},
			},
			templateRetrieveByIDExpectation: &test.TemplateActionRetrieveByIDExpectation{
				ExpectedID:       uuid.MustParse("00000000-0000-0000-0005-000000000000"),
				ReturnedTemplate: test.ArbitraryTemplate(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ta := test.NewTemplateAction(t)
			ta.ExpectRetrieveByID(tt.templateRetrieveByIDExpectation)

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveByID(tt.templateKindRetrieveByIDExpectation)

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			for _, e := range tt.namespaceRetrieveParentsExpectations {
				na.ExpectRetrieveParents(e)
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, nil)

			h := handler.NewTemplateHandler(ta, tka, mka, dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Preview(ctx, make(map[string]string), bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			ta.AssertExpectations(t)
			tka.AssertExpectations(t)
			mka.AssertExpectations(t)
			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestTemplateHandlerPreviewByAttributes(t *testing.T) {
	tests := map[string]struct {
		requestBody   []byte
		expectedData  interface{}
		expectedMeta  map[string]interface{}
		expectedError error
	}{
		"when successful": {
			requestBody: []byte("{" +
				`"languageTag":"language_tag",` +
				`"body":"body {{.key}}",` +
				`"input":{"key":"value"}` +
				"}"),
			expectedData:  "body value",
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when request body failed to be parsed": {
			requestBody:   []byte(""),
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing preview request: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when language tag is empty": {
			requestBody: []byte("{" +
				`"languageTag":"",` +
				`"body":"body {{.key}}",` +
				`"input":{"key":"value"}` +
				"}"),
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("invalid language tag"),
		},
		"when body is empty": {
			requestBody: []byte("{" +
				`"languageTag":"language_tag",` +
				`"body":"",` +
				`"input":{"key":"value"}` +
				"}"),
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("invalid body"),
		},
		"when input is empty": {
			requestBody: []byte("{" +
				`"languageTag":"language_tag",` +
				`"body":"body"` +
				"}"),
			expectedData:  "body",
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, nil)

			h := handler.NewTemplateHandler(nil, nil, nil, nil, nil, test.NewAuthorizer([]test.Permission{}))
			data, meta, err := h.Preview(ctx, make(map[string]string), bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)
		})
	}
}
