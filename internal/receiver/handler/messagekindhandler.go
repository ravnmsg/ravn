package handler

import (
	"context"
	"fmt"
	"io"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// MessageKindHandler defines an HTTP server handler for message kind endpoints.
type MessageKindHandler struct {
	mka   action.MessageKindActioner
	na    action.NamespaceActioner
	authz authorizer.Authorizer
}

// New creates a new message kind handler.
func NewMessageKindHandler(
	mka action.MessageKindActioner,
	na action.NamespaceActioner,
	authz authorizer.Authorizer,
) *MessageKindHandler {
	return &MessageKindHandler{
		mka:   mka,
		na:    na,
		authz: authz,
	}
}

func (h *MessageKindHandler) GetMany(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	q, err := h.getManyQuery(ctx, params)
	if err != nil {
		return nil, meta, err
	}

	messageKinds, err := h.mka.RetrieveMany(q)
	if err != nil {
		return nil, meta, err
	}

	return messageKinds, meta, nil
}

func (h *MessageKindHandler) GetByID(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	messageKind, err := h.retrieveMessageKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, messageKind.NamespaceID, resource.TypeMessageKind, "read"); err != nil {
		return nil, meta, err
	}

	return messageKind, meta, nil
}

// Post handles the creation of a new message kind.
func (h *MessageKindHandler) Post(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	messageKind := new(resource.MessageKind)
	if err := handler.ParseRequestBody(body, messageKind); err != nil {
		return nil, meta, fmt.Errorf("error parsing message kind: %w", handler.ErrInvalidRequestBody)
	}

	if pathScope := PathScope(ctx); pathScope != nil {
		if messageKind.NamespaceID == uuid.Nil {
			messageKind.NamespaceID = pathScope.ID
		} else if messageKind.NamespaceID != pathScope.ID {
			// If parent namespace is specified in both path and request body, they must match.
			return nil, meta, handler.ErrInvalidResource
		}
	} else if messageKind.NamespaceID == uuid.Nil {
		// If parent namespace is not specified anywhere.
		return nil, meta, handler.ErrInvalidResource
	}

	if err := isNamespaceDeleted(h.na, messageKind.NamespaceID); err == handler.ErrDeletedResource {
		return nil, meta, handler.ErrInvalidResource
	} else if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, messageKind.NamespaceID, resource.TypeMessageKind, "create"); err != nil {
		return nil, meta, err
	}

	messageKind, err = h.mka.Create(messageKind)
	if err != nil {
		return nil, meta, err
	}

	return messageKind, meta, nil
}

// Put handles the update of an existing message kind, specified by its ID or code, by providing
// all of its attributes.
func (h *MessageKindHandler) Put(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	currentMessageKind, err := h.retrieveMessageKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	err = h.authz.AuthorizeOperation(ctx, currentMessageKind.NamespaceID, resource.TypeMessageKind, "update")
	if err != nil {
		return nil, meta, err
	}

	messageKind := new(resource.MessageKind)
	if err := handler.ParseRequestBody(body, messageKind); err != nil {
		return nil, meta, fmt.Errorf("error parsing message kind: %w", handler.ErrInvalidRequestBody)
	}

	if messageKind.NamespaceID == uuid.Nil {
		messageKind.NamespaceID = currentMessageKind.NamespaceID
	} else if messageKind.NamespaceID != currentMessageKind.NamespaceID {
		return nil, meta, fmt.Errorf("error validating message kind: namespace cannot be modified")
	}

	messageKind, err = h.mka.Update(currentMessageKind.ID, messageKind)
	if err != nil {
		return nil, meta, err
	}

	return messageKind, meta, nil
}

// Patch handles the modification of an existing message kind, specified by its ID, by providing
// only its changed attributes.
func (h *MessageKindHandler) Patch(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	messageKind, err := h.retrieveMessageKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, messageKind.NamespaceID, resource.TypeMessageKind, "update"); err != nil {
		return nil, meta, err
	}

	currentMessageKindNamespaceID := messageKind.NamespaceID

	if err := handler.ParseRequestBody(body, messageKind); err != nil {
		return nil, meta, fmt.Errorf("error parsing message kind: %w", handler.ErrInvalidRequestBody)
	}

	if messageKind.NamespaceID != currentMessageKindNamespaceID {
		return nil, meta, fmt.Errorf("error validating message kind: namespace cannot be modified")
	}

	messageKind, err = h.mka.Update(messageKind.ID, messageKind)
	if err != nil {
		return nil, meta, err
	}

	return messageKind, meta, nil
}

// Delete handles the deletion of a message kind, specified by its ID or code.
func (h *MessageKindHandler) Delete(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	messageKind, err := h.retrieveMessageKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, messageKind.NamespaceID, resource.TypeMessageKind, "delete"); err != nil {
		return nil, meta, err
	}

	if _, err := h.mka.Delete(messageKind.ID); err != nil {
		return nil, meta, err
	}

	return nil, meta, nil
}

func (h *MessageKindHandler) getManyQuery(ctx context.Context, params map[string]string) (*query.Query, error) {
	q, err := handler.ParseOData(params, resource.TypeDeliveryKind)
	if err != nil {
		return nil, fmt.Errorf("error parsing query: %s", err)
	}

	authorizedNamespaceIDs, err := h.authz.AuthorizedNamespaceIDs(ctx, resource.TypeMessageKind, "read")
	if err != nil {
		return nil, err
	}

	if pathScope := PathScope(ctx); pathScope != nil {
		authorizedNamespaceIDs, err = forcePathScope(authorizedNamespaceIDs, pathScope.ID)
		if err != nil {
			return nil, err
		}
	}

	q.Filters = append(
		q.Filters,
		query.NewFilter("NamespaceID", query.OpIn, authorizedNamespaceIDs),
		query.NewFilter("Deleted", query.OpEqual, false),
	)

	return q, nil
}

func (h *MessageKindHandler) retrieveMessageKind(ctx context.Context, id uuid.UUID) (*resource.MessageKind, error) {
	messageKind, namespaceID, err := retrieveMessageKind(h.mka, h.na, id)
	if err == handler.ErrDeletedResource {
		return nil, handler.ErrInvalidResourceID
	} else if err != nil {
		return nil, err
	}

	// Validates the message kind's scope with the one defined in the path, if any.
	if pathScope := PathScope(ctx); pathScope != nil && pathScope.ID != namespaceID {
		return nil, handler.ErrInvalidResourceID
	}

	return messageKind, nil
}
