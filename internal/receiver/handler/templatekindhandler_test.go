//go:build !integration && !endtoend
// +build !integration,!endtoend

package handler_test

import (
	"bytes"
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	serverhandler "gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/receiver/handler"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestTemplateKindHandlerGetMany(t *testing.T) {
	tests := map[string]struct {
		queryFilters                        string
		queryOrderBy                        string
		queryPageTop                        string
		queryPageSkip                       string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		deliveryKindRetrieveManyExpectation *test.DeliveryKindActionRetrieveManyExpectation
		templateKindRetrieveManyExpectation *test.TemplateKindActionRetrieveManyExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					{ID: uuid.MustParse("00000000-0000-0000-0002-000000000000")},
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			expectedData: []*resource.TemplateKind{
				test.ArbitraryTemplateKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when user has permission on multiple namespaces": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"), ResourceType: resource.TypeTemplateKind, Operation: "read"},

				// This ID should not appear in the filters because it is for another resource type.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000003"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},

				// This ID should not appear in the filters because it is for the create operation.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000004"), ResourceType: resource.TypeTemplateKind, Operation: "create"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001"), uuid.MustParse("00000000-0000-0000-0001-000000000002")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					{ID: uuid.MustParse("00000000-0000-0000-0002-000000000000")},
					{ID: uuid.MustParse("00000000-0000-0000-0002-000000000001")},
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000"), uuid.MustParse("00000000-0000-0000-0002-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			expectedData: []*resource.TemplateKind{
				test.ArbitraryTemplateKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query specified": {
			queryFilters:  "targetAttr eq target_attr_000000000000",
			queryOrderBy:  "targetAttr desc",
			queryPageTop:  "5",
			queryPageSkip: "8",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					{ID: uuid.MustParse("00000000-0000-0000-0002-000000000000")},
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("TargetAttr", query.OpEqual, "target_attr_000000000000"),
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("TargetAttr", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			expectedData: []*resource.TemplateKind{
				test.ArbitraryTemplateKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query invalid": {
			queryFilters: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing query: error parsing $filter: invalid token count"),
		},
		"when not authorized": {
			userPermissions: []test.Permission{},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{},
			},
			expectedData:  []*resource.TemplateKind{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when template kinds failed to be found": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					{ID: uuid.MustParse("00000000-0000-0000-0002-000000000000")},
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{},
			},
			expectedData:  []*resource.TemplateKind{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when delivery kinds failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kinds failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					{ID: uuid.MustParse("00000000-0000-0000-0002-000000000000")},
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{},
				ReturnedError:         test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope is specified": {
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000000"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					{ID: uuid.MustParse("00000000-0000-0000-0002-000000000000")},
				},
			},
			templateKindRetrieveManyExpectation: &test.TemplateKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("DeliveryKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedTemplateKinds: []*resource.TemplateKind{
					test.ArbitraryTemplateKind(t),
				},
			},
			expectedData: []*resource.TemplateKind{
				test.ArbitraryTemplateKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveMany(tt.templateKindRetrieveManyExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveMany(tt.deliveryKindRetrieveManyExpectation)

			params := map[string]string{
				"$filter":  tt.queryFilters,
				"$orderby": tt.queryOrderBy,
				"$top":     tt.queryPageTop,
				"$skip":    tt.queryPageSkip,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateKindHandler(tka, dka, nil, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.GetMany(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			tka.AssertExpectations(t)
			dka.AssertExpectations(t)
		})
	}
}

func TestTemplateKindHandlerGetByID(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		templateKindRetrieveByIDExpectation *test.TemplateKindActionRetrieveByIDExpectation
		deliveryKindRetrieveByIDExpectation *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  test.ArbitraryTemplateKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id:              "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when template kind failed to be found": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template kind failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kind is deleted": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: &resource.TemplateKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when path scope is the parent namespace": {
			id:        "00000000-0000-0000-0004-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  test.ArbitraryTemplateKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope is not the parent namespace": {
			id:        "00000000-0000-0000-0004-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "read"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveByID(tt.templateKindRetrieveByIDExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateKindHandler(tka, dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.GetByID(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			tka.AssertExpectations(t)
			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestTemplateKindHandlerPost(t *testing.T) {
	tests := map[string]struct {
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		deliveryKindRetrieveByIDExpectation *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		templateKindCreateExpectation       *test.TemplateKindActionCreateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			requestBody: []byte("{" +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"new_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "create"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindCreateExpectation: &test.TemplateKindActionCreateExpectation{
				ExpectedTemplateKind: &resource.TemplateKind{
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					TargetAttr:     "new_target_attr",
				},
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			expectedData:  test.ArbitraryTemplateKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when not authorized": {
			requestBody: []byte("{" +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"new_target_attr"` +
				"}"),
			userPermissions: []test.Permission{},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing template kind: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when delivery kind failed to be retrieved": {
			requestBody: []byte("{" +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"new_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "create"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			requestBody: []byte("{" +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"new_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "create"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when delivery kind is in a deleted namespace branch": {
			requestBody: []byte("{" +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"new_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "create"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when parent namespaces failed to be retrieved": {
			requestBody: []byte("{" +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"new_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "create"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when template kind failed to be created": {
			requestBody: []byte("{" +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"new_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "create"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindCreateExpectation: &test.TemplateKindActionCreateExpectation{
				ExpectedTemplateKind: &resource.TemplateKind{
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					TargetAttr:     "new_target_attr",
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			requestBody: []byte("{" +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"new_target_attr"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "create"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindCreateExpectation: &test.TemplateKindActionCreateExpectation{
				ExpectedTemplateKind: &resource.TemplateKind{
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					TargetAttr:     "new_target_attr",
				},
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			expectedData:  test.ArbitraryTemplateKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			requestBody: []byte("{" +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"new_target_attr"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "create"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			tka := test.NewTemplateKindAction(t)
			tka.ExpectCreate(tt.templateKindCreateExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateKindHandler(tka, dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Post(ctx, make(map[string]string), bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			tka.AssertExpectations(t)
			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestTemplateKindHandlerPut(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		templateKindRetrieveByIDExpectation *test.TemplateKindActionRetrieveByIDExpectation
		deliveryKindRetrieveByIDExpectation *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		templateKindUpdateExpectation       *test.TemplateKindActionUpdateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindUpdateExpectation: &test.TemplateKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedTemplateKind: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					TargetAttr:     "updated_target_attr",
					CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
				},
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			expectedData:  test.ArbitraryTemplateKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			id:          "00000000-0000-0000-0004-000000000000",
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing template kind: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with modified parent delivery kind id": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-ffffffffffff",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating template kind: delivery kind cannot be modified"),
		},
		"when template kind failed to be found": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template kind failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kind is deleted": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: &resource.TemplateKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind deleted": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when template kind failed to be updated": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindUpdateExpectation: &test.TemplateKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedTemplateKind: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					TargetAttr:     "updated_target_attr",
					CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindUpdateExpectation: &test.TemplateKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedTemplateKind: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					TargetAttr:     "updated_target_attr",
					CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
				},
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			expectedData:  test.ArbitraryTemplateKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0004-000000000000",` +
				`"deliveryKindId":"00000000-0000-0000-0002-000000000000",` +
				`"targetAttr":"updated_target_attr",` +
				`"createTime":"2000-01-01T04:00:58Z",` +
				`"updateTime":"2000-01-01T04:00:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveByID(tt.templateKindRetrieveByIDExpectation)
			tka.ExpectUpdate(tt.templateKindUpdateExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateKindHandler(tka, dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Put(ctx, params, bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			tka.AssertExpectations(t)
			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestTemplateKindHandlerPatch(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		templateKindRetrieveByIDExpectation *test.TemplateKindActionRetrieveByIDExpectation
		deliveryKindRetrieveByIDExpectation *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		templateKindUpdateExpectation       *test.TemplateKindActionUpdateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindUpdateExpectation: &test.TemplateKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedTemplateKind: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					TargetAttr:     "updated_target_attr",
					CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
				},
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			expectedData:  test.ArbitraryTemplateKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			id:          "00000000-0000-0000-0004-000000000000",
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing template kind: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with modified parent delivery kind id": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"deliveryKindId":"00000000-0000-0000-0002-ffffffffffff"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating template kind: delivery kind cannot be modified"),
		},
		"when template kind failed to be found": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template kind failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kind is deleted": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: &resource.TemplateKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind deleted": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when template kind failed to be updated": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindUpdateExpectation: &test.TemplateKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedTemplateKind: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					TargetAttr:     "updated_target_attr",
					CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id: "00000000-0000-0000-0004-000000000000",
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindUpdateExpectation: &test.TemplateKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ExpectedTemplateKind: &resource.TemplateKind{
					ID:             uuid.MustParse("00000000-0000-0000-0004-000000000000"),
					DeliveryKindID: test.MakeUUID("00000000-0000-0000-0002-000000000000", t),
					TargetAttr:     "updated_target_attr",
					CreateTime:     test.MakeTime("2000-01-01T04:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T04:00:59Z", t),
				},
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			expectedData:  test.ArbitraryTemplateKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id:        "00000000-0000-0000-0004-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			requestBody: []byte("{" +
				`"targetAttr":"updated_target_attr"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "update"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveByID(tt.templateKindRetrieveByIDExpectation)
			tka.ExpectUpdate(tt.templateKindUpdateExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateKindHandler(tka, dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Patch(ctx, params, bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			tka.AssertExpectations(t)
			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestTemplateKindHandlerDelete(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		templateKindRetrieveByIDExpectation *test.TemplateKindActionRetrieveByIDExpectation
		deliveryKindRetrieveByIDExpectation *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		templateKindDeleteExpectation       *test.TemplateKindActionDeleteExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindDeleteExpectation: &test.TemplateKindActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: nil,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id:              "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when template kind failed to be found": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when template kind failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when template kind is deleted": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: &resource.TemplateKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind deleted": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind is in a deleted namespace": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when template kind failed to be deleted": {
			id: "00000000-0000-0000-0004-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindDeleteExpectation: &test.TemplateKindActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id:        "00000000-0000-0000-0004-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			templateKindDeleteExpectation: &test.TemplateKindActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedError: nil,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id:        "00000000-0000-0000-0004-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeTemplateKind, Operation: "delete"},
			},
			templateKindRetrieveByIDExpectation: &test.TemplateKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0004-000000000000"),
				ReturnedTemplateKind: test.ArbitraryTemplateKind(t),
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			tka := test.NewTemplateKindAction(t)
			tka.ExpectRetrieveByID(tt.templateKindRetrieveByIDExpectation)
			tka.ExpectDelete(tt.templateKindDeleteExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewTemplateKindHandler(tka, dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Delete(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			tka.AssertExpectations(t)
			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}
