package handler

import (
	"context"

	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// ctxKey defines a context key. See https://blog.golang.org/context for more information.
type ctxKey int

// List of keys that can be added to a request context.
const (
	ctxKeyPathScope ctxKey = iota
)

func PathScope(ctx context.Context) *resource.Namespace {
	return ctx.Value(ctxKeyPathScope).(*resource.Namespace)
}

func SetPathScope(ctx context.Context, scope *resource.Namespace) context.Context {
	return context.WithValue(ctx, ctxKeyPathScope, scope)
}
