//go:build !integration && !endtoend
// +build !integration,!endtoend

package handler_test

import (
	"bytes"
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	serverhandler "gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/receiver/handler"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestMessageKindHandlerGetMany(t *testing.T) {
	tests := map[string]struct {
		queryFilters                       string
		queryOrderBy                       string
		queryPageTop                       string
		queryPageSkip                      string
		pathScope                          *resource.Namespace
		userPermissions                    []test.Permission
		messageKindRetrieveManyExpectation *test.MessageKindActionRetrieveManyExpectation
		expectedData                       interface{}
		expectedMeta                       map[string]interface{}
		expectedError                      error
	}{
		"when successful": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			expectedData: []*resource.MessageKind{
				test.ArbitraryMessageKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when user has permission on multiple namespaces": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"), ResourceType: resource.TypeMessageKind, Operation: "read"},

				// This ID should not appear in the filters because it is for another resource type.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000003"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},

				// This ID should not appear in the filters because it is for the create operation.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000004"), ResourceType: resource.TypeMessageKind, Operation: "create"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001"), uuid.MustParse("00000000-0000-0000-0001-000000000002")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			expectedData: []*resource.MessageKind{
				test.ArbitraryMessageKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query specified": {
			queryFilters:  "code eq message_kind_000000000000",
			queryOrderBy:  "code desc",
			queryPageTop:  "5",
			queryPageSkip: "8",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Code", query.OpEqual, "message_kind_000000000000"),
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("Code", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			expectedData: []*resource.MessageKind{
				test.ArbitraryMessageKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query invalid": {
			queryFilters: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing query: error parsing $filter: invalid token count"),
		},
		"when not authorized": {
			userPermissions: []test.Permission{},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessageKinds: []*resource.MessageKind{},
			},
			expectedData:  []*resource.MessageKind{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when message kinds failed to be found": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessageKinds: []*resource.MessageKind{},
			},
			expectedData:  []*resource.MessageKind{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when message kinds failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessageKinds: []*resource.MessageKind{},
				ReturnedError:        test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope is specified": {
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000000"), ResourceType: resource.TypeMessageKind, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			expectedData: []*resource.MessageKind{
				test.ArbitraryMessageKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveMany(tt.messageKindRetrieveManyExpectation)

			params := map[string]string{
				"$filter":  tt.queryFilters,
				"$orderby": tt.queryOrderBy,
				"$top":     tt.queryPageTop,
				"$skip":    tt.queryPageSkip,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewMessageKindHandler(mka, nil, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.GetMany(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			mka.AssertExpectations(t)
		})
	}
}

func TestMessageKindHandlerGetByID(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		messageKindRetrieveByIDExpectation  *test.MessageKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  test.ArbitraryMessageKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id:              "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when message kind failed to be found": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind failed to be retrieved": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind is deleted": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when path scope is the parent namespace": {
			id:        "00000000-0000-0000-0003-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  test.ArbitraryMessageKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope is not the parent namespace": {
			id:        "00000000-0000-0000-0003-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewMessageKindHandler(mka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.GetByID(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			mka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestMessageKindHandlerPost(t *testing.T) {
	tests := map[string]struct {
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		messageKindCreateExpectation        *test.MessageKindActionCreateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			requestBody: []byte("{" +
				`"code":"new_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindCreateExpectation: &test.MessageKindActionCreateExpectation{
				ExpectedMessageKind: &resource.MessageKind{
					Code:        "new_message_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    true,
				},
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			expectedData:  test.ArbitraryMessageKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when not authorized": {
			requestBody: []byte("{" +
				`"code":"new_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing message kind: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with parent namespace id missing": {
			requestBody: []byte("{" +
				`"code":"new_message_kind",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when message kind is in a deleted namespace branch": {
			requestBody: []byte("{" +
				`"code":"new_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when parent namespaces failed to be retrieved": {
			requestBody: []byte("{" +
				`"code":"new_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when message kind failed to be created": {
			requestBody: []byte("{" +
				`"code":"new_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindCreateExpectation: &test.MessageKindActionCreateExpectation{
				ExpectedMessageKind: &resource.MessageKind{
					Code:        "new_message_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    true,
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			requestBody: []byte("{" +
				`"code":"new_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindCreateExpectation: &test.MessageKindActionCreateExpectation{
				ExpectedMessageKind: &resource.MessageKind{
					Code:        "new_message_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    true,
				},
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			expectedData:  test.ArbitraryMessageKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified and no parent namespace ids": {
			requestBody: []byte("{" +
				`"code":"new_message_kind",` +
				`"disabled":true` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindCreateExpectation: &test.MessageKindActionCreateExpectation{
				ExpectedMessageKind: &resource.MessageKind{
					Code:        "new_message_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    true,
				},
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			expectedData:  test.ArbitraryMessageKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			requestBody: []byte("{" +
				`"code":"new_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mka := test.NewMessageKindAction(t)
			mka.ExpectCreate(tt.messageKindCreateExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewMessageKindHandler(mka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Post(ctx, make(map[string]string), bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			mka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestMessageKindHandlerPut(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		messageKindRetrieveByIDExpectation  *test.MessageKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		messageKindUpdateExpectation        *test.MessageKindActionUpdateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindUpdateExpectation: &test.MessageKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedMessageKind: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    true,
					CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
				},
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			expectedData:  test.ArbitraryMessageKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			id:          "00000000-0000-0000-0003-000000000000",
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing message kind: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with modified parent namespace id": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-ffffffffffff",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating message kind: namespace cannot be modified"),
		},
		"when message kind failed to be found": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind failed to be retrieved": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind is deleted": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when message kind failed to be updated": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindUpdateExpectation: &test.MessageKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedMessageKind: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    true,
					CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindUpdateExpectation: &test.MessageKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedMessageKind: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    true,
					CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
				},
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			expectedData:  test.ArbitraryMessageKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified and no parent namespace ids": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindUpdateExpectation: &test.MessageKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedMessageKind: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    true,
					CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
				},
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			expectedData:  test.ArbitraryMessageKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0003-000000000000",` +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T03:00:58Z",` +
				`"updateTime":"2000-01-01T03:00:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)
			mka.ExpectUpdate(tt.messageKindUpdateExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewMessageKindHandler(mka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Put(ctx, params, bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			mka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestMessageKindHandlerPatch(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		messageKindRetrieveByIDExpectation  *test.MessageKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		messageKindUpdateExpectation        *test.MessageKindActionUpdateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_message_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindUpdateExpectation: &test.MessageKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedMessageKind: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    false,
					CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
				},
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			expectedData:  test.ArbitraryMessageKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			requestBody: []byte("{" +
				`"code":"updated_message_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_message_kind"` +
				"}"),
			userPermissions: []test.Permission{},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			id:          "00000000-0000-0000-0003-000000000000",
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing message kind: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with modified parent namespace id": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_message_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-ffffffffffff"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating message kind: namespace cannot be modified"),
		},
		"when message kind failed to be found": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_message_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind failed to be retrieved": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_message_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind is deleted": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_message_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_message_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_message_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when message kind failed to be updated": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_message_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindUpdateExpectation: &test.MessageKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedMessageKind: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    false,
					CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id: "00000000-0000-0000-0003-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_message_kind"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindUpdateExpectation: &test.MessageKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ExpectedMessageKind: &resource.MessageKind{
					ID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Code:        "updated_message_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Disabled:    false,
					CreateTime:  test.MakeTime("2000-01-01T03:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T03:00:59Z", t),
				},
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			expectedData:  test.ArbitraryMessageKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id:        "00000000-0000-0000-0003-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			requestBody: []byte("{" +
				`"code":"updated_message_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "update"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)
			mka.ExpectUpdate(tt.messageKindUpdateExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewMessageKindHandler(mka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Patch(ctx, params, bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			mka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestMessageKindHandlerDelete(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		messageKindRetrieveByIDExpectation  *test.MessageKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		messageKindDeleteExpectation        *test.MessageKindActionDeleteExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "delete"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindDeleteExpectation: &test.MessageKindActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: nil,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "delete"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id:              "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when message kind failed to be found": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "delete"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind failed to be retrieved": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "delete"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind is deleted": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "delete"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: &resource.MessageKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "delete"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "delete"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when message kind failed to be deleted": {
			id: "00000000-0000-0000-0003-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "delete"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindDeleteExpectation: &test.MessageKindActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id:        "00000000-0000-0000-0003-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "delete"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			messageKindDeleteExpectation: &test.MessageKindActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: nil,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id:        "00000000-0000-0000-0003-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "delete"},
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)
			mka.ExpectDelete(tt.messageKindDeleteExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewMessageKindHandler(mka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Delete(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			mka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}
