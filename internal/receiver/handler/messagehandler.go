package handler

import (
	"context"
	"fmt"
	"io"
	"time"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/clock"
	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// MessageHandler defines an HTTP server handler for message endpoints.
type MessageHandler struct {
	ma    action.MessageActioner
	da    action.DeliveryActioner
	mka   action.MessageKindActioner
	dka   action.DeliveryKindActioner
	na    action.NamespaceActioner
	authz authorizer.Authorizer
	clock clock.Clocker
}

// New creates a new message handler.
func NewMessageHandler(
	ma action.MessageActioner,
	da action.DeliveryActioner,
	mka action.MessageKindActioner,
	dka action.DeliveryKindActioner,
	na action.NamespaceActioner,
	authz authorizer.Authorizer,
	clocker clock.Clocker,
) *MessageHandler {
	return &MessageHandler{
		ma:    ma,
		da:    da,
		mka:   mka,
		dka:   dka,
		na:    na,
		authz: authz,
		clock: clocker,
	}
}

// GetMany retrieves multiple messages. Messages under a message kind that is considered deleted
// will not be returned, although deliveries under deleted delivery kinds will.
func (h *MessageHandler) GetMany(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	messageKinds, err := h.authorizedMessageKinds(ctx)
	if err != nil {
		return nil, meta, err
	}

	q, err := h.getManyQuery(params, messageKinds)
	if err != nil {
		return nil, meta, err
	}

	messages, err := h.ma.RetrieveMany(q)
	if err != nil {
		return nil, meta, err
	}

	msgResp, err := h.convertMessagesToResponse(messages, nil, messageKinds, nil)
	if err != nil {
		return nil, meta, fmt.Errorf("error building response: %s", err)
	}

	return msgResp, meta, nil
}

// GetByID retrieves the message with the specified ID.
//
// The message is returned even if it is considered deleted by its message kind.
//
// TODO: Add warning in metadata if the message is considered deleted.
func (h *MessageHandler) GetByID(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	message, err := h.ma.RetrieveByID(id)
	if err == action.ErrInvalidResourceID {
		return nil, meta, handler.ErrInvalidResourceID
	} else if err != nil {
		return nil, meta, err
	}

	messageKind, err := h.mka.RetrieveByID(message.MessageKindID)
	if err != nil {
		return nil, meta, err
	}

	// Validates the message's scope with the one defined in the path, if any.
	if pathScope := PathScope(ctx); pathScope != nil && pathScope.ID != messageKind.NamespaceID {
		return nil, meta, handler.ErrInvalidResourceID
	}

	if err := h.authz.AuthorizeOperation(ctx, messageKind.NamespaceID, resource.TypeMessage, "read"); err != nil {
		return nil, meta, err
	}

	msgResp, err := h.convertMessageToResponse(
		message,
		nil,
		[]*resource.MessageKind{messageKind},
		nil,
	)
	if err != nil {
		return nil, meta, fmt.Errorf("error building response: %s", err)
	}

	return msgResp, meta, nil
}

// Post handles the creation of a new message and a delivery, and pushes this delivery into the
// message queue to be delivered by the Deliverer service.
//nolint:funlen // This method is straightforward and should be easy to read.
func (h *MessageHandler) Post(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	msgReq := new(MessageRequest)
	if err := handler.ParseRequestBody(body, msgReq); err != nil {
		return nil, meta, fmt.Errorf("error parsing message: %w", handler.ErrInvalidRequestBody)
	}

	messageKind, err := h.retrieveMessageKind(ctx, msgReq.MessageKind)
	if err != nil {
		return nil, meta, err
	}

	deliveryKind, err := h.retrieveDeliveryKind(msgReq.DeliveryKind)
	if err != nil {
		return nil, meta, err
	}

	// Permission to create new messages is determined by the message kind's namespace, not the
	// delivery kind's.
	if err := h.authz.AuthorizeOperation(ctx, messageKind.NamespaceID, resource.TypeMessage, "create"); err != nil {
		return nil, meta, err
	}

	message := &resource.Message{
		MessageKindID: messageKind.ID,
		Input:         msgReq.Input,
		Status:        "active",
	}

	if msgReq.ScheduleTime != nil {
		message.Status = "scheduled"
		message.ScheduleTime = msgReq.ScheduleTime
	}

	message, err = h.ma.Create(message)
	if err != nil {
		return nil, meta, err
	}

	// TODO: Past this point, investigate what to do with the message (and potentially the delivery)
	// that is in the database, during a failure.

	delivery := &resource.Delivery{
		MessageID:      message.ID,
		DeliveryKindID: deliveryKind.ID,
		LanguageTag:    msgReq.LanguageTag,
		Status:         "pending",
	}

	delivery, err = h.da.Create(delivery)
	if err != nil {
		return nil, meta, err
	}

	// Messages that are not scheduled are queued immediately.
	if message.ScheduleTime == nil || message.ScheduleTime.Before(h.clock.Now()) {
		if err := h.da.QueueRequest(delivery.ID); err != nil {
			return nil, meta, fmt.Errorf("error queuing delivery request: %s", err)
		}
	}

	msgResp, err := h.convertMessageToResponse(
		message,
		[]*resource.Delivery{delivery},
		[]*resource.MessageKind{messageKind},
		[]*resource.DeliveryKind{deliveryKind},
	)
	if err != nil {
		return nil, meta, fmt.Errorf("error building response: %s", err)
	}

	return msgResp, meta, nil
}

func (h *MessageHandler) authorizedMessageKinds(ctx context.Context) ([]*resource.MessageKind, error) {
	namespaceIDs, err := h.authz.AuthorizedNamespaceIDs(ctx, resource.TypeMessage, "read")
	if err != nil {
		return nil, err
	}

	if pathScope := PathScope(ctx); pathScope != nil {
		namespaceIDs, err = forcePathScope(namespaceIDs, pathScope.ID)
		if err != nil {
			return nil, err
		}
	}

	q := query.NewQuery(
		query.WithFilterArgs("NamespaceID", query.OpIn, namespaceIDs),
		query.WithFilterArgs("Deleted", query.OpEqual, false),
	)
	messageKinds, err := h.mka.RetrieveMany(q)
	if err != nil {
		return nil, err
	}

	// TODO
	// if len(messageKinds) == 0 {
	// 	return nil, authorizer.ErrUnauthorized
	// }

	return messageKinds, nil
}

func (h *MessageHandler) getManyQuery(
	params map[string]string,
	messageKinds []*resource.MessageKind,
) (*query.Query, error) {
	q, err := handler.ParseOData(params, resource.TypeMessage)
	if err != nil {
		return nil, fmt.Errorf("error parsing query: %s", err)
	}

	messageKindIDs := make([]uuid.UUID, len(messageKinds))
	for i, mk := range messageKinds {
		messageKindIDs[i] = mk.ID
	}

	q.Filters = append(q.Filters, query.NewFilter("MessageKindID", query.OpIn, messageKindIDs))

	return q, nil
}

func (h *MessageHandler) retrieveMessageKind(ctx context.Context, idOrCode string) (*resource.MessageKind, error) {
	messageKind, err := h.mka.RetrieveByIDOrCode(idOrCode)
	switch {
	case err == action.ErrInvalidResourceIDOrCode:
		return nil, handler.ErrInvalidResource
	case err != nil:
		return nil, err
	case messageKind.Disabled:
		return nil, handler.ErrDisabledResource
	case messageKind.Deleted:
		return nil, handler.ErrInvalidResource
	}

	if err := isNamespaceDeleted(h.na, messageKind.NamespaceID); err == handler.ErrDeletedResource {
		return nil, handler.ErrInvalidResource
	} else if err != nil {
		return nil, err
	}

	if pathScope := PathScope(ctx); pathScope != nil && pathScope.ID != messageKind.NamespaceID {
		return nil, handler.ErrInvalidResource
	}

	return messageKind, nil
}

func (h *MessageHandler) retrieveDeliveryKind(idOrCode string) (*resource.DeliveryKind, error) {
	deliveryKind, err := h.dka.RetrieveByIDOrCode(idOrCode)
	switch {
	case err == action.ErrInvalidResourceIDOrCode:
		return nil, handler.ErrInvalidResource
	case err != nil:
		return nil, err
	case deliveryKind.Disabled:
		return nil, handler.ErrDisabledResource
	case deliveryKind.Deleted:
		return nil, handler.ErrInvalidResource
	}

	if err := isNamespaceDeleted(h.na, deliveryKind.NamespaceID); err == handler.ErrDeletedResource {
		return nil, handler.ErrInvalidResource
	} else if err != nil {
		return nil, err
	}

	return deliveryKind, nil
}

//nolint:funlen // This method is straightforward and should be easy to read.
func (h *MessageHandler) convertMessagesToResponse(
	messages []*resource.Message,
	deliveries []*resource.Delivery,
	messageKinds []*resource.MessageKind,
	deliveryKinds []*resource.DeliveryKind,
) ([]*MessageResponse, error) {
	msgResponses := make([]*MessageResponse, len(messages))
	var err error

	if deliveries == nil {
		deliveries, err = h.retrieveDeliveries(messages)
		if err != nil {
			return nil, fmt.Errorf("error retrieving deliveries: %s", err)
		}
	}

	if messageKinds == nil {
		messageKinds, err = h.retrieveMessageKinds(messages)
		if err != nil {
			return nil, fmt.Errorf("error retrieving message kinds: %s", err)
		}
	}

	if deliveryKinds == nil {
		deliveryKinds, err = h.retrieveDeliveryKinds(deliveries)
		if err != nil {
			return nil, fmt.Errorf("error retrieving delivery kinds: %s", err)
		}
	}

	for i, m := range messages {
		var messageKind *resource.MessageKind
		for _, mk := range messageKinds {
			if mk.ID == m.MessageKindID {
				messageKind = mk
				break
			}
		}

		if messageKind == nil {
			return nil, fmt.Errorf("message's message kind could not be found")
		}

		msgResp := &MessageResponse{
			ID:           m.ID,
			MessageKind:  messageKind.Code,
			Input:        m.Input,
			Status:       m.Status,
			ScheduleTime: m.ScheduleTime,
			Deliveries:   make([]*DeliveryResponse, 0),
			CreateTime:   m.CreateTime,
		}

		for _, d := range deliveries {
			if d.MessageID != m.ID {
				continue
			}

			var deliveryKind *resource.DeliveryKind
			for _, dk := range deliveryKinds {
				if dk.ID == d.DeliveryKindID {
					deliveryKind = dk
					break
				}
			}

			if deliveryKind == nil {
				return nil, fmt.Errorf("delivery's delivery kind could not be found")
			}

			delResp := &DeliveryResponse{
				DeliveryKind:  deliveryKind.Code,
				LanguageTag:   d.LanguageTag,
				Status:        d.Status,
				StatusMessage: d.StatusMessage,
				DeliverTime:   d.DeliverTime,
			}

			msgResp.Deliveries = append(msgResp.Deliveries, delResp)
		}

		msgResponses[i] = msgResp
	}

	return msgResponses, nil
}

func (h *MessageHandler) convertMessageToResponse(
	message *resource.Message,
	deliveries []*resource.Delivery,
	messageKinds []*resource.MessageKind,
	deliveryKinds []*resource.DeliveryKind,
) (*MessageResponse, error) {
	resp, err := h.convertMessagesToResponse([]*resource.Message{message}, deliveries, messageKinds, deliveryKinds)
	if err != nil {
		return nil, err
	}

	if len(resp) != 1 {
		return nil, fmt.Errorf("error converting message to response: expected only one message")
	}

	return resp[0], nil
}

func (h *MessageHandler) retrieveDeliveries(messages []*resource.Message) ([]*resource.Delivery, error) {
	messageIDs := make([]uuid.UUID, len(messages))
	for i, m := range messages {
		messageIDs[i] = m.ID
	}

	q := query.NewQuery(query.WithFilterArgs("MessageID", query.OpIn, messageIDs))
	deliveries, err := h.da.RetrieveMany(q)
	if err != nil {
		return nil, err
	}

	return deliveries, nil
}

func (h *MessageHandler) retrieveMessageKinds(messages []*resource.Message) ([]*resource.MessageKind, error) {
	messageKindIDs := make([]uuid.UUID, 0, len(messages))
	for _, m := range messages {
		found := false
		for _, id := range messageKindIDs {
			if m.MessageKindID == id {
				found = true
				break
			}
		}
		if !found {
			messageKindIDs = append(messageKindIDs, m.MessageKindID)
		}
	}

	q := query.NewQuery(query.WithFilterArgs("ID", query.OpIn, messageKindIDs))
	messageKinds, err := h.mka.RetrieveMany(q)
	if err != nil {
		return nil, err
	}

	return messageKinds, nil
}

func (h *MessageHandler) retrieveDeliveryKinds(deliveries []*resource.Delivery) ([]*resource.DeliveryKind, error) {
	deliveryKindIDs := make([]uuid.UUID, 0, len(deliveries))
	for _, d := range deliveries {
		found := false
		for _, id := range deliveryKindIDs {
			if d.DeliveryKindID == id {
				found = true
				break
			}
		}
		if !found {
			deliveryKindIDs = append(deliveryKindIDs, d.DeliveryKindID)
		}
	}

	q := query.NewQuery(query.WithFilterArgs("ID", query.OpIn, deliveryKindIDs))
	deliveryKinds, err := h.dka.RetrieveMany(q)
	if err != nil {
		return nil, err
	}

	return deliveryKinds, nil
}

// MessageRequest defines the body of a request related to a message resource.
type MessageRequest struct {
	MessageKind  string                 `json:"messageKind"`
	DeliveryKind string                 `json:"deliveryKind"`
	LanguageTag  string                 `json:"languageTag"`
	Input        map[string]interface{} `json:"input"`
	ScheduleTime *time.Time             `json:"scheduleTime"`
}

// MessageResponse defines the body of a response related to a message resource.
type MessageResponse struct {
	ID           uuid.UUID              `json:"id"`
	MessageKind  string                 `json:"messageKind"`
	Input        map[string]interface{} `json:"input"`
	Status       string                 `json:"status"`
	ScheduleTime *time.Time             `json:"scheduleTime,omitempty"`
	CreateTime   time.Time              `json:"createTime"`
	Deliveries   []*DeliveryResponse    `json:"deliveries"`
}

// DeliveryResponse defines the body of a response related to a delivery resource.
type DeliveryResponse struct {
	DeliveryKind  string     `json:"deliveryKind"`
	LanguageTag   string     `json:"languageTag"`
	Status        string     `json:"status"`
	StatusMessage string     `json:"statusMessage,omitempty"`
	DeliverTime   *time.Time `json:"deliverTime"`
}
