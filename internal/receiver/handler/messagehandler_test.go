//go:build !integration && !endtoend
// +build !integration,!endtoend

package handler_test

import (
	"bytes"
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	serverhandler "gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/receiver/handler"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestMessageHandlerGetMany(t *testing.T) {
	tests := map[string]struct {
		queryFilters                        string
		queryOrderBy                        string
		queryPageTop                        string
		queryPageSkip                       string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		messageKindRetrieveManyExpectation  *test.MessageKindActionRetrieveManyExpectation
		messageRetrieveManyExpectation      *test.MessageActionRetrieveManyExpectation
		deliveryRetrieveManyExpectation     *test.DeliveryActionRetrieveManyExpectation
		deliveryKindRetrieveManyExpectation *test.DeliveryKindActionRetrieveManyExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			messageRetrieveManyExpectation: &test.MessageActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessages: []*resource.Message{
					test.ArbitraryMessage(t),
				},
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0006-000000000000")}),
				),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			expectedData: []*handler.MessageResponse{
				{
					ID:          uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKind: "message_kind_000000000000",
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status:       "succeeded",
					ScheduleTime: test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
					Deliveries: []*handler.DeliveryResponse{
						{
							DeliveryKind:  "delivery_kind_000000000000",
							LanguageTag:   "language_tag_000000000000",
							Status:        "delivered",
							StatusMessage: "status_message_000000000000",
							DeliverTime:   test.MakeTimeRef("2000-01-01T07:00:00Z", t),
						},
					},
				},
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when user has permission on multiple namespaces": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"), ResourceType: resource.TypeMessage, Operation: "read"},

				// This ID should not appear in the filters because it is for another resource type.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000003"), ResourceType: resource.TypeMessageKind, Operation: "read"},

				// This ID should not appear in the filters because it is for the create operation.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000004"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001"), uuid.MustParse("00000000-0000-0000-0001-000000000002")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
					{ID: uuid.MustParse("00000000-0000-0000-0003-000000000001"), Code: "message_kind_000000000001"},
				},
			},
			messageRetrieveManyExpectation: &test.MessageActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000"), uuid.MustParse("00000000-0000-0000-0003-000000000001")}),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessages: []*resource.Message{
					test.ArbitraryMessage(t),
					{
						ID:            uuid.MustParse("00000000-0000-0000-0006-000000000001"),
						MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000001"),
						Input:         map[string]interface{}{},
						Status:        "failed",
						CreateTime:    test.MakeTime("2000-01-01T06:01:58Z", t),
						UpdateTime:    test.MakeTime("2000-01-01T06:01:59Z", t),
					},
				},
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0006-000000000000"), uuid.MustParse("00000000-0000-0000-0006-000000000001")}),
				),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
					{
						ID:             uuid.MustParse("00000000-0000-0000-0007-000000000001"),
						MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000001"),
						DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000001"),
						LanguageTag:    "language_tag_000000000001",
						Status:         "undelivered",
						StatusMessage:  "status_message_000000000001",
						DeliverTime:    nil,
						ExternalID:     "",
						CreateTime:     test.MakeTime("2000-01-01T07:01:58Z", t),
						UpdateTime:     test.MakeTime("2000-01-01T07:01:59Z", t),
					},
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000"), uuid.MustParse("00000000-0000-0000-0002-000000000001")}),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
					{ID: uuid.MustParse("00000000-0000-0000-0002-000000000001"), Code: "delivery_kind_000000000001"},
				},
			},
			expectedData: []*handler.MessageResponse{
				{
					ID:          uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKind: "message_kind_000000000000",
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status:       "succeeded",
					ScheduleTime: test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
					Deliveries: []*handler.DeliveryResponse{
						{
							DeliveryKind:  "delivery_kind_000000000000",
							LanguageTag:   "language_tag_000000000000",
							Status:        "delivered",
							StatusMessage: "status_message_000000000000",
							DeliverTime:   test.MakeTimeRef("2000-01-01T07:00:00Z", t),
						},
					},
				},
				{
					ID:          uuid.MustParse("00000000-0000-0000-0006-000000000001"),
					MessageKind: "message_kind_000000000001",
					Input:       map[string]interface{}{},
					Status:      "failed",
					CreateTime:  test.MakeTime("2000-01-01T06:01:58Z", t),
					Deliveries: []*handler.DeliveryResponse{
						{
							DeliveryKind:  "delivery_kind_000000000001",
							LanguageTag:   "language_tag_000000000001",
							Status:        "undelivered",
							StatusMessage: "status_message_000000000001",
						},
					},
				},
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query specified": {
			queryFilters:  "status eq successful",
			queryOrderBy:  "messageKindId desc",
			queryPageTop:  "5",
			queryPageSkip: "8",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			messageRetrieveManyExpectation: &test.MessageActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Status", query.OpEqual, "successful"),
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithOrderArgs("MessageKindID", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ReturnedMessages: []*resource.Message{
					test.ArbitraryMessage(t),
				},
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0006-000000000000")}),
				),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			expectedData: []*handler.MessageResponse{
				{
					ID:          uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKind: "message_kind_000000000000",
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status:       "succeeded",
					ScheduleTime: test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
					Deliveries: []*handler.DeliveryResponse{
						{
							DeliveryKind:  "delivery_kind_000000000000",
							LanguageTag:   "language_tag_000000000000",
							Status:        "delivered",
							StatusMessage: "status_message_000000000000",
							DeliverTime:   test.MakeTimeRef("2000-01-01T07:00:00Z", t),
						},
					},
				},
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query invalid": {
			queryFilters: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing query: error parsing $filter: invalid token count"),
		},
		"when not authorized": {
			userPermissions: []test.Permission{},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{},
			},
			messageRetrieveManyExpectation: &test.MessageActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{}),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessages: []*resource.Message{},
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{}),
				),
				ReturnedDeliveries: []*resource.Delivery{},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{}),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{},
			},
			expectedData:  []*handler.MessageResponse{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when message kinds failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when no messages were found in the database": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			messageRetrieveManyExpectation: &test.MessageActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessages: []*resource.Message{},
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{}),
				),
				ReturnedDeliveries: []*resource.Delivery{},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{}),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{},
			},
			expectedData:  []*handler.MessageResponse{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when messages failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			messageRetrieveManyExpectation: &test.MessageActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when deliveries failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			messageRetrieveManyExpectation: &test.MessageActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessages: []*resource.Message{
					test.ArbitraryMessage(t),
				},
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0006-000000000000")}),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error building response: error retrieving deliveries: arbitrary"),
		},
		"when delivery kinds failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			messageRetrieveManyExpectation: &test.MessageActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessages: []*resource.Message{
					test.ArbitraryMessage(t),
				},
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0006-000000000000")}),
				),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error building response: error retrieving delivery kinds: arbitrary"),
		},
		"when path scope is specified": {
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000000"), ResourceType: resource.TypeMessage, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageKindRetrieveManyExpectation: &test.MessageKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
				),
				ReturnedMessageKinds: []*resource.MessageKind{
					test.ArbitraryMessageKind(t),
				},
			},
			messageRetrieveManyExpectation: &test.MessageActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageKindID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0003-000000000000")}),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedMessages: []*resource.Message{
					test.ArbitraryMessage(t),
				},
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0006-000000000000")}),
				),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			expectedData: []*handler.MessageResponse{
				{
					ID:          uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKind: "message_kind_000000000000",
					Input: map[string]interface{}{
						"key": "value_000000000000",
					},
					Status:       "succeeded",
					ScheduleTime: test.MakeTimeRef("2000-01-01T06:00:00Z", t),
					CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
					Deliveries: []*handler.DeliveryResponse{
						{
							DeliveryKind:  "delivery_kind_000000000000",
							LanguageTag:   "language_tag_000000000000",
							Status:        "delivered",
							StatusMessage: "status_message_000000000000",
							DeliverTime:   test.MakeTimeRef("2000-01-01T07:00:00Z", t),
						},
					},
				},
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ma := test.NewMessageAction(t)
			ma.ExpectRetrieveMany(tt.messageRetrieveManyExpectation)

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveMany(tt.messageKindRetrieveManyExpectation)

			da := test.NewDeliveryAction(t)
			da.ExpectRetrieveMany(tt.deliveryRetrieveManyExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveMany(tt.deliveryKindRetrieveManyExpectation)

			params := map[string]string{
				"$filter":  tt.queryFilters,
				"$orderby": tt.queryOrderBy,
				"$top":     tt.queryPageTop,
				"$skip":    tt.queryPageSkip,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewMessageHandler(ma, da, mka, dka, nil, test.NewAuthorizer(tt.userPermissions), nil)
			data, meta, err := h.GetMany(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			ma.AssertExpectations(t)
			mka.AssertExpectations(t)
			da.AssertExpectations(t)
			dka.AssertExpectations(t)
		})
	}
}

func TestMessageHandlerGetByID(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		messageRetrieveByIDExpectation      *test.MessageActionRetrieveByIDExpectation
		messageKindRetrieveByIDExpectation  *test.MessageKindActionRetrieveByIDExpectation
		deliveryRetrieveManyExpectation     *test.DeliveryActionRetrieveManyExpectation
		deliveryKindRetrieveManyExpectation *test.DeliveryKindActionRetrieveManyExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0006-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: test.ArbitraryMessage(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0006-000000000000")}),
				),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			expectedData: &handler.MessageResponse{
				ID:          uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKind: "message_kind_000000000000",
				Input: map[string]interface{}{
					"key": "value_000000000000",
				},
				Status:       "succeeded",
				ScheduleTime: test.MakeTimeRef("2000-01-01T06:00:00Z", t),
				CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
				Deliveries: []*handler.DeliveryResponse{
					{
						DeliveryKind:  "delivery_kind_000000000000",
						LanguageTag:   "language_tag_000000000000",
						Status:        "delivered",
						StatusMessage: "status_message_000000000000",
						DeliverTime:   test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					},
				},
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id:              "00000000-0000-0000-0006-000000000000",
			userPermissions: []test.Permission{},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: test.ArbitraryMessage(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when message failed to be found": {
			id: "00000000-0000-0000-0006-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when message failed to be retrieved": {
			id: "00000000-0000-0000-0006-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind failed to be retrieved": {
			id: "00000000-0000-0000-0006-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: test.ArbitraryMessage(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when deliveries failed to be retrieved": {
			id: "00000000-0000-0000-0006-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: test.ArbitraryMessage(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0006-000000000000")}),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error building response: error retrieving deliveries: arbitrary"),
		},
		"when delivery kinds failed to be retrieved": {
			id: "00000000-0000-0000-0006-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: test.ArbitraryMessage(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0006-000000000000")}),
				),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error building response: error retrieving delivery kinds: arbitrary"),
		},
		"when path scope is the parent namespace": {
			id:        "00000000-0000-0000-0006-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: test.ArbitraryMessage(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryRetrieveManyExpectation: &test.DeliveryActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("MessageID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0006-000000000000")}),
				),
				ReturnedDeliveries: []*resource.Delivery{
					test.ArbitraryDelivery(t),
				},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0002-000000000000")}),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			expectedData: &handler.MessageResponse{
				ID:          uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKind: "message_kind_000000000000",
				Input: map[string]interface{}{
					"key": "value_000000000000",
				},
				Status:       "succeeded",
				ScheduleTime: test.MakeTimeRef("2000-01-01T06:00:00Z", t),
				CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
				Deliveries: []*handler.DeliveryResponse{
					{
						DeliveryKind:  "delivery_kind_000000000000",
						LanguageTag:   "language_tag_000000000000",
						Status:        "delivered",
						StatusMessage: "status_message_000000000000",
						DeliverTime:   test.MakeTimeRef("2000-01-01T07:00:00Z", t),
					},
				},
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope is not the parent namespace": {
			id:        "00000000-0000-0000-0006-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "read"},
			},
			messageRetrieveByIDExpectation: &test.MessageActionRetrieveByIDExpectation{
				ExpectedID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				ReturnedMessage: test.ArbitraryMessage(t),
			},
			messageKindRetrieveByIDExpectation: &test.MessageKindActionRetrieveByIDExpectation{
				ExpectedID:          uuid.MustParse("00000000-0000-0000-0003-000000000000"),
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ma := test.NewMessageAction(t)
			ma.ExpectRetrieveByID(tt.messageRetrieveByIDExpectation)

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByID(tt.messageKindRetrieveByIDExpectation)

			da := test.NewDeliveryAction(t)
			da.ExpectRetrieveMany(tt.deliveryRetrieveManyExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveMany(tt.deliveryKindRetrieveManyExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewMessageHandler(ma, da, mka, dka, nil, test.NewAuthorizer(tt.userPermissions), nil)
			data, meta, err := h.GetByID(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			ma.AssertExpectations(t)
			mka.AssertExpectations(t)
			da.AssertExpectations(t)
			dka.AssertExpectations(t)
		})
	}
}

func TestMessageHandlerPost(t *testing.T) {
	tests := map[string]struct {
		requestBody                               []byte
		pathScope                                 *resource.Namespace
		userPermissions                           []test.Permission
		messageKindRetrieveByIDOrCodeExpectation  *test.MessageKindActionRetrieveByIDOrCodeExpectation
		deliveryKindRetrieveByIDOrCodeExpectation *test.DeliveryKindActionRetrieveByIDOrCodeExpectation
		namespaceRetrieveParentsExpectations      []*test.NamespaceActionRetrieveParentsExpectation
		messageCreateExpectation                  *test.MessageActionCreateExpectation
		deliveryCreateExpectation                 *test.DeliveryActionCreateExpectation
		deliveryQueueRequestExpectation           *test.DeliveryActionQueueRequestExpectation
		expectedData                              interface{}
		expectedMeta                              map[string]interface{}
		expectedError                             error
	}{
		"when successful": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:     "delivery_kind_000000000000",
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			messageCreateExpectation: &test.MessageActionCreateExpectation{
				ExpectedMessage: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "active",
					ScheduleTime: nil,
					CreateTime:   time.Time{},
					UpdateTime:   time.Time{},
				},
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "active",
					ScheduleTime: nil,
					CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:   test.MakeTime("2000-01-01T06:00:59Z", t),
				},
			},
			deliveryCreateExpectation: &test.DeliveryActionCreateExpectation{
				ExpectedDelivery: &resource.Delivery{
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
				},
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
					StatusMessage:  "",
					DeliverTime:    nil,
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
				},
			},
			deliveryQueueRequestExpectation: &test.DeliveryActionQueueRequestExpectation{
				ExpectedDeliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedError:      nil,
			},
			expectedData: &handler.MessageResponse{
				ID:          uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKind: "message_kind_000000000000",
				Input: map[string]interface{}{
					"key": "new_value",
				},
				Status:     "active",
				CreateTime: test.MakeTime("2000-01-01T06:00:58Z", t),
				Deliveries: []*handler.DeliveryResponse{
					{
						DeliveryKind: "delivery_kind_000000000000",
						LanguageTag:  "new_language_tag",
						Status:       "pending",
					},
				},
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when schedule time is specified in the past": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"},` +
				`"scheduleTime":"1900-01-01T00:00:00Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:     "delivery_kind_000000000000",
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			messageCreateExpectation: &test.MessageActionCreateExpectation{
				ExpectedMessage: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "scheduled",
					ScheduleTime: test.MakeTimeRef("1900-01-01T00:00:00Z", t),
					CreateTime:   time.Time{},
					UpdateTime:   time.Time{},
				},
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "scheduled",
					ScheduleTime: test.MakeTimeRef("1900-01-01T00:00:00Z", t),
					CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:   test.MakeTime("2000-01-01T06:00:59Z", t),
				},
			},
			deliveryCreateExpectation: &test.DeliveryActionCreateExpectation{
				ExpectedDelivery: &resource.Delivery{
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
				},
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
					StatusMessage:  "",
					DeliverTime:    nil,
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
				},
			},
			deliveryQueueRequestExpectation: &test.DeliveryActionQueueRequestExpectation{
				ExpectedDeliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedError:      nil,
			},
			expectedData: &handler.MessageResponse{
				ID:          uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKind: "message_kind_000000000000",
				Input: map[string]interface{}{
					"key": "new_value",
				},
				Status:       "scheduled",
				ScheduleTime: test.MakeTimeRef("1900-01-01T00:00:00Z", t),
				CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
				Deliveries: []*handler.DeliveryResponse{
					{
						DeliveryKind: "delivery_kind_000000000000",
						LanguageTag:  "new_language_tag",
						Status:       "pending",
					},
				},
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when schedule time is specified in the future": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"},` +
				`"scheduleTime":"2100-01-01T00:00:00Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:     "delivery_kind_000000000000",
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			messageCreateExpectation: &test.MessageActionCreateExpectation{
				ExpectedMessage: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "scheduled",
					ScheduleTime: test.MakeTimeRef("2100-01-01T00:00:00Z", t),
					CreateTime:   time.Time{},
					UpdateTime:   time.Time{},
				},
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "scheduled",
					ScheduleTime: test.MakeTimeRef("2100-01-01T00:00:00Z", t),
					CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:   test.MakeTime("2000-01-01T06:00:59Z", t),
				},
			},
			deliveryCreateExpectation: &test.DeliveryActionCreateExpectation{
				ExpectedDelivery: &resource.Delivery{
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
				},
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
					StatusMessage:  "",
					DeliverTime:    nil,
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
				},
			},
			expectedData: &handler.MessageResponse{
				ID:          uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKind: "message_kind_000000000000",
				Input: map[string]interface{}{
					"key": "new_value",
				},
				Status:       "scheduled",
				ScheduleTime: test.MakeTimeRef("2100-01-01T00:00:00Z", t),
				CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
				Deliveries: []*handler.DeliveryResponse{
					{
						DeliveryKind: "delivery_kind_000000000000",
						LanguageTag:  "new_language_tag",
						Status:       "pending",
					},
				},
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when message kind and delivery kind are in different namespaces": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode: "delivery_kind_000000000000",
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "delivery_kind_000000000000",
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					Host:        "host_000000000000",
					Disabled:    false,
					Deleted:     false,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000002")},
					},
				},
			},
			messageCreateExpectation: &test.MessageActionCreateExpectation{
				ExpectedMessage: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "active",
					ScheduleTime: nil,
					CreateTime:   time.Time{},
					UpdateTime:   time.Time{},
				},
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "active",
					ScheduleTime: nil,
					CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:   test.MakeTime("2000-01-01T06:00:59Z", t),
				},
			},
			deliveryCreateExpectation: &test.DeliveryActionCreateExpectation{
				ExpectedDelivery: &resource.Delivery{
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
				},
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
					StatusMessage:  "",
					DeliverTime:    nil,
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
				},
			},
			deliveryQueueRequestExpectation: &test.DeliveryActionQueueRequestExpectation{
				ExpectedDeliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedError:      nil,
			},
			expectedData: &handler.MessageResponse{
				ID:          uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKind: "message_kind_000000000000",
				Input: map[string]interface{}{
					"key": "new_value",
				},
				Status:     "active",
				CreateTime: test.MakeTime("2000-01-01T06:00:58Z", t),
				Deliveries: []*handler.DeliveryResponse{
					{
						DeliveryKind: "delivery_kind_000000000000",
						LanguageTag:  "new_language_tag",
						Status:       "pending",
					},
				},
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when not authorized": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:     "delivery_kind_000000000000",
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing message: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when message kind failed to be found": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode: "message_kind_000000000000",
				ReturnedError:    action.ErrInvalidResourceIDOrCode,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when message kind failed to be retrieved": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode: "message_kind_000000000000",
				ReturnedError:    test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when message kind is disabled": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: &resource.MessageKind{Disabled: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrDisabledResource,
		},
		"when message kind is deleted": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: &resource.MessageKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when message kind parent namespaces failed to be retrieved": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when message kind is in a deleted namespace branch": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when delivery kind failed to be found": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode: "delivery_kind_000000000000",
				ReturnedError:    action.ErrInvalidResourceIDOrCode,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when delivery kind failed to be retrieved": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode: "delivery_kind_000000000000",
				ReturnedError:    test.ErrArbitrary,
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is disabled": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:     "delivery_kind_000000000000",
				ReturnedDeliveryKind: &resource.DeliveryKind{Disabled: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrDisabledResource,
		},
		"when delivery kind is deleted": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:     "delivery_kind_000000000000",
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when delivery kind parent namespaces failed to be retrieved": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:     "delivery_kind_000000000000",
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedError: test.ErrArbitrary,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when delivery kind is in a deleted namespace branch": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode: "delivery_kind_000000000000",
				ReturnedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"),
				},
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000002"),
					ReturnedError: action.ErrNamespaceDeleted,
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when message failed to be created": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:     "delivery_kind_000000000000",
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			messageCreateExpectation: &test.MessageActionCreateExpectation{
				ExpectedMessage: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "active",
					ScheduleTime: nil,
					CreateTime:   time.Time{},
					UpdateTime:   time.Time{},
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery failed to be created": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:     "delivery_kind_000000000000",
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			messageCreateExpectation: &test.MessageActionCreateExpectation{
				ExpectedMessage: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "active",
					ScheduleTime: nil,
					CreateTime:   time.Time{},
					UpdateTime:   time.Time{},
				},
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "active",
					ScheduleTime: nil,
					CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:   test.MakeTime("2000-01-01T06:00:59Z", t),
				},
			},
			deliveryCreateExpectation: &test.DeliveryActionCreateExpectation{
				ExpectedDelivery: &resource.Delivery{
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery request failed to be published": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:     "delivery_kind_000000000000",
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			messageCreateExpectation: &test.MessageActionCreateExpectation{
				ExpectedMessage: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "active",
					ScheduleTime: nil,
					CreateTime:   time.Time{},
					UpdateTime:   time.Time{},
				},
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "active",
					ScheduleTime: nil,
					CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:   test.MakeTime("2000-01-01T06:00:59Z", t),
				},
			},
			deliveryCreateExpectation: &test.DeliveryActionCreateExpectation{
				ExpectedDelivery: &resource.Delivery{
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
				},
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
					StatusMessage:  "",
					DeliverTime:    nil,
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
				},
			},
			deliveryQueueRequestExpectation: &test.DeliveryActionQueueRequestExpectation{
				ExpectedDeliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedError:      test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error queuing delivery request: arbitrary"),
		},
		"when path scope specified and parent namespace id matches": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			deliveryKindRetrieveByIDOrCodeExpectation: &test.DeliveryKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:     "delivery_kind_000000000000",
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			messageCreateExpectation: &test.MessageActionCreateExpectation{
				ExpectedMessage: &resource.Message{
					ID:            uuid.Nil,
					MessageKindID: test.MakeUUID("00000000-0000-0000-0003-000000000000", t),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "active",
					ScheduleTime: nil,
					CreateTime:   time.Time{},
					UpdateTime:   time.Time{},
				},
				ReturnedMessage: &resource.Message{
					ID:            uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					MessageKindID: uuid.MustParse("00000000-0000-0000-0003-000000000000"),
					Input: map[string]interface{}{
						"key": "new_value",
					},
					Status:       "active",
					ScheduleTime: nil,
					CreateTime:   test.MakeTime("2000-01-01T06:00:58Z", t),
					UpdateTime:   test.MakeTime("2000-01-01T06:00:59Z", t),
				},
			},
			deliveryCreateExpectation: &test.DeliveryActionCreateExpectation{
				ExpectedDelivery: &resource.Delivery{
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
				},
				ReturnedDelivery: &resource.Delivery{
					ID:             uuid.MustParse("00000000-0000-0000-0007-000000000000"),
					MessageID:      uuid.MustParse("00000000-0000-0000-0006-000000000000"),
					DeliveryKindID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					LanguageTag:    "new_language_tag",
					Status:         "pending",
					StatusMessage:  "",
					DeliverTime:    nil,
					ExternalID:     "",
					CreateTime:     test.MakeTime("2000-01-01T07:00:58Z", t),
					UpdateTime:     test.MakeTime("2000-01-01T07:00:59Z", t),
				},
			},
			deliveryQueueRequestExpectation: &test.DeliveryActionQueueRequestExpectation{
				ExpectedDeliveryID: uuid.MustParse("00000000-0000-0000-0007-000000000000"),
				ReturnedError:      nil,
			},
			expectedData: &handler.MessageResponse{
				ID:          uuid.MustParse("00000000-0000-0000-0006-000000000000"),
				MessageKind: "message_kind_000000000000",
				Input: map[string]interface{}{
					"key": "new_value",
				},
				Status:     "active",
				CreateTime: test.MakeTime("2000-01-01T06:00:58Z", t),
				Deliveries: []*handler.DeliveryResponse{
					{
						DeliveryKind: "delivery_kind_000000000000",
						LanguageTag:  "new_language_tag",
						Status:       "pending",
					},
				},
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			requestBody: []byte("{" +
				`"messageKind":"message_kind_000000000000",` +
				`"deliveryKind":"delivery_kind_000000000000",` +
				`"languageTag":"new_language_tag",` +
				`"input":{"key":"new_value"}` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessage, Operation: "create"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeMessageKind, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			messageKindRetrieveByIDOrCodeExpectation: &test.MessageKindActionRetrieveByIDOrCodeExpectation{
				ExpectedIDOrCode:    "message_kind_000000000000",
				ReturnedMessageKind: test.ArbitraryMessageKind(t),
			},
			namespaceRetrieveParentsExpectations: []*test.NamespaceActionRetrieveParentsExpectation{
				{
					ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					ReturnedNamespaces: []*resource.Namespace{
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
						{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
					},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			ma := test.NewMessageAction(t)
			ma.ExpectCreate(tt.messageCreateExpectation)

			mka := test.NewMessageKindAction(t)
			mka.ExpectRetrieveByIDOrCode(tt.messageKindRetrieveByIDOrCodeExpectation)

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByIDOrCode(tt.deliveryKindRetrieveByIDOrCodeExpectation)

			da := test.NewDeliveryAction(t)
			da.ExpectCreate(tt.deliveryCreateExpectation)
			da.ExpectQueueRequest(tt.deliveryQueueRequestExpectation)

			na := test.NewNamespaceAction(t)
			for _, e := range tt.namespaceRetrieveParentsExpectations {
				na.ExpectRetrieveParents(e)
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewMessageHandler(ma, da, mka, dka, na, test.NewAuthorizer(tt.userPermissions), test.NewClock("2000-01-01T00:00:00Z"))
			data, meta, err := h.Post(ctx, make(map[string]string), bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			ma.AssertExpectations(t)
			mka.AssertExpectations(t)
			da.AssertExpectations(t)
			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}
