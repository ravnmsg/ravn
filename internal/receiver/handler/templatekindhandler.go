package handler

import (
	"context"
	"fmt"
	"io"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// TemplateKindHandler defines an HTTP server handler for template kind endpoints.
type TemplateKindHandler struct {
	tka   action.TemplateKindActioner
	dka   action.DeliveryKindActioner
	na    action.NamespaceActioner
	authz authorizer.Authorizer
}

// NewTemplateKindHandler creates a new template kind handler.
func NewTemplateKindHandler(
	tka action.TemplateKindActioner,
	dka action.DeliveryKindActioner,
	na action.NamespaceActioner,
	authz authorizer.Authorizer,
) *TemplateKindHandler {
	return &TemplateKindHandler{
		tka:   tka,
		dka:   dka,
		na:    na,
		authz: authz,
	}
}

func (h *TemplateKindHandler) GetMany(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	q, err := h.getManyQuery(ctx, params)
	if err != nil {
		return nil, meta, err
	}

	templateKinds, err := h.tka.RetrieveMany(q)
	if err != nil {
		return nil, meta, err
	}

	return templateKinds, meta, nil
}

func (h *TemplateKindHandler) GetByID(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	templateKind, namespaceID, err := h.retrieveTemplateKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespaceID, resource.TypeTemplateKind, "read"); err != nil {
		return nil, meta, err
	}

	return templateKind, meta, nil
}

// Post handles the creation of a new template kind.
func (h *TemplateKindHandler) Post(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	templateKind := new(resource.TemplateKind)
	if err := handler.ParseRequestBody(body, templateKind); err != nil {
		return nil, meta, fmt.Errorf("error parsing template kind: %w", handler.ErrInvalidRequestBody)
	}

	if templateKind.DeliveryKindID == uuid.Nil {
		return nil, meta, handler.ErrInvalidResource
	}

	_, namespaceID, err := retrieveDeliveryKind(h.dka, h.na, templateKind.DeliveryKindID)
	if err == handler.ErrInvalidResourceID || err == handler.ErrDeletedResource {
		return nil, meta, handler.ErrInvalidResource
	} else if err != nil {
		return nil, meta, err
	}

	if pathScope := PathScope(ctx); pathScope != nil && pathScope.ID != namespaceID {
		return nil, meta, handler.ErrInvalidResource
	}

	if err := h.authz.AuthorizeOperation(ctx, namespaceID, resource.TypeTemplateKind, "create"); err != nil {
		return nil, meta, err
	}

	templateKind, err = h.tka.Create(templateKind)
	if err != nil {
		return nil, meta, err
	}

	return templateKind, meta, nil
}

// Put handles the update of an existing template kind, specified by its ID or code, by providing
// all of its attributes.
func (h *TemplateKindHandler) Put(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	currentTemplateKind, namespaceID, err := h.retrieveTemplateKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespaceID, resource.TypeTemplateKind, "update"); err != nil {
		return nil, meta, err
	}

	templateKind := new(resource.TemplateKind)
	if err := handler.ParseRequestBody(body, templateKind); err != nil {
		return nil, meta, fmt.Errorf("error parsing template kind: %w", handler.ErrInvalidRequestBody)
	}

	if templateKind.DeliveryKindID != currentTemplateKind.DeliveryKindID {
		return nil, meta, fmt.Errorf("error validating template kind: delivery kind cannot be modified")
	}

	templateKind, err = h.tka.Update(currentTemplateKind.ID, templateKind)
	if err != nil {
		return nil, meta, err
	}

	return templateKind, meta, nil
}

// Patch handles the modification of an existing template kind, specified by its ID, by providing
// only its changed attributes.
func (h *TemplateKindHandler) Patch(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	templateKind, namespaceID, err := h.retrieveTemplateKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespaceID, resource.TypeTemplateKind, "update"); err != nil {
		return nil, meta, err
	}

	currentDeliveryKindID := templateKind.DeliveryKindID

	if err := handler.ParseRequestBody(body, templateKind); err != nil {
		return nil, meta, fmt.Errorf("error parsing template kind: %w", handler.ErrInvalidRequestBody)
	}

	if templateKind.DeliveryKindID != currentDeliveryKindID {
		return nil, meta, fmt.Errorf("error validating template kind: delivery kind cannot be modified")
	}

	templateKind, err = h.tka.Update(templateKind.ID, templateKind)
	if err != nil {
		return nil, meta, err
	}

	return templateKind, meta, nil
}

// Delete handles the deletion of a template kind, specified by its ID or code.
func (h *TemplateKindHandler) Delete(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	templateKind, namespaceID, err := h.retrieveTemplateKind(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespaceID, resource.TypeTemplateKind, "delete"); err != nil {
		return nil, meta, err
	}

	if _, err := h.tka.Delete(templateKind.ID); err != nil {
		return nil, meta, err
	}

	return nil, meta, nil
}

func (h *TemplateKindHandler) getManyQuery(ctx context.Context, params map[string]string) (*query.Query, error) {
	q, err := handler.ParseOData(params, resource.TypeTemplateKind)
	if err != nil {
		return nil, fmt.Errorf("error parsing query: %s", err)
	}

	authorizedNamespaceIDs, err := h.authz.AuthorizedNamespaceIDs(ctx, resource.TypeTemplateKind, "read")
	if err != nil {
		return nil, err
	}

	if pathScope := PathScope(ctx); pathScope != nil {
		authorizedNamespaceIDs, err = forcePathScope(authorizedNamespaceIDs, pathScope.ID)
		if err != nil {
			return nil, err
		}
	}

	authorizedDeliveryKindIDs, err := h.deliveryKindIDsInNamespaces(authorizedNamespaceIDs)
	if err != nil {
		return nil, err
	}

	q.Filters = append(
		q.Filters,
		query.NewFilter("DeliveryKindID", query.OpIn, authorizedDeliveryKindIDs),
		query.NewFilter("Deleted", query.OpEqual, false),
	)

	return q, nil
}

func (h *TemplateKindHandler) deliveryKindIDsInNamespaces(namespaceIDs []uuid.UUID) ([]uuid.UUID, error) {
	q := query.NewQuery(
		query.WithFilterArgs("NamespaceID", query.OpIn, namespaceIDs),
		query.WithFilterArgs("Deleted", query.OpEqual, false),
	)
	deliveryKinds, err := h.dka.RetrieveMany(q)
	if err != nil {
		return nil, err
	}

	// TODO: Investigate returning error when no authorized namespaces or other parent resources.
	// if len(deliveryKinds) == 0 {
	// 	return nil, authorizer.ErrUnauthorized
	// }

	ids := make([]uuid.UUID, len(deliveryKinds))
	for i, dk := range deliveryKinds {
		ids[i] = dk.ID
	}

	return ids, nil
}

func (h *TemplateKindHandler) retrieveTemplateKind(
	ctx context.Context,
	id uuid.UUID,
) (*resource.TemplateKind, uuid.UUID, error) {
	templateKind, namespaceID, err := retrieveTemplateKind(h.tka, h.dka, h.na, id)
	if err == handler.ErrDeletedResource {
		return nil, uuid.Nil, handler.ErrInvalidResourceID
	} else if err != nil {
		return nil, uuid.Nil, err
	}

	// TODO: specific server error code for when a sub-resource fails to be retrieved, incl. not
	// found in database.

	// Validates the template kind's scope with the one defined in the path, if any.
	if pathScope := PathScope(ctx); pathScope != nil && pathScope.ID != namespaceID {
		return nil, uuid.Nil, handler.ErrInvalidResourceID
	}

	return templateKind, namespaceID, nil
}
