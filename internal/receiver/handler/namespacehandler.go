package handler

import (
	"context"
	"fmt"
	"io"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// NamespaceHandler defines an HTTP server handler for namespace endpoints.
type NamespaceHandler struct {
	na    action.NamespaceActioner
	authz authorizer.Authorizer
}

// New creates a new namespace handler.
func NewNamespaceHandler(
	na action.NamespaceActioner,
	authz authorizer.Authorizer,
) *NamespaceHandler {
	return &NamespaceHandler{
		na:    na,
		authz: authz,
	}
}

// GetMany handles a request to retrieve multiple namespaces. The params can contain OData values to
// filter, sort, and paginate the results.
func (h *NamespaceHandler) GetMany(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	q, err := h.getManyQuery(ctx, params)
	if err != nil {
		return nil, meta, err
	}

	namespaces, err := h.na.RetrieveMany(q)
	if err != nil {
		return nil, meta, err
	}

	return namespaces, meta, nil
}

// GetByID handles a request to retrieve an existing namespace, specified by its ID.
func (h *NamespaceHandler) GetByID(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	namespace, err := h.retrieveNamespace(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespace.ID, resource.TypeNamespace, "read"); err != nil {
		return nil, meta, err
	}

	return namespace, meta, nil
}

// Post handles a request to create a new namespace.
func (h *NamespaceHandler) Post(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	namespace := new(resource.Namespace)
	if err := handler.ParseRequestBody(body, namespace); err != nil {
		return nil, meta, fmt.Errorf("error parsing namespace: %w", handler.ErrInvalidRequestBody)
	}

	if pathScope := PathScope(ctx); pathScope != nil {
		if namespace.ParentID == nil {
			namespace.ParentID = &pathScope.ID
		} else if *namespace.ParentID != pathScope.ID {
			// If parent namespace is specified in both path and request body, they must match.
			return nil, meta, handler.ErrInvalidResource
		}
	} else if namespace.ParentID == nil {
		// If parent namespace is not specified anywhere.
		// TODO: Allow for creation of namespaces with no parents when creating new companies.
		//       This will require the request on a public endpoint.
		return nil, meta, handler.ErrInvalidResource
	}

	if err := isNamespaceDeleted(h.na, *namespace.ParentID); err == handler.ErrDeletedResource {
		return nil, meta, handler.ErrInvalidResourceID
	} else if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, *namespace.ParentID, resource.TypeNamespace, "create"); err != nil {
		return nil, meta, err
	}

	// TODO: Code validation.

	namespace, err = h.na.Create(namespace)
	if err != nil {
		return nil, meta, err
	}

	return namespace, meta, nil
}

// Put handles a request to update an existing namespace, specified by its ID, by providing all of
// its attributes.
func (h *NamespaceHandler) Put(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	currentNamespace, err := h.retrieveNamespace(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, currentNamespace.ID, resource.TypeNamespace, "update"); err != nil {
		return nil, meta, err
	}

	namespace := new(resource.Namespace)
	if err := handler.ParseRequestBody(body, namespace); err != nil {
		return nil, meta, fmt.Errorf("error parsing namespace: %w", handler.ErrInvalidRequestBody)
	}

	// TODO: Code validation.
	// if namespace.Code != currentNamespace.Code {	}

	if namespace.ParentID == nil {
		namespace.ParentID = currentNamespace.ParentID
	} else if *namespace.ParentID != *currentNamespace.ParentID {
		return nil, meta, fmt.Errorf("error validating namespace: parent namespace cannot be modified")
	}

	namespace, err = h.na.Update(currentNamespace.ID, namespace)
	if err != nil {
		return nil, meta, err
	}

	return namespace, meta, nil
}

// Patch handles a request to update an existing namespace, specified by its ID, by only providing
// its changed attributes.
func (h *NamespaceHandler) Patch(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	namespace, err := h.retrieveNamespace(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespace.ID, resource.TypeNamespace, "update"); err != nil {
		return nil, meta, err
	}

	// Remembers some attributes that must be validated after they may be overridden. This technique
	// was chosen instead of the following uglier code
	//
	//     namespace := new(*resource.Namespace)
	//     *namespace = *currentNamespace
	//     namespace.ParentID = currentNamespace.ParentID
	//
	// After this, namespace.ParentID and currentNamespace.ParentID both point to the same UUID
	// slice, and both will be overridden when the request body is parsed into any of them.
	//
	// TODO: Investigate whether the parent ID being nil (so ending up as uuid.Nil), could be problematic.
	currentNamespaceParentID := *namespace.ParentID

	if err := handler.ParseRequestBody(body, namespace); err != nil {
		return nil, meta, fmt.Errorf("error parsing namespace: %w", handler.ErrInvalidRequestBody)
	}

	// TODO: Code validation.
	// if namespace.Code != currentNamespace.Code {	}

	if *namespace.ParentID != currentNamespaceParentID {
		return nil, meta, fmt.Errorf("error validating namespace: parent namespace cannot be modified")
	}

	namespace, err = h.na.Update(namespace.ID, namespace)
	if err != nil {
		return nil, meta, err
	}

	return namespace, meta, nil
}

// Delete handles a request to delete an existing namespace, specified by its ID.
func (h *NamespaceHandler) Delete(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	namespace, err := h.retrieveNamespace(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespace.ID, resource.TypeNamespace, "delete"); err != nil {
		return nil, meta, err
	}

	// TODO: Verify that the namespace can be deleted (rel. to children resources).

	if _, err := h.na.Delete(namespace.ID); err != nil {
		return nil, meta, err
	}

	return nil, meta, nil
}

func (h *NamespaceHandler) getManyQuery(ctx context.Context, params map[string]string) (*query.Query, error) {
	q, err := handler.ParseOData(params, resource.TypeNamespace)
	if err != nil {
		return nil, fmt.Errorf("error parsing query: %s", err)
	}

	authorizedNamespaceIDs, err := h.authz.AuthorizedNamespaceIDs(ctx, resource.TypeNamespace, "read")
	if err != nil {
		return nil, err
	}

	q.Filters = append(
		q.Filters,
		query.NewFilter("ID", query.OpIn, authorizedNamespaceIDs),
		query.NewFilter("Deleted", query.OpEqual, false),
	)

	if pathScope := PathScope(ctx); pathScope != nil {
		q.Filters = append(q.Filters, query.NewFilter("ParentID", query.OpEqual, pathScope.ID))
	}

	return q, nil
}

func (h *NamespaceHandler) retrieveNamespace(ctx context.Context, id uuid.UUID) (*resource.Namespace, error) {
	namespace, err := retrieveNamespace(h.na, id)
	if err == handler.ErrDeletedResource || err == handler.ErrInvalidResource {
		return nil, handler.ErrInvalidResourceID
	} else if err != nil {
		return nil, err
	}

	// Validates the namespace's parent with the scope defined in the path, if any.
	if pathScope := PathScope(ctx); pathScope != nil {
		if namespace.ParentID == nil || pathScope.ID != *namespace.ParentID {
			return nil, handler.ErrInvalidResourceID
		}
	}

	return namespace, nil
}
