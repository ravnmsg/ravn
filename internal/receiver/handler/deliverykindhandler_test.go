//go:build !integration && !endtoend
// +build !integration,!endtoend

package handler_test

import (
	"bytes"
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	serverhandler "gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/receiver/handler"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestDeliveryKindHandlerGetMany(t *testing.T) {
	tests := map[string]struct {
		queryFilters                        string
		queryOrderBy                        string
		queryPageTop                        string
		queryPageSkip                       string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		deliveryKindRetrieveManyExpectation *test.DeliveryKindActionRetrieveManyExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			expectedData: []*resource.DeliveryKind{
				test.ArbitraryDeliveryKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when user has permission on multiple namespaces": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},

				// This ID should not appear in the filters because it is for another resource type.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000003"), ResourceType: resource.TypeMessageKind, Operation: "read"},

				// This ID should not appear in the filters because it is for the create operation.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000004"), ResourceType: resource.TypeDeliveryKind, Operation: "create"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001"), uuid.MustParse("00000000-0000-0000-0001-000000000002")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			expectedData: []*resource.DeliveryKind{
				test.ArbitraryDeliveryKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query specified": {
			queryFilters:  "code eq delivery_kind_000000000000",
			queryOrderBy:  "code desc",
			queryPageTop:  "5",
			queryPageSkip: "8",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Code", query.OpEqual, "delivery_kind_000000000000"),
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("Code", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			expectedData: []*resource.DeliveryKind{
				test.ArbitraryDeliveryKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query invalid": {
			queryFilters: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing query: error parsing $filter: invalid token count"),
		},
		"when not authorized": {
			userPermissions: []test.Permission{},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{},
			},
			expectedData:  []*resource.DeliveryKind{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when delivery kinds failed to be found": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{},
			},
			expectedData:  []*resource.DeliveryKind{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when delivery kinds failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{},
				ReturnedError:         test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope is specified": {
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000000"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveManyExpectation: &test.DeliveryKindActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("NamespaceID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedDeliveryKinds: []*resource.DeliveryKind{
					test.ArbitraryDeliveryKind(t),
				},
			},
			expectedData: []*resource.DeliveryKind{
				test.ArbitraryDeliveryKind(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveMany(tt.deliveryKindRetrieveManyExpectation)

			params := map[string]string{
				"$filter":  tt.queryFilters,
				"$orderby": tt.queryOrderBy,
				"$top":     tt.queryPageTop,
				"$skip":    tt.queryPageSkip,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewDeliveryKindHandler(dka, nil, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.GetMany(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			dka.AssertExpectations(t)
		})
	}
}

func TestDeliveryKindHandlerGetByID(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		deliveryKindRetrieveByIDExpectation *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  test.ArbitraryDeliveryKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id:              "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when delivery kind failed to be found": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when path scope is the parent namespace": {
			id:        "00000000-0000-0000-0002-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  test.ArbitraryDeliveryKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope is not the parent namespace": {
			id:        "00000000-0000-0000-0002-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewDeliveryKindHandler(dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.GetByID(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestDeliveryKindHandlerPost(t *testing.T) {
	tests := map[string]struct {
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		deliveryKindCreateExpectation       *test.DeliveryKindActionCreateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			requestBody: []byte("{" +
				`"code":"new_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"new_host",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindCreateExpectation: &test.DeliveryKindActionCreateExpectation{
				ExpectedDeliveryKind: &resource.DeliveryKind{
					Code:        "new_delivery_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "new_host",
					Disabled:    true,
				},
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			expectedData:  test.ArbitraryDeliveryKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when not authorized": {
			requestBody: []byte("{" +
				`"code":"new_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"new_host",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing delivery kind: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with parent namespace id missing": {
			requestBody: []byte("{" +
				`"code":"new_delivery_kind",` +
				`"host":"new_host",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when delivery kind is in a deleted namespace branch": {
			requestBody: []byte("{" +
				`"code":"new_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"new_host",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when parent namespaces failed to be retrieved": {
			requestBody: []byte("{" +
				`"code":"new_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"new_host",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when delivery kind failed to be created": {
			requestBody: []byte("{" +
				`"code":"new_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"new_host",` +
				`"disabled":true` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindCreateExpectation: &test.DeliveryKindActionCreateExpectation{
				ExpectedDeliveryKind: &resource.DeliveryKind{
					Code:        "new_delivery_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "new_host",
					Disabled:    true,
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			requestBody: []byte("{" +
				`"code":"new_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"new_host",` +
				`"disabled":true` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindCreateExpectation: &test.DeliveryKindActionCreateExpectation{
				ExpectedDeliveryKind: &resource.DeliveryKind{
					Code:        "new_delivery_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "new_host",
					Disabled:    true,
				},
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			expectedData:  test.ArbitraryDeliveryKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified and no parent namespace ids": {
			requestBody: []byte("{" +
				`"code":"new_delivery_kind",` +
				`"host":"new_host",` +
				`"disabled":true` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindCreateExpectation: &test.DeliveryKindActionCreateExpectation{
				ExpectedDeliveryKind: &resource.DeliveryKind{
					Code:        "new_delivery_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "new_host",
					Disabled:    true,
				},
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			expectedData:  test.ArbitraryDeliveryKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			requestBody: []byte("{" +
				`"code":"new_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"new_host",` +
				`"disabled":true` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectCreate(tt.deliveryKindCreateExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewDeliveryKindHandler(dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Post(ctx, make(map[string]string), bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestDeliveryKindHandlerPut(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		deliveryKindRetrieveByIDExpectation *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		deliveryKindUpdateExpectation       *test.DeliveryKindActionUpdateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindUpdateExpectation: &test.DeliveryKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "updated_host",
					Disabled:    true,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			expectedData:  test.ArbitraryDeliveryKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			id:          "00000000-0000-0000-0002-000000000000",
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing delivery kind: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with modified parent namespace id": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-ffffffffffff",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating delivery kind: namespace cannot be modified"),
		},
		"when delivery kind failed to be found": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when delivery kind failed to be updated": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindUpdateExpectation: &test.DeliveryKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "updated_host",
					Disabled:    true,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindUpdateExpectation: &test.DeliveryKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "updated_host",
					Disabled:    true,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			expectedData:  test.ArbitraryDeliveryKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified and no parent namespace ids": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindUpdateExpectation: &test.DeliveryKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "updated_host",
					Disabled:    true,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			expectedData:  test.ArbitraryDeliveryKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0002-000000000000",` +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-000000000001",` +
				`"host":"updated_host",` +
				`"disabled":true,` +
				`"createTime":"2000-01-01T02:00:58Z",` +
				`"updateTime":"2000-01-01T02:00:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)
			dka.ExpectUpdate(tt.deliveryKindUpdateExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewDeliveryKindHandler(dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Put(ctx, params, bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestDeliveryKindHandlerPatch(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		deliveryKindRetrieveByIDExpectation *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		deliveryKindUpdateExpectation       *test.DeliveryKindActionUpdateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindUpdateExpectation: &test.DeliveryKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "host_000000000000",
					Disabled:    false,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			expectedData:  test.ArbitraryDeliveryKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind"` +
				"}"),
			userPermissions: []test.Permission{},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			id:          "00000000-0000-0000-0002-000000000000",
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing delivery kind: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with modified parent namespace id": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind",` +
				`"namespaceId":"00000000-0000-0000-0001-ffffffffffff"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating delivery kind: namespace cannot be modified"),
		},
		"when delivery kind failed to be found": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when delivery kind failed to be updated": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindUpdateExpectation: &test.DeliveryKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "host_000000000000",
					Disabled:    false,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id: "00000000-0000-0000-0002-000000000000",
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindUpdateExpectation: &test.DeliveryKindActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ExpectedDeliveryKind: &resource.DeliveryKind{
					ID:          uuid.MustParse("00000000-0000-0000-0002-000000000000"),
					Code:        "updated_delivery_kind",
					NamespaceID: test.MakeUUID("00000000-0000-0000-0001-000000000001", t),
					Host:        "host_000000000000",
					Disabled:    false,
					CreateTime:  test.MakeTime("2000-01-01T02:00:58Z", t),
					UpdateTime:  test.MakeTime("2000-01-01T02:00:59Z", t),
				},
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			expectedData:  test.ArbitraryDeliveryKind(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id:        "00000000-0000-0000-0002-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			requestBody: []byte("{" +
				`"code":"updated_delivery_kind"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "update"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)
			dka.ExpectUpdate(tt.deliveryKindUpdateExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewDeliveryKindHandler(dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Patch(ctx, params, bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}

func TestDeliveryKindHandlerDelete(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		deliveryKindRetrieveByIDExpectation *test.DeliveryKindActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		deliveryKindDeleteExpectation       *test.DeliveryKindActionDeleteExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "delete"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindDeleteExpectation: &test.DeliveryKindActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: nil,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "delete"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id:              "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when delivery kind failed to be found": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "delete"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind failed to be retrieved": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "delete"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when delivery kind is deleted": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "delete"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: &resource.DeliveryKind{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when delivery kind is in a deleted namespace branch": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "delete"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "delete"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when delivery kind failed to be deleted": {
			id: "00000000-0000-0000-0002-000000000000",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "delete"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindDeleteExpectation: &test.DeliveryKindActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id:        "00000000-0000-0000-0002-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "delete"},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindDeleteExpectation: &test.DeliveryKindActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedError: nil,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id:        "00000000-0000-0000-0002-000000000000",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeDeliveryKind, Operation: "delete"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			deliveryKindRetrieveByIDExpectation: &test.DeliveryKindActionRetrieveByIDExpectation{
				ExpectedID:           uuid.MustParse("00000000-0000-0000-0002-000000000000"),
				ReturnedDeliveryKind: test.ArbitraryDeliveryKind(t),
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			dka := test.NewDeliveryKindAction(t)
			dka.ExpectRetrieveByID(tt.deliveryKindRetrieveByIDExpectation)
			dka.ExpectDelete(tt.deliveryKindDeleteExpectation)

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewDeliveryKindHandler(dka, na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Delete(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			dka.AssertExpectations(t)
			na.AssertExpectations(t)
		})
	}
}
