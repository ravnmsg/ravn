package handler

import (
	"net/http"

	"github.com/google/uuid"
	"github.com/gorilla/mux"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

func pathScopeMiddleware(h http.Handler, na action.NamespaceActioner) http.Handler {
	f := func(w http.ResponseWriter, r *http.Request) {
		var pathScope *resource.Namespace

		namespaceValue := mux.Vars(r)["namespace_id"]
		if namespaceValue != "" {
			namespaceID, err := uuid.Parse(namespaceValue)
			if err != nil {
				// TODO: indicate that the error is with the namespace id.
				handler.WriteErrorResponse(w, handler.ErrInvalidResourceIDOrCode, nil)
				return
			}

			namespace, err := na.RetrieveByID(namespaceID)
			if err != nil {
				// TODO: decide a better error message instead of ErrInvalidResourceIDOrCode
				// eg. ErrInvalidResource, or ErrInvalidResourcePath
				//
				// TODO: err here might not always be an invalid resource id or code.
				handler.WriteErrorResponse(w, handler.ErrInvalidResourceIDOrCode, nil)
				return
			}

			pathScope = namespace
		}

		ctx := SetPathScope(r.Context(), pathScope)

		h.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(f)
}
