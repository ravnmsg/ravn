//go:build !integration && !endtoend
// +build !integration,!endtoend

package handler_test

import (
	"bytes"
	"context"
	"fmt"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	serverhandler "gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/receiver/handler"
	"gitlab.com/ravnmsg/ravn/internal/resource"
	"gitlab.com/ravnmsg/ravn/internal/test"
)

func TestNamespaceHandlerGetMany(t *testing.T) {
	tests := map[string]struct {
		queryFilters                     string
		queryOrderBy                     string
		queryPageTop                     string
		queryPageSkip                    string
		pathScope                        *resource.Namespace
		userPermissions                  []test.Permission
		namespaceRetrieveManyExpectation *test.NamespaceActionRetrieveManyExpectation
		expectedData                     interface{}
		expectedMeta                     map[string]interface{}
		expectedError                    error
	}{
		"when successful": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveManyExpectation: &test.NamespaceActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedNamespaces: []*resource.Namespace{
					test.ArbitraryNamespace(t),
				},
			},
			expectedData: []*resource.Namespace{
				test.ArbitraryNamespace(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when user has permission on multiple namespaces": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000002"), ResourceType: resource.TypeNamespace, Operation: "read"},

				// This ID should not appear in the filters because it is for another resource type.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000003"), ResourceType: resource.TypeDeliveryKind, Operation: "read"},

				// This ID should not appear in the filters because it is for the create operation.
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000004"), ResourceType: resource.TypeNamespace, Operation: "create"},
			},
			namespaceRetrieveManyExpectation: &test.NamespaceActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001"), uuid.MustParse("00000000-0000-0000-0001-000000000002")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedNamespaces: []*resource.Namespace{
					test.ArbitraryNamespace(t),
				},
			},
			expectedData: []*resource.Namespace{
				test.ArbitraryNamespace(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query specified": {
			queryFilters:  "code eq namespace_000000000001",
			queryOrderBy:  "code desc",
			queryPageTop:  "5",
			queryPageSkip: "8",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveManyExpectation: &test.NamespaceActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("Code", query.OpEqual, "namespace_000000000001"),
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("Code", query.DirDescending),
					query.WithPageArgs(8, 5),
				),
				ReturnedNamespaces: []*resource.Namespace{
					test.ArbitraryNamespace(t),
				},
			},
			expectedData: []*resource.Namespace{
				test.ArbitraryNamespace(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when query invalid": {
			queryFilters: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing query: error parsing $filter: invalid token count"),
		},
		"when not authorized": {
			userPermissions: []test.Permission{},
			namespaceRetrieveManyExpectation: &test.NamespaceActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedNamespaces: []*resource.Namespace{},
			},
			expectedData:  []*resource.Namespace{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when namespaces failed to be found": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveManyExpectation: &test.NamespaceActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedNamespaces: []*resource.Namespace{},
			},
			expectedData:  []*resource.Namespace{},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when namespaces failed to be retrieved": {
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveManyExpectation: &test.NamespaceActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope is specified": {
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveManyExpectation: &test.NamespaceActionRetrieveManyExpectation{
				ExpectedQuery: query.NewQuery(
					query.WithFilterArgs("ID", query.OpIn, []uuid.UUID{uuid.MustParse("00000000-0000-0000-0001-000000000001")}),
					query.WithFilterArgs("Deleted", query.OpEqual, false),
					query.WithFilterArgs("ParentID", query.OpEqual, uuid.MustParse("00000000-0000-0000-0001-000000000000")),
					query.WithOrderArgs("CreateTime", query.DirAscending),
					query.WithPageArgs(0, 20),
				),
				ReturnedNamespaces: []*resource.Namespace{
					test.ArbitraryNamespace(t),
				},
			},
			expectedData: []*resource.Namespace{
				test.ArbitraryNamespace(t),
			},
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveMany(tt.namespaceRetrieveManyExpectation)

			params := map[string]string{
				"$filter":  tt.queryFilters,
				"$orderby": tt.queryOrderBy,
				"$top":     tt.queryPageTop,
				"$skip":    tt.queryPageSkip,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewNamespaceHandler(na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.GetMany(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			na.AssertExpectations(t)
		})
	}
}

func TestNamespaceHandlerGetByID(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		namespaceRetrieveByIDExpectation    *test.NamespaceActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  test.ArbitraryNamespace(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id:              "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when namespace failed to be found": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when namespace failed to be retrieved": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when namespace is deleted": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: &resource.Namespace{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when namespace is in a deleted namespace branch": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when path scope is the parent namespace": {
			id:        "00000000-0000-0000-0001-000000000001",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  test.ArbitraryNamespace(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope is not the parent namespace": {
			id:        "00000000-0000-0000-0001-000000000001",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "read"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveByID(tt.namespaceRetrieveByIDExpectation)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewNamespaceHandler(na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.GetByID(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			na.AssertExpectations(t)
		})
	}
}

func TestNamespaceHandlerPost(t *testing.T) {
	tests := map[string]struct {
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		namespaceCreateExpectation          *test.NamespaceActionCreateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			requestBody: []byte("{" +
				`"code":"new_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000001"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceCreateExpectation: &test.NamespaceActionCreateExpectation{
				ExpectedNamespace: &resource.Namespace{
					Code:     "new_namespace",
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
				},
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			expectedData:  test.ArbitraryNamespace(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when not authorized": {
			requestBody: []byte("{" +
				`"code":"new_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000001"` +
				"}"),
			userPermissions: []test.Permission{},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing namespace: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with parent namespace id missing": {
			requestBody: []byte("{" +
				`"code":"new_namespace"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
		"when namespace is in a deleted namespace branch": {
			requestBody: []byte("{" +
				`"code":"new_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000001"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			requestBody: []byte("{" +
				`"code":"new_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000001"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when namespace failed to be created": {
			requestBody: []byte("{" +
				`"code":"new_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000001"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceCreateExpectation: &test.NamespaceActionCreateExpectation{
				ExpectedNamespace: &resource.Namespace{
					Code:     "new_namespace",
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			requestBody: []byte("{" +
				`"code":"new_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000001"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceCreateExpectation: &test.NamespaceActionCreateExpectation{
				ExpectedNamespace: &resource.Namespace{
					Code:     "new_namespace",
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
				},
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			expectedData:  test.ArbitraryNamespace(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified and no parent namespace ids": {
			requestBody: []byte("{" +
				`"code":"new_namespace"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "create"},
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceCreateExpectation: &test.NamespaceActionCreateExpectation{
				ExpectedNamespace: &resource.Namespace{
					Code:     "new_namespace",
					ParentID: test.MakeUUIDRef("00000000-0000-0000-0001-000000000001", t),
				},
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			expectedData:  test.ArbitraryNamespace(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			requestBody: []byte("{" +
				`"code":"new_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000001"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "create"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResource,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)
			na.ExpectCreate(tt.namespaceCreateExpectation)

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewNamespaceHandler(na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Post(ctx, make(map[string]string), bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			na.AssertExpectations(t)
		})
	}
}

func TestNamespaceHandlerPut(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		namespaceRetrieveByIDExpectation    *test.NamespaceActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		namespaceUpdateExpectation          *test.NamespaceActionUpdateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000000",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceUpdateExpectation: &test.NamespaceActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedNamespace: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
				},
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			expectedData:  test.ArbitraryNamespace(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000000",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000000",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			userPermissions: []test.Permission{},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			id:          "00000000-0000-0000-0001-000000000001",
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing namespace: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with modified namespace parent id": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-ffffffffffff",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating namespace: parent namespace cannot be modified"),
		},
		"when namespace failed to be found": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000000",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when namespace failed to be retrieved": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000000",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when namespace is deleted": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000000",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: &resource.Namespace{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when namespace is in a deleted namespace branch": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000000",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000000",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when namespace failed to be updated": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000000",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceUpdateExpectation: &test.NamespaceActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedNamespace: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000000",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceUpdateExpectation: &test.NamespaceActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedNamespace: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
				},
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			expectedData:  test.ArbitraryNamespace(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified and no parent namespace ids": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceUpdateExpectation: &test.NamespaceActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedNamespace: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
				},
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			expectedData:  test.ArbitraryNamespace(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"id":"00000000-0000-0000-0001-000000000001",` +
				`"code":"updated_namespace",` +
				`"parentId":"00000000-0000-0000-0001-000000000000",` +
				`"createTime":"2000-01-01T01:01:58Z",` +
				`"updateTime":"2000-01-01T01:01:59Z"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveByID(tt.namespaceRetrieveByIDExpectation)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)
			na.ExpectUpdate(tt.namespaceUpdateExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewNamespaceHandler(na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Put(ctx, params, bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			na.AssertExpectations(t)
		})
	}
}

func TestNamespaceHandlerPatch(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		requestBody                         []byte
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		namespaceRetrieveByIDExpectation    *test.NamespaceActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		namespaceUpdateExpectation          *test.NamespaceActionUpdateExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"code":"updated_namespace"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceUpdateExpectation: &test.NamespaceActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedNamespace: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
				},
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			expectedData:  test.ArbitraryNamespace(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			requestBody: []byte("{" +
				`"code":"updated_namespace"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"code":"updated_namespace"` +
				"}"),
			userPermissions: []test.Permission{},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when request body failed to be parsed": {
			id:          "00000000-0000-0000-0001-000000000001",
			requestBody: []byte(""),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error parsing namespace: %w", serverhandler.ErrInvalidRequestBody),
		},
		"when validation failed with modified namespace parent id": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"parentId":"00000000-0000-0000-0001-ffffffffffff"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error validating namespace: parent namespace cannot be modified"),
		},
		"when namespace failed to be found": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"code":"updated_namespace"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when namespace failed to be retrieved": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"code":"updated_namespace"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when namespace is deleted": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"code":"updated_namespace"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: &resource.Namespace{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when namespace is in a deleted namespace branch": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"code":"updated_namespace"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"code":"updated_namespace"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when namespace failed to be updated": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"code":"updated_namespace"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceUpdateExpectation: &test.NamespaceActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedNamespace: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
				},
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id: "00000000-0000-0000-0001-000000000001",
			requestBody: []byte("{" +
				`"code":"updated_namespace"` +
				"}"),
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceUpdateExpectation: &test.NamespaceActionUpdateExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ExpectedNamespace: &resource.Namespace{
					ID:         uuid.MustParse("00000000-0000-0000-0001-000000000001"),
					Code:       "updated_namespace",
					ParentID:   test.MakeUUIDRef("00000000-0000-0000-0001-000000000000", t),
					CreateTime: test.MakeTime("2000-01-01T01:01:58Z", t),
					UpdateTime: test.MakeTime("2000-01-01T01:01:59Z", t),
				},
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			expectedData:  test.ArbitraryNamespace(t),
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id:        "00000000-0000-0000-0001-000000000001",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			requestBody: []byte("{" +
				`"code":"updated_namespace"` +
				"}"),
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "update"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveByID(tt.namespaceRetrieveByIDExpectation)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)
			na.ExpectUpdate(tt.namespaceUpdateExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewNamespaceHandler(na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Patch(ctx, params, bytes.NewReader(tt.requestBody))

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			na.AssertExpectations(t)
		})
	}
}

func TestNamespaceHandlerDelete(t *testing.T) {
	tests := map[string]struct {
		id                                  string
		pathScope                           *resource.Namespace
		userPermissions                     []test.Permission
		namespaceRetrieveByIDExpectation    *test.NamespaceActionRetrieveByIDExpectation
		namespaceRetrieveParentsExpectation *test.NamespaceActionRetrieveParentsExpectation
		namespaceDeleteExpectation          *test.NamespaceActionDeleteExpectation
		expectedData                        interface{}
		expectedMeta                        map[string]interface{}
		expectedError                       error
	}{
		"when successful": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "delete"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceDeleteExpectation: &test.NamespaceActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: nil,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when specified id is invalid": {
			id: "invalid",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "delete"},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when not authorized": {
			id:              "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: authorizer.ErrUnauthorized,
		},
		"when namespace failed to be found": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "delete"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrInvalidResourceID,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when namespace failed to be retrieved": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "delete"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when namespace is deleted": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "delete"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: &resource.Namespace{Deleted: true},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when namespace is in a deleted namespace branch": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "delete"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: action.ErrNamespaceDeleted,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
		"when parent namespaces failed to be retrieved": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "delete"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: fmt.Errorf("error retrieving parent namespaces: arbitrary"),
		},
		"when namespace failed to be deleted": {
			id: "00000000-0000-0000-0001-000000000001",
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "delete"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceDeleteExpectation: &test.NamespaceActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: test.ErrArbitrary,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: test.ErrArbitrary,
		},
		"when path scope specified and parent namespace id matches": {
			id:        "00000000-0000-0000-0001-000000000001",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "delete"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			namespaceDeleteExpectation: &test.NamespaceActionDeleteExpectation{
				ExpectedID:    uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedError: nil,
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: nil,
		},
		"when path scope specified but parent namespace id does not match": {
			id:        "00000000-0000-0000-0001-000000000001",
			pathScope: &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-ffffffffffff")},
			userPermissions: []test.Permission{
				{NamespaceID: uuid.MustParse("00000000-0000-0000-0001-000000000001"), ResourceType: resource.TypeNamespace, Operation: "delete"},
			},
			namespaceRetrieveByIDExpectation: &test.NamespaceActionRetrieveByIDExpectation{
				ExpectedID:        uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespace: test.ArbitraryNamespace(t),
			},
			namespaceRetrieveParentsExpectation: &test.NamespaceActionRetrieveParentsExpectation{
				ExpectedID: uuid.MustParse("00000000-0000-0000-0001-000000000001"),
				ReturnedNamespaces: []*resource.Namespace{
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")},
					{ID: uuid.MustParse("00000000-0000-0000-0001-000000000000")},
				},
			},
			expectedData:  nil,
			expectedMeta:  map[string]interface{}{},
			expectedError: serverhandler.ErrInvalidResourceID,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			na := test.NewNamespaceAction(t)
			na.ExpectRetrieveByID(tt.namespaceRetrieveByIDExpectation)
			na.ExpectRetrieveParents(tt.namespaceRetrieveParentsExpectation)
			na.ExpectDelete(tt.namespaceDeleteExpectation)

			params := map[string]string{
				"id": tt.id,
			}

			ctx := context.Background()
			ctx = handler.SetPathScope(ctx, tt.pathScope)

			h := handler.NewNamespaceHandler(na, test.NewAuthorizer(tt.userPermissions))
			data, meta, err := h.Delete(ctx, params, nil)

			assert.Equal(t, tt.expectedData, data)
			assert.Equal(t, tt.expectedMeta, meta)
			assert.Equal(t, tt.expectedError, err)

			na.AssertExpectations(t)
		})
	}
}
