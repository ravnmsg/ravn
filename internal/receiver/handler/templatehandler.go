package handler

import (
	"context"
	"fmt"
	"io"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/deliverer/processor"
	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/query"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

// TemplateHandler defines an HTTP server handler for template endpoints.
type TemplateHandler struct {
	ta    action.TemplateActioner
	tka   action.TemplateKindActioner
	mka   action.MessageKindActioner
	dka   action.DeliveryKindActioner
	na    action.NamespaceActioner
	authz authorizer.Authorizer
}

// NewTemplateHandler creates a new template handler.
func NewTemplateHandler(
	ta action.TemplateActioner,
	tka action.TemplateKindActioner,
	mka action.MessageKindActioner,
	dka action.DeliveryKindActioner,
	na action.NamespaceActioner,
	authz authorizer.Authorizer,
) *TemplateHandler {
	return &TemplateHandler{
		ta:    ta,
		tka:   tka,
		mka:   mka,
		dka:   dka,
		na:    na,
		authz: authz,
	}
}

func (h *TemplateHandler) GetMany(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	q, err := h.getManyQuery(ctx, params)
	if err != nil {
		return nil, meta, err
	}

	templates, err := h.ta.RetrieveMany(q)
	if err != nil {
		return nil, meta, err
	}

	return templates, meta, nil
}

func (h *TemplateHandler) GetByID(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	template, namespaceID, err := h.retrieveTemplate(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespaceID, resource.TypeTemplate, "read"); err != nil {
		return nil, meta, err
	}

	return template, meta, nil
}

// Post handles the creation of a new template.
func (h *TemplateHandler) Post(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	template := new(resource.Template)
	if err := handler.ParseRequestBody(body, template); err != nil {
		return nil, meta, fmt.Errorf("error parsing template: %w", handler.ErrInvalidRequestBody)
	}

	_, namespaceID, err := retrieveMessageKind(h.mka, h.na, template.MessageKindID)
	if err == handler.ErrInvalidResourceID || err == handler.ErrDeletedResource {
		return nil, meta, handler.ErrInvalidResource
	} else if err != nil {
		return nil, meta, err
	}

	// A template's message kind, not its template kind, determines the namespace.
	if pathScope := PathScope(ctx); pathScope != nil && pathScope.ID != namespaceID {
		return nil, meta, handler.ErrInvalidResource
	}

	_, _, err = retrieveTemplateKind(h.tka, h.dka, h.na, template.TemplateKindID)
	if err == handler.ErrInvalidResourceID || err == handler.ErrDeletedResource {
		return nil, meta, handler.ErrInvalidResource
	} else if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespaceID, resource.TypeTemplate, "create"); err != nil {
		return nil, meta, err
	}

	template, err = h.ta.Create(template)
	if err != nil {
		return nil, meta, err
	}

	return template, meta, nil
}

// Put handles the update of an existing template, specified by its ID, by providing all of its
// attributes.
func (h *TemplateHandler) Put(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	currentTemplate, namespaceID, err := h.retrieveTemplate(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespaceID, resource.TypeTemplate, "update"); err != nil {
		return nil, meta, err
	}

	template := new(resource.Template)
	if err := handler.ParseRequestBody(body, template); err != nil {
		return nil, meta, fmt.Errorf("error parsing template: %w", handler.ErrInvalidRequestBody)
	}

	if template.MessageKindID != currentTemplate.MessageKindID {
		return nil, meta, fmt.Errorf("error validating template: message kind cannot be modified")
	} else if template.TemplateKindID != currentTemplate.TemplateKindID {
		return nil, meta, fmt.Errorf("error validating template: template kind cannot be modified")
	}

	template, err = h.ta.Update(currentTemplate.ID, template)
	if err != nil {
		return nil, meta, err
	}

	return template, meta, nil
}

// Patch handles the modification of an existing template, specified by its ID, by providing only
// its changed attributes.
func (h *TemplateHandler) Patch(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	template, namespaceID, err := h.retrieveTemplate(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespaceID, resource.TypeTemplate, "update"); err != nil {
		return nil, meta, err
	}

	currentTemplateMessageKindID := template.MessageKindID
	currentTemplateTemplateKindID := template.TemplateKindID

	if err := handler.ParseRequestBody(body, template); err != nil {
		return nil, meta, fmt.Errorf("error parsing template: %w", handler.ErrInvalidRequestBody)
	}

	if template.MessageKindID != currentTemplateMessageKindID {
		return nil, meta, fmt.Errorf("error validating template: message kind cannot be modified")
	} else if template.TemplateKindID != currentTemplateTemplateKindID {
		return nil, meta, fmt.Errorf("error validating template: template kind cannot be modified")
	}

	template, err = h.ta.Update(template.ID, template)
	if err != nil {
		return nil, meta, err
	}

	return template, meta, nil
}

// Delete handles the deletion of a template, specified by its ID.
func (h *TemplateHandler) Delete(
	ctx context.Context,
	params map[string]string,
	_ io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	id, err := uuid.Parse(params["id"])
	if err != nil {
		return nil, meta, handler.ErrInvalidResourceID
	}

	template, namespaceID, err := h.retrieveTemplate(ctx, id)
	if err != nil {
		return nil, meta, err
	}

	if err := h.authz.AuthorizeOperation(ctx, namespaceID, resource.TypeTemplate, "delete"); err != nil {
		return nil, meta, err
	}

	if _, err := h.ta.Delete(template.ID); err != nil {
		return nil, meta, err
	}

	return nil, meta, nil
}

func (h *TemplateHandler) Preview(
	ctx context.Context,
	params map[string]string,
	body io.Reader,
) (data interface{}, meta map[string]interface{}, err error) {
	meta = make(map[string]interface{})

	previewReq := new(TemplatePreviewRequest)
	if err := handler.ParseRequestBody(body, previewReq); err != nil {
		return nil, meta, fmt.Errorf("error parsing preview request: %w", handler.ErrInvalidRequestBody)
	}

	var template *resource.Template
	if previewReq.TemplateID != uuid.Nil {
		var namespaceID uuid.UUID
		template, namespaceID, err = h.retrieveTemplate(ctx, previewReq.TemplateID)
		if err != nil {
			return nil, meta, err
		} else if err := h.authz.AuthorizeOperation(ctx, namespaceID, resource.TypeTemplate, "read"); err != nil {
			return nil, meta, err
		}
	} else {
		if previewReq.LanguageTag == "" {
			return nil, meta, fmt.Errorf("invalid language tag")
		} else if previewReq.Body == "" {
			return nil, meta, fmt.Errorf("invalid body")
		}

		template = &resource.Template{
			LanguageTag: previewReq.LanguageTag,
			Body:        previewReq.Body,
		}
	}

	p := processor.NewTemplateProcessor()
	value, err := p.Process(template, previewReq.Input)
	if err != nil {
		return nil, meta, err
	}

	return value, meta, nil
}

func (h *TemplateHandler) getManyQuery(ctx context.Context, params map[string]string) (*query.Query, error) {
	q, err := handler.ParseOData(params, resource.TypeTemplate)
	if err != nil {
		return nil, fmt.Errorf("error parsing query: %s", err)
	}

	authorizedNamespaceIDs, err := h.authz.AuthorizedNamespaceIDs(ctx, resource.TypeTemplate, "read")
	if err != nil {
		return nil, err
	}

	if pathScope := PathScope(ctx); pathScope != nil {
		authorizedNamespaceIDs, err = forcePathScope(authorizedNamespaceIDs, pathScope.ID)
		if err != nil {
			return nil, err
		}
	}

	authorizedMessageKindIDs, err := h.messageKindIDsInNamespaces(authorizedNamespaceIDs)
	if err != nil {
		return nil, err
	}

	authorizedTemplateKindIDs, err := h.templateKindIDsInNamespaces(authorizedNamespaceIDs)
	if err != nil {
		return nil, err
	}

	q.Filters = append(
		q.Filters,
		query.NewFilter("MessageKindID", query.OpIn, authorizedMessageKindIDs),
		query.NewFilter("TemplateKindID", query.OpIn, authorizedTemplateKindIDs),
		query.NewFilter("Deleted", query.OpEqual, false),
	)

	return q, nil
}

func (h *TemplateHandler) messageKindIDsInNamespaces(namespaceIDs []uuid.UUID) ([]uuid.UUID, error) {
	q := query.NewQuery(
		query.WithFilterArgs("NamespaceID", query.OpIn, namespaceIDs),
		query.WithFilterArgs("Deleted", query.OpEqual, false),
	)
	messageKinds, err := h.mka.RetrieveMany(q)
	if err != nil {
		return nil, err
	}

	// TODO
	// if len(messageKinds) == 0 {
	// 	return nil, authorizer.ErrUnauthorized
	// }

	ids := make([]uuid.UUID, len(messageKinds))
	for i, mk := range messageKinds {
		ids[i] = mk.ID
	}

	return ids, nil
}

func (h *TemplateHandler) templateKindIDsInNamespaces(namespaceIDs []uuid.UUID) ([]uuid.UUID, error) {
	q := query.NewQuery(
		query.WithFilterArgs("NamespaceID", query.OpIn, namespaceIDs),
		query.WithFilterArgs("Deleted", query.OpEqual, false),
	)
	deliveryKinds, err := h.dka.RetrieveMany(q)
	if err != nil {
		return nil, err
	}

	// TODO
	// if len(deliveryKinds) == 0 {
	// 	return nil, authorizer.ErrUnauthorized
	// }

	deliveryKindIDs := make([]uuid.UUID, len(deliveryKinds))
	for i, dk := range deliveryKinds {
		deliveryKindIDs[i] = dk.ID
	}

	q = query.NewQuery(
		query.WithFilterArgs("DeliveryKindID", query.OpIn, deliveryKindIDs),
		query.WithFilterArgs("Deleted", query.OpEqual, false),
	)
	templateKinds, err := h.tka.RetrieveMany(q)
	if err != nil {
		return nil, err
	}

	// TODO
	// if len(templateKinds) == 0 {
	// 	return nil, authorizer.ErrUnauthorized
	// }

	ids := make([]uuid.UUID, len(templateKinds))
	for i, tk := range templateKinds {
		ids[i] = tk.ID
	}

	return ids, nil
}

func (h *TemplateHandler) retrieveTemplate(ctx context.Context, id uuid.UUID) (*resource.Template, uuid.UUID, error) {
	template, namespaceID, err := retrieveTemplate(h.ta, h.tka, h.mka, h.dka, h.na, id)
	if err == handler.ErrDeletedResource {
		return nil, uuid.Nil, handler.ErrInvalidResourceID
	} else if err != nil {
		return nil, uuid.Nil, err
	}

	// Validates the template's scope with the one defined in the path, if any.
	if pathScope := PathScope(ctx); pathScope != nil && pathScope.ID != namespaceID {
		return nil, uuid.Nil, handler.ErrInvalidResourceID
	}

	return template, namespaceID, nil
}

// TemplatePreviewRequest defines the body of a request related to a template preview. A template ID
// can be provided, or a language tag and a body.
type TemplatePreviewRequest struct {
	TemplateID  uuid.UUID              `json:"templateID"`
	LanguageTag string                 `json:"languageTag"`
	Body        string                 `json:"body"`
	Input       map[string]interface{} `json:"input"`
}
