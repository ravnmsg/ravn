//go:build !integration && !endtoend
// +build !integration,!endtoend

package handler_test

import (
	"context"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"gitlab.com/ravnmsg/ravn/internal/receiver/handler"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

func TestPathScope(t *testing.T) {
	ctx := context.Background()
	namespace := &resource.Namespace{ID: uuid.MustParse("00000000-0000-0000-0001-000000000001")}

	ctx = handler.SetPathScope(ctx, namespace)

	assert.Equal(t, namespace, handler.PathScope(ctx))
}
