package handler

import (
	"context"
	"fmt"
	"io"
	"net/http"

	"github.com/google/uuid"

	"gitlab.com/ravnmsg/ravn/internal/action"
	"gitlab.com/ravnmsg/ravn/internal/authorizer"
	"gitlab.com/ravnmsg/ravn/internal/httpserver/handler"
	"gitlab.com/ravnmsg/ravn/internal/resource"
)

type RequestHandler func(context.Context, map[string]string, io.Reader) (interface{}, map[string]interface{}, error)

// HandleRequest receives the request and splits it into usable data that are agnostic of HTTP. It
// will also handle the response, based on the handler's result.
func HandleRequest(requestHandler RequestHandler, na action.NamespaceActioner) http.Handler {
	f := func(w http.ResponseWriter, r *http.Request) {
		reqParams := handler.RequestParams(r)

		data, meta, err := requestHandler(r.Context(), reqParams, r.Body)
		defer r.Body.Close()

		if meta == nil {
			meta = make(map[string]interface{})
		}

		if requestID := handler.RequestID(r.Context()); requestID != "" && meta["requestId"] != "" {
			meta["requestId"] = requestID
		}

		if err != nil {
			err = fmt.Errorf("failed to %s request: %w", r.Method, err)
			handler.WriteErrorResponse(w, err, meta)
			return
		}

		status := http.StatusOK
		if r.Method == http.MethodPost {
			status = http.StatusCreated
		}

		handler.WriteResponse(w, status, data, meta)
	}

	var h http.Handler = http.HandlerFunc(f)
	h = pathScopeMiddleware(h, na)

	return h
}

// isNamespaceDeleted validates that the namespace with specified ID is not deleted, or in a deleted
// namespace branch.
func isNamespaceDeleted(na action.NamespaceActioner, namespaceID uuid.UUID) error {
	if _, err := na.RetrieveParents(namespaceID); err == action.ErrNamespaceDeleted {
		return handler.ErrDeletedResource
	} else if err != nil {
		return fmt.Errorf("error retrieving parent namespaces: %s", err)
	}

	return nil
}

func retrieveNamespace(
	na action.NamespaceActioner,
	namespaceID uuid.UUID,
) (*resource.Namespace, error) {
	namespace, err := na.RetrieveByID(namespaceID)
	switch {
	case err == action.ErrInvalidResourceID:
		return nil, handler.ErrInvalidResourceID
	case err != nil:
		return nil, err
	case namespace.Deleted:
		return nil, handler.ErrDeletedResource
	}

	if err := isNamespaceDeleted(na, namespace.ID); err != nil {
		return nil, err
	}

	return namespace, nil
}

func retrieveDeliveryKind(
	dka action.DeliveryKindActioner,
	na action.NamespaceActioner,
	deliveryKindID uuid.UUID,
) (*resource.DeliveryKind, uuid.UUID, error) {
	deliveryKind, err := dka.RetrieveByID(deliveryKindID)
	switch {
	case err == action.ErrInvalidResourceID:
		return nil, uuid.Nil, handler.ErrInvalidResourceID
	case err != nil:
		return nil, uuid.Nil, err
	case deliveryKind.Deleted:
		return nil, uuid.Nil, handler.ErrDeletedResource
	}

	namespaceID := deliveryKind.NamespaceID
	if err := isNamespaceDeleted(na, deliveryKind.NamespaceID); err != nil {
		return nil, uuid.Nil, err
	}

	return deliveryKind, namespaceID, nil
}

func retrieveMessageKind(
	mka action.MessageKindActioner,
	na action.NamespaceActioner,
	messageKindID uuid.UUID,
) (*resource.MessageKind, uuid.UUID, error) {
	messageKind, err := mka.RetrieveByID(messageKindID)
	switch {
	case err == action.ErrInvalidResourceID:
		return nil, uuid.Nil, handler.ErrInvalidResourceID
	case err != nil:
		return nil, uuid.Nil, err
	case messageKind.Deleted:
		return nil, uuid.Nil, handler.ErrDeletedResource
	}

	namespaceID := messageKind.NamespaceID
	if err := isNamespaceDeleted(na, namespaceID); err != nil {
		return nil, uuid.Nil, err
	}

	return messageKind, namespaceID, nil
}

func retrieveTemplateKind(
	tka action.TemplateKindActioner,
	dka action.DeliveryKindActioner,
	na action.NamespaceActioner,
	templateKindID uuid.UUID,
) (*resource.TemplateKind, uuid.UUID, error) {
	templateKind, err := tka.RetrieveByID(templateKindID)
	switch {
	case err == action.ErrInvalidResourceID:
		return nil, uuid.Nil, handler.ErrInvalidResourceID
	case err != nil:
		return nil, uuid.Nil, err
	case templateKind.Deleted:
		return nil, uuid.Nil, handler.ErrDeletedResource
	}

	_, namespaceID, err := retrieveDeliveryKind(dka, na, templateKind.DeliveryKindID)
	if err != nil {
		return nil, uuid.Nil, err
	}

	return templateKind, namespaceID, nil
}

func retrieveTemplate(
	ta action.TemplateActioner,
	tka action.TemplateKindActioner,
	mka action.MessageKindActioner,
	dka action.DeliveryKindActioner,
	na action.NamespaceActioner,
	templateID uuid.UUID,
) (*resource.Template, uuid.UUID, error) {
	template, err := ta.RetrieveByID(templateID)
	switch {
	case err == action.ErrInvalidResourceID:
		return nil, uuid.Nil, handler.ErrInvalidResourceID
	case err != nil:
		return nil, uuid.Nil, err
	case template.Deleted:
		return nil, uuid.Nil, handler.ErrDeletedResource
	}

	_, namespaceID, err := retrieveMessageKind(mka, na, template.MessageKindID)
	if err != nil {
		return nil, uuid.Nil, err
	}

	_, _, err = retrieveTemplateKind(tka, dka, na, template.TemplateKindID)
	if err != nil {
		return nil, uuid.Nil, err
	}

	return template, namespaceID, nil
}

func forcePathScope(authorizedNamespaceIDs []uuid.UUID, pathScopeID uuid.UUID) ([]uuid.UUID, error) {
	for _, id := range authorizedNamespaceIDs {
		if id == pathScopeID {
			return []uuid.UUID{pathScopeID}, nil
		}
	}

	return nil, authorizer.ErrUnauthorized
}
