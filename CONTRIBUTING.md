# Contributing

The Ravn application is written in [Go](https://golang.org/). All contributions must pass through a GitLab issue and a merge request at the [Ravn repository](https://gitlab.com/ravnmsg/ravn).

This project is released with a Contributor Code of Conduct. Participation in this project implies agreement to abide by its terms.

## Issues

Most changes to the Ravn application must go through [issues](https://gitlab.com/ravnmsg/ravn/-/issues). This is where the community can discuss a specific problem or proposal.

## Merge requests

A [merge request](https://gitlab.com/ravnmsg/ravn/-/merge_requests) to the `master` branch must be created. This is also where the community will discuss the code related to the current issue.

## Continuous integration

A continuous integration workflow is setup to automatically run tests on each commit. Merge requests will not be accepted if a test is failing.

## Development

It is recommended to use [Docker](https://www.docker.com/) and [Docker Compose](https://docs.docker.com/compose/) when working on the application, at least for the third-party services (NATS and PostgreSQL).

The instructions in this file install Ravn in unsecured mode. This is usually not advisable in production, unless security is not a concern.

### Installation

All images referenced in `docker-compose.yaml` can first be downloaded with the following command:

```sh
docker compose pull
```

#### Quick installation with everything in containers

Ravn and its dependencies can be started quickly via docker compose:

```sh
docker compose build
docker compose up --detach
```

The following will be started:

* the Receiver service, unsecured, listening on [`localhost:50000`](http://localhost:50000)
* the Deliverer service
* the Retryer service, with a timer of 10 seconds
* the Scheduler service, with a timer of 60 seconds
* a [NATS](https://nats.io/) message queue server
* a [PostgreSQL](https://www.postgresql.org/) database

When everything is running in Docker containers, the Ravn binary needs to be recompiled everytime there are changes to the code. The following command will rebuild the images and, if already running, restart the services automatically:

```sh
make build
```

#### Local development with dependencies in containers

During local development, it may be easier to use a local build of Ravn. With the Go runtime available in the system, the following will start Ravn's dependencies, compile the binary, and start all services:

```sh
docker compose up --detach nats postgres
go build -o ./ravn ./cmd
RAVN_RECEIVER_SERVER_PORT=50000 \
  RAVN_RETRYER_TIMER_SECONDS=10 \
  RAVN_SCHEDULER_TIMER_SECONDS=60 \
  RAVN_UNSECURED=true \
  RAVN_NATS_URI=nats://localhost:4222 \
  RAVN_NATS_SUBJECT=delivery_requests \
  RAVN_POSTGRESQL_URI=postgres://ravn:ravn@localhost:5432/ravn \
  ./ravn --receiver --deliverer --retryer --scheduler
```

For most development purposes, though, only the Receiver and Deliverer services are needed.

### Setup

The database needs to be migrated before using the application. The following commands will migrate the database's schema and insert a few resources.

```sh
make migrate
make seed
```

If using the PostgreSQL defined in `docker-compose.yaml`, the data is persisted locally in a Docker volume, so these commands are only needed during the first time.

### Usage

The Receiver service, as defined in the `docker-compose.yaml` file, is by default unsecured, and doesn't ask for any authorization.

If the `RAVN_UNSECURED` environment variable is not set, a JWT containing the user's roles will be required on all endpoints. For example, with resources generated with `make seed`, the following JWT grants `ns_editor` to namespace `00000000-0000-0000-0001-000000000001`, and `ns-reader` to namespace `00000000-0000-0000-0001-000000000003`. It can be exported as a bearer token in an environment variable for use in later `curl` requests.

```sh
export RAVN_JWT_AUTH='Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlcyI6W3siY29kZSI6Im5zX2VkaXRvciIsIm5hbWVzcGFjZUlkIjoiMDAwMDAwMDAtMDAwMC0wMDAwLTAwMDEtMDAwMDAwMDAwMDAxIn0seyJjb2RlIjoibnNfcmVhZGVyIiwibmFtZXNwYWNlSWQiOiIwMDAwMDAwMC0wMDAwLTAwMDAtMDAwMS0wMDAwMDAwMDAwMDMifV19.06aq4p3hqO5BwPKtIqsmo1XHd5QPPMU8kZ9YD7wP9UM'
```

An unsecured Ravn can receive any requests:

```sh
$ curl -XPOST localhost:50000/v1/messages --data '{"messageKind":"test","deliveryKind":"log","languageTag":"en","input":{"username":"John"}}' | jq
{
  "data": {
    "id": "577d27cb-2e0e-4630-813c-1745e821ba04",
    "messageKind": "test",
    "input": {
      "username": "John"
    },
    "status": "active",
    "createTime": "2022-02-19T01:43:56.47177Z",
    "deliveries": [
      {
        "deliveryKind": "log",
        "languageTag": "en",
        "status": "pending",
        "deliverTime": null
      }
    ]
  },
  "meta": {
    "requestId": "fc5156b8-2318-415c-8525-ab772f16a9a3"
  }
}
```

```sh
$ curl localhost:50000/v1/delivery-kinds | jq
{
  "data": [
    {
      "id": "e5d65250-ae84-468c-8986-7d0cce19a633",
      "code": "noop",
      "namespaceId": "00000000-0000-0000-0001-000000000001",
      "host": "",
      "disabled": false,
      "createTime": "2022-02-18T13:34:39.979309Z",
      "updateTime": "2022-02-18T13:34:39.979309Z"
    },
    {
      "id": "8dae8001-58e2-4af5-9e60-d94c41332561",
      "code": "log",
      "namespaceId": "00000000-0000-0000-0001-000000000001",
      "host": "",
      "disabled": false,
      "createTime": "2022-02-18T13:34:39.979309Z",
      "updateTime": "2022-02-18T13:34:39.979309Z"
    }
  ],
  "meta": {
    "requestId": "e2d39878-7abb-44f0-98e2-ec76460ca454"
  }
}
```

### Tests

The Ravn codebase must pass all style checks and tests. The following commands can be used locally:

```sh
# This is a Simple command to execute all tests without using the makefile.
#
# Since these tests include integration and end-to-end tests, running them like
# that will require NATS, PostgreSQL, a Deliverer, and two Receivers.
#
# make test-unit, test-int, and test-e2e are a lot easier to use, will take care
# of starting the necessary services, and will stop them after the tests.
go test ./...

# Runs style checks using golangci-lint.
make test-style

# Runs unit tests only.
make test-unit

# Runs integration tests only.
make test-int

# Runs end-to-end tests only.
make test-e2e

# Runs quick smoke tests. These can be useful when developing to see if the
# system, as a whole, still behaves as expected. They are idempotent, and will
# do some basic GET requests on a few resources.
#
# Warning: the smoke tests require the database to contain some resources, in
# particular a valid message. It will fail on an empty database.
make test-smoke
```

`make test-int` executes the integration tests and requires NATS and PostgreSQL. These will be started automatically on available ports, and stopped when the tests are done. Alternatively, the environment variables `RAVN_NATS_HOST` and `RAVN_POSTGRESQL_HOST` can be set when the services were manually started and available on the system. This will avoid starting and stopping the services everytime the tests are run, and can be useful when the tests are expected to be executed often. The database is assumed to exclusively contain the resources in `./.meta/scripts/test-seed.sql`.

`make test-e2e` is similar to `test-int`, but executes end-to-end tests. As well as requiring the same services, it also requires a Deliverer service, and two Receiver services, one of which in unsecured mode. All of them will start on available ports, and stopped after the tests. Although the same environment variables as the integration tests can be provided, the database will be modified. Also, the Receiver hosts cannot be provided at the moment.

The `TEST_ARGS` argument, by default `./...`, can also be provided to `make` and will be forwarded to the `go test` command. Some examples:

```sh
# Runs unit tests while checking for race conditions.
make test-unit TEST_ARGS='-race ./...'

# Runs all tests without using the cache.
make test-unit TEST_ARGS='-count=1 ./...'

# Runs all tests multiple times in parallel.
make test-unit TEST_ARGS='-count=100 -parallel=10 ./...'

# Calculates the test coverage.
TF=$(mktemp)
make test-unit TEST_ARGS="-coverprofile=${TF} ./..."
go tool cover -func=${TF}

# Because TEST_ARGS is used in the makefile, dollar signs need to be doubled, or
# else they will expanded. This runs the test named TestDeliveryKindHandlerPost
# in the folder ./internal/receiver.
make test-unit TEST_ARGS='-run ^TestDeliveryKindHandlerPost$$ ./internal/receiver/...'
```

### Message queue, database and migrations

If needed, a message can be manually published to the NATS server with

```sh
make publish MSG='{"deliveryId":"00001111-2222-3333-4444-555566667777"}'
```

The PostgreSQL shell can be accessed manually with

```sh
docker compose exec postgres psql --dbname=ravn --username=ravn
```

Migrations are located in the `db/migrations/` folder, and contain the SQL code to execute when migrating the database schema up or down. Migrations are handled by [`golang-migrate`](https://github.com/golang-migrate/migrate). The database can be updated with the following command:

```sh
make migrate
```

New migration files (up and down) can be created with the following example command:

```sh
docker run --volume ${PWD}/db/migrations:/db/migrations --rm migrate/migrate:latest create -ext sql -dir /db/migrations 'create_delivery_kinds_table'
```
